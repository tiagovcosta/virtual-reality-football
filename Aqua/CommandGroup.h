#ifndef COMMANDGROUP_H
#define COMMANDGROUP_H

#include "AquaTypedefs.h"

namespace Aqua
{
	class CommandGroup
	{
	public:
		CommandGroup(uint size, uint numCommands, const void* pCommandsBlob);
		~CommandGroup();

		//Returns the size of the command group in number of bytes
		uint getSize() const;

		//Returns number of commands in this group
		uint getNumCommands() const;

		Blob getCommandsBlob() const;

	private:
		uint  _Size;
		uint  _NumCommands;

		Blob _pCommandsBlob;
	};
};

#endif