#include "BlendState.h"

using namespace Aqua;

BlendState::BlendState(ID3D11BlendState* pState, uint nameID)
{
	_NameID = nameID;
	_pState = pState;
}

BlendState::~BlendState()
{}

ID3D11BlendState* BlendState::getState() const
{
	return _pState;
}

void BlendState::destroy()
{
	SAFE_RELEASE(_pState);
}