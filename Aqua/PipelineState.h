#ifndef PIPELINESTATE_H
#define PIPELINESTATE_H

#include "AquaTypedefs.h"

namespace Aqua
{
	enum PIPELINE_STATE_TYPE
	{
		UNKNOWN_STATE,
		BLEND_STATE,
		RASTERIZER_STATE,
		DEPTH_STENCIL_STATE
	};

	class PipelineState
	{
	public:
		PipelineState();
		~PipelineState();

		uint                getNameID() const;

		PIPELINE_STATE_TYPE getType() const;

		virtual void destroy() = 0;

	protected:
		uint                _NameID;
		PIPELINE_STATE_TYPE _Type;
	};
};

#endif