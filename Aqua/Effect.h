#ifndef EFFECT_H
#define EFFECT_H

#include "Resource.h"

#include "StackAllocator.h"

#include "Commands.h"
#include "CommandGroup.h"
#include "CommandGroupWriter.h"

#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#include "ComputeShader.h"

namespace Aqua
{
	struct EffectDesc
	{
		VertexShader*    pVertexShader;
		HullShader*      pHullShader;
		DomainShader*    pDomainShader;
		GeometryShader*  pGeometryShader;
		PixelShader*     pPixelShader;

		EffectDesc()
		{
			pVertexShader   = NULL;
			pHullShader     = NULL;
			pDomainShader   = NULL;
			pGeometryShader = NULL;
			pPixelShader    = NULL;
		}
	};

	class Effect : public Resource
	{
	public:
		Effect(const EffectDesc& desc);
		~Effect();

		const CommandGroup* getCommandGroup() const;

		void destroy();

	private:
		CommandGroup* _pCommandGroup;
	};
};

#endif