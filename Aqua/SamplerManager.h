#ifndef SAMPLERMANAGER_H
#define SAMPLERMANAGER_H

#include "PCH.h"
#include "Utilities.h"

#include "PipelineManager.h"

namespace Aqua
{
	struct SamplerState
	{
		uint nameID;
		ID3D11SamplerState* pSamplerState;
	};

	class SamplerManager
	{
	public:
		SamplerManager();
		~SamplerManager();

		//Initializes the Sampler Manager and creates the default sampler states
		void init(ID3D11Device* pDevice, PipelineManager& pipelineManager);
		//Shutsdown the Sampler Manager and destroys all sampler states
		void shutdown();

		int createSamplerState(const char* name, const D3D11_SAMPLER_DESC* desc);

		ID3D11SamplerState* getSamplerState(const char* name);
	private:
		ID3D11Device*                    _pDevice;

		std::vector<SamplerState> _SamplerStates;
	};
};

Aqua::SamplerManager& getSamplerManager();

#endif