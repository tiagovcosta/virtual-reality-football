#ifndef PIXELSHADER_H
#define PIXELSHADER_H

#include "PCH.h"

#include "Shader.h"

namespace Aqua
{
	class PixelShader : public Shader
	{
	public:
		PixelShader(ID3D11PixelShader* pShader);
		~PixelShader();

		void destroy();

		ID3D11PixelShader* getShader() const;

	private:
		ID3D11PixelShader* _pShader;
	};
};

#endif