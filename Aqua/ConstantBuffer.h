#ifndef CONSTANTBUFFER_H
#define CONSTANTBUFFER_H

#include "Resource.h"

#include <D3DX11.h>

namespace Aqua
{
	class ConstantBuffer : public Resource
	{
	public:
		ConstantBuffer();
		~ConstantBuffer();

		ID3D11Buffer* getBuffer() const;

		void destroy();

	private:
		ID3D11Buffer* _pBuffer;

		friend class ResourceManager;
	};
};

#endif