#include "ComputeShader.h"

using namespace Aqua;

ComputeShader::ComputeShader(ID3D11ComputeShader* pShader)
{
	_pShader = pShader;
	_Type = COMPUTE_SHADER;
}

ComputeShader::~ComputeShader()
{}

void ComputeShader::destroy()
{
	//getLogger().Write(L"Shader: \"" + _FileName + L"\" destroyed.");
	SAFE_RELEASE(_pShader);
}

ID3D11ComputeShader* ComputeShader::getShader() const
{
	return _pShader;
}