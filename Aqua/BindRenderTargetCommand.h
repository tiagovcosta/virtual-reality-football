#ifndef BINDRENDERTARGETCOMMAND_H
#define BINDRENDERTARGETCOMMAND_H

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindRenderTargetCommand : public Command
	{
	public:
		BindRenderTargetCommand();
		~BindRenderTargetCommand();

		void execute(PipelineManager& pipelineManager);

		void setRenderTarget(ID3D11RenderTargetView* pRenderTarget);
		void setSlot(uint slot);

	private:
		ID3D11RenderTargetView* _pRenderTarget;
		uint                    _Slot;
	};
};

#endif