#ifndef RENDERER_H
#define RENDERER_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2011 - 2012              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "PCH.h"
#include "StackAllocator.h"
#include "BlobIterator.h"
#include "Timer.h"

#if _DEBUG
#include "UI.h"
#endif

#include "RendererEnums.h"
#include "VertexLayouts.h"

#include "ShaderManager.h"
#include "SamplerManager.h"
#include "PipelineStateManager.h"
#include "ResourceManager.h"
#include "PostProcessor.h"

#include "SpriteDrawer.h"

#include "RenderingPass.h"

#include "Commands.h"

#include "Scene.h"
#include "Camera.h"
#include "Actor.h"
#include "Effect.h"

#include "CSMGenerator.h"

const int STACK_SIZE = 104857600; //100 megabytes

namespace Aqua
{
	struct FrameCB
	{
		D3DXMATRIX _ViewMatrix;
		D3DXMATRIX _ProjMatrix;
		D3DXMATRIX _ViewProjMatrix;
		D3DXMATRIX _InvViewProjMatrix;
		D3DXMATRIX _InvViewMatrix;
		D3DXMATRIX _InvProjMatrix;
		Vector4D   _AmbientLightColor;
		Vector3D   _EyePosition;
		float      _DeltaTime;
		Vector2D   _ClipDistances;
		Vector2D   pad2;
	};

	class Renderer
	{
	public:
		Renderer();
		~Renderer();

		void init(uint width, uint height, HWND hWnd, bool windowed);
		void shutdown();

		void addRenderingPass(RenderingPass* pPass);

		void drawScene(float deltaTime, Scene* pScene, Camera* pCamera, D3D11_VIEWPORT viewport);

		void present();

		void                         dispatchCommands(const CommandGroup* const* ppCommandGroups, uint numCommandGroups);

		void setCSMGenerator(CSMGenerator* pGenerator);

	private:
		//void                         dispatchCommands(const CommandGroup* const* ppCommandGroups, uint numCommandGroups);

		void                         sortEntities();

		void                         drawFullScreen(const PixelShader* pPixelShader);
		void                         drawSceneEntities(Camera* pCamera, SORT_ORDER sortOrder, ENTITY_TYPE entityType, uint context);
		void                         drawActor(Actor* pActor, uint context, Camera* pCamera);

		void                         drawDirLight(DirLight* pLight, const Texture* pShadow);
		void                         drawPointLight(PointLight* pLight);

		D3D_FEATURE_LEVEL            getSupportedFeatureLevel();

		HRESULT                      initD3D(uint width, uint height, HWND hWnd, bool windowed);

		PipelineManager              _PipelineManager;

		PostProcessor                _PostProcessor;

		ID3D11Device*                _pD3dDevice;
		ID3D11DeviceContext*         _pImmediateContext;
		IDXGISwapChain*              _pSwapChain;
		ID3D11Debug*				 _pDebugger;

		//The Render Target that is presented to the user
		ID3D11RenderTargetView*      _pMainRenderTarget;

		uint                         _BackbufferWidth;
		uint                         _BackbufferHeight;
		HWND                         _HWnd;
		bool                         _Windowed;

		//Rendering passes
		std::vector<RenderingPass*>  _pRenderingPasses;

		//Scene being drawn
		Scene*                       _pScene;
		Camera*                      _pCamera;

		//Fullscreen VS used in drawFullScreen function
		const VertexShader*          _pFullScreenVS;

		const Effect*                _pDirectionalLightEffect;
		const Effect*                _pVolumeLightEffect;

		//Shader Constant Buffer that is updated once per frame
		FrameCB                      _FrameCB;
		const ConstantBuffer*        _pFrameCB;

		//Shader Constant Buffer that is updated once per actor drawn
		const ConstantBuffer*        _pActorCB;

		//Shader Constant Buffer that is updated once per light drawn
		const ConstantBuffer*        _pDirLightCB;
		const ConstantBuffer*        _pPointLightCB;

		CSMGenerator* _pCSMGenerator;

		bool _pointLights;
	};
};

Aqua::Renderer& getRenderer();

#endif