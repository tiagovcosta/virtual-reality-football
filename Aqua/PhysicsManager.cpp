#include "PhysicsManager.h"

#include "Logger.h"

static physx::PxDefaultErrorCallback gDefaultErrorCallback;
static physx::PxDefaultAllocator gDefaultAllocatorCallback;

PhysicsManager::PhysicsManager()
{
	_accumulator = 0.0f;
	_step_size   = 1.0f / 60.0f;
}

PhysicsManager::~PhysicsManager()
{

}

bool PhysicsManager::init()
{
	_foundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);

	if(!_foundation)
		return false;

	_profile_zone_manager = &physx::PxProfileZoneManager::createProfileZoneManager(_foundation);

	if(!_profile_zone_manager)
		return false;

	bool record_memory_allocations = true;

	_physics = PxCreatePhysics(PX_PHYSICS_VERSION, *_foundation, 
		                       physx::PxTolerancesScale(), record_memory_allocations,
							   _profile_zone_manager);

	if(!_physics)
		return false;

	// check if PvdConnection manager is available on this platform
	if(_physics->getPvdConnectionManager() == NULL)
		return false;

	// setup connection parameters
	const char*     pvd_host_ip = "127.0.0.1";  // IP of the PC which is running PVD
	int             port        = 5425;         // TCP port to connect to, where PVD is listening
	unsigned int    timeout     = 100;          // timeout in milliseconds to wait for PVD to respond,
												// consoles and remote PCs need a higher timeout.
	physx::PxVisualDebuggerConnectionFlags connectionFlags = physx::PxVisualDebuggerExt::getAllConnectionFlags();

	// and now try to connect
	_debugger_connection = physx::PxVisualDebuggerExt::createConnection(_physics->getPvdConnectionManager(),
		pvd_host_ip, port, timeout, connectionFlags);


	physx::PxSceneDesc scene_desc(_physics->getTolerancesScale());
	scene_desc.gravity = physx::PxVec3(0.0f, -9.81f, 0.0f);

	if(!scene_desc.cpuDispatcher)
	{
		physx::PxDefaultCpuDispatcher* cpu_dispatcher = physx::PxDefaultCpuDispatcherCreate(1);

		if(!cpu_dispatcher)
			return false;

		scene_desc.cpuDispatcher = cpu_dispatcher;
	}

	if(!scene_desc.filterShader)
		scene_desc.filterShader = physx::PxDefaultSimulationFilterShader;

	scene_desc.simulationEventCallback = this;

	_scene = _physics->createScene(scene_desc);

	if(!_scene)
		return false;

	_material = _physics->createMaterial(0.5f, 0.4f, 0.4f);    //static friction, dynamic friction, restitution

	if(!_material)
		return false;

	_football_material = _physics->createMaterial(0.25f, 0.25f, 0.4f);    //static friction, dynamic friction, restitution

	if(!_football_material)
		return false;

	_ground_plane = PxCreatePlane(*_physics, physx::PxPlane(physx::PxVec3(0, 1, 0), 0), *_material);

	if(!_ground_plane)
			return false;

	_scene->addActor(*_ground_plane);

	return true;
}

void PhysicsManager::shutdown()
{
	if(_debugger_connection)
		_debugger_connection->release();

	_physics->release();
	_profile_zone_manager->release();
	_foundation->release();
}

bool PhysicsManager::update(float dt)
{
	_accumulator += dt;

	if(_accumulator < _step_size)
		return false;

	_accumulator -= _step_size;

	_scene->simulate(_step_size);

	_scene->fetchResults(true);

	return true;
}

physx::PxRigidDynamic* PhysicsManager::createBone(const physx::PxTransform& pos, float half_width, float half_height, float half_depth)
{
	physx::PxRigidDynamic* actor = physx::PxCreateDynamic(*_physics, pos, 
														  physx::PxBoxGeometry(half_width, half_height, half_depth),
														  *_material, 10);

	actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);

	_scene->addActor(*actor);

	return actor;
}

void PhysicsManager::deleteBone(physx::PxRigidDynamic* bone)
{
	_scene->removeActor(*bone);
}

physx::PxRigidDynamic* PhysicsManager::createBall(const physx::PxTransform& transform, float radius)
{
	physx::PxRigidDynamic* actor = physx::PxCreateDynamic(*_physics, transform, 
										   physx::PxSphereGeometry(radius),
										   *_football_material, 0.15);

	actor->userData = "ball";

	_scene->addActor(*actor);

	return actor;
}

void PhysicsManager::setBallPosition(physx::PxRigidDynamic* ball, const physx::PxTransform& transform)
{
	ball->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);

	ball->setKinematicTarget(transform);

	ball->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, false);
}

void PhysicsManager::createStaticBox(const physx::PxTransform& transform, float half_width, float half_height, float half_depth)
{
	physx::PxRigidStatic* actor = physx::PxCreateStatic(*_physics, transform, 
														  physx::PxBoxGeometry(half_width, half_height, half_depth),
														  *_material);

	_scene->addActor(*actor);
}

physx::PxRigidDynamic* PhysicsManager::createTrigger(const physx::PxTransform& transform, float half_width, float half_height, float half_depth,
													 TriggerCallback callback)
{
	physx::PxRigidDynamic* actor = physx::PxCreateDynamic(*_physics, transform,
														physx::PxBoxGeometry(half_width, half_height, half_depth),
														*_material, 1.0f);

	physx::PxShape* shape;
	actor->getShapes(&shape, 1);
	shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
	shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);

	actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
	actor->userData = callback;

	_scene->addActor(*actor);

	return actor;
}

void PhysicsManager::onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count)
{
	for(physx::PxU32 i = 0; i < count; i++)
	{
		// ignore pairs when shapes have been deleted
		if(pairs[i].flags & (physx::PxTriggerPairFlag::eDELETED_SHAPE_TRIGGER | physx::PxTriggerPairFlag::eDELETED_SHAPE_OTHER))
			continue;

		if(pairs[i].status != physx::PxPairFlag::eNOTIFY_TOUCH_FOUND)
			continue;

		((TriggerCallback)pairs[i].triggerActor->userData)(pairs[i].otherActor->userData);
	}
}