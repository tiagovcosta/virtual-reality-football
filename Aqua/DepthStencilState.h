#ifndef DEPTHSTENCILSTATE_H
#define DEPTHSTENCILSTATE_H

#include "PCH.h"
#include "PipelineState.h"

namespace Aqua
{
	class DepthStencilState : public PipelineState
	{
	public:
		DepthStencilState(ID3D11DepthStencilState* pState, uint nameID);
		~DepthStencilState();

		ID3D11DepthStencilState* getState() const;

		void                    destroy();

	private:
		ID3D11DepthStencilState* _pState;
	};
};

#endif