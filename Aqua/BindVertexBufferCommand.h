#ifndef BINDVERTEXBUFFERCOMMAND_H
#define BINDVERTEXBUFFERCOMMAND_H

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindVertexBufferCommand : public Command
	{
	public:
		BindVertexBufferCommand();
		~BindVertexBufferCommand();

		void execute(PipelineManager& pipelineManager);

		void setVertexBuffer(ID3D11Buffer* pBuffer);
		void setStride(uint stride);
		void setOffset(uint offset);
		void setSlot(uint slot);

	private:
		ID3D11Buffer* _pBuffer;
		uint    _Stride;
		uint    _Offset;
		uint    _Slot;
	};
};

#endif