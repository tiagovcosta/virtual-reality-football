#include "InputAssemblerStage.h"

using namespace Aqua;

InputAssemblerStage::InputAssemblerStage()
{
	clearState();
}

InputAssemblerStage::~InputAssemblerStage()
{}

void InputAssemblerStage::init(ID3D11DeviceContext* pContext)
{
	_pContext = pContext;
	applyState();
}

void InputAssemblerStage::shutdown()
{
	clearState();
	applyState();
}

void InputAssemblerStage::setVertexBuffer(ID3D11Buffer* pResource, uint stride, uint offset, uint slot)
{
	if(pResource != _pVertexBuffers[slot] || stride != _VertexStrides[slot] || offset != _VertexOffsets[slot])
	{
		_pVertexBuffers[slot] = (ID3D11Buffer*) pResource;
		_VertexStrides[slot] = stride;
		_VertexOffsets[slot] = offset;
		_UpdateVertexBuffers = true;
	}
}

void InputAssemblerStage::setIndexBuffer(ID3D11Buffer* pResource, INDEX_BUFFER_FORMAT format, uint offset)
{
	if(pResource != _pIndexBuffer || offset != _IndexOffset)
	{
		_pIndexBuffer = (ID3D11Buffer*) pResource;

		if(format == UINT16_INDEX)
			_IndexFormat = DXGI_FORMAT_R16_UINT;
		else
			_IndexFormat = DXGI_FORMAT_R32_UINT;

		_IndexOffset = offset;

		_UpdateIndexBuffer = true;
	}
}

void InputAssemblerStage::setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
{
	if(primitiveTopology != _PrimitiveTopology)
	{
		_PrimitiveTopology = primitiveTopology;
		_UpdatePrimitiveTopology = true;
	}
}

void InputAssemblerStage::setInputLayout(ID3D11InputLayout* pInputLayout)
{
	if(pInputLayout != _pInputLayout)
	{
		_pInputLayout = pInputLayout;
		_UpdateInputLayout = true;
	}
}

void InputAssemblerStage::clearState()
{
	_UpdateVertexBuffers = true;
	for(uint i = 0; i < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT; i++)
	{
		_pVertexBuffers[i] = NULL;
		_VertexOffsets[i]  = 0;
		_VertexStrides[i]  = 0;
	}

	_UpdateIndexBuffer = true;
	_pIndexBuffer = NULL;
	_IndexFormat  = DXGI_FORMAT_R16_UINT;
	_IndexOffset  = 0;

	_UpdatePrimitiveTopology = true;
	_PrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;

	_UpdateInputLayout = true;
	_pInputLayout = NULL;
}

void InputAssemblerStage::applyState()
{
	uint num = 0;

	if(_UpdateVertexBuffers)
	{
		while(num < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT && _pVertexBuffers[num] != NULL)
			num++;

		_pContext->IASetVertexBuffers(0, num, _pVertexBuffers, _VertexStrides, _VertexOffsets);

		_UpdateVertexBuffers = false;
	}

	if(_UpdateIndexBuffer)
	{
		_pContext->IASetIndexBuffer(_pIndexBuffer, _IndexFormat, _IndexOffset);
		_UpdateIndexBuffer = false;
	}

	if(_UpdatePrimitiveTopology)
	{
		_pContext->IASetPrimitiveTopology(_PrimitiveTopology);
		_UpdatePrimitiveTopology = false;
	}

	if(_UpdateInputLayout)
	{
		_pContext->IASetInputLayout(_pInputLayout);
		_UpdateInputLayout = false;
	}
}

void InputAssemblerStage::reApplyState()
{
	uint num = 0;

	while(num < D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT && _pVertexBuffers[num] != NULL)
		num++;

	_pContext->IASetVertexBuffers(0, num, _pVertexBuffers, _VertexStrides, _VertexOffsets);

		_UpdateVertexBuffers = false;

	_pContext->IASetIndexBuffer(_pIndexBuffer, _IndexFormat, _IndexOffset);
	_UpdateIndexBuffer = false;

	_pContext->IASetPrimitiveTopology(_PrimitiveTopology);
	_UpdatePrimitiveTopology = false;

	_pContext->IASetInputLayout(_pInputLayout);
	_UpdateInputLayout = false;
}