#include "DrawIndexedCommand.h"

using namespace Aqua;

DrawIndexedCommand::DrawIndexedCommand()
{
	_IndexCount = 0;
	_StartIndex = 0;
	_StartVertex = 0;
}

DrawIndexedCommand::~DrawIndexedCommand()
{
	_IndexCount = 0;
	_StartIndex = 0;
	_StartVertex = 0;
}

void DrawIndexedCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.drawIndexed(_IndexCount, _StartIndex, _StartVertex);
}

void DrawIndexedCommand::setIndexCount(uint indexCount)
{
	_IndexCount = indexCount;
}

void DrawIndexedCommand::setStartIndex(uint startIndex)
{
	_StartIndex = startIndex;
}

void DrawIndexedCommand::setStartVertex(uint startVertex)
{
	_StartVertex = startVertex;
}