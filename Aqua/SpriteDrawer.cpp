/*#include "SpriteDrawer.h"

using namespace Aqua;

SpriteDrawer& getSpriteDrawer()
{
	static SpriteDrawer drawer;

	return drawer;
}

SpriteDrawer::SpriteDrawer()
{
	_pCBuffer      = NULL;
}

SpriteDrawer::~SpriteDrawer()
{}

void SpriteDrawer::init(Mesh* pMesh, Effect effect, uint windowWidth, uint windowHeight)
{
	_pMesh        = pMesh;
	_pCBuffer     = getResourceManager().createConstantBuffer(SPRITE_CBUFFER_SIZE, "Sprite CBuffer");
	_Effect       = effect;
	_WindowWidth  = windowWidth;
	_WindowHeight = windowHeight;
}

void SpriteDrawer::draw(Texture* pTexture, D3DXVECTOR2 pos, D3DXVECTOR2 scale)
{
	draw(pTexture->getShaderResource(), pos, scale);
}

void SpriteDrawer::draw(ID3D11ShaderResourceView* pTexture, D3DXVECTOR2 pos, D3DXVECTOR2 scale)
{
	_CBData.pos   = pos;
	_CBData.scale = scale;

	getResourceManager().updateCBuffer(_pCBuffer, &_CBData, sizeof(CbPerSprite));

	//getPipelineManager().useMainRenderTarget();
	getPipelineManager().getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	getPipelineManager().getIAStage()->setVertexBuffer(_pMesh->getVertexBuffer(), sizeof(PTCVertex), 0, 0);
	getPipelineManager().getIAStage()->setIndexBuffer(_pMesh->getIndexBuffer(), _pMesh->getIndexFormat(), 0);

	getPipelineManager().bindEffect(&_Effect);

	getPipelineManager().bindConstantBuffer(VERTEX_STAGE, _pCBuffer, 2);
	getPipelineManager().bindShaderResourceView(PIXEL_STAGE, pTexture, 0);

	getPipelineManager().drawIndexed(6, 0, 0);
	getPipelineManager().bindShaderResourceView(PIXEL_STAGE, NULL, 0);
}

void SpriteDrawer::shutdown()
{}*/