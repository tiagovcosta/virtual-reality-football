#ifndef PIPELINESTATEMANAGER_H
#define PIPELINESTATEMANAGER_H

#include "PCH.h"
#include "Utilities.h"

#include "BlendState.h"
#include "DepthStencilState.h"
#include "RasterizerState.h"

namespace Aqua
{
	class PipelineStateManager
	{
	public:
		PipelineStateManager();
		~PipelineStateManager();

		//Initializes the DepthStencil Manager and creates the default sampler states
		void init(ID3D11Device* pDevice);
		//Shutsdown the DepthStencil Manager and destroys all sampler states
		void shutdown();

		//Getting a state using the state unique ID is faster than using the state name
		const PipelineState* getState(uint stateID) const;
		const PipelineState* getState(const char* name) const;

		//Use this method to get the unique state ID if you're going to access the state many times
		uint           getStateID(const char* name) const;

		uint           createDepthStencilState(const char* name, const D3D11_DEPTH_STENCIL_DESC* desc);
		uint           createBlendState(const char* name, const D3D11_BLEND_DESC* desc);
		uint           createRasterizerState(const char* name, const D3D11_RASTERIZER_DESC* desc);

	private:
		ID3D11Device*                  _pDevice;

		std::vector<PipelineState*>    _pPipelineStates;
	};
};

Aqua::PipelineStateManager& getPipelineStateManager();

#endif