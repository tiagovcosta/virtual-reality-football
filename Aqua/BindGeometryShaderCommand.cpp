#include "BindGeometryShaderCommand.h"

using namespace Aqua;

BindGeometryShaderCommand::BindGeometryShaderCommand()
{
	_pShader = NULL;
}

BindGeometryShaderCommand::~BindGeometryShaderCommand()
{
	_pShader = NULL;
}

void BindGeometryShaderCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindShader(_pShader);
}

void BindGeometryShaderCommand::setShader(GeometryShader* pShader)
{
	_pShader = pShader;
}