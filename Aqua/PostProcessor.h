#ifndef POSTPROCESSMANAGER_H
#define POSTPROCESSMANAGER_H

#include "PCH.h"
#include "ResourceManager.h"
#include "ShaderManager.h"
#include "PipelineManager.h"
#include "PipelineStateManager.h"

#if _DEBUG
#include "UI.h"
#endif

#define NUM_LUMINANCE_TEXTURES 3
#define NUM_BLOOM_TEXTURES 2
#define BLOOM_TEXTURES_WIDTH 320
#define BLOOM_TEXTURES_HEIGHT 200

namespace Aqua
{
	struct PostProcessCBData
	{
		float middleGrey;
		float white;
		float brightPassThreshold;
		float bloomIntensity;
		int tonemapOperator;
		float A;
		float B;
		float C;
		float D;
		float E;
		float F;
		float W;
	};

	enum PostProcessEffects
	{
		BLOOM_PPE = 1
	};

	class PostProcessor
	{
	public:
		PostProcessor();
		~PostProcessor();

		void init(u8 effects);

		void toogleEffect(PostProcessEffects effect);

		void execute(float delta, PipelineManager& pipelineManager, const Texture* sceneMap, ID3D11RenderTargetView* pRenderTarget);

	private:
		u8                      _Effects;

		const RenderTexture*    _pBloomTextures[NUM_BLOOM_TEXTURES];
		const RenderTexture*    _pLuminanceTextures[NUM_LUMINANCE_TEXTURES];
		
		const ConstantBuffer*   _pPostProcessCB;
		PostProcessCBData       _cbData;

		const VertexShader*     _pFullScreenVS;
		const PixelShader*      _pLuminancePS;
		const PixelShader*      _pEyeAdaptPS;
		const PixelShader*      _pBrightPassPS;
		const PixelShader*      _pHBlurPS;
		const PixelShader*      _pVBlurPS;
		const PixelShader*      _pToneMapPS;

		D3D11_VIEWPORT          _LuminanceViewport;
		D3D11_VIEWPORT          _EyeAdaptViewport;
		D3D11_VIEWPORT          _BloomViewport;

		bool                    _SwapEyeTextures;
	};
};

#endif