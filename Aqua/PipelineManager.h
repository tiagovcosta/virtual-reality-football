#ifndef PIPELINEMANAGER_H
#define PIPELINEMANAGER_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2011              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "PCH.h"

#include "RendererEnums.h"

#include "InputAssemblerStage.h"
#include "RasterizerStage.h"
#include "OutputMergerStage.h"

#include "VertexShaderStage.h"
#include "HullShaderStage.h"
#include "DomainShaderStage.h"
#include "GeometryShaderStage.h"
#include "PixelShaderStage.h"
#include "ComputeShaderStage.h"

#include "ResourceManager.h"

#include "Commands.h"
#include "BlobIterator.h"

//Multiple Buffers, Shader resources, etc have to be set continuosly... Or else the ones after the first NULL will be ignored

namespace Aqua
{
	class PipelineManager
	{
	public:
		PipelineManager();
		~PipelineManager();

		void init(ID3D11DeviceContext* pContext);
		void shutdown();

		InputAssemblerStage* getIAStage();
		RasterizerStage*     getRSStage();
		OutputMergerStage*   getOMStage();

		//Shader Stages methods
		void bindVertexShader(const VertexShader* pShader);
		void bindHullShader(const HullShader* pShader);
		void bindDomainShader(const DomainShader* pShader);
		void bindGeometryShader(const GeometryShader* pShader);
		void bindPixelShader(const PixelShader* pShader);
		void bindComputeShader(const ComputeShader* pShader);

		void bindSamplerState(uint bindFlags, ID3D11SamplerState* pSampler, uint slot);
		void bindConstantBuffer(uint bindFlags, ID3D11Buffer* pConstantBuffer, uint slot);
		void bindShaderResourceView(uint bindFlags, ID3D11ShaderResourceView* pResource, uint slot);

		void updatePipeline();
		void reApplyPipeline();

		void draw(uint vertexCount, uint startVertex);
		void drawIndexed(uint indexCount, uint startIndex, uint startVertex);
		void drawInstanced();
		void drawIndexedInstanced();

		void setMainRenderTarget(ID3D11RenderTargetView* pMainRenderTarget);

		//Binds the Main Render Target to Output Merger Stage at slot 0
		void useMainRenderTarget();

		void updateCBuffer(ID3D11Buffer* pBuffer, const void* pData, uint size);
		void generateSRVMips(ID3D11ShaderResourceView* pShaderResource);

		void clearRenderTarget(ID3D11RenderTargetView* pRenderTarget, float clear);
		void clearDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilTarget, float depth);

	private:
		ID3D11DeviceContext* _pContext;

		VertexShaderStage       _VSStage;
		HullShaderStage         _HSStage;
		DomainShaderStage       _DSStage;
		GeometryShaderStage     _GSStage;
		PixelShaderStage        _PSStage;
		ComputeShaderStage      _CSStage;

		InputAssemblerStage     _IAStage;
		RasterizerStage         _RSStage;
		OutputMergerStage       _OMStage;

		ID3D11RenderTargetView* _pMainRenderTarget;

		bool                    _ReApply;
	};
};

#endif