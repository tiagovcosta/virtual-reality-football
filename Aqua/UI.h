#ifndef UI_H
#define UI_H

#include <AntTweakBar.h>
#include <D3D11.h>

#include "AquaTypedefs.h"

namespace Aqua
{
	class UI
	{
	public:
		UI();
		~UI();

		void init(ID3D11Device* pDevice, uint width, uint height);
		void shutdown();

		void addUInt(const char* name, uint* value, const char* config);
		void addString(const char* name, const char* str, const char* config);

		void addCheckBox(const char* name, bool* value, const char* config);
		void addSlider(const char* name, float* value, const char* config);
		void addDropList(const char* name, void* value, TwEnumVal options[], int numOptions, const char* config);

		void draw(int fps);
	private:
		TwBar* _MainBar;
		TwBar* _DebugBar;
		int    _FPS;
		float  _MSPF;
	};
};

Aqua::UI& getUI();

#endif