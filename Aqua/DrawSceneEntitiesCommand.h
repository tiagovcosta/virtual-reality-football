#ifndef DRAWSCENEENTITIESCOMMAND_H
#define DRAWSCENEENTITIESCOMMAND_H

#include "RendererEnums.h"

#include "Command.h"

#include "PipelineManager.h"

#include "Entity.h"

namespace Aqua
{
	/*class DrawSceneEntitiesCommand : public Command
	{
	public:
		DrawSceneEntitiesCommand();
		~DrawSceneEntitiesCommand();

		ENTITY_TYPE getEntitiesType() const;
		SORT_ORDER getSortOrder() const;

		void setEntitiesType(ENTITY_TYPE type);
		void setSortOrder(SORT_ORDER order);

	private:
		ENTITY_TYPE _EntitiesType;
		SORT_ORDER _SortOrder;
	};*/

	struct DrawSceneEntitiesCommand
	{
		ENTITY_TYPE entitiesType;
		SORT_ORDER sortOrder;
	};
};

#endif