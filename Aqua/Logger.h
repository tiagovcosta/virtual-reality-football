#ifndef LOGGER_H
#define LOGGER_H

#include <Windows.h>
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>



namespace Aqua
{
	enum MESSAGE_LEVEL
	{
		INFO_MESSAGE = 0,
		WARNING_MESSAGE = 1,
		ERROR_MESSAGE = 2
	};

	class Logger
	{
	public:
		Logger();
		~Logger();

		bool open(const std::string& logName);
		void close();

		bool write(std::string text, MESSAGE_LEVEL level);
		bool writeSeparater();

		void setMessageLevel(MESSAGE_LEVEL level);

	private:
		bool write(const char* text);

		std::ofstream _Log;
		unsigned char _MinMessageLevel;
	};
}

Aqua::Logger& getLogger();

#endif