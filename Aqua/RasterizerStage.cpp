#include "RasterizerStage.h"

using namespace Aqua;

RasterizerStage::RasterizerStage()
{
	clearState();
}

RasterizerStage::~RasterizerStage()
{}

void RasterizerStage::init(ID3D11DeviceContext* pContext)
{
	_pContext = pContext;
	applyState();
}

void RasterizerStage::shutdown()
{
	clearState();
	applyState();
}

void RasterizerStage::setState(ID3D11RasterizerState* pState)
{
	if(_pState != pState)
	{
		_pState = pState;
		_UpdateState = true;
	}
}

void RasterizerStage::setViewport(uint numViewports, const D3D11_VIEWPORT* viewPorts)
{
	if(numViewports != _NumViewports)
	{
		_NumViewports = numViewports;
		_pViewports = viewPorts;
		_UpdateViewports = true;
	} else
	{
		for(uint i = 0; i < _NumViewports; i++)
		{
			//if(_pViewports[i] != viewPorts[i])
			//{
				_pViewports = viewPorts;
				_UpdateViewports = true;
				break;
			//}
		}
	}
}

void RasterizerStage::setDefaultViewport(D3D11_VIEWPORT viewport)
{
	_DefaultViewport = viewport;
}

void RasterizerStage::useDefaultViewport()
{
	setViewport(1, &_DefaultViewport);
}

void RasterizerStage::clearState()
{
	_UpdateState     = true;
	_pState          = NULL;

	_UpdateViewports = true;
	_NumViewports    = 0;
	_pViewports      = NULL;
}

void RasterizerStage::applyState()
{
	if(_UpdateState)
	{
		_pContext->RSSetState(_pState);
		_UpdateState = false;
	}

	if(_UpdateViewports)
	{
		_pContext->RSSetViewports(_NumViewports, _pViewports);
		_UpdateViewports = false;
	}
}

void RasterizerStage::reApplyState()
{
	_pContext->RSSetState(_pState);
	_UpdateState = false;

	_pContext->RSSetViewports(_NumViewports, _pViewports);
	_UpdateViewports = false;
}