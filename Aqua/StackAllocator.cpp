#include "StackAllocator.h"

using namespace Aqua;

StackAllocator& getStackAllocator()
{
	static StackAllocator allocator;

	return allocator;
}

StackAllocator::StackAllocator()
{
	_InitialPosition = NULL;
	_CurrentPosition = NULL;
}

StackAllocator::StackAllocator(u32 size)
{
	_InitialPosition = malloc(size);
	_CurrentPosition = _InitialPosition;

	_Size = size;
	_UsedMemory = 0;
	_AlignmentMemory = 0;
}

void* StackAllocator::allocateUnaligned(size_t size)
{
	ASSERT(_UsedMemory + size > _Size);

	void* m = _CurrentPosition;

	_CurrentPosition = static_cast<void*>( static_cast<char*>(_CurrentPosition) + size);

	_UsedMemory += size;

	return m;
}

void* StackAllocator::allocate(size_t size, size_t alignment)
{
	void* alignedMarker;
	void* rawAddress = allocateUnaligned(size + alignment);

	//Calculate adjustment needed to keep objects correctly aligned
	u32 adjustment = alignment - ( (size_t) rawAddress & (alignment-1));

	_AlignmentMemory += alignment;

	//Calculate the adjusted address
	alignedMarker = static_cast<void*>( static_cast<char*>(rawAddress)  + adjustment);

	//Store the adjustment in the first unaligned byte to be used by the blob iterator
	*static_cast<char*>(rawAddress) = adjustment;

	//Store the adjustment in be byte immediatly preceding the aligned address
	//so we can also free the aligment when freeing memory
	*(static_cast<char*>(alignedMarker)-1) = adjustment;

	return alignedMarker;
}

void StackAllocator::freeToMarker(void* marker)
{
	_CurrentPosition = marker;
}

void StackAllocator::clear()
{
	_CurrentPosition = _InitialPosition;
}

void StackAllocator::destroy()
{
	free(_InitialPosition);
}

u32 StackAllocator::getSize()
{
	return _Size;
}

u32 StackAllocator::getUsedMemory()
{
	return _UsedMemory;
}

u32 StackAllocator::getAlignmentMemory()
{
	return _AlignmentMemory;
}

void* StackAllocator::getCurrentPosition()
{
	return _CurrentPosition;
}