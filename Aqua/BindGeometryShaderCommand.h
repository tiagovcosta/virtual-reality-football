#ifndef BINDGEOMETRYSHADERCOMMAND_H
#define BINDGEOMETRYSHADERCOMMAND_H

#include "Command.h"

#include "GeometryShader.h"

#include "PipelineManager.h"

typedef uint8_t u8;

namespace Aqua
{
	class BindGeometryShaderCommand : public Command
	{
	public:
		BindGeometryShaderCommand();
		~BindGeometryShaderCommand();

		void execute(PipelineManager& pipelineManager);

		void setShader(GeometryShader* pShader);

	private:
		GeometryShader* _pShader;
	};
};

#endif