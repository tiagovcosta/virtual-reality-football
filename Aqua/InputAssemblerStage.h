#ifndef INPUTASSEMBLERSTAGE_H
#define INPUTASSEMBLERSTAGE_H

#include "PCH.h"
#include "Logger.h"

#include "RendererEnums.h"

namespace Aqua
{
	class InputAssemblerStage
	{
	public:
		InputAssemblerStage();
		~InputAssemblerStage();

		void init(ID3D11DeviceContext* pContext);
		void shutdown();

		void setVertexBuffer(ID3D11Buffer* pResource, uint stride, uint offset, uint slot);
		void setIndexBuffer(ID3D11Buffer* pResource, INDEX_BUFFER_FORMAT format, uint offset);
		void setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology);
		void setInputLayout(ID3D11InputLayout* pInputLayout);

		void clearState();

		void applyState();

		void reApplyState();

	private:
		ID3D11DeviceContext*     _pContext;

		bool                     _UpdateVertexBuffers;
		ID3D11Buffer*            _pVertexBuffers[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
		uint                     _VertexOffsets[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];
		uint                     _VertexStrides[D3D11_IA_VERTEX_INPUT_RESOURCE_SLOT_COUNT];

		bool                     _UpdateIndexBuffer;
		ID3D11Buffer*            _pIndexBuffer;
		DXGI_FORMAT              _IndexFormat;
		uint                     _IndexOffset;

		bool                     _UpdatePrimitiveTopology;
		D3D11_PRIMITIVE_TOPOLOGY _PrimitiveTopology;

		bool                     _UpdateInputLayout;
		ID3D11InputLayout* _pInputLayout;
	};
};

#endif