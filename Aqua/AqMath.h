#ifndef AQMATH_H
#define AQMATH_H

#include <math.h>
#include <D3DX10math.h>

namespace Aqua
{
	struct Vector2D
	{
		Vector2D(){}
		Vector2D(float _x, float _y) : x(_x), y(_y) {}
		Vector2D(const D3DXVECTOR2& vec2) : x(vec2.x), y(vec2.y) {}
		float x;
		float y;
	};

	struct Vector3D
	{
		Vector3D(){}
		Vector3D(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
		Vector3D(const D3DXVECTOR3& vec3) : x(vec3.x), y(vec3.y), z(vec3.z) {}
		float x;
		float y;
		float z;
	};

	struct Vector4D
	{
		Vector4D(){}
		Vector4D(Vector3D vec3, float _w) : x(vec3.x), y(vec3.y), z(vec3.z), w(_w) {}
		Vector4D(float _x, float _y, float _z, float _w) : x(_x), y(_y), z(_z), w(_w) {}
		float x;
		float y;
		float z;
		float w;
	};

	//Vector Length
	float vector2DLength(const Vector2D& vec);
	float vector3DLength(const Vector3D& vec);
	float vector4DLength(const Vector4D& vec);

	//Vector Normalization
	Vector2D vector2DNormalize(const Vector2D& vec);
	Vector3D vector3DNormalize(const Vector3D& vec);
	Vector4D vector4DNormalize(const Vector4D& vec);
};

#endif