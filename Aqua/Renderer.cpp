#include "Renderer.h"

using namespace Aqua;

Renderer& getRenderer()
{
	static Renderer renderer;

	return renderer;
}

Renderer::Renderer()
{
	_pD3dDevice               = NULL;
	_pSwapChain               = NULL;
	_pImmediateContext        = NULL;
	_pDebugger                = NULL;
	_pMainRenderTarget        = NULL;
	_pFrameCB                 = NULL;
	_pActorCB                 = NULL;
}

Renderer::~Renderer()
{}

D3D_FEATURE_LEVEL Renderer::getSupportedFeatureLevel()
{
	D3D_FEATURE_LEVEL featureLevel;

	if(FAILED(D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, NULL, 0,
								D3D11_SDK_VERSION, NULL, &featureLevel, NULL)))
	{
		PostQuitMessage(0);
	}

	return featureLevel;
}

void Renderer::init(uint width, uint height, HWND hWnd, bool windowed)
{
	initD3D(width, height, hWnd, windowed);
	_pointLights = true;

	#if _DEBUG
	getUI().init(_pD3dDevice, width, height);
	//TwEnumVal passesEV[] = { {FINAL_PASS, "Final"}, {GBUFFER_PASS, "GBuffer"}, {DEPTH_PASS, "Depth"}, {LIGHTING_PASS, "Lighting"}, {MATERIAL_PASS, "Material"} };
	//getDebugUI().addDropList("Show:", &_ShowPass, passesEV, 5, NULL);
	_pointLights = false;

	getUI().addCheckBox("Point Lights", &_pointLights, NULL);
	#endif

	getStackAllocator() = StackAllocator(STACK_SIZE);

	_PipelineManager.init(_pImmediateContext);

	getShaderManager().init(_pD3dDevice);
	getSamplerManager().init(_pD3dDevice, _PipelineManager);
	getPipelineStateManager().init(_pD3dDevice);
	getResourceManager().init(_pD3dDevice);

	_PostProcessor.init(BLOOM_PPE);

	_PipelineManager.getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	_PipelineManager.setMainRenderTarget(_pMainRenderTarget);
	_PipelineManager.getRSStage()->useDefaultViewport();

	//Create constant buffers
	_pFrameCB      = getResourceManager().createConstantBuffer(sizeof(FrameCB), "Frame CBuffer");
	_pActorCB      = getResourceManager().createConstantBuffer(sizeof(ActorCB), "Actor CBuffer");
	_pDirLightCB   = getResourceManager().createConstantBuffer(sizeof(DirLight), "DirLight CBuffer");
	_pPointLightCB = getResourceManager().createConstantBuffer(sizeof(PointLight), "PointLight CBuffer");

	_pFullScreenVS  = dynamic_cast<const VertexShader*>(getShaderManager().getShader("FullScreenVS.hlsl"));

	_pDirectionalLightEffect = dynamic_cast<const Effect*>(getResourceManager().getResource(EFFECT, "DirectionalLightFX.effect.xml"));
	_pVolumeLightEffect      = dynamic_cast<const Effect*>(getResourceManager().getResource(EFFECT, "VolumeLightFX.effect.xml"));
}

void Renderer::shutdown()
{
	#if _DEBUG
	getUI().shutdown();
	#endif

	//getSpriteDrawer().shutdown();

	getResourceManager().shutdown();
	getShaderManager().shutdown();
	getSamplerManager().shutdown();
	getPipelineStateManager().shutdown();
	getStackAllocator().destroy();

	_PipelineManager.shutdown();

	if(_pImmediateContext)
		_pImmediateContext->ClearState();

	SAFE_RELEASE(_pMainRenderTarget);
	SAFE_RELEASE(_pSwapChain);
	SAFE_RELEASE(_pDebugger);
	SAFE_RELEASE(_pImmediateContext);
	SAFE_RELEASE(_pD3dDevice);
}

HRESULT Renderer::initD3D(uint width, uint height, HWND hWnd, bool windowed)
{
	_BackbufferWidth    = width;
	_BackbufferHeight   = height;
	_HWnd               = hWnd;
	_Windowed           = windowed;

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));

	sd.BufferCount                        = 1;
	sd.BufferDesc.Width                   = width;
	sd.BufferDesc.Height                  = height;
	sd.BufferDesc.Format                  = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.ScanlineOrdering        = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling                 = DXGI_MODE_SCALING_UNSPECIFIED;
	sd.BufferDesc.RefreshRate.Numerator   = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage                        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow                       = hWnd;
	sd.SampleDesc.Count                   = 1;
	sd.SampleDesc.Quality                 = 0;
	sd.SwapEffect                         = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags                              = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	sd.Windowed                           = windowed;

	D3D_FEATURE_LEVEL featureLevel[] = { getSupportedFeatureLevel() };

	#if !_DEBUG
	// Create D3D11Device and SwapChain
	if(FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevel, 1, D3D11_SDK_VERSION, &sd, 
											&_pSwapChain, &_pD3dDevice, NULL, &_pImmediateContext)))
	{
		if(FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_WARP, NULL, 0, featureLevel, 1, D3D11_SDK_VERSION, &sd, 
												&_pSwapChain, &_pD3dDevice, NULL, &_pImmediateContext)))
		{
			MessageBox(NULL, "Error creating Direct3D Device.", "DirectX Error", MB_OK);
			PostQuitMessage(0);
		}
	}
	#else

	// Create D3D11 Device, Context and SwapChain
	if(FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_DEBUG, featureLevel, 1, D3D11_SDK_VERSION, &sd, 
											&_pSwapChain, &_pD3dDevice, NULL, &_pImmediateContext)))
	{
		if(FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_WARP, NULL, D3D11_CREATE_DEVICE_DEBUG, featureLevel, 1, D3D11_SDK_VERSION, &sd, 
												&_pSwapChain, &_pD3dDevice, NULL, &_pImmediateContext)))
		{
			getLogger().write("Error creating D3D Device", ERROR_MESSAGE);
			MessageBox(NULL, "Error creating Direct3D Device.", "DirectX Error", MB_OK);
			PostQuitMessage(0);

			return E_FAIL;
		}
	}
	#endif

	getLogger().write("--D3D Device Created", INFO_MESSAGE);

	//Access Backbuffer Texture
	ID3D11Texture2D*  pSwapChainBuffer = 0;

	if(FAILED(_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pSwapChainBuffer)))
	{
		getLogger().write("Error accessing Direct3D Backbuffer texture.", ERROR_MESSAGE);
		PostQuitMessage(0);
	}

	//Access Debugger Device
	#if _DEBUG
	if(FAILED(_pD3dDevice->QueryInterface( __uuidof(ID3D11Debug), (void**)&_pDebugger)))
	{
		getLogger().write("Error accessing Direct3D Debugger.", ERROR_MESSAGE);
		PostQuitMessage(0);
	}
	#endif

	//Creates Main Render Target
	if(FAILED(_pD3dDevice->CreateRenderTargetView(pSwapChainBuffer, 0, &_pMainRenderTarget)))
	{
		getLogger().write("Error creating Direct3D Main Render Target.", ERROR_MESSAGE);
		PostQuitMessage(0);
	}

	pSwapChainBuffer->Release();

	return S_OK;
}

void Renderer::dispatchCommands(const CommandGroup* const * ppCommandGroups, uint numCommandGroups)
{
	for(uint i = 0; i < numCommandGroups; i++)
	{
		const CommandGroup* pCommandGroup = ppCommandGroups[i];
		uint numCommands = pCommandGroup->getNumCommands();

		BlobIterator blobIterator(pCommandGroup->getCommandsBlob());

		for(uint j = 0; j < numCommands; j++)
		{
			const COMMAND_TYPE cmdType = *(blobIterator.nextObject<COMMAND_TYPE>());

			switch (cmdType)
			{
			case DRAW:
				{
					const DrawCommand* cmd = blobIterator.nextObject<DrawCommand>();
					_PipelineManager.draw(cmd->vertexCount, cmd->startVertex);
				}
				break;

			case DRAW_INDEXED:
				{
					_PipelineManager.getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
					const DrawIndexedCommand* cmd = blobIterator.nextObject<DrawIndexedCommand>();
					_PipelineManager.drawIndexed(cmd->indexCount, cmd->startIndex, cmd->startVertex);
				}
				break;

			case BIND_VERTEX_BUFFER:
				{
					const BindVertexBufferCommand* cmd = blobIterator.nextObject<BindVertexBufferCommand>();
					_PipelineManager.getIAStage()->setVertexBuffer(cmd->pBuffer, cmd->stride, cmd->offset, cmd->slot);
				}
				break;

			case BIND_INDEX_BUFFER:
				{
					const BindIndexBufferCommand* cmd = blobIterator.nextObject<BindIndexBufferCommand>();
					_PipelineManager.getIAStage()->setIndexBuffer(cmd->pBuffer, cmd->format, cmd->offset);
				}
				break;

			case BIND_CONSTANT_BUFFER:
				{
					const BindConstantBufferCommand* cmd = blobIterator.nextObject<BindConstantBufferCommand>();
					_PipelineManager.bindConstantBuffer(cmd->bindFlags, cmd->pConstantBuffer, cmd->slot);
				}
				break;

			case BIND_TEXTURE:
				{
					const BindTextureCommand* cmd = blobIterator.nextObject<BindTextureCommand>();
					if(cmd->pTexture == NULL)
						_PipelineManager.bindShaderResourceView(cmd->bindFlags, NULL, cmd->slot);
					else
						_PipelineManager.bindShaderResourceView(cmd->bindFlags, cmd->pTexture->getShaderResource(), cmd->slot);
				}
				break;

			case BIND_VERTEX_SHADER:
				{
					const BindVertexShaderCommand* cmd = blobIterator.nextObject<BindVertexShaderCommand>();
					_PipelineManager.bindVertexShader(cmd->pShader);

					if(cmd->pShader != NULL)
						_PipelineManager.getIAStage()->setInputLayout(cmd->pShader->getInputLayout());
				}
				break;

			case BIND_HULL_SHADER:
				{
					const BindHullShaderCommand* cmd = blobIterator.nextObject<BindHullShaderCommand>();
					_PipelineManager.bindHullShader(cmd->pShader);
				}
				break;

			case BIND_DOMAIN_SHADER:
				{
					const BindDomainShaderCommand* cmd = blobIterator.nextObject<BindDomainShaderCommand>();
					_PipelineManager.bindDomainShader(cmd->pShader);
				}
				break;

			case BIND_GEOMETRY_SHADER:
				{
					const BindGeometryShaderCommand* cmd = blobIterator.nextObject<BindGeometryShaderCommand>();
					_PipelineManager.bindGeometryShader(cmd->pShader);
				}
				break;

			case BIND_PIXEL_SHADER:
				{
					const BindPixelShaderCommand* cmd = blobIterator.nextObject<BindPixelShaderCommand>();
					_PipelineManager.bindPixelShader(cmd->pShader);
				}
				break;

			case BIND_COMPUTE_SHADER:
				{
					const BindComputeShaderCommand* cmd = blobIterator.nextObject<BindComputeShaderCommand>();
					_PipelineManager.bindComputeShader(cmd->pShader);
				}
				break;

			case BIND_SAMPLER_STATE:	
				{
					const BindSamplerStateCommand* cmd = blobIterator.nextObject<BindSamplerStateCommand>();
					_PipelineManager.bindSamplerState(cmd->bindFlags, cmd->pSamplerState, cmd->slot);
				}
				break;

			case BIND_BLEND_STATE:	
				{
					const BindBlendStateCommand* cmd = blobIterator.nextObject<BindBlendStateCommand>();
					if(cmd->pBlendState == NULL)
						_PipelineManager.getOMStage()->setBlendState(NULL);
					else
						_PipelineManager.getOMStage()->setBlendState(cmd->pBlendState->getState());
				}
				break;

			case BIND_RASTERIZER_STATE:
				{
					const BindRasterizerStateCommand* cmd = blobIterator.nextObject<BindRasterizerStateCommand>();
					if(cmd->pRasterizerState == NULL)
						_PipelineManager.getRSStage()->setState(NULL);
					else
						_PipelineManager.getRSStage()->setState(cmd->pRasterizerState->getState());
				}
				break;

			case BIND_DEPTH_STENCIL_STATE:
				{
					const BindDepthStencilStateCommand* cmd = blobIterator.nextObject<BindDepthStencilStateCommand>();
					if(cmd->pDepthStencilState == NULL)
						_PipelineManager.getOMStage()->setDepthStencilState(NULL);
					else
						_PipelineManager.getOMStage()->setDepthStencilState(cmd->pDepthStencilState->getState());
				}
				break;

			case BIND_VIEWPORT:
				{
					const BindViewportCommand* cmd = blobIterator.nextObject<BindViewportCommand>();
					_PipelineManager.getRSStage()->setViewport(1, &cmd->pViewport->getD3DViewport(cmd->targetWidth, cmd->targetHeight));
				}
				break;

			case BIND_RENDER_TARGET:
				{
					const BindRenderTargetCommand* cmd = blobIterator.nextObject<BindRenderTargetCommand>();
					_PipelineManager.getOMStage()->bindRenderTarget(cmd->pRenderTarget, cmd->slot);
				}
				break;

			case BIND_DEPTH_STENCIL_TARGET:
				{
					const BindDepthStencilTargetCommand* cmd = blobIterator.nextObject<BindDepthStencilTargetCommand>();
					_PipelineManager.getOMStage()->bindDepthStencilTarget(cmd->pDepthStencilTarget);
				}
				break;

			case CLEAR_RENDER_TARGET:
				{
					const ClearRenderTargetCommand* cmd = blobIterator.nextObject<ClearRenderTargetCommand>();
					_PipelineManager.clearRenderTarget(cmd->pRenderTarget, cmd->value);
				}
				break;

			case CLEAR_DEPTH_STENCIL_TARGET:
				{
					const ClearDepthStencilTargetCommand* cmd = blobIterator.nextObject<ClearDepthStencilTargetCommand>();
					_PipelineManager.clearDepthStencilTarget(cmd->pDepthStencilTarget, cmd->value);
				}
				break;

			case UPDATE_CBUFFER:
				{
					const UpdateCBufferCommand* cmd = blobIterator.nextObject<UpdateCBufferCommand>();
					_PipelineManager.updateCBuffer(cmd->pCBuffer, cmd->pData, cmd->size);
				}
				break;

			case DRAW_FULLSCREEN:
				{
					const DrawFullScreenCommand* cmd = blobIterator.nextObject<DrawFullScreenCommand>();
					drawFullScreen(cmd->pPixelShader);
				}
				break;

			case DRAW_SCENE_ENTITIES:
				{
					const DrawSceneEntitiesCommand* cmd = blobIterator.nextObject<DrawSceneEntitiesCommand>();
					drawSceneEntities(cmd->pCamera, cmd->sortOrder, cmd->entitiesType, cmd->context);
				}
				break;

			case BIND_MAIN_RENDER_TARGET:
				{
					_PipelineManager.useMainRenderTarget();
					_PipelineManager.getRSStage()->useDefaultViewport();
				}
				break;

			case DISPATCH_COMMANDS:
				{
					const DispatchCommandsCommand* cmd = blobIterator.nextObject<DispatchCommandsCommand>();
					dispatchCommands(&cmd->pCmdGroup, 1);
				}
				break;

			default:
				{
					ASSERT(false);
				}
				break;
			}
		}
	}
}

void Renderer::addRenderingPass(RenderingPass* pPass)
{
	_pRenderingPasses.push_back(pPass);
}

void Renderer::drawActor(Actor* pActor, uint context, Camera* pCamera)
{
	_PipelineManager.getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//Update CBPerActor
	ActorCB cb;
	D3DXMatrixMultiply(&cb._WorldViewMatrix, &pActor->getWorldMatrix(), &pCamera->getView());
	D3DXMatrixMultiply(&cb._WorldViewProjMatrix, &pActor->getWorldMatrix(), &pCamera->getViewProj());

	_PipelineManager.updateCBuffer(_pActorCB->getBuffer(), (void*) &cb, sizeof(ActorCB));

	_PipelineManager.bindConstantBuffer(VERTEX_STAGE, _pActorCB->getBuffer(), 3);

	//Get Model
	const Model*       model = dynamic_cast<const Model*>( getResourceManager().getResource(pActor->getModel()) );

	uint               numSubsets = model->getNumSubsets();
	const ModelSubset* subsets = model->getSubsets();

	const CommandGroup* cmdGroups[4];
	cmdGroups[2] = model->getGeometry()->getCommandGroup();

	//Loop through the subsets of the model and dispatch commands
	for(uint i = 0; i < numSubsets; i++)
	{
		const ModelSubset subset = subsets[i];

		const MaterialContext* pMaterialContext = subsets[i].pMaterial->getMaterialContext(context);

		if(pMaterialContext != NULL)
		{
			cmdGroups[0] = pMaterialContext->pEffect->getCommandGroup();
			cmdGroups[1] = pMaterialContext->pCmdGroup;
			cmdGroups[3] = subset.pCmdGroup;

			dispatchCommands(cmdGroups, 4);
		}
	}
}

void Renderer::drawDirLight(DirLight* pLight, const Texture* pShadow)
{
	_PipelineManager.getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	//bind NULL vertex/index buffers
	_PipelineManager.getIAStage()->setVertexBuffer(NULL, 0, 0, 0);
	_PipelineManager.getIAStage()->setIndexBuffer(NULL, UINT16_INDEX, 0);

	_PipelineManager.updateCBuffer(_pDirLightCB->getBuffer(), pLight, sizeof(DirLight));
	_PipelineManager.bindConstantBuffer(GEOMETRY_STAGE, _pDirLightCB->getBuffer(), 2);

	if(pShadow != NULL)
		_PipelineManager.bindShaderResourceView(PIXEL_STAGE, pShadow->getShaderResource(), 3);
	else
		_PipelineManager.bindShaderResourceView(PIXEL_STAGE, NULL, 3);

	_PipelineManager.draw(1,0);
}

void Renderer::drawPointLight(PointLight* pLight)
{
	_PipelineManager.getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	//bind NULL vertex/index buffers
	_PipelineManager.getIAStage()->setVertexBuffer(NULL, 0, 0, 0);
	_PipelineManager.getIAStage()->setIndexBuffer(NULL, UINT16_INDEX, 0);

	_PipelineManager.updateCBuffer(_pPointLightCB->getBuffer(), pLight, sizeof(PointLight));
	_PipelineManager.bindConstantBuffer(GEOMETRY_STAGE, _pPointLightCB->getBuffer(), 2);

	//No shadows for point lights
	_PipelineManager.bindShaderResourceView(PIXEL_STAGE, NULL, 3);

	_PipelineManager.draw(1,0);
}

void Renderer::drawFullScreen(const PixelShader* pPixelShader)
{
	_PipelineManager.getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	_PipelineManager.bindVertexShader(_pFullScreenVS);
	_PipelineManager.bindHullShader(NULL);
	_PipelineManager.bindDomainShader(NULL);
	_PipelineManager.bindGeometryShader(NULL);
	_PipelineManager.bindPixelShader(pPixelShader);

	_PipelineManager.draw(3, 0);
}

void Renderer::drawSceneEntities(Camera* pCamera, SORT_ORDER sortOrder, ENTITY_TYPE entityType, uint context)
{
	_PipelineManager.updatePipeline();

	switch (entityType)
	{
	case ACTOR:
		{
			uint numActors       = _pScene->getNumActors();
			Actor*const* pActors = _pScene->getActors();

			for(uint i = 0; i < numActors; i++)
			{
				if(pCamera != NULL)
					drawActor(pActors[i], context, pCamera);
				else
					drawActor(pActors[i], context, _pCamera);
			}
		}
		break;

	case LIGHT:
		{
			//draw directional lights
			DirLight*const* pDirLights = _pScene->getDirLights();
			uint numDirLights = _pScene->getNumDirLights();

			const CommandGroup* pEffectCmdGroup = _pDirectionalLightEffect->getCommandGroup();
			/*dispatchCommands(&pEffectCmdGroup, 1);*/

			const RasterizerState* pRS = dynamic_cast<const RasterizerState*>(getPipelineStateManager().getState("noCullRS"));
			const DepthStencilState* pDSS = dynamic_cast<const DepthStencilState*>(getPipelineStateManager().getState("disabledDSS"));
			const BlendState* pBS = dynamic_cast<const BlendState*>(getPipelineStateManager().getState("additiveBS"));

			const RenderTexture* _pDiffuseMapRT=  dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "diffuseMap"));
			const RenderTexture* _pNormalMapRT=  dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "normalMap"));
			const RenderTexture*_pDepthMapRT  = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "depthMap"));

			const RenderTexture*_pLightMapRT  = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "lightingMap"));

			for(uint i = 0; i < numDirLights; i++)
			{
				const Texture* pShadow = NULL;
				CommandGroup* pCSMGenCmd = _pCSMGenerator->execute(_pScene, _pCamera, pDirLights[i]->dir, &pShadow);
				dispatchCommands(&pCSMGenCmd, 1);

				_PipelineManager.getRSStage()->setState(pRS->getState());
				_PipelineManager.getOMStage()->setBlendState(pBS->getState());
				_PipelineManager.getOMStage()->setDepthStencilState(pDSS->getState());

				_PipelineManager.bindShaderResourceView(PIXEL_STAGE, _pDiffuseMapRT->getTexture()->getShaderResource(), 0);
				_PipelineManager.bindShaderResourceView(PIXEL_STAGE, _pNormalMapRT->getTexture()->getShaderResource(), 1);
				_PipelineManager.bindShaderResourceView(PIXEL_STAGE, _pDepthMapRT->getTexture()->getShaderResource(), 2);

				_PipelineManager.getOMStage()->bindRenderTarget(_pLightMapRT->getRenderTarget(), 0);

				dispatchCommands(&pEffectCmdGroup, 1);

				drawDirLight(pDirLights[i], pShadow);
			}

			if(_pointLights)
			{
				//draw volume lights
				pEffectCmdGroup = _pVolumeLightEffect->getCommandGroup();
				dispatchCommands(&pEffectCmdGroup, 1);

				PointLight*const* pPointLights = _pScene->getPointLights();
				uint numPointLights = _pScene->getNumPointLights();

				for(uint i = 0; i < numPointLights; i++)
					drawPointLight(pPointLights[i]);
			}
		}
		break;
	}
}

void Renderer::drawScene(float deltaTime, Scene* pScene, Camera* pCamera, D3D11_VIEWPORT viewport)
{
	_pScene = pScene;
	_pCamera = pCamera;

	_PipelineManager.updatePipeline();
	_PipelineManager.getIAStage()->setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	_PipelineManager.getRSStage()->setDefaultViewport(viewport);
	_PipelineManager.getRSStage()->useDefaultViewport();

	//Clear main render target
	#if _DEBUG
	float clearColor[4] = {0.0f, 0.0f, 0.0f, 0.0f};
	_pImmediateContext->ClearRenderTargetView(_pMainRenderTarget, clearColor);
	#endif

	//Update CBPerFrame
	if(pCamera == NULL)
		return;

	pCamera->update();
	_FrameCB._ViewMatrix = pCamera->getView();
	_FrameCB._ProjMatrix = pCamera->getProj();
	D3DXMatrixMultiply(&_FrameCB._ViewProjMatrix, &_FrameCB._ViewMatrix, &_FrameCB._ProjMatrix);
	D3DXMatrixInverse(&_FrameCB._InvViewProjMatrix, NULL, &_FrameCB._ViewProjMatrix);
	D3DXMatrixInverse(&_FrameCB._InvViewMatrix, NULL, &_FrameCB._ViewMatrix);
	D3DXMatrixInverse(&_FrameCB._InvProjMatrix, NULL, &_FrameCB._ProjMatrix);
	_FrameCB._AmbientLightColor = pScene->getAmbientLightColor();
	_FrameCB._EyePosition = Vector3D(pCamera->getPosition());
	_FrameCB._DeltaTime = deltaTime;
	_FrameCB._ClipDistances = Vector2D(pCamera->getClipDistances());
	
	//Bind CBPerFrame
	_PipelineManager.updateCBuffer(_pFrameCB->getBuffer(), (void*) &_FrameCB, sizeof(_FrameCB));
	_PipelineManager.bindConstantBuffer(VERTEX_STAGE | GEOMETRY_STAGE | PIXEL_STAGE, _pFrameCB->getBuffer(), 1);

	for(uint i = 0; i < _pRenderingPasses.size(); i++)
	{
		CommandGroup* pPassCommands = _pRenderingPasses.at(i)->draw(pScene, pCamera);

		if(pPassCommands == NULL)
			continue;

		dispatchCommands(&pPassCommands, 1);
	}

	///////////////
	//POST PROCESS
	///////////////

	const RenderTexture* pSceneMapRT = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "lightingMap"));

	_PostProcessor.execute(0, _PipelineManager, pSceneMapRT->getTexture() , _pMainRenderTarget);

	_PipelineManager.getRSStage()->useDefaultViewport();

	
	///////////////
	//DEBUGGING CODE
	///////////////
	
	_PipelineManager.useMainRenderTarget();
	_PipelineManager.getOMStage()->applyState();
	
	#if _DEBUG
	getUI().draw(getMainTimer().getFramerate());
	#endif
}

void Renderer::present()
{
	_pSwapChain->Present(0,0);
}

void Renderer::setCSMGenerator(CSMGenerator* pGenerator)
{
	_pCSMGenerator = pGenerator;
}