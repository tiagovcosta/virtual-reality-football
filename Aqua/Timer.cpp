#include "Timer.h"

using namespace Aqua;

Timer& getMainTimer()
{
	static Timer timer;
	return timer;
}

Timer::Timer()
{
	_FramesPerSecond = 0;
	_MaxFramesPerSecond = 0;
	_FrameCount = 0;

	_Delta = 0;
	_FixedDelta = 0.0f;
	_bUseFixedStep = false;

	QueryPerformanceFrequency((LARGE_INTEGER*)&_TicksPerSecond64);
	QueryPerformanceCounter((LARGE_INTEGER*)&_CurrentTicks64);
	_StartTicks64 = _CurrentTicks64;
	_OneSecTicks64 = _CurrentTicks64;

	_bStopped = false;
}

Timer::~Timer()
{}

void Timer::reset()
{
	_FramesPerSecond = 0;
	_FrameCount = 0;
	_Delta = 0;
}

void Timer::tick( )
{
	_PrevTicks64 = _CurrentTicks64;
	QueryPerformanceCounter((LARGE_INTEGER*)&_CurrentTicks64);

	// Update the time increment
	
	if ( _bUseFixedStep )
		_Delta = _FixedDelta;
	else
		_Delta = (float)((__int64)_CurrentTicks64 - (__int64)_PrevTicks64) / (__int64)_TicksPerSecond64;

	// Continue counting the frame rate regardless of the time step.

	if ((float)((__int64)_CurrentTicks64 - (__int64)_OneSecTicks64)
		/ (__int64)_TicksPerSecond64 < 1.0f)
	{
		_FrameCount++;
	}
	else
	{
		_FramesPerSecond = _FrameCount;
		
		if ( _FramesPerSecond > _MaxFramesPerSecond )
			_MaxFramesPerSecond = _FramesPerSecond;

		_FrameCount = 0;
		_OneSecTicks64 = _CurrentTicks64;
	}
}

void Timer::start()
{
	_FramesPerSecond = 0;
	_FrameCount = 0;
	_Delta = 0;
}

void Timer::stop()
{

}

float Timer::getElapsedTime( )
{
	return( _Delta );
}

int Timer::getFramerate( )
{
	return( _FramesPerSecond );
}

float Timer::getRuntime( )
{
	return ( (float)((__int64)_CurrentTicks64 - (__int64)_StartTicks64) / (__int64)_TicksPerSecond64 );
}

int Timer::getMaxFramerate()
{
	return( _MaxFramesPerSecond );
}

int Timer::getFrameCount()
{
	return( _FrameCount );
}

float Timer::getMillisecondsPerFrame()
{
	return (1000.0f/_FramesPerSecond);
}