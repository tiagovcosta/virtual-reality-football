#ifndef HULLSHADERSTAGE_H
#define HULLSHADERSTAGE_H

#include "ShaderStage.h"
#include "HullShader.h"

namespace Aqua
{
	class HullShaderStage : private ShaderStage
	{
	public:
		HullShaderStage();
		~HullShaderStage();

		void setShader(const HullShader* pShader);

		void clearState();

		void applyState();
	private:
		ID3D11HullShader* _pShader;

		friend class PipelineManager;
	};
};

#endif