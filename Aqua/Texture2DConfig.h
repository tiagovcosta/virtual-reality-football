#ifndef TEXTURE2DCONFIG_H
#define TEXTURE2DCONFIG_H

#include "PCH.h"

namespace Aqua
{
	class Texture2DConfig
	{
	public:
		Texture2DConfig();
		Texture2DConfig(const D3D11_TEXTURE2D_DESC& state);
		~Texture2DConfig();

		void setDefaults();
		void setDefaultDepthBuffer( uint width, uint height );
		void setDefaultRenderTarget( uint width, uint height );

		void setWidth( uint state );
		void setHeight( uint state );
		void setMipLevels( uint state );
		void setArraySize( uint state );
		void setFormat( DXGI_FORMAT state );
		void setSampleDesc( DXGI_SAMPLE_DESC state );
		void setUsage( D3D11_USAGE state ); 
		void setBindFlags( uint state );
		void setCPUAccessFlags( uint state );
		void setMiscFlags( uint state );

		const D3D11_TEXTURE2D_DESC& getTextureDesc() const;

	protected:
		D3D11_TEXTURE2D_DESC _State;
	};
};

#endif