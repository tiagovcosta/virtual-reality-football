#include "DomainShaderStage.h"

using namespace Aqua;

DomainShaderStage::DomainShaderStage() : ShaderStage()
{}

DomainShaderStage::~DomainShaderStage()
{}

void DomainShaderStage::setShader(const DomainShader* pShader)
{
	ID3D11DomainShader* shader = NULL;
	
	if(pShader != NULL)
		shader = pShader->getShader();

	if(shader != _pShader)
	{
		_pShader = shader;
		_UpdateShader = true;
	}
}

void DomainShaderStage::clearState()
{
	_UpdateShader = true;
	_pShader = NULL;
	clearStageResources();
}

void DomainShaderStage::applyState()
{
	int num;

	if(_UpdateShader)
	{
		_pContext->DSSetShader(_pShader, NULL, 0);
		_UpdateShader = false;
	}

	num = _EndCB - _StartCB;
	if(num >= 0)
	{
		_pContext->DSSetConstantBuffers(_StartCB, num+1, &_pConstantBuffers[_StartCB]);

		_StartCB = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT;
		_EndCB = -1;
	}

	num = _EndSR - _StartSR;

	if(num >= 0)
	{
		_pContext->DSSetShaderResources(_StartSR, num+1, &_pShaderResources[_StartSR]);

		_StartSR = D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT;
		_EndSR = -1;
	}

	num = _EndST - _StartST;

	if(num >= 0)
	{
		_pContext->DSSetSamplers(_StartST, num+1, &_pSamplerStates[_StartST]);

		_StartST = D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT;
		_EndST = -1;
	}
}