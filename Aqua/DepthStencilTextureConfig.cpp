#include "DepthStencilTextureConfig.h"

using namespace Aqua;

DepthStencilTextureConfig::DepthStencilTextureConfig()
{
	_Default                        = false;

	_TextureDesc.Width              = 0;
	_TextureDesc.Height             = 0;
	_TextureDesc.MipLevels          = 1;
	_TextureDesc.ArraySize          = 1;
	_TextureDesc.Format             = DXGI_FORMAT_UNKNOWN;
	_TextureDesc.SampleDesc.Count   = 1;
	_TextureDesc.SampleDesc.Quality = 0;
	_TextureDesc.Usage              = D3D11_USAGE_DEFAULT;
	_TextureDesc.BindFlags          = D3D11_BIND_DEPTH_STENCIL;
	_TextureDesc.CPUAccessFlags     = 0; 
	_TextureDesc.MiscFlags          = 0;
}

DepthStencilTextureConfig::~DepthStencilTextureConfig()
{}

void DepthStencilTextureConfig::setDefaultDepthStencilTexture(uint width, uint height)
{
	_TextureDesc.Width  = width;
	_TextureDesc.Height = height;
	_TextureDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

	_Default = true;
}

void DepthStencilTextureConfig::setDefaultShadowMap(uint width, uint height)
{
	_TextureDesc.Width     = width;
	_TextureDesc.Height    = height;
	_TextureDesc.Format    = DXGI_FORMAT_R32_TYPELESS;
	_TextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;

	_DepthStencilDesc.Format = DXGI_FORMAT_D32_FLOAT;
	_DepthStencilDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	_DepthStencilDesc.Texture2D.MipSlice = 0;
	_DepthStencilDesc.Flags              = 0;

	_ShaderResourceDesc.Format = DXGI_FORMAT_R32_FLOAT;
	_ShaderResourceDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	_ShaderResourceDesc.Texture2D.MipLevels = _TextureDesc.MipLevels;
	_ShaderResourceDesc.Texture2D.MostDetailedMip = 0;

	_Default = false;
}

const D3D11_TEXTURE2D_DESC* DepthStencilTextureConfig::getTextureDesc() const
{
	return &_TextureDesc;
}

const D3D11_SHADER_RESOURCE_VIEW_DESC* DepthStencilTextureConfig::getShaderResourceDesc() const
{
	if(_Default)
		return NULL;

	return &_ShaderResourceDesc;
}

const D3D11_DEPTH_STENCIL_VIEW_DESC* DepthStencilTextureConfig::getDepthStencilDesc() const
{
	if(_Default)
		return NULL;

	return &_DepthStencilDesc;
}