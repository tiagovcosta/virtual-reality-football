#include "InputManager.h"

using namespace Aqua;

InputManager& getInputManager()
{
	static InputManager manager;

	return manager;
}

InputManager::InputManager()
{}

InputManager::~InputManager()
{}

void InputManager::init(HWND hWnd, uint wndWidth, uint wndHeight)
{
	_hWnd = hWnd;
	_WndWidth = wndWidth;
	_WndHeight = wndHeight;
	_CurrMousePos.x = 0;
	_CurrMousePos.y = 0;
	moveMouseTo(0,0);
}

int InputManager::keyPressed(char c)
{
	return (GetAsyncKeyState(c) & 0x8000);
}

POINT InputManager::getMousePos()
{
	POINT point;

	if(GetCursorPos(&point))
		ScreenToClient(_hWnd, &point);

	_LastMousePos.x = point.x-_WndWidth/2;
	_LastMousePos.y = _WndHeight/2 - point.y;

	return _LastMousePos;
}
		
POINT InputManager::getMousePosVariation()
{
	POINT p;
	p.x = _CurrMousePos.x - _LastMousePos.x;
	p.y = _CurrMousePos.y - _LastMousePos.y;

	_LastMousePos = _CurrMousePos;

	return p;
}

void InputManager::moveMouseTo(int x, int y)
{
	POINT point;
	point.x = x + _WndWidth/2;
	point.y = _WndHeight/2 - y;

	ClientToScreen(_hWnd, &point);
	SetCursorPos(point.x, point.y);

	_LastMousePos = point;
}

void InputManager::setMousePos(int x, int y)
{
	_LastMousePos = _CurrMousePos;
	_CurrMousePos.x = x - _WndWidth/2;
	_CurrMousePos.y = _WndHeight/2 - y;
}