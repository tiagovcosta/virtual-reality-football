#ifndef ACTOR_H
#define ACTOR_H

#include "PCH.h"

#include "Commands.h"
#include "CommandGroup.h"
#include "CommandGroupWriter.h"

#include "Entity.h"

namespace Aqua
{
	struct ActorCB
	{
		D3DXMATRIX _WorldViewMatrix;
		D3DXMATRIX _WorldViewProjMatrix;
	};

	class Actor : public Entity
	{
	public:
		Actor(uint nameID);
		~Actor();

		void setModel(uint modelID);
		
		void setPosition(const Vector3D& position);
		void setRotation(float x, float y, float z);
		void setScaling(float x, float y, float z);

		uint getModel() const;

		void setWorldMatrix(const D3DXMATRIX& matrix);

		const D3DXMATRIX& getWorldMatrix();
	private:
		void updateWorldMatrix();

		//CommandGroup* _pCmdGroup;

		uint          _Model;

		D3DXMATRIX    _WorldMatrix;

		bool          _UpdateWorldMatrix;

		Vector3D      _Position;
		D3DXMATRIX    _RotationMatrix;
		Vector3D      _Scaling;

		friend class Scene;
	};
};

#endif