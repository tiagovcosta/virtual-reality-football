#include "OutputMergerStage.h"

using namespace Aqua;

OutputMergerStage::OutputMergerStage()
{
	clearState();
}

OutputMergerStage::~OutputMergerStage()
{}

void OutputMergerStage::init(ID3D11DeviceContext* pContext)
{
	_pContext = pContext;
	applyState();
}

void OutputMergerStage::shutdown()
{
	clearState();
	applyState();
}

void OutputMergerStage::bindRenderTarget(ID3D11RenderTargetView* pRenderTarget, uint index)
{
	if(pRenderTarget != _pRenderTargets[index])
	{
		_pRenderTargets[index] = pRenderTarget;
		_UpdateRenderTargets = true;
	}
}

void OutputMergerStage::bindDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilView)
{
	if(pDepthStencilView != _pDepthStencilTarget)
	{
		_pDepthStencilTarget = pDepthStencilView;
		_UpdateRenderTargets = true;
	}
}

void OutputMergerStage::setBlendState(ID3D11BlendState* pState)
{
	if(pState != _pBState)
	{
		_pBState = pState;
		_UpdateBState = true;
	}
}

void OutputMergerStage::setDepthStencilState(ID3D11DepthStencilState* pState)
{
	if(pState != _pDSState)
	{
		_pDSState = pState;
		_UpdateDSState = true;
	}
}

void OutputMergerStage::clearState()
{
	_UpdateRenderTargets = true;
	_UpdateDSState = true;
	_UpdateBState = true;

	for(uint i = 0; i < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT; i++)
		_pRenderTargets[i] = NULL;

	_pDepthStencilTarget = NULL;
	_pDSState = NULL;
	_pBState = NULL;
}

void OutputMergerStage::applyState()
{
	uint num = 0;

	if(_UpdateRenderTargets)
	{
		while(num < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT && _pRenderTargets[num] != NULL)
			num++;

		_pContext->OMSetRenderTargets(num, _pRenderTargets, _pDepthStencilTarget);

		_UpdateRenderTargets = false;
	}

	if(_UpdateDSState)
	{
		_pContext->OMSetDepthStencilState(_pDSState, 0);
		_UpdateDSState = false;
	}

	if(_UpdateBState)
	{
		_pContext->OMSetBlendState(_pBState, NULL, 0xFFFFFFFF);
		_UpdateBState = false;
	}
}

void OutputMergerStage::reApplyState()
{
	uint num = 0;

	while(num < D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT && _pRenderTargets[num] != NULL)
		num++;

	_pContext->OMSetRenderTargets(num, _pRenderTargets, _pDepthStencilTarget);

	_UpdateRenderTargets = false;

	_pContext->OMSetDepthStencilState(_pDSState, 0);
	_UpdateDSState = false;

	_pContext->OMSetBlendState(_pBState, NULL, 0xFFFFFFFF);
	_UpdateBState = false;
}