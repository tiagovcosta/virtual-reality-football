#pragma once

#include <PxPhysicsAPI.h>

typedef void(*TriggerCallback)(const void* user_data);

class PhysicsManager: public physx::PxSimulationEventCallback
{
public:
	PhysicsManager();
	~PhysicsManager();

	bool init();
	void shutdown();

	bool update(float dt);

	physx::PxRigidDynamic* createBone(const physx::PxTransform& transform, float half_width, float half_height, float half_depth);

	void                   deleteBone(physx::PxRigidDynamic* bone);

	physx::PxRigidDynamic* createBall(const physx::PxTransform& transform, float radius);

	void                   setBallPosition(physx::PxRigidDynamic* ball, const physx::PxTransform& transform);

	void                   createStaticBox(const physx::PxTransform& transform, float half_width, float half_height, float half_depth);

	physx::PxRigidDynamic* createTrigger(const physx::PxTransform& transform, float half_width, float half_height, float half_depth,
										 TriggerCallback callback);

	// Implements PxSimulationEventCallback
	virtual void onContact(const physx::PxContactPairHeader&, const physx::PxContactPair*, physx::PxU32) {};
	virtual void onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count);
	virtual void onConstraintBreak(physx::PxConstraintInfo*, physx::PxU32) {}
	virtual void onWake(physx::PxActor**, physx::PxU32) {}
	virtual void onSleep(physx::PxActor**, physx::PxU32){}

private:
	PhysicsManager(const PhysicsManager&);
	PhysicsManager& operator=(PhysicsManager);

	physx::PxFoundation*               _foundation;
	physx::PxProfileZoneManager*       _profile_zone_manager;
	physx::PxPhysics*                  _physics;
	physx::PxVisualDebuggerConnection* _debugger_connection;

	physx::PxScene*                    _scene;

	physx::PxMaterial*    _material;
	physx::PxMaterial*    _football_material;
	physx::PxRigidStatic* _ground_plane;

	float _accumulator;
	float _step_size;
};