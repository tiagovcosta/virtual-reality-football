#include "SamplerManager.h"

using namespace Aqua;

SamplerManager& getSamplerManager()
{
	static SamplerManager sampler;

	return sampler;
}

SamplerManager::SamplerManager()
{
	_pDevice = 0;
	_SamplerStates.reserve(3);
}

SamplerManager::~SamplerManager()
{}

void SamplerManager::init(ID3D11Device* pDevice, PipelineManager& pipelineManager)
{
	_pDevice = pDevice;

	const char* name;
	D3D11_SAMPLER_DESC desc;

	name = "gTriLinearSampler";
	desc.Filter   = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.MipLODBias = 0;
	desc.MaxAnisotropy = 16;
	desc.MinLOD        = 0;
	desc.MaxLOD        = D3D11_FLOAT32_MAX;

	createSamplerState(name, &desc);

	name = "gTriPointSampler";
	desc.Filter   = D3D11_FILTER_MIN_MAG_MIP_POINT;

	createSamplerState(name, &desc);

	name = "gAnisoSampler";
	desc.Filter   = D3D11_FILTER_ANISOTROPIC;

	createSamplerState(name, &desc);

	for(uint i = 0; i < _SamplerStates.size(); i++)
		pipelineManager.bindSamplerState(0xFF, _SamplerStates.at(i).pSamplerState, i);
}

void SamplerManager::shutdown()
{
	for(uint i = 0; i < _SamplerStates.size(); i++)
		SAFE_RELEASE(_SamplerStates.at(i).pSamplerState);

	_SamplerStates.clear();
}

int SamplerManager::createSamplerState(const char* name, const D3D11_SAMPLER_DESC* desc)
{
	SamplerState sampler;

	if(FAILED(_pDevice->CreateSamplerState(desc, &(sampler.pSamplerState))))
	{
		QuitWithErrorMessage(std::string("Error creating Sampler State") + name + std::string("."));

		return AqFAIL;
	}

#if _DEBUG
	sampler.pSamplerState->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
#endif

	sampler.nameID = getStringID(name);
	_SamplerStates.push_back(sampler);

	return AqOK;
}

ID3D11SamplerState* SamplerManager::getSamplerState(const char* name)
{
	uint nameID = getStringID(name);

	for(uint i = 0; i < _SamplerStates.size(); i++)
	{
		if(_SamplerStates.at(i).nameID == nameID)
			return _SamplerStates.at(i).pSamplerState;
	}

	return NULL;
}