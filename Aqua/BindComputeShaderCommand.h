#ifndef BINDCOMPUTESHADERCOMMAND_H
#define BINDCOMPUTESHADERCOMMAND_H

#include "Command.h"

#include "ComputeShader.h"

#include "PipelineManager.h"

typedef uint8_t u8;

namespace Aqua
{
	class BindComputeShaderCommand : public Command
	{
	public:
		BindComputeShaderCommand();
		~BindComputeShaderCommand();

		void execute(PipelineManager& pipelineManager);

		void setShader(ComputeShader* pShader);

	private:
		ComputeShader* _pShader;
	};
};

#endif