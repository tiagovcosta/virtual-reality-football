#include "PixelShader.h"

using namespace Aqua;

PixelShader::PixelShader(ID3D11PixelShader* pShader)
{
	_pShader = pShader;
	_Type = PIXEL_SHADER;
}

PixelShader::~PixelShader()
{}

void PixelShader::destroy()
{
	//getLogger().Write(L"Shader: \"" + _FileName + L"\" destroyed.");
	SAFE_RELEASE(_pShader);
}

ID3D11PixelShader* PixelShader::getShader() const
{
	return _pShader;
}