#ifndef BINDRENDERTEXTURESRCOMMAND_H
#define BINDRENDERTEXTURESRCOMMAND_H

#include "RendererEnums.h"

#include "Command.h"

#include "PipelineManager.h"

#include "RenderTexture.h"

namespace Aqua
{
	struct BindRenderTextureSRCommand
	{
		RenderTexture* renderTexture;
		uint slot;
		u8 bindFlags;
	};
};

#endif