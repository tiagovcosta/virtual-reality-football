#ifndef MODEL_H
#define MODEL_H

#include "StackAllocator.h"

#include "Commands.h"
#include "CommandGroup.h"
#include "CommandGroupWriter.h"

#include "Geometry.h"
#include "Material.h"

namespace Aqua
{
	struct ModelSubsetDesc
	{
		uint             geometrySubset;
		const Material*  pMaterial;
	};

	struct ModelDesc
	{
		const Geometry*  pGeometry;
		uint             numSubsets;
		ModelSubsetDesc* pSubsetsDesc;
	};

	struct ModelSubset
	{
		CommandGroup*   pCmdGroup;
		const Material* pMaterial;
	};

	class Model : public Resource
	{
	public:
		Model();
		Model(const ModelDesc& modelDesc);
		~Model();

		const Geometry* getGeometry() const;

		uint  getNumSubsets() const ;
		const ModelSubset* getSubsets() const;

		void destroy();

	private:
		//Array of command groups (each CommandGroup corresponds to a subset)
		CommandGroup** _ppCommandGroups;

		const Geometry* _pGeometry;
		uint            _NumSubsets;
		ModelSubset*    _pSubsets;
	};
};

#endif