#ifndef RENDERTEXTURECONFIG_H
#define RENDERTEXTURECONFIG_H

#include <D3DX11.h>

#include "AquaTypedefs.h"

#include "RendererEnums.h"

namespace Aqua
{
	class RenderTextureConfig
	{
	public:
		RenderTextureConfig();
		~RenderTextureConfig();

		void        setDefaultRenderTexture(RENDER_TEXTURE_FORMAT format, uint width, uint height);

		void        setSize(uint width, uint height);
		void        generateMips(bool generateMips);

		const D3D11_TEXTURE2D_DESC*            getTextureDesc() const;
		const D3D11_SHADER_RESOURCE_VIEW_DESC* getShaderResourceDesc() const;
		const D3D11_RENDER_TARGET_VIEW_DESC*   getRenderTargetDesc() const;

		const RENDER_TEXTURE_FORMAT            getFormat() const;

	private:
		bool                            _Default;

		D3D11_TEXTURE2D_DESC            _TextureDesc;
		D3D11_SHADER_RESOURCE_VIEW_DESC _ShaderResourceDesc;
		D3D11_RENDER_TARGET_VIEW_DESC   _RenderTargetDesc;

		RENDER_TEXTURE_FORMAT           _Format;
	};
};

#endif