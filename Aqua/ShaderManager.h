#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2012              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "PCH.h"
#include "Utilities.h"

#include "Shader.h"
#include "ShaderConfig.h"

#include "Shader.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#include "ComputeShader.h"

//TODO - Return error message when fails to load shader config from file
////////////////////////////////////////////////////////////////////////

const uint NUM_SHADERS = 50;
const uint NUM_INPUTLAYOUTS = 3;

namespace Aqua
{
	enum IL_ELEMENT_FLAGS
	{
		POSITION = 0x01,
		TANGENT = 0x02,
		NORMAL = 0x04,
		TEXCOORD = 0x08
	};

	struct InputLayout
	{
		InputLayoutConfig  config;
		u8                 bitfield;
		ID3D11InputLayout* pLayout;
	};

	//-The Shader Manager is responsible for the creation and management of Shaders and Input Layouts.
	//---Use the ShaderManager by calling the getShaderManager() function, you should NOT create new instances of the Shader Manager.
	//-Use the loadShader(...) method to load new Shaders that dont use permutations.
	//-Use the loadShaderWithPermutations(...) method to load new Shaders that use permutations.
	class ShaderManager
	{
	public:

		ShaderManager();
		~ShaderManager();

		//Loads a list containing every shader and its configuration used in the game
		//This function should only be called once before loading any shader
		int           loadShadersList(const std::string& filename);

		const Shader* getShader(const std::string& filename, u32 permutation = 0);
		ShaderConfig* getShaderConfig(const std::string& filename);

		//Loads Shader
		Shader*       compileShader(const ShaderConfig& shaderConfig, u32 permutation = 0);
		Shader*       compileShader(const std::string& filename, u32 permutation = 0);
		ShaderConfig* loadShaderConfigFromFile(const std::string& filename);

	private:

		void                init(ID3D11Device* pDevice);
		void                shutdown();

		D3D10_SHADER_MACRO* resolvePermutation(const ShaderConfig* pConfig, u32 permutation);

		ID3D11InputLayout*  getInputLayout(u8 bitfield);
		InputLayoutConfig   createInputLayoutConfig(u8 bitfield);

		ID3D11Device*             _pDevice;

		std::vector<Shader*>      _Shaders;
		uint                      _NumShaders;
		
		std::vector<ShaderConfig> _ShaderConfigs;
		std::vector<InputLayout>  _InputLayouts;

		D3D11_INPUT_ELEMENT_DESC  _InputElements[4];

		//Keywords used to load shader configs from file faster
		uint                      _DoubleSlashWordID;
		uint                      _FunctionWordID;
		uint                      _ProfileWordID;
		uint                      _TypeWordID;
		uint                      _InputLayoutWordID;
		uint                      _DefinesWordID;

		friend class Renderer;
	};
};

Aqua::ShaderManager& getShaderManager();

#endif