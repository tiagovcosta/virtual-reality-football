#ifndef RENDERTEXTURE_H
#define RENDERTEXTURE_H

#include <D3D11.h>

#include "RendererEnums.h"

#include "Resource.h"

#include "Texture.h"

namespace Aqua
{
	class RenderTexture : public Resource
	{
	public:
		RenderTexture();
		~RenderTexture();

		ID3D11RenderTargetView* getRenderTarget() const;
		const Texture*          getTexture() const;

		uint                    getWidth() const;
		uint                    getHeight() const;
		RENDER_TEXTURE_FORMAT   getFormat() const;

		void destroy();

	private:
		ID3D11RenderTargetView* _pRenderTarget;
		Texture                 _Texture;

		uint                    _Width;
		uint                    _Height;
		RENDER_TEXTURE_FORMAT   _Format;

		friend class ResourceManager;
	};
};

#endif