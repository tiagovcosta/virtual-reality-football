#ifndef VERTEXSHADERSTATE_H
#define VERTEXSHADERSTATE_H

#include "ShaderStage.h"
#include "VertexShader.h"

namespace Aqua
{
	class VertexShaderStage : private ShaderStage
	{
	public:
		VertexShaderStage();
		~VertexShaderStage();

		void setShader(const VertexShader* pShader);

		void clearState();

		void applyState();
	private:
		ID3D11VertexShader* _pShader;

		friend class PipelineManager;
	};
};

#endif