#ifndef DEBUG_H
#define DEBUG_H

#include "Logger.h"

using namespace Aqua;

#define debugBreak() asm{ int 3 }

#define _T(x) L ## x
#define CHAR(x) #x
#define CHAR2(x) CHAR(x)

#if _DEBUG
#define ASSERT(x) ASSERT2(x, CHAR(x), __FILE__, CHAR2(__LINE__))
#else
#define ASSERT(x) 
#endif

#define ASSERT2(x, t, f, l) \
if(!x) \
{ \
    getLogger().write("Assert " t " failed. In file " f " at line " l ".", ERROR_MESSAGE); \
	DebugBreak(); \
}

inline void QuitWithErrorMessage(std::string x)
{
	getLogger().write(x, ERROR_MESSAGE);
	PostQuitMessage(0);
}

enum RETURN_CODES
{
	AqOK,
	AqFAIL
};

#endif