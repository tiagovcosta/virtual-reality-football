#ifndef SHADERCONFIG_H
#define SHADERCONFIG_H

#include "PCH.h"

#include "InputLayoutConfig.h"
#include "Shader.h"

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2011            
/////////////////////////////////////////////////////////////////////////////////////////////

namespace Aqua
{
	//The ShaderConfig class is used to provide the Shader Manager with the info needed to create a new shader.
	//An InputLayoutConfig* must be provided using the setInputLayoutConfig(...) method when creating Vertex Shaders.
	class ShaderConfig
	{
	public:
		ShaderConfig();
		~ShaderConfig();

		//Sets the name of the file that will be used to create the shader.
		void setFilename(const std::string& filename);
		//Sets the Shader Type (check the SHADER_TYPE enum for more info.
		void setShaderType(SHADER_TYPE type);
		//Sets the name of the function inside the file that the shader has to execute
		void setFunction(const std::string& function);
		//Sets the version of the shader Ex: "vs_4_0" or "ps_5_0"
		void setProfile(const std::string& profile);
		//Sets shader creation flags using this method
		void setFlags(uint flags);
		//Use this method to define shader macros
		void addDefine(std::string pDefine);
		//Sets the input layout bitfield used by this shader when creating Vertex Shaders
		void setInputLayoutBitfield(u8 bitfield);

	private:
		std::string              _Filename;
		SHADER_TYPE              _Type;
		std::string              _Function;
		std::string              _Profile;
		uint                     _Flags;
		std::vector<std::string> _pDefines;
		u8                       _InputLayoutBitfield;

		friend class ShaderManager;
	};
};

#endif