#ifndef CLEARDEPTHSTENCILTARGETCOMMAND_H
#define CLEARDEPTHSTENCILTARGETCOMMAND_H

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class ClearDepthStencilTargetCommand : public Command
	{
	public:
		ClearDepthStencilTargetCommand();
		~ClearDepthStencilTargetCommand();

		void execute(PipelineManager& pipelineManager);

		void setDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilTarget);
		void setValue(float value);

	private:
		ID3D11DepthStencilView* _pDepthStencilTarget;
		float                   _Value;
	};
};

#endif