#ifndef MATERIAL_PASS
#define MATERIAL_PASS

#include "RenderingPass.h"

namespace Aqua
{
	class MaterialPass : public RenderingPass
	{
	public:
		MaterialPass();
		~MaterialPass();

		void init(uint width, uint height);

		CommandGroup* draw(Scene* pScene, Camera* pCamera);

	private:
		const DepthStencilTexture* _pDepthStencilTexture;

		const RenderTexture*       _pSceneRT;
		const RenderTexture*       _pLightingMapRT;
	};
};

#endif