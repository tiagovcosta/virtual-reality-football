#ifndef GBUFFER_PASS
#define GBUFFER_PASS

#include "RenderingPass.h"

namespace Aqua
{
	class GBufferPass : public RenderingPass
	{
	public:
		GBufferPass();
		~GBufferPass();

		void init(uint width, uint height);

		CommandGroup* draw(Scene* pScene, Camera* pCamera);

	private:
		const DepthStencilTexture* _pDepthStencilTexture;

		const RenderTexture*       _pDiffuseMapRT;
		const RenderTexture*       _pNormalMapRT;
		const RenderTexture*       _pDepthMapRT;
	};
};

#endif