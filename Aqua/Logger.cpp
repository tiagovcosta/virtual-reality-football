#include "Logger.h"

using namespace Aqua;

Logger& getLogger()
{
	static Logger logger;

	return logger;
}

Logger::Logger()
{
	_MinMessageLevel = WARNING_MESSAGE;
}

Logger::~Logger()
{}

bool Logger::open(const std::string& logName)
{
	std::string filename = logName+".txt";
	_Log.open(filename.c_str());

	writeSeparater();

	return true;
}

bool Logger::write(const char* text)
{
	_Log << text << "\n";

	#if _DEBUG
	::OutputDebugString( text );
	::OutputDebugString( "\n" );
	#endif

	_Log.flush();

	return true;
}

bool Logger::write( std::string text, MESSAGE_LEVEL level)
{
	if(level < _MinMessageLevel)
		return false;

	if(level == WARNING_MESSAGE)
		text = "WARNING: "+text;
	else if(level == ERROR_MESSAGE)
		text = "ERROR: "+text;

	return write(text.c_str());
}

void Logger::close()
{
	writeSeparater();
	_Log.close();
}

bool Logger::writeSeparater()
{
	write( "------------------------------------------------------------" );

	return true;
}

void Logger::setMessageLevel(MESSAGE_LEVEL level)
{
	_MinMessageLevel = level;
}