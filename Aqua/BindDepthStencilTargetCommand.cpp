#include "BindDepthStencilTargetCommand.h"

using namespace Aqua;

BindDepthStencilTargetCommand::BindDepthStencilTargetCommand()
{
	_pDepthStencilTarget = NULL;
}

BindDepthStencilTargetCommand::~BindDepthStencilTargetCommand()
{
	_pDepthStencilTarget = NULL;
}

void BindDepthStencilTargetCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.getOMStage()->bindDepthStencilTarget(_pDepthStencilTarget);
}

void BindDepthStencilTargetCommand::setDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilTarget)
{
	_pDepthStencilTarget = pDepthStencilTarget;
}