#include "RasterizerState.h"

using namespace Aqua;

RasterizerState::RasterizerState(ID3D11RasterizerState* pState, uint nameID)
{
	_NameID = nameID;
	_pState = pState;
}

RasterizerState::~RasterizerState()
{}

ID3D11RasterizerState* RasterizerState::getState() const
{
	return _pState;
}

void RasterizerState::destroy()
{
	SAFE_RELEASE(_pState);
}