#include "ShaderStage.h"

using namespace Aqua;

ShaderStage::ShaderStage()
{
	_pContext = NULL;
}

ShaderStage::~ShaderStage()
{}

void ShaderStage::init(ID3D11DeviceContext* pContext)
{
	_pContext = pContext;
}

void ShaderStage::shutdown()
{
}

void ShaderStage::setConstantBuffer(ID3D11Buffer* pResource, int slot)
{
	if(pResource != _pConstantBuffers[slot])
	{
		_pConstantBuffers[slot] = (ID3D11Buffer*) pResource;
			
		if(slot < _StartCB)
			_StartCB = slot;
		if (slot > _EndCB)
			_EndCB = slot;
	}
}

void ShaderStage::setShaderResourceView(ID3D11ShaderResourceView* pResource, int slot)
{
	if(pResource != _pShaderResources[slot])
		{
			_pShaderResources[slot] = pResource;
			
			if(slot < _StartSR)
				_StartSR = slot;
			if (slot > _EndSR)
				_EndSR = slot;
		}
}

void ShaderStage::setSamplerState(ID3D11SamplerState* pSamplerState, int slot)
{
	if(pSamplerState != _pSamplerStates[slot])
	{
		_pSamplerStates[slot] = pSamplerState;
			
		if(slot < _StartST)
			_StartST = slot;
		if (slot > _EndST)
			_EndST = slot;
	}
}

void ShaderStage::clearStageResources()
{
	uint i;

	_StartCB = 0;
	_EndCB = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT - 1;

	for(i = 0; i < D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT; i++)
		_pConstantBuffers[i] = NULL;

	_StartSR = 0;
	_EndSR = D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT - 1;

	for(i = 0; i < D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT; i++)
		_pShaderResources[i] = NULL;

	_StartST = 0;
	_EndST = D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT - 1;

	for(i = 0; i < D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT; i++)
		_pSamplerStates[i] = NULL;
}