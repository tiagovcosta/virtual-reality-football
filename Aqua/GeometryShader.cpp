#include "GeometryShader.h"

using namespace Aqua;

GeometryShader::GeometryShader(ID3D11GeometryShader* pShader)
{
	_pShader = pShader;
	_Type = GEOMETRY_SHADER;
}

GeometryShader::~GeometryShader()
{}

void GeometryShader::destroy()
{
	//getLogger().Write(L"Shader: \"" + _FileName + L"\" destroyed.");
	SAFE_RELEASE(_pShader);
}

ID3D11GeometryShader* GeometryShader::getShader() const
{
	return _pShader;
}