#include "PipelineState.h"

using namespace Aqua;

PipelineState::PipelineState()
{
	_NameID = 0;
	_Type = UNKNOWN_STATE;
}

PipelineState::~PipelineState()
{}

uint PipelineState::getNameID() const
{
	return _NameID;
}

PIPELINE_STATE_TYPE PipelineState::getType() const
{
	return _Type;
}