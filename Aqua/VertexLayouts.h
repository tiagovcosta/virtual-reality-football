#ifndef VERTEXLAYOUTS_H
#define VERTEXLAYOUTS_H

#include "PCH.h"

namespace Aqua
{
	struct PVertex
	{
		PVertex() : pos(0,0,0) {}
		PVertex(float x, float y,float z) : pos(x,y,z){}

		D3DXVECTOR3 pos;
	};

	struct PTCVertex
	{
		PTCVertex(){}
		PTCVertex(float x, float y,float z, 
					  float u, float v) 
					  : pos(x,y,z), texC(u,v){}

		D3DXVECTOR3 pos;
		D3DXVECTOR2 texC;
	};

	struct PTNTCVertex
	{
		PTNTCVertex(){}
		PTNTCVertex(float x, float y,float z,
								   float tx, float ty,float tz,
								   float nx, float ny,float nz,
								   float u, float v) 
								   : pos(x,y,z), tangent(tx,ty,tz), normal(nx,ny,nz), texC(u,v){}

		D3DXVECTOR3 pos;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 texC;
	};

	struct NTVertex
	{
		NTVertex(){}
		NTVertex(float nx, float ny,float nz,
								float tx, float ty,float tz) 
								: normal(nx,ny,nz), tangent(tx,ty,tz){}

		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
	};

	struct TCVertex
	{
		TCVertex(){}
		TCVertex(float x, float y) : texC(x,y){}

		D3DXVECTOR2 texC;
	};
};

#endif