#ifndef RASTERIZERSTAGE_H
#define RASTERIZERSTAGE_H

#include "PCH.h"
#include "Logger.h"

#include "Viewport.h"

namespace Aqua
{
	class RasterizerStage
	{
	public:
		RasterizerStage();
		~RasterizerStage();

		void init(ID3D11DeviceContext* pContext);
		void shutdown();

		void setState(ID3D11RasterizerState* pState);
		void setViewport(uint numViewports, const D3D11_VIEWPORT* viewports);

		void setDefaultViewport(D3D11_VIEWPORT viewport);
		void useDefaultViewport();

		void clearState();

		void applyState();

		void reApplyState();

	private:
		ID3D11DeviceContext*   _pContext;

		bool                   _UpdateState;
		ID3D11RasterizerState* _pState;

		bool                   _UpdateViewports;
		uint                   _NumViewports;
		const D3D11_VIEWPORT*  _pViewports;

		D3D11_VIEWPORT         _DefaultViewport;
	};
};

#endif