#include "Effect.h"

Effect::Effect(const EffectDesc& desc)
{
	CommandGroupWriter cmdWriter;
	cmdWriter.begin(&getStackAllocator());

	cmdWriter.addBindVertexShaderCommand(desc.pVertexShader);
	cmdWriter.addBindHullShaderCommand(desc.pHullShader);
	cmdWriter.addBindDomainShaderCommand(desc.pDomainShader);
	cmdWriter.addBindGeometryShaderCommand(desc.pGeometryShader);
	cmdWriter.addBindPixelShaderCommand(desc.pPixelShader);

	_pCommandGroup = cmdWriter.end();
}

Effect::~Effect()
{
	_pCommandGroup = NULL;
}

const CommandGroup* Effect::getCommandGroup() const
{
	return _pCommandGroup;
}

void Effect::destroy()
{

}