#ifndef STACK_ALLOCATOR_H
#define STACK_ALLOCATOR_H

#include "AquaTypedefs.h"

#include "Debug.h"

namespace Aqua
{
	class StackAllocator
	{
	public:
		StackAllocator();
		StackAllocator(u32 size);

		void* allocate(size_t size, size_t alignemt = 16);

		template <class T>
		T* allocateNew()
		{
			return new (allocate(sizeof(T), __alignof(T))) T;
		}
		template <class T>
		T* allocateNew(const T& t)
		{
			return new (allocate(sizeof(T), __alignof(T))) T(t);
		}

		template<class T>
		T* allocateArray(uint maxNumObjects)
		{
			if(maxNumObjects == 0)
				return NULL;

			return new (allocate(sizeof(T)*maxNumObjects, __alignof(T))) T;
		}

		template<class T>
		void deleteObject(T* pObject)
		{
			if (pObject != NULL)
			{
				pObject->~T(); // call the object's destructor
			}
		}

		void freeToMarker(void* marker);

		void clear();

		void destroy();

		//Returns size of the stack
		u32 getSize();

		//Returns ammount of memory in use;
		u32 getUsedMemory();

		//Returns ammount of memory wasted in order to keep objects aligned
		u32 getAlignmentMemory();

		//Returns the pointer to the current position of the stack
		void* getCurrentPosition();

	public:
		void* allocateUnaligned(size_t size);

		void* _InitialPosition;
		void* _CurrentPosition;

		u32 _Size;
		u32 _UsedMemory;
		u32 _AlignmentMemory;
	};
};

Aqua::StackAllocator& getStackAllocator();

#endif