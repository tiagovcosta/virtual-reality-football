#include "BindPixelShaderCommand.h"

using namespace Aqua;

BindPixelShaderCommand::BindPixelShaderCommand()
{
	_pShader = NULL;
}

BindPixelShaderCommand::~BindPixelShaderCommand()
{
	_pShader = NULL;
}

void BindPixelShaderCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindShader(_pShader);
}

void BindPixelShaderCommand::setShader(PixelShader* pShader)
{
	_pShader = pShader;
}