#include "PostProcessor.h"

using namespace Aqua;

PostProcessor::PostProcessor()
{
	_SwapEyeTextures = false;
}

PostProcessor::~PostProcessor()
{}

void PostProcessor::init(u8 effects)
{
	_Effects = effects;

	//Create render textures
	RenderTextureConfig config;
	config.setDefaultRenderTexture(RT_R16, 0, 0);
	config.generateMips(true);

	int luminanceSizes[] = {256, 1, 1};
	for(uint i = 0; i < NUM_LUMINANCE_TEXTURES; i++)
	{
		config.setSize(luminanceSizes[i], luminanceSizes[i]);
		_pLuminanceTextures[i] = getResourceManager().createRenderTexture("luminanceRT", config);
	}

	//_pDeviceContext->ClearRenderTargetView(_pLuminanceTextures[2]->getRenderTargetView(), D3DXCOLOR(20.0f, 0.0f, 0.0f, 0.0f));

	config.setDefaultRenderTexture(RT_RGBA8, BLOOM_TEXTURES_WIDTH, BLOOM_TEXTURES_HEIGHT);

	for(int i = 0; i < NUM_BLOOM_TEXTURES; i++)
	{
		_pBloomTextures[i] = getResourceManager().createRenderTexture("bloomRT", config);
	}

	//Create Post Process CB and Initialize
	_pPostProcessCB = getResourceManager().createConstantBuffer(sizeof(PostProcessCBData), "PostProcessCB");

	_cbData.middleGrey = 1.00f;
	_cbData.white = 2.0f;
	_cbData.brightPassThreshold = 0.90f;
	_cbData.bloomIntensity = 1.00f;
	_cbData.tonemapOperator = 0;
	_cbData.A = 0.22f;
	_cbData.B = 0.30f;
	_cbData.C = 0.10f;
	_cbData.D = 0.20f;
	_cbData.E = 0.025f;
	_cbData.F = 0.30f;
	_cbData.W = 0.25f;

	//Load effects
	_pFullScreenVS = static_cast<const VertexShader*>(getShaderManager().getShader("FullScreenVS.hlsl"));
	_pLuminancePS  = static_cast<const PixelShader*>(getShaderManager().getShader("LuminancePS.hlsl"));
	_pEyeAdaptPS   = static_cast<const PixelShader*>(getShaderManager().getShader("EyeAdaptPS.hlsl"));
	_pBrightPassPS = static_cast<const PixelShader*>(getShaderManager().getShader("BrightPassPS.hlsl"));
	_pHBlurPS      = static_cast<const PixelShader*>(getShaderManager().getShader("HBlurPS.hlsl"));
	_pVBlurPS      = static_cast<const PixelShader*>(getShaderManager().getShader("VBlurPS.hlsl"));
	_pToneMapPS    = static_cast<const PixelShader*>(getShaderManager().getShader("ToneMapPS.hlsl"));


	_LuminanceViewport.Width    = static_cast<float>(luminanceSizes[0]);
	_LuminanceViewport.Height   = static_cast<float>(luminanceSizes[0]);
	_LuminanceViewport.MinDepth = 0.0f;
	_LuminanceViewport.MaxDepth = 1.0f;
	_LuminanceViewport.TopLeftX = 0;
	_LuminanceViewport.TopLeftY = 0;

	_EyeAdaptViewport.Width    = 1;
	_EyeAdaptViewport.Height   = 1;
	_EyeAdaptViewport.MinDepth = 0.0f;
	_EyeAdaptViewport.MaxDepth = 1.0f;
	_EyeAdaptViewport.TopLeftX = 0;
	_EyeAdaptViewport.TopLeftY = 0;

	_BloomViewport.Width    = BLOOM_TEXTURES_WIDTH;
	_BloomViewport.Height   = BLOOM_TEXTURES_HEIGHT;
	_BloomViewport.MinDepth = 0.0f;
	_BloomViewport.MaxDepth = 1.0f;
	_BloomViewport.TopLeftX = 0;
	_BloomViewport.TopLeftY = 0;

	//Add vars to DebugUI
	#if _DEBUG
	getUI().addSlider("Middle Grey", &_cbData.middleGrey, "min=0.0 max=20.0 step=0.01");
	getUI().addSlider("White", &_cbData.white, "min=0.0 max=20.0 step=0.01");
	getUI().addSlider("Bright Pass Threshold", &_cbData.brightPassThreshold, "min=0.0 max=1.0 step=0.01");
	getUI().addSlider("Bloom Intensity", &_cbData.bloomIntensity, "min=0.0 max=1.0 step=0.01");
	TwEnumVal operators[] = {{0, "Filmic"}, {1, "Reinhard"}};
	getUI().addDropList("Tonemap Op", &_cbData.tonemapOperator, operators, 2, NULL);

	getUI().addSlider("Shoulder Str", &_cbData.A, "min=0.0 max=20.0 step=0.01");
	getUI().addSlider("Linear Str", &_cbData.B, "min=0.0 max=20.0 step=0.01");
	getUI().addSlider("Linear Angle", &_cbData.C, "min=0.0 max=20.0 step=0.01");
	getUI().addSlider("Toe Str", &_cbData.D, "min=0.0 max=20.0 step=0.01");
	getUI().addSlider("Toe Numerator", &_cbData.E, "min=0.0 max=20.0 step=0.001");
	getUI().addSlider("Toe Denominator", &_cbData.F, "min=0.0 max=20.0 step=0.01");
	getUI().addSlider("Linear White", &_cbData.W, "min=0.0 max=50.0 step=0.01");
	#endif
}

void PostProcessor::toogleEffect(PostProcessEffects effect)
{
	_Effects = _Effects ^ effect; //boolean XOR
}

void PostProcessor::execute(float dt, PipelineManager& pipelineManager, const Texture* sceneSR, ID3D11RenderTargetView* pRenderTarget)
{
	//No need for Depth testing in PostProces
	pipelineManager.getOMStage()->bindDepthStencilTarget(NULL); 

	pipelineManager.updateCBuffer(_pPostProcessCB->getBuffer(), &_cbData, sizeof(PostProcessCBData));
	pipelineManager.bindConstantBuffer(PIXEL_STAGE, _pPostProcessCB->getBuffer(), 4);

	//Bind Scene texture as shader resource
	pipelineManager.bindShaderResourceView(PIXEL_STAGE, sceneSR->getShaderResource(), 0);

	//Fullscreen VS is used in all post effects
	pipelineManager.bindVertexShader(_pFullScreenVS);
	pipelineManager.bindHullShader(NULL);
	pipelineManager.bindDomainShader(NULL);
	pipelineManager.bindGeometryShader(NULL);

	//Calculate Luminance
	pipelineManager.getRSStage()->setViewport(1, &_LuminanceViewport);
	pipelineManager.bindPixelShader(_pLuminancePS);
	pipelineManager.getOMStage()->bindRenderTarget(_pLuminanceTextures[0]->getRenderTarget(), 0);

	pipelineManager.draw(3, 0);

	pipelineManager.generateSRVMips(_pLuminanceTextures[0]->getTexture()->getShaderResource());

	//Eye Adaptation
	pipelineManager.getRSStage()->setViewport(1, &_EyeAdaptViewport);
	pipelineManager.bindPixelShader(_pEyeAdaptPS);

	if(!_SwapEyeTextures)
	{
		pipelineManager.getOMStage()->bindRenderTarget(_pLuminanceTextures[1]->getRenderTarget(), 0);
		pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pLuminanceTextures[2]->getTexture()->getShaderResource(), 1);
		//_SwapEyeTextures = true;
	} else
	{
		pipelineManager.getOMStage()->bindRenderTarget(_pLuminanceTextures[2]->getRenderTarget(), 0);
		pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pLuminanceTextures[1]->getTexture()->getShaderResource(), 1);
		//_SwapEyeTextures = false;
	}

	pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pLuminanceTextures[0]->getTexture()->getShaderResource(), 2);

	pipelineManager.draw(3, 0);

	//Bind Average Luminance as Shader Resource
	if(!_SwapEyeTextures)
	{
		pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pLuminanceTextures[2]->getTexture()->getShaderResource(), 1);
		_SwapEyeTextures = true;
	} else
	{
		pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pLuminanceTextures[1]->getTexture()->getShaderResource(), 1);
		_SwapEyeTextures = false;
	}

	if(_Effects & BLOOM_PPE)
	{
		pipelineManager.getRSStage()->setViewport(1, &_BloomViewport);

		//Bright Pass
		pipelineManager.getOMStage()->bindRenderTarget(_pBloomTextures[0]->getRenderTarget(), 0);
		pipelineManager.bindPixelShader(_pBrightPassPS);

		pipelineManager.draw(3, 0);

		//HBlur
		pipelineManager.getOMStage()->bindRenderTarget(_pBloomTextures[1]->getRenderTarget(), 0);
		pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pBloomTextures[0]->getTexture()->getShaderResource(), 2);
		pipelineManager.bindPixelShader(_pHBlurPS);

		pipelineManager.draw(3, 0);

		//VBlur
		pipelineManager.getOMStage()->bindRenderTarget(NULL, 0);
		pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pBloomTextures[1]->getTexture()->getShaderResource(), 2);
		pipelineManager.updatePipeline();
		pipelineManager.getOMStage()->bindRenderTarget(_pBloomTextures[0]->getRenderTarget(), 0);
		pipelineManager.bindPixelShader(_pVBlurPS);

		pipelineManager.draw(3, 0);

		pipelineManager.bindShaderResourceView(PIXEL_STAGE, _pBloomTextures[0]->getTexture()->getShaderResource(), 2);
	}

	//Tone Map
	pipelineManager.getOMStage()->bindRenderTarget(pRenderTarget, 0);
	pipelineManager.getRSStage()->useDefaultViewport();
	pipelineManager.bindPixelShader(_pToneMapPS);

	pipelineManager.draw(3, 0);

	pipelineManager.bindShaderResourceView(PIXEL_STAGE, NULL, 0);
}