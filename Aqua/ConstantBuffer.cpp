#include "ConstantBuffer.h"

using namespace Aqua;

ConstantBuffer::ConstantBuffer()
{
	_Type = CBUFFER;
}

ConstantBuffer::~ConstantBuffer()
{}

ID3D11Buffer* ConstantBuffer::getBuffer() const
{
	return _pBuffer;
}

void ConstantBuffer::destroy()
{
	SAFE_RELEASE(_pBuffer);
}