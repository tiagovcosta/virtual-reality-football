#include "Geometry.h"

using namespace Aqua;

Geometry::Geometry(const GeometryDesc& desc)
{
	_Type = GEOMETRY;

	_pVertexBuffer      = desc.pVertexBuffer;
	_VertexBufferStride = desc.vertexBufferStride;
	_pIndexBuffer       = desc.pIndexBuffer;
	_IndexFormat        = desc.indexFormat;

	_NumSubsets         = desc.numSubsets;
	_pSubsets           = getStackAllocator().allocateArray<GeometrySubset>(_NumSubsets);

	for(uint i = 0; i < _NumSubsets; i++)
	{
		_pSubsets[i] = desc.pSubsets[i];
	}

	CommandGroupWriter cmdWriter;
	cmdWriter.begin(&getStackAllocator());

	cmdWriter.addBindVertexBufferCommand(_pVertexBuffer, _VertexBufferStride, 0, 0);

	cmdWriter.addBindIndexBufferCommand(_pIndexBuffer, _IndexFormat, 0);

	_pCommandGroup = cmdWriter.end();
}

Geometry::~Geometry()
{}

uint Geometry::getNumSubsets() const
{
	return _NumSubsets;
}

const GeometrySubset& Geometry::getSubset(uint index) const
{
	ASSERT((index < _NumSubsets));

	return _pSubsets[index];
}

const CommandGroup* Geometry::getCommandGroup() const
{
	return _pCommandGroup;
}

void Geometry::destroy()
{
	SAFE_RELEASE(_pVertexBuffer);
	SAFE_RELEASE(_pIndexBuffer);
}