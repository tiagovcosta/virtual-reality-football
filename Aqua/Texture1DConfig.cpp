#include "Texture1DConfig.h"

using namespace Aqua;

Texture1DConfig::Texture1DConfig()
{
	setDefaults();
}

Texture1DConfig::Texture1DConfig(const D3D11_TEXTURE1D_DESC& state)
{
	_State = state; 
}

Texture1DConfig::~Texture1DConfig()
{
}

void Texture1DConfig::setDefaults()
{
	// Set the state to the default configuration.  These are the D3D11 default
	// values as well.

    _State.Width = 1;
    _State.MipLevels = 1;
    _State.ArraySize = 1;
    _State.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    _State.Usage = D3D11_USAGE_DEFAULT;
    _State.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    _State.CPUAccessFlags = 0;
    _State.MiscFlags = 0;
}

void Texture1DConfig::setWidth( uint state )
{
	_State.Width = state;
}

void Texture1DConfig::setMipLevels( uint state )
{
	_State.MipLevels = state;
}

void Texture1DConfig::setArraySize( uint state )
{
	_State.ArraySize = state;
}

void Texture1DConfig::setFormat( DXGI_FORMAT state )
{
	_State.Format = state;
}

void Texture1DConfig::setUsage( D3D11_USAGE state ) 
{
	_State.Usage = state;
}

void Texture1DConfig::setBindFlags( uint state )
{
	_State.BindFlags = state;
}

void Texture1DConfig::setCPUAccessFlags( uint state )
{
	_State.CPUAccessFlags = state;
}

void Texture1DConfig::setMiscFlags( uint state )
{
	_State.MiscFlags = state;
}

const D3D11_TEXTURE1D_DESC& Texture1DConfig::getTextureDesc() const
{
	return _State;
}