#ifndef BINDINDEXBUFFERCOMMAND_H
#define BINDINDEXBUFFERCOMMAND_H

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindIndexBufferCommand : public Command
	{
	public:
		BindIndexBufferCommand();
		~BindIndexBufferCommand();

		void execute(PipelineManager& pipelineManager);

		void setIndexBuffer(ID3D11Buffer* pBuffer);
		void setFormat(INDEX_BUFFER_FORMAT format);
		void setOffset(uint offset);

	private:
		ID3D11Buffer*       _pBuffer;
		INDEX_BUFFER_FORMAT _Format;
		uint                _Offset;
	};
};

#endif