#include "Viewport.h"

using namespace Aqua;

Viewport::Viewport()
{
	_PosX = 0.0f;
	_PosY = 0.0f;
	_Width = 1.0f;
	_Height = 1.0f;
}

Viewport::~Viewport()
{
	_PosX = 0.0f;
	_PosY = 0.0f;
	_Width = 1.0f;
	_Height = 1.0f;
}

void Viewport::setSize(float x, float y, float width, float height)
{
	//Conditionals used to make sure that the viewport stays inside the [0.0f, 1.0f] range

	if(x < 0.0f)
		_PosX = 0.0f;
	else if(x > 1.0f)
		_PosX = 1.0f;
	else
		_PosX = x;

	if(y < 0.0f)
		_PosY = 0.0f;
	else if(y > 1.0f)
		_PosY = 1.0f;
	else
		_PosY = y;

	if(width + _PosX > 1.0f)
		_Width = 1.0f - _PosX;
	else
		_Width = width;

	if(height + _PosY > 1.0f)
		_Height = 1.0f - _PosY;
	else 
		_Height = height;
}

D3D11_VIEWPORT Viewport::getD3DViewport(uint renderTextureWidth, uint renderTextureHeight) const
{
	D3D11_VIEWPORT vp;

	vp.TopLeftX = _PosX * renderTextureWidth;
	vp.TopLeftY = _PosY * renderTextureHeight;
	vp.Width    = _Width * renderTextureWidth;
	vp.Height   = _Height * renderTextureHeight;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	return vp;
}