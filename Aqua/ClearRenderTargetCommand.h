#ifndef CLEARRENDERTARGETCOMMAND_H
#define CLEARRENDERTARGETCOMMAND_H

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class ClearRenderTargetCommand : public Command
	{
	public:
		ClearRenderTargetCommand();
		~ClearRenderTargetCommand();

		void execute(PipelineManager& pipelineManager);

		void setRenderTarget(ID3D11RenderTargetView* pRenderTarget);
		void setValue(float value);

	private:
		ID3D11RenderTargetView* _pRenderTarget;
		float                   _Value;
	};
};

#endif