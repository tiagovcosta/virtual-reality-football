#include "ComputeShaderStage.h"

using namespace Aqua;

ComputeShaderStage::ComputeShaderStage() : ShaderStage()
{}

ComputeShaderStage::~ComputeShaderStage()
{}

void ComputeShaderStage::setShader(const ComputeShader* pShader)
{
	ID3D11ComputeShader* shader = NULL;
	
	if(pShader != NULL)
		shader = pShader->getShader();

	if(shader != _pShader)
	{
		_pShader = shader;
		_UpdateShader = true;
	}
}

void ComputeShaderStage::clearState()
{
	_UpdateShader = true;
	_pShader = NULL;
	clearStageResources();
}

void ComputeShaderStage::applyState()
{
	int num;

	if(_UpdateShader)
	{
		_pContext->CSSetShader(_pShader, NULL, 0);
		_UpdateShader = false;
	}

	num = _EndCB - _StartCB;
	if(num >= 0)
	{
		_pContext->CSSetConstantBuffers(_StartCB, num+1, &_pConstantBuffers[_StartCB]);

		_StartCB = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT;
		_EndCB = -1;
	}

	num = _EndSR - _StartSR;

	if(num >= 0)
	{
		_pContext->CSSetShaderResources(_StartSR, num+1, &_pShaderResources[_StartSR]);

		_StartSR = D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT;
		_EndSR = -1;
	}

	num = _EndST - _StartST;

	if(num >= 0)
	{
		_pContext->CSSetSamplers(_StartST, num+1, &_pSamplerStates[_StartST]);

		_StartST = D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT;
		_EndST = -1;
	}
}