#include "LightingPass.h"

using namespace Aqua;

LightingPass::LightingPass()
{
	_pCommandGroup        = NULL;
	_pDiffuseMapRT        = NULL;
	_pNormalMapRT         = NULL;
	_pDepthMapRT          = NULL;
	_pLightingMapRT       = NULL;
}

LightingPass::~LightingPass()
{
	_pCommandGroup        = NULL;
	_pDiffuseMapRT        = NULL;
	_pNormalMapRT         = NULL;
	_pDepthMapRT          = NULL;
	_pLightingMapRT       = NULL;
}

void LightingPass::init(uint width, uint height)
{
	//Get GBuffer RTs to use as shader resource
	_pDiffuseMapRT = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "diffuseMap"));
	_pNormalMapRT = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "normalMap"));
	_pDepthMapRT  = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "depthMap"));

	RenderTextureConfig rtConfig;

	//Create LightingMap
	rtConfig.setDefaultRenderTexture(RT_RGBA16, width, height);
	_pLightingMapRT = getResourceManager().createRenderTexture("lightingMap", rtConfig);

	_pEnvMap = dynamic_cast<const Texture*>(getResourceManager().getResource(TEXTURE, "tenerife_CM1.dds"));

	//Create command group
	CommandGroupWriter cmdWriter;
	cmdWriter.begin(&getStackAllocator());

	cmdWriter.addClearRenderTargetCommand(_pLightingMapRT->getRenderTarget(), 0.0f);

	//Disable back-face culling and depth writing/testing and enable additive blending
	cmdWriter.addBindRasterizerStateCommand(dynamic_cast<const RasterizerState*>(getPipelineStateManager().getState("noCullRS")));
	cmdWriter.addBindDepthStencilStateCommand(dynamic_cast<const DepthStencilState*>(getPipelineStateManager().getState("disabledDSS")));
	cmdWriter.addBindBlendStateCommand(dynamic_cast<const BlendState*>(getPipelineStateManager().getState("additiveBS")));

	cmdWriter.addBindTextureCommand(_pDiffuseMapRT->getTexture(), 0, PIXEL_STAGE);
	cmdWriter.addBindTextureCommand(_pNormalMapRT->getTexture(), 1, PIXEL_STAGE);
	cmdWriter.addBindTextureCommand(_pDepthMapRT->getTexture(), 2, PIXEL_STAGE);
	cmdWriter.addBindTextureCommand(_pEnvMap, 4, PIXEL_STAGE);

	cmdWriter.addBindRenderTargetCommand(_pLightingMapRT->getRenderTarget(), 0);

	cmdWriter.addDrawSceneEntitiesCommand(NULL, LIGHT, FRONT_TO_BACK, getStringID("Lighting"));

	cmdWriter.addBindRenderTargetCommand(NULL, 0);

	cmdWriter.addBindTextureCommand(NULL, 0, PIXEL_STAGE);
	cmdWriter.addBindTextureCommand(NULL, 1, PIXEL_STAGE);
	cmdWriter.addBindTextureCommand(NULL, 2, PIXEL_STAGE);

	_pCommandGroup = cmdWriter.end();
}

CommandGroup* LightingPass::draw(Scene* pScene, Camera* pCamera)
{
	return _pCommandGroup;
}