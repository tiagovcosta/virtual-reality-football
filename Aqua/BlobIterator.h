#ifndef BLOBITERATOR_H
#define BLOBITERATOR_H

typedef const void* Blob;

namespace Aqua
{
	class BlobIterator
	{
	public:
		BlobIterator(Blob pStart)
		{
			_pStart = pStart;

			reset();
		}

		~BlobIterator()
		{

		}

		template<typename T>
		const T* nextObject()
		{
			const char* adjustment = static_cast<const char*>(_pCurrent); //Extract adjustment

			const void* pObject = static_cast<const void*>(static_cast<const char*>(_pCurrent) + *adjustment); //Add adjustment to the current position to get pointer to object

			_pCurrent = static_cast<const void*>(static_cast<const char*>(_pCurrent) + sizeof(T) + __alignof(T)); //Add size of object

			return static_cast<const T*>(pObject);
		}

		void reset()
		{
			_pCurrent = _pStart;
		}

	private:
		const void* _pStart;
		const void* _pCurrent;
	};
};

#endif