#include "BindDomainShaderCommand.h"

using namespace Aqua;

BindDomainShaderCommand::BindDomainShaderCommand()
{
	_pShader = NULL;
}

BindDomainShaderCommand::~BindDomainShaderCommand()
{
	_pShader = NULL;
}

void BindDomainShaderCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindShader(_pShader);
}

void BindDomainShaderCommand::setShader(DomainShader* pShader)
{
	_pShader = pShader;
}