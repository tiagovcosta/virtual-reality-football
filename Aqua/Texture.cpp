#include "Texture.h"

using namespace Aqua;

Texture::Texture()
{
	_Type = TEXTURE;

	_pView = NULL;
}

Texture::~Texture()
{
	_pView = NULL;
}

ID3D11ShaderResourceView* Texture::getShaderResource() const
{
	return _pView;
}

void Texture::destroy()
{
	SAFE_RELEASE(_pView);
}