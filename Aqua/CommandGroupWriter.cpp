#include "CommandGroupWriter.h"

using namespace Aqua;

CommandGroupWriter::CommandGroupWriter()
{
	_pAllocator  = NULL;
	_NumCommands = 0;
	_Blob        = NULL;
}

CommandGroupWriter::~CommandGroupWriter()
{
	_pAllocator = NULL;
	_NumCommands = 0;
	_Blob = NULL;

	_pCmdGroup = NULL;
}

void CommandGroupWriter::begin(StackAllocator* pAllocator)
{
	_pAllocator = pAllocator;
	_NumCommands = 0;

	_pCmdGroup = _pAllocator->allocateNew<CommandGroup>(CommandGroup(0, 0, NULL));
	_Blob = _pAllocator->getCurrentPosition();
}

void CommandGroupWriter::addDrawCommand(uint vertexCount, uint startVertex)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(DRAW);

	DrawCommand cmd;
	cmd.vertexCount = vertexCount;
	cmd.startVertex = startVertex;

	_pAllocator->allocateNew<DrawCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addDrawIndexedCommand(uint indexCount, uint startIndex, uint startVertex)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(DRAW_INDEXED);

	DrawIndexedCommand cmd;
	cmd.indexCount  = indexCount;
	cmd.startIndex  = startIndex;
	cmd.startVertex = startVertex;

	_pAllocator->allocateNew<DrawIndexedCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindVertexBufferCommand(ID3D11Buffer* pBuffer, uint stride, uint offset, u8 slot)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_VERTEX_BUFFER);

	BindVertexBufferCommand cmd;
	cmd.pBuffer = pBuffer;
	cmd.stride  = stride;
	cmd.offset  = offset;
	cmd.slot    = slot;

	_pAllocator->allocateNew<BindVertexBufferCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindIndexBufferCommand(ID3D11Buffer* pBuffer, INDEX_BUFFER_FORMAT format, uint offset)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_INDEX_BUFFER);

	BindIndexBufferCommand cmd;
	cmd.pBuffer = pBuffer;
	cmd.format  = format;
	cmd.offset  = offset;

	_pAllocator->allocateNew<BindIndexBufferCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindConstantBuffeCommand(ID3D11Buffer* pCBuffer, u8 slot, u8 bindFlags)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_CONSTANT_BUFFER);

	BindConstantBufferCommand cmd;
	cmd.pConstantBuffer = pCBuffer;
	cmd.slot            = slot;
	cmd.bindFlags       = bindFlags;

	_pAllocator->allocateNew<BindConstantBufferCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindTextureCommand(const Texture* pTexture, u8 slot, u8 bindFlags)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_TEXTURE);

	BindTextureCommand cmd;
	cmd.pTexture  = pTexture;
	cmd.slot      = slot;
	cmd.bindFlags = bindFlags;

	_pAllocator->allocateNew<BindTextureCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindVertexShaderCommand(VertexShader* pShader)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_VERTEX_SHADER);

	BindVertexShaderCommand cmd;
	cmd.pShader = pShader;

	_pAllocator->allocateNew<BindVertexShaderCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindHullShaderCommand(HullShader* pShader)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_HULL_SHADER);

	BindHullShaderCommand cmd;
	cmd.pShader = pShader;

	_pAllocator->allocateNew<BindHullShaderCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindDomainShaderCommand(DomainShader* pShader)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_DOMAIN_SHADER);

	BindDomainShaderCommand cmd;
	cmd.pShader = pShader;

	_pAllocator->allocateNew<BindDomainShaderCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindGeometryShaderCommand(GeometryShader* pShader)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_GEOMETRY_SHADER);

	BindGeometryShaderCommand cmd;
	cmd.pShader = pShader;

	_pAllocator->allocateNew<BindGeometryShaderCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindPixelShaderCommand(PixelShader* pShader)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_PIXEL_SHADER);

	BindPixelShaderCommand cmd;
	cmd.pShader = pShader;

	_pAllocator->allocateNew<BindPixelShaderCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindComputeShaderCommand(ComputeShader* pShader)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_COMPUTE_SHADER);

	BindComputeShaderCommand cmd;
	cmd.pShader = pShader;

	_pAllocator->allocateNew<BindComputeShaderCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindSamplerStateCommand(ID3D11SamplerState* pState, u8 bindFlags, u8 slot)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_SAMPLER_STATE);

	BindSamplerStateCommand cmd;
	cmd.pSamplerState = pState;
	cmd.bindFlags     = bindFlags;
	cmd.slot          = slot;

	_pAllocator->allocateNew<BindSamplerStateCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindBlendStateCommand(const BlendState* pState)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_BLEND_STATE);

	BindBlendStateCommand cmd;
	cmd.pBlendState = pState;

	_pAllocator->allocateNew<BindBlendStateCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindDepthStencilStateCommand(const DepthStencilState* pState)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_DEPTH_STENCIL_STATE);

	BindDepthStencilStateCommand cmd;
	cmd.pDepthStencilState = pState;

	_pAllocator->allocateNew<BindDepthStencilStateCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindRasterizerStateCommand(const RasterizerState* pState)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_RASTERIZER_STATE);

	BindRasterizerStateCommand cmd;
	cmd.pRasterizerState = pState;

	_pAllocator->allocateNew<BindRasterizerStateCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindViewportCommand(const Viewport* pViewport, uint targetWidth, uint targetHeight)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_VIEWPORT);

	BindViewportCommand cmd;
	cmd.pViewport    = pViewport;
	cmd.targetWidth  = targetWidth;
	cmd.targetHeight = targetHeight;

	_pAllocator->allocateNew<BindViewportCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindRenderTargetCommand(ID3D11RenderTargetView* pTarget, u8 slot)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_RENDER_TARGET);

	BindRenderTargetCommand cmd;
	cmd.pRenderTarget = pTarget;
	cmd.slot          = slot;

	_pAllocator->allocateNew<BindRenderTargetCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindDepthStencilTargetCommand(ID3D11DepthStencilView* pTarget)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_DEPTH_STENCIL_TARGET);

	BindDepthStencilTargetCommand cmd;
	cmd.pDepthStencilTarget = pTarget;

	_pAllocator->allocateNew<BindDepthStencilTargetCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addClearRenderTargetCommand(ID3D11RenderTargetView* pTarget, float value)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(CLEAR_RENDER_TARGET);

	ClearRenderTargetCommand cmd;
	cmd.pRenderTarget = pTarget;
	cmd.value         = value;

	_pAllocator->allocateNew<ClearRenderTargetCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addClearDepthStencilTargetCommand(ID3D11DepthStencilView* pTarget, float value)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(CLEAR_DEPTH_STENCIL_TARGET);

	ClearDepthStencilTargetCommand cmd;
	cmd.pDepthStencilTarget = pTarget;
	cmd.value               = value;

	_pAllocator->allocateNew<ClearDepthStencilTargetCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addUpdateCBufferCommand(ID3D11Buffer* pCBuffer, const void* pData, uint size)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(UPDATE_CBUFFER);

	UpdateCBufferCommand cmd;
	cmd.pCBuffer = pCBuffer;
	cmd.pData    = pData;
	cmd.size     = size;

	_pAllocator->allocateNew<UpdateCBufferCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addDrawFullScreenCommand(const PixelShader* pShader)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(DRAW_FULLSCREEN);

	DrawFullScreenCommand cmd;
	cmd.pPixelShader = pShader;

	_pAllocator->allocateNew<DrawFullScreenCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addDrawSceneEntitiesCommand(Camera* pCamera, ENTITY_TYPE entitiesType, SORT_ORDER sortOrder, uint context)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(DRAW_SCENE_ENTITIES);

	DrawSceneEntitiesCommand cmd;
	cmd.pCamera      = pCamera;
	cmd.entitiesType = entitiesType;
	cmd.sortOrder    = sortOrder;
	cmd.context      = context;

	_pAllocator->allocateNew<DrawSceneEntitiesCommand>(cmd);

	_NumCommands++;
}

void CommandGroupWriter::addBindMainRenderTargetCommand()
{
	_pAllocator->allocateNew<COMMAND_TYPE>(BIND_MAIN_RENDER_TARGET);

	_NumCommands++;
}

void CommandGroupWriter::addDispatchCommandsCommand(const CommandGroup* pCmdGroup)
{
	_pAllocator->allocateNew<COMMAND_TYPE>(DISPATCH_COMMANDS);

	DispatchCommandsCommand cmd;
	cmd.pCmdGroup = pCmdGroup;

	_pAllocator->allocateNew<DispatchCommandsCommand>(cmd);

	_NumCommands++;
}

CommandGroup* CommandGroupWriter::end()
{
	uint size = (uintptr_t)(_pAllocator->getCurrentPosition()) - (uintptr_t)(_Blob);

	*_pCmdGroup = CommandGroup(size, _NumCommands, _Blob);

	return _pCmdGroup;
}