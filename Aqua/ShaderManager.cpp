#include "ShaderManager.h"

#include <D3DX11.h>

using namespace Aqua;

ShaderManager& getShaderManager()
{
	static ShaderManager manager;

	return manager;
}

ShaderManager::ShaderManager()
{
	_pDevice = 0;
	_InputElements[0].SemanticName = "POSITION";
	_InputElements[0].SemanticIndex = 0;
	_InputElements[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	_InputElements[0].InputSlot = 0;
	_InputElements[0].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	_InputElements[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	_InputElements[0].InstanceDataStepRate = 0;

	_InputElements[1].SemanticName = "TANGENT";
	_InputElements[1].SemanticIndex = 0;
	_InputElements[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	_InputElements[1].InputSlot = 0;
	_InputElements[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	_InputElements[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	_InputElements[1].InstanceDataStepRate = 0;

	_InputElements[2].SemanticName = "NORMAL";
	_InputElements[2].SemanticIndex = 0;
	_InputElements[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	_InputElements[2].InputSlot = 0;
	_InputElements[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	_InputElements[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	_InputElements[2].InstanceDataStepRate = 0;

	_InputElements[3].SemanticName = "TEXCOORD";
	_InputElements[3].SemanticIndex = 0;
	_InputElements[3].Format = DXGI_FORMAT_R32G32_FLOAT;
	_InputElements[3].InputSlot = 0;
	_InputElements[3].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	_InputElements[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	_InputElements[3].InstanceDataStepRate = 0;

	_DoubleSlashWordID = getStringID("//");
	_FunctionWordID    = getStringID("Function");;
	_ProfileWordID     = getStringID("Profile");;
	_TypeWordID        = getStringID("Type");;
	_InputLayoutWordID = getStringID("InputLayout");;
	_DefinesWordID     = getStringID("Defines");;
}

ShaderManager::~ShaderManager()
{}

void ShaderManager::init(ID3D11Device* pDevice)
{
	_pDevice = pDevice;
	_Shaders.reserve(NUM_SHADERS);
	_InputLayouts.reserve(NUM_INPUTLAYOUTS);
}

void ShaderManager::shutdown()
{
	for(uint i = 0; i < _Shaders.size(); i++)
		_Shaders.at(i)->destroy();

	_Shaders.clear();

	_ShaderConfigs.clear();

	for(uint i = 0; i < _InputLayouts.size(); i++)
		SAFE_RELEASE(_InputLayouts.at(i).pLayout);

	_InputLayouts.clear();
}

int ShaderManager::loadShadersList(const std::string& filename)
{
	return AqOK;
}

const Shader* ShaderManager::getShader(const std::string& filename, u32 permutation)
{
	uint id = getStringID(filename.c_str());

	for(uint i = 0; i < _Shaders.size(); i++)
	{
		Shader* shader = _Shaders.at(i);
		if(shader->getNameID() == id && shader->getPermutation() == permutation)
			return shader;
	}

	return compileShader(filename, permutation);
}

Shader* ShaderManager::compileShader(const ShaderConfig& shaderConfig, u32 permutation)
{
	ID3DBlob*           pCompiledShader = NULL;
	ID3DBlob*           pErrorMessages  = NULL;
	D3D10_SHADER_MACRO* pDefines        = resolvePermutation(&shaderConfig, permutation);

    uint flags = shaderConfig._Flags | D3D10_SHADER_PACK_MATRIX_ROW_MAJOR;
#ifdef _DEBUG
    flags |= D3D10_SHADER_DEBUG | D3D10_SHADER_SKIP_OPTIMIZATION | D3D10_SHADER_WARNINGS_ARE_ERRORS;
#else
	flags |= D3D10_SHADER_OPTIMIZATION_LEVEL3;
#endif

	std::string filePath;
	filePath.append("../shaders/");
	filePath.append(shaderConfig._Filename);

	if(FAILED(D3DX11CompileFromFile(filePath.c_str(),
									pDefines,
									0,
									shaderConfig._Function.c_str(),
									shaderConfig._Profile.c_str(),
									flags,
									0,//uint Flags2,
									0,
									&pCompiledShader,
									&pErrorMessages,
									NULL)))
	{

		std::stringstream message;

		message << "--Error compiling shader program: " << shaderConfig._Filename << std::endl;
		message << "The following error was reported:" << std::endl;

		if ( pErrorMessages != 0 )
		{
			LPVOID pCompileErrors = pErrorMessages->GetBufferPointer();
			const char* pMessage = (const char*)pCompileErrors;
			std::string string = std::string(pMessage);
			message << string;
		}

		QuitWithErrorMessage(message.str());

		return NULL;
	}

	HRESULT hr = S_OK;

	Shader* pShader;

	switch(shaderConfig._Type)
	{
	case VERTEX_SHADER:
		{
			/*if(shaderConfig._InputLayoutBitfield == 0)
			{
				QuitWithErrorMessage(std::string("A Input Layout Config must be specified when creating Vertex Shaders"));

				return NULL;
			}*/

			pShader = (VertexShader*) getStackAllocator().allocate(sizeof(VertexShader));

			ID3D11VertexShader* pVertexShader = NULL;
			ID3D11InputLayout*  pInputLayout  = NULL;

			hr = _pDevice->CreateVertexShader( pCompiledShader->GetBufferPointer(),
												pCompiledShader->GetBufferSize(),
												0, &pVertexShader );
			#if _DEBUG
			pVertexShader->SetPrivateData(WKPDID_D3DDebugObjectName, shaderConfig._Filename.length(), shaderConfig._Filename.c_str());
			#endif

			pInputLayout = getInputLayout(shaderConfig._InputLayoutBitfield);

			if(shaderConfig._InputLayoutBitfield != 0)
			{
				if(pInputLayout == NULL)
				{
					InputLayoutConfig config = createInputLayoutConfig(shaderConfig._InputLayoutBitfield);

					hr = hr == E_FAIL ? hr : _pDevice->CreateInputLayout(config.getInputLayoutElements(), 
																			config.getNumElements(), 
																			pCompiledShader->GetBufferPointer(), 
																			pCompiledShader->GetBufferSize(), &pInputLayout);

					_InputLayouts.back().pLayout = pInputLayout;
				}
			}

			new(pShader) VertexShader( pVertexShader,  pInputLayout);
			break;
		}

	case HULL_SHADER:
		{
			pShader = (HullShader*) getStackAllocator().allocate(sizeof(HullShader));

			ID3D11HullShader* pHullShader = 0;

			hr = _pDevice->CreateHullShader( pCompiledShader->GetBufferPointer(),
											  pCompiledShader->GetBufferSize(),
											  0, &pHullShader );

			#if _DEBUG
				pHullShader->SetPrivateData(WKPDID_D3DDebugObjectName, shaderConfig._Filename.length(), shaderConfig._Filename.c_str());
			#endif

			new(pShader) HullShader( pHullShader );
			break;
		}

	case DOMAIN_SHADER:
		{
			pShader = (DomainShader*) getStackAllocator().allocate(sizeof(DomainShader));

			ID3D11DomainShader* pDomainShader = 0;

			hr = _pDevice->CreateDomainShader( pCompiledShader->GetBufferPointer(),
												pCompiledShader->GetBufferSize(),
												0, &pDomainShader );

			#if _DEBUG
				pDomainShader->SetPrivateData(WKPDID_D3DDebugObjectName, shaderConfig._Filename.length(), shaderConfig._Filename.c_str());
			#endif

			new(pShader) DomainShader( pDomainShader );
			break;
		}

	case GEOMETRY_SHADER:
		{
			pShader = (GeometryShader*) getStackAllocator().allocate(sizeof(GeometryShader));

			ID3D11GeometryShader* pGeometryShader = 0;

			hr = _pDevice->CreateGeometryShader( pCompiledShader->GetBufferPointer(),
												  pCompiledShader->GetBufferSize(),
												  0, &pGeometryShader );

			#if _DEBUG
				pGeometryShader->SetPrivateData(WKPDID_D3DDebugObjectName, shaderConfig._Filename.length(), shaderConfig._Filename.c_str());
			#endif

			new(pShader) GeometryShader( pGeometryShader );
			break;
		}

	case PIXEL_SHADER:
		{
			pShader = (PixelShader*) getStackAllocator().allocate(sizeof(PixelShader));

			ID3D11PixelShader* pPixelShader = 0;

			hr = _pDevice->CreatePixelShader( pCompiledShader->GetBufferPointer(),
											   pCompiledShader->GetBufferSize(),
											   0, &pPixelShader );

			#if _DEBUG
				pPixelShader->SetPrivateData(WKPDID_D3DDebugObjectName, shaderConfig._Filename.length(), shaderConfig._Filename.c_str());
			#endif

			new(pShader) PixelShader( pPixelShader );
			break;
		}

	case COMPUTE_SHADER:
		{
			pShader = (ComputeShader*) getStackAllocator().allocate(sizeof(ComputeShader));

			ID3D11ComputeShader* pComputeShader = 0;

			hr = _pDevice->CreateComputeShader( pCompiledShader->GetBufferPointer(),
												 pCompiledShader->GetBufferSize(),
												 0, &pComputeShader );

			#if _DEBUG
				pComputeShader->SetPrivateData(WKPDID_D3DDebugObjectName, shaderConfig._Filename.length(), shaderConfig._Filename.c_str());
			#endif

			new(pShader) ComputeShader( pComputeShader );
			break;
		}

	default:
		QuitWithErrorMessage(std::string("Invalid SHADER_TYPE!"));
	}

	if ( FAILED( hr ) )
	{
		pCompiledShader->Release();
		QuitWithErrorMessage(std::string("Failed to create shader: \"") +  shaderConfig._Filename + std::string("\"!!!"));
		return NULL;
	}

	getLogger().write(std::string("Shader: \"") + shaderConfig._Filename + std::string("\" created."), INFO_MESSAGE);

	pShader->setShaderNameID(getStringID(shaderConfig._Filename.c_str()));
	pShader->setPermutation(permutation);
	pShader->setShaderBlob(pCompiledShader);

	_Shaders.push_back(pShader);

	if(pDefines != NULL)
		delete[] pDefines;

	return pShader;
}

Shader* ShaderManager::compileShader(const std::string& filename, u32 permutation)
{
	ShaderConfig* config = getShaderConfig(filename);

	return compileShader(*config, permutation);
}

ShaderConfig* ShaderManager::getShaderConfig(const std::string& filename)
{
	for(uint i = 0; i < _ShaderConfigs.size(); i++)
		if(_ShaderConfigs.at(i)._Filename.compare(filename) == 0)
			return &_ShaderConfigs.at(i);

	return loadShaderConfigFromFile(filename);
}

D3D10_SHADER_MACRO* ShaderManager::resolvePermutation(const ShaderConfig* pConfig, u32 permutation)
{
	int count = 0;

	if(permutation == 0)
		return NULL;

	for(int i = 0; i < 32; i++)
	{
		if(permutation & (1 << i))
			count++;
	}

	if(count == 0)
		return NULL;

	D3D10_SHADER_MACRO* pDefines = new D3D10_SHADER_MACRO[count+1];

	int k = 0;
	for(int i = 0; i < 32; i++)
	{
		if(permutation & (1 << i))
		{
			pDefines[k].Name       = pConfig->_pDefines.at(i).c_str();
			pDefines[k].Definition = "1";
			k++;
		}
	}

	pDefines[k].Definition = NULL;
	pDefines[k].Name       = NULL;

	return pDefines;
}

ShaderConfig* ShaderManager::loadShaderConfigFromFile(const std::string& filename)
{
	std::string word;
	word.append("../shaders/");
	word.append(filename);

	std::ifstream inStream;

	inStream.open(word, std::ios::in);

	if(!inStream.good())
	{
		inStream.close();
		return NULL;
	}

	ShaderConfig config;
	config.setFilename(filename);

	inStream >> word;
	int wordID = getStringID(word.c_str());

	while(wordID == _DoubleSlashWordID)
	{
		inStream >> word;
		wordID = getStringID(word.c_str());

		if(wordID == _FunctionWordID)
		{
			inStream >> word;
			inStream >> word;
			config.setFunction(word);
		} else if(wordID == _ProfileWordID)
		{
			inStream >> word;
			inStream >> word;
			config.setProfile(word);
		} else if(wordID == _TypeWordID)
		{
			inStream >> word;
			inStream >> word;
			if(word.compare("Vertex") == 0)
				config.setShaderType(VERTEX_SHADER);
			else if(word.compare("Hull") == 0)
				config.setShaderType(HULL_SHADER);
			else if(word.compare("Domain") == 0)
				config.setShaderType(DOMAIN_SHADER);
			else if(word.compare("Geometry") == 0)
				config.setShaderType(GEOMETRY_SHADER);
			else if(word.compare("Pixel") == 0)
				config.setShaderType(PIXEL_SHADER);
			else if(word.compare("Compute") == 0)
				config.setShaderType(COMPUTE_SHADER);
		} else if(wordID == _InputLayoutWordID)
		{
			int il;
			inStream >> word;
			inStream >> il;
			config.setInputLayoutBitfield(il);
		} else if(wordID == _DefinesWordID)
		{
			inStream >> word;
			inStream >> word;
			wordID = getStringID(word.c_str());

			while(wordID != _DoubleSlashWordID)
			{
				config.addDefine(word);
				inStream >> word;
				wordID = getStringID(word.c_str());
			}

			continue;
		}

		inStream >> word;
		wordID = getStringID(word.c_str());
	}

	_ShaderConfigs.push_back(config);
	return &_ShaderConfigs.back();
}

ID3D11InputLayout* ShaderManager::getInputLayout(u8 bitfield)
{
	for(uint i = 0; i < _InputLayouts.size(); i++)
		if(_InputLayouts.at(i).bitfield == bitfield)
			return _InputLayouts.at(i).pLayout;

	return NULL;
}

InputLayoutConfig ShaderManager::createInputLayoutConfig(u8 bitfield)
{
	InputLayout layout;
	layout.bitfield = bitfield;

	int count = 0;
	for(int i = 0; i < 8; i++)
		if(bitfield & (1 << i))
			count++;

	layout.config = InputLayoutConfig(count);

	if(bitfield & (1 << 0))
		layout.config.addElement(_InputElements[0]);
	if(bitfield & (1 << 1))
		layout.config.addElement(_InputElements[1]);
	if(bitfield & (1 << 2))
		layout.config.addElement(_InputElements[2]);
	if(bitfield & (1 << 3))
		layout.config.addElement(_InputElements[3]);

	_InputLayouts.push_back(layout);

	return _InputLayouts.back().config;
}