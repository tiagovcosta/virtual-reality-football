#include "Aqua.h"

using namespace Aqua;

AquaGame* aqua = NULL;

LRESULT CALLBACK WindowProc(HWND hWnd, uint messAqua, WPARAM wParam, LPARAM lParam);

AquaGame::AquaGame(LPCSTR gameTitle, HINSTANCE hInstance, int showCmd, int width, int height, int nSamples, int samplesQuality)
{
	_GameTitle = gameTitle;
	_HInst     = hInstance;
	_ShowCmd   = showCmd;

	_WndWidth = width;
	_WndHeight = height;

	_GameState = GAME_RUNNING;
	_WndState  = WINDOW_MAX;

	aqua = this;

	if( FAILED ( createWindow() ) )
		PostQuitMessage(0);

	//Check Memory leaks
#if _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	//_CrtSetBreakAlloc(522207);
#endif
}

AquaGame::~AquaGame()
{}

void AquaGame::setGameState(GAME_STATE gameState)
{
	_GameState = gameState;
}

HRESULT AquaGame::createWindow()
{
	WNDCLASSEX wc;

	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = _HInst;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = "AquaWindowClass";

	RegisterClassEx(&wc);

	RECT rect;
	rect.left = 0;
	rect.top = 0;
	rect.right = _WndWidth;
	rect.bottom = _WndHeight;

	AdjustWindowRectEx(&rect, WS_CAPTION | /*WS_MAXIMIZE |*/ WS_SYSMENU, false, 0);

	int wchars_num =  MultiByteToWideChar( CP_UTF8 , 0 , _GameTitle , -1, NULL , 0 );
	wchar_t* wGameTitle = new wchar_t[wchars_num];
	MultiByteToWideChar( CP_UTF8 , 0 , _GameTitle , -1, wGameTitle , wchars_num );

	_HWnd = CreateWindowEx(NULL,
						  "AquaWindowClass",
						  _GameTitle,
						  /*WS_OVERLAPPEDWINDOW*/ WS_CAPTION | /*WS_MAXIMIZE |*/ WS_SYSMENU,
						  5, 5,
						  rect.right-rect.left, rect.bottom-rect.top,
						  NULL,
						  NULL,
						  _HInst,
						  NULL);

	delete[] wGameTitle;

	ShowWindow(_HWnd, _ShowCmd);

#if _DEBUG
	//AllocConsole();
#endif

	return S_OK;
}

int AquaGame::run()
{
	MSG  msg;

	getMainTimer().reset();

	while(true)
	{
		if(PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ))
		{
			if(msg.message == WM_QUIT)
			{
				aqua->shutdown();
				break;
			}

			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}else
		{
			getMainTimer().tick();

			if(_GameState == GAME_RUNNING)
			{
				/*#if _DEBUG
				std::stringstream outs;   
				outs.precision(6);
				outs << "Milliseconds: Per Frame: " << _Timer.getMillisecondsPerFrame() << " \n" << " FPS: " << _Timer.getFramerate();
				::OutputDebugString( outs.str().c_str() );
				::OutputDebugString( "\n" );
				_FrameStats = outs.str();		
				#endif*/
				update(getMainTimer().getElapsedTime());
			}
		}

	}

	return (int)msg.wParam;
}

void AquaGame::init()
{
#if _DEBUG
	getLogger().open("AquaLog");
#endif
	getRenderer().init(_WndWidth, _WndHeight, _HWnd, true);
	getInputManager().init(_HWnd, _WndWidth, _WndHeight);
}

void AquaGame::update(float dt)
{
	render();
}

void AquaGame::render()
{
	getRenderer().present();
}

void AquaGame::shutdown()
{
	getRenderer().shutdown();

#if _DEBUG
	getLogger().close();
#endif
}

void AquaGame::drawStats()
{
	//TODO
	std::string temp(_FrameStats.length(), ' ');
	std::copy(_FrameStats.begin(), _FrameStats.end(), temp.begin()); 
}

HWND AquaGame::getWindow()
{
	return _HWnd;
}

HINSTANCE AquaGame::getInstance()
{
	return _HInst;
}

bool AquaGame::isGamePaused()
{
	if(_GameState == GAME_PAUSED)
	{
		return true;
	}

	return false;
}

uint AquaGame::getWindowHeight()
{
	return _WndHeight;
}

uint AquaGame::getWindowWidth()
{
	return _WndWidth;
}

LRESULT CALLBACK WindowProc(HWND hWnd, uint message, WPARAM wParam, LPARAM lParam)
{
#if _DEBUG
	if( TwEventWin(hWnd, message, wParam, lParam) ) // send event message to AntTweakBar
		return 0; // event has been handled by AntTweakBar
#endif

	switch(message)
	{
		case WM_DESTROY:
			{
				PostQuitMessage(0);
				return 0;
			} break;
		case WM_SIZE:
			if( wParam == SIZE_MINIMIZED )
			{
				aqua->setGameState(GAME_PAUSED);
				getMainTimer().stop();
			} else if( wParam == SIZE_MAXIMIZED || wParam == SIZE_RESTORED )
			{
				aqua->setGameState(GAME_RUNNING);
				getMainTimer().start();
			}
			break;
		case WM_ACTIVATEAPP:
			if(wParam == FALSE)
			{
				aqua->setGameState(GAME_PAUSED);
				getMainTimer().stop();
			} else
			{
				aqua->setGameState(GAME_RUNNING);
				getMainTimer().start();
			}
			break;
		case WM_MOUSEMOVE:
			break;
	}

	return DefWindowProc (hWnd, message, wParam, lParam);
}