#include "BindVertexShaderCommand.h"

using namespace Aqua;

BindVertexShaderCommand::BindVertexShaderCommand()
{
	_pShader = NULL;
}

BindVertexShaderCommand::~BindVertexShaderCommand()
{
	_pShader = NULL;
}

void BindVertexShaderCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindShader(_pShader);
}

void BindVertexShaderCommand::setShader(VertexShader* pShader)
{
	_pShader = pShader;
}