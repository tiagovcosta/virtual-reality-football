#ifndef BINDPIXELSHADERCOMMAND_H
#define BINDPIXELSHADERCOMMAND_H

#include "Command.h"

#include "PixelShader.h"

#include "PipelineManager.h"

typedef uint8_t u8;

namespace Aqua
{
	class BindPixelShaderCommand : public Command
	{
	public:
		BindPixelShaderCommand();
		~BindPixelShaderCommand();

		void execute(PipelineManager& pipelineManager);

		void setShader(PixelShader* pShader);

	private:
		PixelShader* _pShader;
	};
};

#endif