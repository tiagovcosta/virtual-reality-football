#include "VertexShaderStage.h"

using namespace Aqua;

VertexShaderStage::VertexShaderStage() : ShaderStage()
{}

VertexShaderStage::~VertexShaderStage()
{}

void VertexShaderStage::setShader(const VertexShader* pShader)
{
	ID3D11VertexShader* shader = NULL;
	
	if(pShader != NULL)
		shader = pShader->getShader();

	if(shader != _pShader)
	{
		_pShader = shader;
		_UpdateShader = true;
	}
}

void VertexShaderStage::clearState()
{
	_UpdateShader = true;
	_pShader = NULL;
	clearStageResources();
}

void VertexShaderStage::applyState()
{
	int num;

	if(_UpdateShader)
	{
		_pContext->VSSetShader(_pShader, NULL, 0);
		_UpdateShader = false;
	}

	num = _EndCB - _StartCB;
	if(num >= 0)
	{
		_pContext->VSSetConstantBuffers(_StartCB, num+1, &_pConstantBuffers[_StartCB]);

		_StartCB = D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT;
		_EndCB = -1;
	}

	num = _EndSR - _StartSR;

	if(num >= 0)
	{
		_pContext->VSSetShaderResources(_StartSR, num+1, &_pShaderResources[_StartSR]);

		_StartSR = D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT;
		_EndSR = -1;
	}

	num = _EndST - _StartST;

	if(num >= 0)
	{
		_pContext->VSSetSamplers(_StartST, num+1, &_pSamplerStates[_StartST]);

		_StartST = D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT;
		_EndST = -1;
	}
}