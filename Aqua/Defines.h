#ifndef DEFINES_H
#define DEFINES_H

#define SAFE_RELEASE( x ) {if(x){(x)->Release();(x)=NULL;}}
#define SAFE_DESTROY( x ) {if(x){(x)->destroy();(x)=NULL;}}

#endif