#include "BindRenderTargetCommand.h"

using namespace Aqua;

BindRenderTargetCommand::BindRenderTargetCommand()
{
	_pRenderTarget = NULL;
	_Slot = 0;
}

BindRenderTargetCommand::~BindRenderTargetCommand()
{
	_pRenderTarget = NULL;
	_Slot = 0;
}

void BindRenderTargetCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.getOMStage()->bindRenderTarget(_pRenderTarget, _Slot);
}

void BindRenderTargetCommand::setRenderTarget(ID3D11RenderTargetView* pRenderTarget)
{
	_pRenderTarget = pRenderTarget;
}

void BindRenderTargetCommand::setSlot(uint slot)
{
	_Slot = slot;
}