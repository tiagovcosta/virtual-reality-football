#ifndef SKYDYNAMIC_H
#define SKYDYNAMIC_H

#include "RenderingPass.h"

namespace Aqua
{
	struct SkyDynamicData
	{
		Vector3D InvWavelength;
		float _pad1;
		Vector3D WavelengthMie;
		float _pad2;
		Vector3D v3SunDir;
		float _pad3;
	};

	class SkyDynamic : public RenderingPass
	{
	public:
		SkyDynamic();
		~SkyDynamic();

		void init(float radius);
		void update();

		CommandGroup* draw(Scene* pScene, Camera* pCamera);

		void setSunDirection(const Vector3D& dir);

		void changeSunDirection(float theta, float phi);

		D3DXVECTOR3 getSunDirection() const;
		D3DXVECTOR3 getSunColor() const;

		const ConstantBuffer* getSkyDynamicCB() const;

	private:
		void buildSkyDome();

		float _radius;

		CommandGroup*         _pDrawCommandGroup;
		CommandGroup*         _pUpdateRayleighCommandGroup;
		CommandGroup*         _pUpdateMieCommandGroup;

		D3DXMATRIX            _WorldViewProj;
		const ConstantBuffer* _pSkyCBuffer;

		const RenderTexture*  _pRayleighRT;
		const RenderTexture*  _pMieRT;

		const PixelShader*    _pUpdateRayleighPS;
		const PixelShader*    _pUpdateMiePS;
		const Effect*         _pSkyDynamicEffect;

		const Geometry*       _pSkyDome;

		Viewport              _UpdateViewport;

		SkyDynamicData        _SkyDynamicData;
		const ConstantBuffer* _pSkyDynamicCB;

		const RenderTexture*       _pSceneMapRT;
		const DepthStencilTexture* _pMainDepthStencilTexture;

		Vector3D              _Wavelengths;
		float                 _Theta;
		float                 _Phi;

		bool                  _Update;
		//Used to make sure that mie updates after rayleigh updates.
		//Otherwise if direction changed every frame mie wouldnt be updated.
		bool                  _UpdateMie;
	};
};

#endif