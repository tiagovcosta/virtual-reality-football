#ifndef DRAWCOMMAND_H
#define DRAWCOMMAND_H

#include "PipelineManager.h"

namespace Aqua
{
	class DrawCommand
	{
	public:
		DrawCommand();
		~DrawCommand();

		void execute(PipelineManager& pipelineManager);

		void setVertexCount(uint vertexCount);

		void setStartVertex(uint startVertex);

	private:
		uint _VertexCount;
		uint _StartVertex;
	};
};

#endif