#include "RenderTexture.h"

using namespace Aqua;

RenderTexture::RenderTexture()
{
	_Type = RENDER_TEXTURE;
}

RenderTexture::~RenderTexture()
{
}

ID3D11RenderTargetView* RenderTexture::getRenderTarget() const
{
	return _pRenderTarget;
}

const Texture* RenderTexture::getTexture() const
{
	return &_Texture;
}

uint RenderTexture::getWidth() const
{
	return _Width;
}

uint RenderTexture::getHeight() const
{
	return _Height;
}

void RenderTexture::destroy()
{
	SAFE_RELEASE(_pRenderTarget);
	_Texture.destroy();
}