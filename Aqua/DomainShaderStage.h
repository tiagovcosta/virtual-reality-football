#ifndef DOMAINSHADERSTAGE_H
#define DOMAINSHADERSTAGE_H

#include "ShaderStage.h"
#include "DomainShader.h"

namespace Aqua
{
	class DomainShaderStage : private ShaderStage
	{
	public:
		DomainShaderStage();
		~DomainShaderStage();

		void setShader(const DomainShader* pShader);

		void clearState();

		void applyState();
	private:
		ID3D11DomainShader* _pShader;

		friend class PipelineManager;
	};
};

#endif