#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <D3D11.h>

#include "StackAllocator.h"

#include "RendererEnums.h"

#include "Commands.h"
#include "CommandGroup.h"
#include "CommandGroupWriter.h"

#include "Resource.h"

namespace Aqua
{
	struct GeometrySubset
	{
		uint indexCount;
		uint startIndex;
	};

	struct GeometryDesc
	{
		GeometryDesc()
		{
			pVertexBuffer      = NULL;;
			vertexBufferStride = 0;
			pIndexBuffer       = NULL;
			indexFormat        = UINT16_INDEX;
			numSubsets         = 0;
			pSubsets           = NULL;
		}

		ID3D11Buffer*         pVertexBuffer;
		uint                  vertexBufferStride;
		ID3D11Buffer*         pIndexBuffer;
		INDEX_BUFFER_FORMAT   indexFormat;
		uint                  numSubsets;
		const GeometrySubset* pSubsets;
	};

	class Geometry : public Resource
	{
	public:
		Geometry(const GeometryDesc& desc);
		~Geometry();

		uint                  getNumSubsets() const;
		const GeometrySubset& getSubset(uint index) const;

		const CommandGroup*   getCommandGroup() const;

		void                  destroy();

	private:
		CommandGroup*       _pCommandGroup;

		ID3D11Buffer*       _pVertexBuffer;
		uint                _VertexBufferStride;

		ID3D11Buffer*       _pIndexBuffer;
		INDEX_BUFFER_FORMAT _IndexFormat;

		uint                _NumSubsets;
		GeometrySubset*     _pSubsets;
	};
};

#endif