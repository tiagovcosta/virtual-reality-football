#ifndef BUFFERCONFIG_H
#define BUFFERCONFIG_H

#include "PCH.h"

namespace Aqua
{
	class BufferConfig
	{
	public:
		BufferConfig();
		BufferConfig(const D3D11_BUFFER_DESC& state);
		~BufferConfig();

		void setDefaults();

		void setDefaultConstantBuffer( uint size, bool dynamic, bool cpuAccess );
		void setDefaultVertexBuffer( uint size, bool dynamic );
		void setDefaultIndexBuffer( uint size, bool dynamic );
		void setDefaultStructuredBuffer( uint size, uint structsize );
		void setDefaultByteAddressBuffer( uint size );
		void setDefaultIndirectArgsBuffer( uint size );
		void setDefaultStagingBuffer( uint size );

		void setByteWidth( uint state );
		void setUsage( D3D11_USAGE state );
	    void setBindFlags( uint state );
	    void setCPUAccessFlags( uint state );
	    void setMiscFlags( uint state );	
	    void setStructureByteStride( uint state );

		const D3D11_BUFFER_DESC& getBufferDesc() const;

	private:
		D3D11_BUFFER_DESC 		_State;
	};
};

#endif