#include "InputLayoutConfig.h"

using namespace Aqua;

InputLayoutConfig::InputLayoutConfig()
{
	/*_NumElements =  0;
	_NumElementsAdded = 0;
	_Config = 0;*/
}

InputLayoutConfig::InputLayoutConfig(uint numElements)
{
	/*_NumElements =  numElements;
	_NumElementsAdded = 0;
	_Config = new D3D11_INPUT_ELEMENT_DESC[numElements];*/
}

InputLayoutConfig::~InputLayoutConfig()
{
	//delete _Config;
}

/*void InputLayoutConfig::addPerVertexElement(LPCSTR name, uint semanticIndex, FORMAT format, uint inputSlot, uint alignedByteOffset)
{
	assert(_NumElementsAdded < _NumElements);

	_Config[_NumElementsAdded].SemanticName = name;
	_Config[_NumElementsAdded].SemanticIndex = semanticIndex;
	_Config[_NumElementsAdded].Format = format;
	_Config[_NumElementsAdded].InputSlot = inputSlot;
	_Config[_NumElementsAdded].AlignedByteOffset = alignedByteOffset;
	_Config[_NumElementsAdded].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	_Config[_NumElementsAdded].InstanceDataStepRate = 0;
	_NumElementsAdded++;
}

void InputLayoutConfig::addPerInstanceElement(LPCSTR name, uint semanticIndex, FORMAT format, uint inputSlot, uint instanceDataStepRate, uint alignedByteOffset)
{
	ASSERT(_NumElementsAdded < _NumElements);

	_Config[_NumElementsAdded].SemanticName = name;
	_Config[_NumElementsAdded].SemanticIndex = semanticIndex;
	_Config[_NumElementsAdded].Format = format;
	_Config[_NumElementsAdded].InputSlot = inputSlot;
	_Config[_NumElementsAdded].AlignedByteOffset = alignedByteOffset;
	_Config[_NumElementsAdded].InputSlotClass = D3D11_INPUT_PER_INSTANCE_DATA;
	_Config[_NumElementsAdded].InstanceDataStepRate = 0;
	_NumElementsAdded++;
}*/



void InputLayoutConfig::addElement(D3D11_INPUT_ELEMENT_DESC element)
{
	_Elements.push_back(element);
}
void InputLayoutConfig::removeLastElement()
{
	_Elements.pop_back();
}

uint InputLayoutConfig::getNumElements()
{
	return _Elements.size();
}

D3D11_INPUT_ELEMENT_DESC* InputLayoutConfig::getInputLayoutElements()
{
	return &(_Elements.front());
}