#ifndef RASTERIZERSTATE_H
#define RASTERIZERSTATE_H

#include "PCH.h"
#include "PipelineState.h"

namespace Aqua
{
	class RasterizerState : public PipelineState
	{
	public:
		RasterizerState(ID3D11RasterizerState* pState, uint nameID);
		~RasterizerState();

		ID3D11RasterizerState* getState() const;

		void                    destroy();

	private:
		ID3D11RasterizerState* _pState;
	};
};

#endif