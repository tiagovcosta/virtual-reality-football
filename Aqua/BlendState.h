#ifndef BLENDSTATE_H
#define BLENDSTATE_H

#include "PCH.h"
#include "PipelineState.h"

namespace Aqua
{
	class BlendState : public PipelineState
	{
	public:
		BlendState(ID3D11BlendState* pState, uint nameID);
		~BlendState();

		ID3D11BlendState* getState() const;

		void                    destroy();

	private:
		ID3D11BlendState* _pState;
	};
};

#endif