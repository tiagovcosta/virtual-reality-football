#include "Shader.h"

using namespace Aqua;

Shader::Shader()
{
	_Type            = UNKNOWN_SHADER;
	_pCompiledShader = NULL;
}

Shader::~Shader()
{
	SAFE_RELEASE(_pCompiledShader);
}

void Shader::setShaderNameID(uint id)
{
	_NameID = id;
}

void Shader::setPermutation(u32 permutation)
{
	_Permutation = permutation;
}

void Shader::setShaderBlob(ID3DBlob* pCompiledShader)
{
	_pCompiledShader = pCompiledShader;
}

uint Shader::getNameID() const
{
	return _NameID;
}

u32 Shader::getPermutation() const
{
	return _Permutation;
}

SHADER_TYPE Shader::getType() const
{
	return _Type;
}

void Shader::printShaderDetails()
{
	/*std::wstringstream s;

	s << L"-----------------------------------------------------------------d-----" << std::endl;
	s << L"Shader details printout for: " << _FileName << std::endl;
	s << L"----------------------------------------------------------------------" << std::endl;

	s << L"Shader Description: " << std::endl;
	s << L"Version: " << _Desc.Version << std::endl;
	s << L"Creator: " << _Desc.Creator << std::endl;
	s << L"Flags: " << _Desc.Flags << std::endl;
	s << L"ConstantBuffers: " << _Desc.ConstantBuffers << std::endl;
	s << L"BoundResources: " << _Desc.BoundResources << std::endl;
	s << L"InputParameters: " << _Desc.InputParameters << std::endl;
	s << L"OutputParameters: " << _Desc.OutputParameters << std::endl;
	s << L"InstructionCount: " << _Desc.InstructionCount << std::endl;
	s << L"TempRegisterCount: " << _Desc.TempRegisterCount << std::endl;
	s << L"TempArrayCount: " << _Desc.TempArrayCount << std::endl;
	s << L"DefCount: " << _Desc.DefCount << std::endl;
	s << L"DclCount: " << _Desc.DclCount << std::endl;
	s << L"TextureNormalInstructions: " << _Desc.TextureNormalInstructions << std::endl;
	s << L"TextureLoadInstructions: " << _Desc.TextureLoadInstructions << std::endl;
	s << L"TextureCompInstructions: " << _Desc.TextureCompInstructions << std::endl;
	s << L"TextureBiasInstructions: " << _Desc.TextureBiasInstructions << std::endl;
	s << L"TextureGradientInstructions: " << _Desc.TextureGradientInstructions << std::endl;
	s << L"FloatInstructionCount: " << _Desc.FloatInstructionCount << std::endl;
	s << L"IntInstructionCount: " << _Desc.IntInstructionCount << std::endl;
	s << L"UintInstructionCount: " << _Desc.UintInstructionCount << std::endl;
	s << L"StaticFlowControlCount: " << _Desc.StaticFlowControlCount << std::endl;
	s << L"DynamicFlowControlCount: " << _Desc.DynamicFlowControlCount << std::endl;
	s << L"MacroInstructionCount: " << _Desc.MacroInstructionCount << std::endl;
	s << L"ArrayInstructionCount: " << _Desc.ArrayInstructionCount << std::endl;
	s << L"CutInstructionCount: " << _Desc.CutInstructionCount << std::endl;
	s << L"EmitInstructionCount: " << _Desc.EmitInstructionCount << std::endl;
	s << L"GSOutputTopology: " << Print_D3D10_PRIMITIVE_TOPOLOGY( _Desc.GSOutputTopology ) << std::endl;
	s << L"GSMaxOutputVertexCount: " << _Desc.GSMaxOutputVertexCount << std::endl;
	s << L"InputPrimitive: " << Print_D3D11_PRIMITIVE( _Desc.InputPrimitive ) << std::endl;
	s << L"PatchConstantParameters: " << _Desc.PatchConstantParameters << std::endl;
	s << L"cGSInstanceCount: " << _Desc.cGSInstanceCount << std::endl;
	s << L"cControlPoints: " << _Desc.cControlPoints << std::endl;
	s << L"HSOutputPrimitive: " << Print_D3D11_TESSELLATOR_OUTPUT_PRIMITIVE( _Desc.HSOutputPrimitive ) << std::endl;
	s << L"HSPartitioning: " << Print_D3D11_TESSELLATOR_PARTITIONING( _Desc.HSPartitioning ) << std::endl;
	s << L"TessellatorDomain: " << Print_D3D11_TESSELLATOR_DOMAIN( _Desc.TessellatorDomain ) << std::endl;
	s << L"cBarrierInstructions: " << _Desc.cBarrierInstructions << std::endl;
	s << L"cInterlockedInstructions: " << _Desc.cInterlockedInstructions << std::endl;
	s << L"cTextureStoreInstructions: " << _Desc.cTextureStoreInstructions << std::endl;
	
	s << std::endl;

	s << L"Number of Input Signature Elements: " << InputSignatureParameters.count() << std::endl;
	for ( int i = 0; i < InputSignatureParameters.count(); i++ )
	{
		s << L"  Semantic Name: " << InputSignatureParameters[i].SemanticName;
		s << L", Semantic Index: " << InputSignatureParameters[i].SemanticIndex;
		s << L", Register: " << InputSignatureParameters[i].Register;
		s << L", System Value Type: " << RendererDX11::TranslateSystemVariableName( InputSignatureParameters[i].SystemValueType ); 
		s << L", Component Type: " << RendererDX11::TranslateRegisterComponentType( InputSignatureParameters[i].ComponentType );
		s << L", Mask: " << (unsigned int)InputSignatureParameters[i].Mask;
		s << L", Read/Write Mask: " << (unsigned int)InputSignatureParameters[i].ReadWriteMask;
		s << std::endl;
	}
	s << std::endl;

	s << L"Number of Output Signature Elements: " << OutputSignatureParameters.count() << std::endl;
	for ( int i = 0; i < OutputSignatureParameters.count(); i++ )
	{
		s << L"  Semantic Name: " << OutputSignatureParameters[i].SemanticName;
		s << L", Semantic Index: " << OutputSignatureParameters[i].SemanticIndex;
		s << L", Register: " << OutputSignatureParameters[i].Register;
		s << L", System Value Type: " << RendererDX11::TranslateSystemVariableName( OutputSignatureParameters[i].SystemValueType );  
		s << L", Component Type: " << RendererDX11::TranslateRegisterComponentType( OutputSignatureParameters[i].ComponentType );
		s << L", Mask: " << (unsigned int)OutputSignatureParameters[i].Mask;
		s << L", Read/Write Mask: " << (unsigned int)OutputSignatureParameters[i].ReadWriteMask;
		s << std::endl;
	}
	s << std::endl;

	s << L"Number of Constant Buffer Descriptions: " << ConstantBuffers.count() << std::endl;
	for ( int i = 0; i < ConstantBuffers.count(); i++ )
	{
		s << L"  Buffer Name: " << ConstantBuffers[i].Description.Name;
		s << L", Buffer Type: " << RendererDX11::TranslateCBufferType( ConstantBuffers[i].Description.Type );
		s << L", Variables: " << ConstantBuffers[i].Description.Variables;
		s << L", Size: " << ConstantBuffers[i].Description.Size;
		s << L", Flags: " << ConstantBuffers[i].Description.uFlags;
		
		for ( int j = 0; j < ConstantBuffers[i].Variables.count(); j++ )
		{
			s << std::endl << L"    ";
			s << L"Variable Name: " << ConstantBuffers[i].Variables[j].Name;
			s << L", Start Offset: " << ConstantBuffers[i].Variables[j].StartOffset;
			s << L", Size: " << ConstantBuffers[i].Variables[j].Size;
			s << L", Flags: " << ConstantBuffers[i].Variables[j].uFlags;
			s << L", Start Texture: " << (int)ConstantBuffers[i].Variables[j].StartTexture;
			s << L", Texture Size: " << ConstantBuffers[i].Variables[j].TextureSize;
			s << L", Start Sampler: " << (int)ConstantBuffers[i].Variables[j].StartSampler;
			s << L", Sampler Size: " << ConstantBuffers[i].Variables[j].SamplerSize;
		}


		for ( int j = 0; j < ConstantBuffers[i].Types.count(); j++ )
		{
			s << std::endl << L"    ";
			s << L"Variable Type Name: " << ConstantBuffers[i].Types[j].Name;
			s << L", Class: " << RendererDX11::TranslateShaderVariableClass( ConstantBuffers[i].Types[j].Class );
			s << L", Type: " << RendererDX11::TranslateShaderVariableType( ConstantBuffers[i].Types[j].Type );			
			s << L", Rows: " << ConstantBuffers[i].Types[j].Rows;
			s << L", Columns: " << ConstantBuffers[i].Types[j].Columns;
			s << L", Elements: " << ConstantBuffers[i].Types[j].Elements;
			s << L", Members: " << ConstantBuffers[i].Types[j].Members;
			s << L", Offset: " << ConstantBuffers[i].Types[j].Offset;
		}
		
		s << std::endl;
	}
	s << std::endl;

	s << L"Number of Resource Binding Descriptions: " << ResourceBindings.count() << std::endl;
	for ( int i = 0; i < ResourceBindings.count(); i++ )
	{
		s << L"  Name: " << ResourceBindings[i].Name;
		s << L", Type: " << RendererDX11::TranslateShaderInputType( ResourceBindings[i].Type );
		s << L", Bind Point: " << ResourceBindings[i].BindPoint;
		s << L", Bind Count: " << ResourceBindings[i].BindCount;
		s << L", Flags: " << ResourceBindings[i].uFlags;
		s << L", Resource Return Type: " << RendererDX11::TranslateResourceReturnType( ResourceBindings[i].ReturnType );
		s << L", Dimension: " << ResourceBindings[i].Dimension;
		s << L", Number of Samples: " << ResourceBindings[i].NumSamples;
		s << std::endl;
	}

	s << std::endl;

	LPVOID pBlobBuffer = pCompiledShader->GetBufferPointer();
	const char* pMessage = (const char*)pBlobBuffer;
	
	s << pMessage << std::endl;

	s << L"----------------------------------------------------------------------" << std::endl;

	Log::Get().Write( s.str() );*/
}