#include "PipelineStateManager.h"

using namespace Aqua;

PipelineStateManager& getPipelineStateManager()
{
	static PipelineStateManager sampler;

	return sampler;
}

PipelineStateManager::PipelineStateManager()
{
	_pDevice = 0;
	_pPipelineStates.reserve(5);
}

PipelineStateManager::~PipelineStateManager()
{}

void PipelineStateManager::init(ID3D11Device* pDevice)
{
	_pDevice = pDevice;
	const char* name;

	//DEPTH STENCIL STATES
	D3D11_DEPTH_STENCIL_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	name                = "lessEqualDSS";
	desc.DepthEnable    = true;
	desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	desc.DepthFunc      = D3D11_COMPARISON_LESS_EQUAL;

	createDepthStencilState(name, &desc);

	name             = "disabledDSS";
	desc.DepthEnable = false;
	desc.StencilEnable = false;

	createDepthStencilState(name, &desc);

	//BLEND STATES
	name = "additiveBS";
	D3D11_BLEND_DESC blendDesc;
	blendDesc.AlphaToCoverageEnable                 = FALSE;
	blendDesc.IndependentBlendEnable                = FALSE;
	blendDesc.RenderTarget[0].BlendEnable           = TRUE;
	blendDesc.RenderTarget[0].SrcBlend              = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend             = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOp               = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha         = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha        = D3D11_BLEND_ONE; 
	blendDesc.RenderTarget[0].BlendOpAlpha          = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL; 

	createBlendState(name, &blendDesc);

	//RASTERIZER STATES
	name = "noCullRS";
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.FrontCounterClockwise = FALSE;
	rasterDesc.DepthBias = 0;
	rasterDesc.SlopeScaledDepthBias = 0.0f;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = TRUE;
	rasterDesc.ScissorEnable = FALSE;
	rasterDesc.MultisampleEnable = FALSE;
	rasterDesc.AntialiasedLineEnable = FALSE;

	createRasterizerState(name, &rasterDesc);
}

void PipelineStateManager::shutdown()
{
	for(uint i = 0; i < _pPipelineStates.size(); i++)
		_pPipelineStates.at(i)->destroy();

	_pPipelineStates.clear();
}

const PipelineState* PipelineStateManager::getState(uint stateID) const
{
	ASSERT(stateID < _pPipelineStates.size());

	if(stateID < _pPipelineStates.size())
		return NULL;

	return _pPipelineStates.at(stateID);
}

const PipelineState* PipelineStateManager::getState(const char* name) const
{
	uint nameID = getStringID(name);

	for(uint i = 0; i < _pPipelineStates.size(); i++)
	{
		if(_pPipelineStates.at(i)->getNameID() == nameID)
			return _pPipelineStates.at(i);
	}

	return NULL;
}

uint PipelineStateManager::getStateID(const char* name) const
{
	INT nameID = getStringID(name);

	for(uint i = 0; i < _pPipelineStates.size(); i++)
	{
		if(_pPipelineStates.at(i)->getNameID() == nameID)
			return i;
	}

	return 0;
}

uint PipelineStateManager::createDepthStencilState(const char* name, const D3D11_DEPTH_STENCIL_DESC* desc)
{
	ID3D11DepthStencilState* pDXState;

	if(FAILED(_pDevice->CreateDepthStencilState(desc, &pDXState)))
	{
		QuitWithErrorMessage(std::string("Error creating DepthStencil State") + name + std::string("."));

		return AqFAIL;
	}

#if _DEBUG
	pDXState->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
#endif

	DepthStencilState state(pDXState, getStringID(name));

	DepthStencilState* pState = getStackAllocator().allocateNew<DepthStencilState>(state);

	_pPipelineStates.push_back(pState);

	return AqOK;
}

uint PipelineStateManager::createBlendState(const char* name, const D3D11_BLEND_DESC* desc)
{
	ID3D11BlendState* pDXState;

	if(FAILED(_pDevice->CreateBlendState(desc, &pDXState)))
	{
		QuitWithErrorMessage(std::string("Error creating Blend State") + name + std::string("."));

		return AqFAIL;
	}

#if _DEBUG
	pDXState->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
#endif

	BlendState state = BlendState(pDXState, getStringID(name));

	BlendState* pState = getStackAllocator().allocateNew<BlendState>(state);

	_pPipelineStates.push_back(pState);

	return AqOK;
}

uint PipelineStateManager::createRasterizerState(const char* name, const D3D11_RASTERIZER_DESC* desc)
{
	ID3D11RasterizerState* pDXState;

	if(FAILED(_pDevice->CreateRasterizerState(desc, &pDXState)))
	{
		QuitWithErrorMessage(std::string("Error creating Rasterizer State") + name + std::string("."));

		return AqFAIL;
	}

#if _DEBUG
	pDXState->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
#endif

	RasterizerState state = RasterizerState(pDXState, getStringID(name));

	RasterizerState* pState = getStackAllocator().allocateNew<RasterizerState>(state);

	_pPipelineStates.push_back(pState);

	return AqOK;
}