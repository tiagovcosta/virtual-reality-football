#ifndef DOMAINSHADER_H
#define DOMAINSHADER_H

#include "PCH.h"

#include "Shader.h"

namespace Aqua
{
	class DomainShader : public Shader
	{
	public:
		DomainShader(ID3D11DomainShader* pShader);
		~DomainShader();

		void destroy();

		ID3D11DomainShader* getShader() const;

	private:
		ID3D11DomainShader* _pShader;
	};
};

#endif