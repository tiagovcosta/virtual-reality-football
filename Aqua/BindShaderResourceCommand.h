#ifndef BINDSHADERRESOURCECOMMAND_H
#define BINDSHADERRESOURCECOMMAND_H

#include <D3DX11.h>

#include "AquaTypedefs.h"

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindShaderResourceCommand : public Command
	{
	public:
		BindShaderResourceCommand();
		~BindShaderResourceCommand();

		void execute(PipelineManager& pipelineManager);

		void setShaderResource(ID3D11ShaderResourceView* pShaderResource);
		void setSlot(uint slot);
		void setBindFlags(u8 bindFlags);

	private:
		ID3D11ShaderResourceView* _pShaderResource;
		uint                      _Slot;
		u8                        _BindFlags;
	};
};

#endif