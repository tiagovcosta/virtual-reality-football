#include "VertexShader.h"

using namespace Aqua;

VertexShader::VertexShader(ID3D11VertexShader* pShader, ID3D11InputLayout*  pInputLayout)
{
	_pShader      = pShader;
	_pInputLayout = pInputLayout;
	_Type         = VERTEX_SHADER;
}

VertexShader::~VertexShader()
{}

void VertexShader::destroy()
{
	//getLogger().Write(L"Shader: \"" + _FileName + L"\" destroyed.");
	SAFE_RELEASE(_pShader);
}

ID3D11VertexShader* VertexShader::getShader() const
{
	return _pShader;
}

ID3D11InputLayout* VertexShader::getInputLayout() const
{
	return _pInputLayout;
}