#include "ShaderConfig.h"

using namespace Aqua;

ShaderConfig::ShaderConfig()
{
	_Type                = UNKNOWN_SHADER;
	_Flags               = 0;
	_InputLayoutBitfield = 0;
}

ShaderConfig::~ShaderConfig()
{
	_Filename.erase();
	_Function.erase();
	_Profile.erase();
}

void ShaderConfig::setFilename(const std::string& filename)
{
	_Filename = filename;
}

void ShaderConfig::setShaderType(SHADER_TYPE type)
{
	_Type = type;
}

void ShaderConfig::setFunction(const std::string& function)
{
	_Function = function;
}

void ShaderConfig::setProfile(const std::string& profile)
{
	_Profile = profile;
}

void ShaderConfig::setFlags(uint flags)
{
	_Flags = flags;
}

void ShaderConfig::addDefine(std::string define)
{
	_pDefines.push_back(define);
}

void ShaderConfig::setInputLayoutBitfield(u8 bitfield)
{
	_InputLayoutBitfield = bitfield;
}