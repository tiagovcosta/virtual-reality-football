#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <D3D11.h>

#include "AquaTypedefs.h"

namespace Aqua
{
	class Viewport
	{
	public:
		Viewport();
		~Viewport();

		void setSize(float x, float y, float width, float height);

		D3D11_VIEWPORT getD3DViewport(uint renderTextureWidth, uint renderTextureHeight) const;

	private:
		float  _PosX;
		float  _PosY;
		float _Width;
		float _Height;
	};
};

#endif