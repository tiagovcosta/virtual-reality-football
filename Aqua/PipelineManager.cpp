#include "PipelineManager.h"

using namespace Aqua;

PipelineManager::PipelineManager()
{
	_pContext             = NULL;
	_pMainRenderTarget    = NULL;
}

PipelineManager::~PipelineManager()
{}

void PipelineManager::init(ID3D11DeviceContext* pContext)
{
	_pContext = pContext;

	_IAStage.init(pContext);
	_RSStage.init(pContext);
	_OMStage.init(pContext);

	_VSStage.init(pContext);
	_HSStage.init(pContext);
	_DSStage.init(pContext);
	_GSStage.init(pContext);
	_PSStage.init(pContext);
	_CSStage.init(pContext);
};

void PipelineManager::shutdown()
{
	_IAStage.shutdown();
	_RSStage.shutdown();
	_OMStage.shutdown();

	_VSStage.shutdown();
	_HSStage.shutdown();
	_DSStage.shutdown();
	_GSStage.shutdown();
	_PSStage.shutdown();
	_CSStage.shutdown();
};

InputAssemblerStage* PipelineManager::getIAStage()
{
	return &_IAStage;
}

RasterizerStage* PipelineManager::getRSStage()
{
	return &_RSStage;
}

OutputMergerStage* PipelineManager::getOMStage()
{
	return &_OMStage;
}

void PipelineManager::bindVertexShader(const VertexShader* pShader)
{
	_VSStage.setShader(pShader);
}

void PipelineManager::bindHullShader(const HullShader* pShader)
{
	_HSStage.setShader(pShader);
}

void PipelineManager::bindDomainShader(const DomainShader* pShader)
{
	_DSStage.setShader(pShader);
}

void PipelineManager::bindGeometryShader(const GeometryShader* pShader)
{
	_GSStage.setShader(pShader);
}

void PipelineManager::bindPixelShader(const PixelShader* pShader)
{
	_PSStage.setShader(pShader);
}

void PipelineManager::bindComputeShader(const ComputeShader* pShader)
{
	_CSStage.setShader(pShader);
}

void PipelineManager::bindSamplerState(uint bindFlags, ID3D11SamplerState* pSampler, uint slot)
{
	if( (bindFlags & VERTEX_STAGE) == VERTEX_STAGE)
		_VSStage.setSamplerState(pSampler, slot);

	if( (bindFlags & HULL_STAGE) == HULL_STAGE)
		_HSStage.setSamplerState(pSampler, slot);

	if( (bindFlags & DOMAIN_STAGE) == DOMAIN_STAGE)
		_DSStage.setSamplerState(pSampler, slot);

	if( (bindFlags & GEOMETRY_STAGE) == GEOMETRY_STAGE)
		_GSStage.setSamplerState(pSampler, slot);

	if( (bindFlags & PIXEL_STAGE) == PIXEL_STAGE)
		_PSStage.setSamplerState(pSampler, slot);

	if( (bindFlags & COMPUTE_STAGE) == COMPUTE_STAGE)
		_CSStage.setSamplerState(pSampler, slot);
}

void PipelineManager::bindConstantBuffer(uint bindFlags, ID3D11Buffer* pConstantBuffer, uint slot)
{
	if((bindFlags & VERTEX_STAGE) == VERTEX_STAGE)
		_VSStage.setConstantBuffer(pConstantBuffer, slot);

	if( (bindFlags & HULL_STAGE) == HULL_STAGE)
		_HSStage.setConstantBuffer(pConstantBuffer, slot);

	if( (bindFlags & DOMAIN_STAGE) == DOMAIN_STAGE)
		_DSStage.setConstantBuffer(pConstantBuffer, slot);

	if( (bindFlags & GEOMETRY_STAGE) == GEOMETRY_STAGE)
		_GSStage.setConstantBuffer(pConstantBuffer, slot);

	if( (bindFlags & PIXEL_STAGE) == PIXEL_STAGE)
		_PSStage.setConstantBuffer(pConstantBuffer, slot);

	if( (bindFlags & COMPUTE_STAGE) == COMPUTE_STAGE)
		_CSStage.setConstantBuffer(pConstantBuffer, slot);
}

void PipelineManager::bindShaderResourceView(uint bindFlags, ID3D11ShaderResourceView* pResource, uint slot)
{
	if( (bindFlags & VERTEX_STAGE) == VERTEX_STAGE)
		_VSStage.setShaderResourceView(pResource, slot);

	if( (bindFlags & HULL_STAGE) == HULL_STAGE)
		_HSStage.setShaderResourceView(pResource, slot);

	if( (bindFlags & DOMAIN_STAGE) == DOMAIN_STAGE)
		_DSStage.setShaderResourceView(pResource, slot);

	if( (bindFlags & GEOMETRY_STAGE) == GEOMETRY_STAGE)
		_GSStage.setShaderResourceView(pResource, slot);

	if( (bindFlags & PIXEL_STAGE) == PIXEL_STAGE)
		_PSStage.setShaderResourceView(pResource, slot);

	if( (bindFlags & COMPUTE_STAGE) == COMPUTE_STAGE)
		_CSStage.setShaderResourceView(pResource, slot);
}

void PipelineManager::updatePipeline()
{
	_IAStage.applyState();
	_RSStage.applyState();
	_OMStage.applyState();

	_VSStage.applyState();
	_HSStage.applyState();
	_DSStage.applyState();
	_GSStage.applyState();
	_PSStage.applyState();
	_CSStage.applyState();
}

void PipelineManager::draw(uint vertexCount, uint startVertex)
{
	updatePipeline();
	_pContext->Draw(vertexCount, startVertex);
}

void PipelineManager::drawIndexed(uint indexCount, uint startIndex, uint startVertex)
{
	updatePipeline();
	_pContext->DrawIndexed(indexCount, startIndex, startVertex);
}

void PipelineManager::setMainRenderTarget(ID3D11RenderTargetView* pMainRenderTarget)
{
	_pMainRenderTarget = pMainRenderTarget;
}

void PipelineManager::useMainRenderTarget()
{
	_OMStage.bindRenderTarget(_pMainRenderTarget, 0);
}

void PipelineManager::updateCBuffer(ID3D11Buffer* pBuffer, const void* pData, uint size)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;

	_pContext->Map(pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	
	memcpy(mappedResource.pData, pData, size);

	_pContext->Unmap(pBuffer, 0);
}

void PipelineManager::generateSRVMips(ID3D11ShaderResourceView* pShaderResource)
{
	_pContext->GenerateMips(pShaderResource);
}

void PipelineManager::clearRenderTarget(ID3D11RenderTargetView* pRenderTarget, float clear)
{
	float clearColor[4] = {clear, clear, clear, clear};
	_pContext->ClearRenderTargetView(pRenderTarget, clearColor);
}

void PipelineManager::clearDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilTarget, float depth)
{
	_pContext->ClearDepthStencilView(pDepthStencilTarget, D3D11_CLEAR_DEPTH, depth, 0);
}