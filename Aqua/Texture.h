#ifndef TEXTURE_H
#define TEXTURE_H

#include "PCH.h"

#include "Resource.h"

namespace Aqua
{
	class Texture : public Resource
	{
	public:
		Texture();
		~Texture();

		ID3D11ShaderResourceView* getShaderResource() const;

		void destroy();

	private:
		ID3D11ShaderResourceView* _pView;

		friend class ResourceManager;
	};
};

#endif