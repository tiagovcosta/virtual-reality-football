#ifndef ENTITY_H
#define ENTITY_H

#include "AquaTypedefs.h"

namespace Aqua
{
	enum ENTITY_TYPE
	{
		UNKNOWN_ENTITY,
		ACTOR,
		LIGHT,
		CAMERA
	};

	class Entity
	{
	public:
		Entity(ENTITY_TYPE type, uint name);
		~Entity();

		ENTITY_TYPE getType() const;

		uint        getName() const;

	private:
		ENTITY_TYPE _Type;
		uint        _Name;
	};
};

#endif