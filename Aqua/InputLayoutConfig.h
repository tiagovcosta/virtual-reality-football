#ifndef INPUTLAYOUTCONFIG_H
#define INPUTLAYOUTCONFIG_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2011 - 2012              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "PCH.h"
#include "Debug.h"

#define FORMAT DXGI_FORMAT

namespace Aqua
{
	struct InputLayoutElement
	{
		LPCSTR                     SemanticName;
		uint                       SemanticIndex;
		DXGI_FORMAT                Format;
		uint                       InputSlot;
		uint                       AlignedByteOffset;
		D3D11_INPUT_CLASSIFICATION InputSlotClass;
		uint                       InstanceDataStepRate;
	};

	class InputLayoutConfig
	{
	public:
		InputLayoutConfig();
		InputLayoutConfig(uint numElements);
		~InputLayoutConfig();

		/*void addPerVertexElement(LPCSTR name, uint semanticIndex, FORMAT format, uint inputSlot, uint alignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT);
		void addPerInstanceElement(LPCSTR name, uint semanticIndex, FORMAT format, uint inputSlot, uint instanceDataStepRate, uint alignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT);
		*/

		void addElement(D3D11_INPUT_ELEMENT_DESC element);
		void removeLastElement();

		uint getNumElements();

		D3D11_INPUT_ELEMENT_DESC* getInputLayoutElements();

	private:
		/*uint                      _NumElements;
		uint                      _NumElementsAdded;
		D3D11_INPUT_ELEMENT_DESC* _Config;*/
		std::vector<D3D11_INPUT_ELEMENT_DESC> _Elements;
	};
};

#endif