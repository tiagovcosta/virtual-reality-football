#include "BindShaderResourceCommand.h"

using namespace Aqua;

BindShaderResourceCommand::BindShaderResourceCommand()
{
	_pShaderResource = NULL;
	_Slot = 0;
	_BindFlags = 0;
}

BindShaderResourceCommand::~BindShaderResourceCommand()
{
	_pShaderResource = NULL;
	_Slot = 0;
	_BindFlags = 0;
}

void BindShaderResourceCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindShaderResourceView(_BindFlags, _pShaderResource, _Slot);
}

void BindShaderResourceCommand::setShaderResource(ID3D11ShaderResourceView* pShaderResource)
{
	_pShaderResource = pShaderResource;
}

void BindShaderResourceCommand::setSlot(uint slot)
{
	_Slot = slot;
}

void BindShaderResourceCommand::setBindFlags(u8 bindFlags)
{
	_BindFlags = bindFlags;
}