#ifndef DRAWINDEXEDCOMMAND_H
#define DRAWINDEXEDCOMMAND_H

#include "PipelineManager.h"

namespace Aqua
{
	class DrawIndexedCommand
	{
	public:
		DrawIndexedCommand();
		~DrawIndexedCommand();

		void execute(PipelineManager& pipelineManager);

		void setIndexCount(uint indexCount);
		void setStartIndex(uint startIndex);
		void setStartVertex(uint startVertex);

	private:
		uint _IndexCount;
		uint _StartIndex;
		uint _StartVertex;
	};
};

#endif