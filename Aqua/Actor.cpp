#include "Actor.h"

using namespace Aqua;

Actor::Actor(uint nameID) : Entity(ACTOR, nameID)
{
	_Model = 0;

	D3DXMatrixIdentity(&_WorldMatrix);
	D3DXMatrixIdentity(&_RotationMatrix);
	_UpdateWorldMatrix = true;

	_Scaling = Vector3D(1,1,1);
}

Actor::~Actor()
{

}

void Actor::setModel(uint modelID)
{
	_Model = modelID;
}

void Actor::setPosition(const Vector3D& position)
{
	_Position = position;
	_UpdateWorldMatrix = true;
}

void Actor::setRotation(float x, float y, float z)
{
	D3DXMatrixRotationYawPitchRoll(&_RotationMatrix, static_cast<float>(D3DXToRadian(x)), static_cast<float>(D3DXToRadian(y)), static_cast<float>(D3DXToRadian(z)));

	_UpdateWorldMatrix = true;
}

void Actor::setScaling(float x, float y, float z)
{
	_Scaling.x = x;
	_Scaling.y = y;
	_Scaling.z = z;
	_UpdateWorldMatrix = true;
}

uint Actor::getModel() const
{
	return _Model;
}

void Actor::setWorldMatrix(const D3DXMATRIX& matrix)
{
	_UpdateWorldMatrix = false;

	_WorldMatrix = matrix;

	//Scale
	_WorldMatrix._11 *= _Scaling.x;
	_WorldMatrix._12 *= _Scaling.x;
	_WorldMatrix._13 *= _Scaling.x;

	_WorldMatrix._21 *= _Scaling.y;
	_WorldMatrix._22 *= _Scaling.y;
	_WorldMatrix._23 *= _Scaling.y;

	_WorldMatrix._31 *= _Scaling.z;
	_WorldMatrix._32 *= _Scaling.z;
	_WorldMatrix._33 *= _Scaling.z;
}

const D3DXMATRIX& Actor::getWorldMatrix()
{
	if(_UpdateWorldMatrix)
		updateWorldMatrix();

	return _WorldMatrix;
}

void Actor::updateWorldMatrix()
{
	D3DXMatrixIdentity(&_WorldMatrix);

	//Scale
	_WorldMatrix._11 *= _Scaling.x;
	_WorldMatrix._12 *= _Scaling.x;
	_WorldMatrix._13 *= _Scaling.x;

	_WorldMatrix._21 *= _Scaling.y;
	_WorldMatrix._22 *= _Scaling.y;
	_WorldMatrix._23 *= _Scaling.y;

	_WorldMatrix._31 *= _Scaling.z;
	_WorldMatrix._32 *= _Scaling.z;
	_WorldMatrix._33 *= _Scaling.z;

	//Rotate
	D3DXMatrixMultiply(&_WorldMatrix, &_WorldMatrix, &_RotationMatrix);

	//Translate
	_WorldMatrix._41 = _Position.x;
	_WorldMatrix._42 = _Position.y;
	_WorldMatrix._43 = _Position.z;
}