#ifndef BINDRENDERTEXTURERTCOMMAND_H
#define BINDRENDERTEXTURERTCOMMAND_H

#include "Command.h"

#include "RenderTexture.h"

namespace Aqua
{
	struct BindRenderTextureRTCommand
	{
		RenderTexture* renderTexture;
		uint slot;
		float clearValue;
	};
};

#endif