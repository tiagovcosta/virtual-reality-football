#include "BufferConfig.h"

using namespace Aqua;

BufferConfig::BufferConfig()
{
	setDefaults();
}

BufferConfig::BufferConfig(const D3D11_BUFFER_DESC& state)
{
	_State = state;
}

BufferConfig::~BufferConfig()
{
}

void BufferConfig::setDefaults()
{
	// Set the state to the default configuration.  These are the D3D11 default
	// values as well.

	_State.ByteWidth = 1;
    _State.Usage = D3D11_USAGE_DEFAULT;
    _State.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    _State.CPUAccessFlags = 0;
    _State.MiscFlags = 0;
    _State.StructureByteStride = 0;
}

void BufferConfig::setDefaultConstantBuffer( uint size, bool dynamic, bool cpuAccess )
{
	// Create the settings for a constant buffer.  This includes setting the 
	// constant buffer flag, allowing the CPU write access, and a dynamic usage.
	// Additional flags may be set as needed.

	_State.ByteWidth = size;
    _State.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    _State.MiscFlags = 0;
    _State.StructureByteStride = 0;

	if ( dynamic )
	{
		if(!cpuAccess)
		{
			_State.Usage = D3D11_USAGE_DEFAULT;
			_State.CPUAccessFlags = 0;
		} else
		{
			_State.Usage = D3D11_USAGE_DYNAMIC;
			_State.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		}
	}
	else
	{
		_State.Usage = D3D11_USAGE_IMMUTABLE;
		_State.CPUAccessFlags = 0;
	}
}

void BufferConfig::setDefaultVertexBuffer( uint size, bool dynamic )
{
	// Create the settings for a vertex buffer.  This includes the setting the
	// vertex buffer flag, 

	_State.ByteWidth = size;
    _State.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    _State.MiscFlags = 0;
    _State.StructureByteStride = 0;

	if ( dynamic )
	{
		_State.Usage = D3D11_USAGE_DYNAMIC;
		_State.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		_State.Usage = D3D11_USAGE_IMMUTABLE;
		_State.CPUAccessFlags = 0;
	}
}

void BufferConfig::setDefaultIndexBuffer( uint size, bool dynamic )
{
	_State.ByteWidth = size;
    _State.BindFlags = D3D11_BIND_INDEX_BUFFER;
    _State.MiscFlags = 0;
    _State.StructureByteStride = 0;

	if ( dynamic )
	{
		_State.Usage = D3D11_USAGE_DYNAMIC;
		_State.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		_State.Usage = D3D11_USAGE_IMMUTABLE;
		_State.CPUAccessFlags = 0;
	}
}

void BufferConfig::setDefaultStructuredBuffer( uint size, uint structsize )
{
	_State.ByteWidth = size * structsize;
    _State.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
    _State.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
    _State.StructureByteStride = structsize;

	_State.Usage = D3D11_USAGE_DEFAULT;
	_State.CPUAccessFlags = 0;
}

void BufferConfig::setDefaultByteAddressBuffer( uint size )
{
	// Set the state to the default configuration.  These are the D3D11 default
	// values as well.

	// TODO: These parameters need to be updated to reflect the byte address buffer!
	_State.ByteWidth = size;
    _State.Usage = D3D11_USAGE_DEFAULT;
    _State.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
    _State.CPUAccessFlags = 0;
    _State.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;
    _State.StructureByteStride = 0;
}

void BufferConfig::setDefaultIndirectArgsBuffer( uint size )
{
	// Set the state to the default configuration.  These are the D3D11 default
	// values as well.

	_State.ByteWidth = size;
    _State.Usage = D3D11_USAGE_DEFAULT;
    _State.BindFlags = 0;
    _State.CPUAccessFlags = 0;
    _State.MiscFlags = D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS;
    _State.StructureByteStride = 0;
}

void BufferConfig::setDefaultStagingBuffer( uint size )
{
	// Set the state to the default configuration.  These are the D3D11 default
	// values as well.

	_State.ByteWidth = size;
    _State.Usage = D3D11_USAGE_STAGING;
    _State.BindFlags = 0;
    _State.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
    _State.MiscFlags = 0;
    _State.StructureByteStride = 0;
}

void BufferConfig::setByteWidth( uint state )
{
	_State.ByteWidth = state;
}

void BufferConfig::setUsage( D3D11_USAGE state )
{
	_State.Usage = state;
}

void BufferConfig::setBindFlags( uint state )
{
	_State.BindFlags = state;
}

void BufferConfig::setCPUAccessFlags( uint state )
{
	_State.CPUAccessFlags = state;
}

void BufferConfig::setMiscFlags( uint state )
{
	_State.MiscFlags = state;
}

void BufferConfig::setStructureByteStride( uint state )
{
	_State.StructureByteStride = state;
}

const D3D11_BUFFER_DESC& BufferConfig::getBufferDesc() const
{
	return _State;
}