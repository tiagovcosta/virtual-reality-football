#include "RenderTextureConfig.h"

using namespace Aqua;

RenderTextureConfig::RenderTextureConfig()
{
	_Default                        = false;

	_TextureDesc.Width              = 0;
	_TextureDesc.Height             = 0;
	_TextureDesc.MipLevels          = 1;
	_TextureDesc.ArraySize          = 1;
	_TextureDesc.Format             = static_cast<DXGI_FORMAT>(RT_UNKNOWN);
	_TextureDesc.SampleDesc.Count   = 1;
	_TextureDesc.SampleDesc.Quality = 0;
	_TextureDesc.Usage              = D3D11_USAGE_DEFAULT;
	_TextureDesc.BindFlags          = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	_TextureDesc.CPUAccessFlags     = 0; 
	_TextureDesc.MiscFlags          = 0;

	_Format                         = RT_UNKNOWN;
}

RenderTextureConfig::~RenderTextureConfig()
{}

void RenderTextureConfig::setDefaultRenderTexture(RENDER_TEXTURE_FORMAT format, uint width, uint height)
{
	_TextureDesc.Width     = width;
	_TextureDesc.Height    = height;
	_TextureDesc.Format    = static_cast<DXGI_FORMAT>(format);
	_TextureDesc.MipLevels = 1;

	_Default               = true;

	_Format                = format;
}

void RenderTextureConfig::setSize(uint width, uint height)
{
	_TextureDesc.Width  = width;
	_TextureDesc.Height = height;
}

void RenderTextureConfig::generateMips(bool generateMips)
{
	if(generateMips)
	{
		_TextureDesc.MipLevels = 0;
		_TextureDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
	} else
	{
		_TextureDesc.MipLevels = 1;
		_TextureDesc.MiscFlags = 0;
	}
}

const D3D11_TEXTURE2D_DESC* RenderTextureConfig::getTextureDesc() const
{
	return &_TextureDesc;
}

const D3D11_SHADER_RESOURCE_VIEW_DESC* RenderTextureConfig::getShaderResourceDesc() const
{
	if(_Default)
		return NULL;

	return &_ShaderResourceDesc;
}

const D3D11_RENDER_TARGET_VIEW_DESC* RenderTextureConfig::getRenderTargetDesc() const
{
	if(_Default)
		return NULL;

	return &_RenderTargetDesc;
}

const RENDER_TEXTURE_FORMAT RenderTextureConfig::getFormat() const
{
	return _Format;
}