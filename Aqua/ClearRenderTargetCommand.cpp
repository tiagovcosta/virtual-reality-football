#include "ClearRenderTargetCommand.h"

using namespace Aqua;

ClearRenderTargetCommand::ClearRenderTargetCommand()
{
	_pRenderTarget = NULL;
	_Value = 0.0f;
}

ClearRenderTargetCommand::~ClearRenderTargetCommand()
{
	_pRenderTarget = NULL;
	_Value = 0.0f;
}

void ClearRenderTargetCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.clearRenderTarget(_pRenderTarget, _Value);
}

void ClearRenderTargetCommand::setRenderTarget(ID3D11RenderTargetView* pRenderTarget)
{
	_pRenderTarget = pRenderTarget;
}

void ClearRenderTargetCommand::setValue(float value)
{
	_Value = value;
}