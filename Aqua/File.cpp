#include "File.h"

using namespace Aqua;

int File::open(const char* filename)
{
	_InputStream.open(filename, std::ios::in);

	if(_InputStream.good())
	{
		return 1;
	}

	return 0;
}

void File::close()
{
	_InputStream.close();

	for(uint i = 0; i < _Words.size(); i++)
		_Words.at(i).clear();

	_Words.clear();
}

const std::string& File::nextWord()
{
	char* c_str = new char[100];
	_InputStream >> c_str;

	std::string str;
	
	str.append(c_str);

	_Words.push_back(str);

	delete[] c_str;

	return _Words.back();
}

int File::nextInt()
{
	int n;

	_InputStream >> n;

	return n;
}

float File::nextFloat()
{
	float n;

	_InputStream >> n;

	return n;
}

uint File::nextUnsigned()
{
	uint n;

	_InputStream >> n;

	return n;
}