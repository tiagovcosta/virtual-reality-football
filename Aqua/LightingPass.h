#ifndef LIGHTING_PASS
#define LIGHTING_PASS

#include "RenderingPass.h"

namespace Aqua
{
	class LightingPass : public RenderingPass
	{
	public:
		LightingPass();
		~LightingPass();

		void init(uint width, uint height);

		CommandGroup* draw(Scene* pScene, Camera* pCamera);

	private:
		const RenderTexture* _pDiffuseMapRT;
		const RenderTexture* _pNormalMapRT;
		const RenderTexture* _pDepthMapRT;
		const RenderTexture* _pLightingMapRT;

		const Texture*       _pEnvMap;
	};
};

#endif