#ifndef BINDBLENDSTATECOMMAND_H
#define BINDBLENDSTATECOMMAND_H

#include "Command.h"

#include "BlendState.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindBlendStateCommand : public Command
	{
	public:
		BindBlendStateCommand();
		~BindBlendStateCommand();

		void execute(PipelineManager& pipelineManager);

		void setBlendState(const BlendState* pBlendState);

	private:
		const BlendState* _pBlendState;
	};
};

#endif