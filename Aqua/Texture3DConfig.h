#ifndef TEXTURE3DCONFIG_H
#define TEXTURE3DCONFIG_H

#include "PCH.h"

namespace Aqua
{
	class Texture3DConfig
	{
	public:
		Texture3DConfig();
		Texture3DConfig(const D3D11_TEXTURE3D_DESC& state);
		~Texture3DConfig();

		void setDefaults();

		void setWidth( uint state );
		void SetHeight( uint state );
		void SetDepth( uint state );
		void setMipLevels( uint state );
		void setFormat( DXGI_FORMAT state );
		void setUsage( D3D11_USAGE state ); 
		void setBindFlags( uint state );
		void setCPUAccessFlags( uint state );
		void setMiscFlags( uint state );

		const D3D11_TEXTURE3D_DESC& getTextureDesc() const;

	protected:
		D3D11_TEXTURE3D_DESC 		_State;
	
	};
};

#endif