#include "AqMath.h"

using namespace Aqua;

float Aqua::vector2DLength(const Vector2D& vec)
{
	return sqrt(vec.x*vec.x + vec.y*vec.y);
}

float Aqua::vector3DLength(const Vector3D& vec)
{
	return sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z);
}

float Aqua::vector4DLength(const Vector4D& vec)
{
	return sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z + vec.w*vec.w);
}

//Vector Normalization
Vector2D Aqua::vector2DNormalize(const Vector2D& vec)
{
	float length = vector2DLength(vec);

	return Vector2D(vec.x/length, vec.y/length);
}

Vector3D Aqua::vector3DNormalize(const Vector3D& vec)
{
	float length = vector3DLength(vec);

	return Vector3D(vec.x/length, vec.y/length, vec.z/length);
}

Vector4D Aqua::vector4DNormalize(const Vector4D& vec)
{
	float length = vector4DLength(vec);

	return Vector4D(vec.x/length, vec.y/length, vec.z/length, vec.w/length);
}