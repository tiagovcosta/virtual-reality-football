#include "BindIndexBufferCommand.h"

using namespace Aqua;

BindIndexBufferCommand::BindIndexBufferCommand()
{
	_pBuffer = NULL;
	_Format = UINT16_INDEX;
	_Offset = 0;
}

BindIndexBufferCommand::~BindIndexBufferCommand()
{
	_pBuffer = NULL;
	_Format = UINT16_INDEX;
	_Offset = 0;
}

void BindIndexBufferCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.getIAStage()->setIndexBuffer(_pBuffer, _Format, _Offset);
}

void BindIndexBufferCommand::setIndexBuffer(ID3D11Buffer* pBuffer)
{
	_pBuffer = pBuffer;
}

void BindIndexBufferCommand::setFormat(INDEX_BUFFER_FORMAT format)
{
	_Format = format;
}

void BindIndexBufferCommand::setOffset(uint offset)
{
	_Offset = offset;
}