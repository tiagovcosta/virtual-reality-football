#ifndef AQUA_H
#define AQUA_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2012              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "PCH.h"
#include "Timer.h"

#include <WindowsX.h>

#include "InputManager.h"
#include "Renderer.h"

namespace Aqua
{

	enum GAME_STATE
	{
		GAME_RUNNING,
		GAME_PAUSED
	};

	enum WINDOW_STATE
	{
		WINDOW_MINI,
		WINDOW_MAX
	};

	class AquaGame
	{
	public:
		AquaGame(LPCSTR gameTitle, HINSTANCE hInstance, int showCmd, int width, int height, int nSamples, int sampleQuality);
		virtual ~AquaGame();

		//Game loop
		int                        run();
		//Initialize the game
		virtual void               init();
		//Updates the game based on player input and physics
		virtual void               update(float dt);
		//Renders the scene
		void                       render();
		//Cleans COM
		virtual void               shutdown();

		HWND                       getWindow();
		HINSTANCE                  getInstance();
		
		//Returns the Window size
		uint                       getWindowHeight();
		uint                       getWindowWidth();

		//Returns if the game is paused or not
		bool                       isGamePaused();

		void                       setGameState(GAME_STATE gameState);

		void                       drawStats();

		//Engine Timer
		//Timer                      _Timer;

	private:
		HRESULT createWindow();

		//Game vars
		LPCSTR        _GameTitle;
		GAME_STATE    _GameState;

		//Window vars
		HWND         _HWnd;
		HINSTANCE    _HInst;
		int          _ShowCmd;
		int          _WndWidth;
		int          _WndHeight;
		WINDOW_STATE _WndState;

		std::string _FrameStats;

	private:
	};
};

extern Aqua::AquaGame* aqua;

#endif