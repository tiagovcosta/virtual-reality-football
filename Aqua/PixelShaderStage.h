#ifndef PIXELSHADERSTAGE_H
#define PIXELSHADERSTAGE_H

#include "ShaderStage.h"
#include "PixelShader.h"

namespace Aqua
{
	class PixelShaderStage : private ShaderStage
	{
	public:
		PixelShaderStage();
		~PixelShaderStage();

		void setShader(const PixelShader* pShader);

		void clearState();

		void applyState();
	private:
		ID3D11PixelShader* _pShader;

		friend class PipelineManager;
	};
};

#endif