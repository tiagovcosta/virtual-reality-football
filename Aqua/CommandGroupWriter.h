#ifndef COMMANDGROUPWRITER_H
#define COMMANDGROUPWRITER_H

#include "AquaTypedefs.h"

#include "StackAllocator.h"

#include "Commands.h"
#include "CommandGroup.h"

namespace Aqua
{
	class CommandGroupWriter
	{
	public:
		CommandGroupWriter();
		~CommandGroupWriter();

		void begin(StackAllocator* pAllocator);

		void addDrawCommand(uint vertexCount, uint startVertex);
		void addDrawIndexedCommand(uint indexCount, uint startIndex, uint startVertex);

		void addBindVertexBufferCommand(ID3D11Buffer* pBuffer, uint stride, uint offset, u8 slot);
		void addBindIndexBufferCommand(ID3D11Buffer* pBuffer, INDEX_BUFFER_FORMAT format, uint offset);

		void addBindConstantBuffeCommand(ID3D11Buffer* pCBuffer, u8 slot, u8 bindFlags);
		void addBindTextureCommand(const Texture* pTexture, u8 slot, u8 bindFlags);

		void addBindVertexShaderCommand(VertexShader* pShader);
		void addBindHullShaderCommand(HullShader* pShader);
		void addBindDomainShaderCommand(DomainShader* pShader);
		void addBindGeometryShaderCommand(GeometryShader* pShader);
		void addBindPixelShaderCommand(PixelShader* pShader);
		void addBindComputeShaderCommand(ComputeShader* pShader);

		void addBindSamplerStateCommand(ID3D11SamplerState* pState, u8 bindFlags, u8 slot);
		void addBindBlendStateCommand(const BlendState* pState);
		void addBindDepthStencilStateCommand(const DepthStencilState* pState);
		void addBindRasterizerStateCommand(const RasterizerState* pState);

		void addBindViewportCommand(const Viewport* pViewport, uint targetWidth, uint targetHeight);

		void addBindRenderTargetCommand(ID3D11RenderTargetView* pTarget, u8 slot);
		void addBindDepthStencilTargetCommand(ID3D11DepthStencilView* pTarget);
		void addClearRenderTargetCommand(ID3D11RenderTargetView* pTarget, float value);
		void addClearDepthStencilTargetCommand(ID3D11DepthStencilView* pTarget, float value);

		void addUpdateCBufferCommand(ID3D11Buffer* pCBuffer, const void* pData, uint size);

		void addDrawFullScreenCommand(const PixelShader* pShader);
		void addDrawSceneEntitiesCommand(Camera* pCamera, ENTITY_TYPE entitiesType, SORT_ORDER sortOrder, uint context);

		void addBindMainRenderTargetCommand();
		void addDispatchCommandsCommand(const CommandGroup* pCmdGroup);

		CommandGroup* end();

	private:
		StackAllocator* _pAllocator;

		CommandGroup*   _pCmdGroup;

		uint            _NumCommands;
		Blob            _Blob;
	};
};

#endif