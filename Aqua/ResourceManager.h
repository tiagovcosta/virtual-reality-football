#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <vector>

#include "PCH.h"
#include "Utilities.h"
#include "File.h"

#include "ShaderManager.h"

#include "ConstantBuffer.h"
#include "Effect.h"
#include "Material.h"
#include "Geometry.h"
#include "Model.h"
#include "Texture.h"
#include "RenderTexture.h"
#include "DepthStencilTexture.h"

#include "BufferConfig.h"
#include "Texture1DConfig.h"
#include "Texture2DConfig.h"
#include "Texture3DConfig.h"

#include "RenderTextureConfig.h"
#include "DepthStencilTextureConfig.h"
#include "GeometryConfig.h"

#include "VertexLayouts.h"

namespace Aqua
{
	class ResourceManager
	{
	public:
		ResourceManager();
		~ResourceManager();

		void                 init(ID3D11Device* pDevice);
		void                 shutdown();

		uint                 loadResourcePackage(const std::string& filename);

		const Resource*      getResource(uint id);

		uint                 getResourceID(RESOURCE_TYPE type, const char* name);
		const Resource*      getResource(RESOURCE_TYPE type, const char* filename);

		const ConstantBuffer*      createConstantBuffer(uint size, const char* name);
		const RenderTexture*       createRenderTexture(const char* name, const RenderTextureConfig& config);
		const DepthStencilTexture* createDepthStencilTexture(const char* name, const DepthStencilTextureConfig& config);
		const Effect*              createEffect(const char* name, const EffectDesc& desc);
		const Geometry*            createGeometry(const char* name, const GeometryConfig& config, uint numSubsets, const GeometrySubset* pSubsets);
		const Model*               createModel(const char* name, const ModelDesc& desc);

	private:
		//Functions used to load
		uint                   loadTexture(const char* filename);
		uint                   loadEffect(const char* filename);
		uint                   loadMaterial(const char* filename);
		uint                   loadGeometry(const char* filename);
		uint                   loadModel(const char* filename);

		ID3D11Device*          _pDevice;

		std::vector<Resource*> _pResources;
	};
};

Aqua::ResourceManager& getResourceManager();

#endif