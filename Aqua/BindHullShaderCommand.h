#ifndef BINDHULLSHADERCOMMAND_H
#define BINDHULLSHADERCOMMAND_H

#include "Command.h"

#include "HullShader.h"

#include "PipelineManager.h"

typedef uint8_t u8;

namespace Aqua
{
	class BindHullShaderCommand : public Command
	{
	public:
		BindHullShaderCommand();
		~BindHullShaderCommand();

		void execute(PipelineManager& pipelineManager);

		void setShader(HullShader* pShader);

	private:
		HullShader* _pShader;
	};
};

#endif