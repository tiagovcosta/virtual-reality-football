#ifndef STRINGIDMANAGER_H
#define STRINGIDMANAGER_H

#include <stdint.h>
#include <map>

#include "Logger.h"

#define TABLE_SIZE 1000

namespace Aqua
{
	//Returns an *unique* hash for that string used to compare strings faster
	unsigned int getStringID(const char* string);

	//Returns the string associated with an hash
	const char* getString(unsigned int id);
};

#endif