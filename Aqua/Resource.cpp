#include "Resource.h"

using namespace Aqua;

Resource::Resource()
{
	_Type = UNKNOWN_RESOURCE;
	_RefCount = 0;
}

Resource::~Resource()
{}

RESOURCE_TYPE Resource::getType()
{
	return _Type;
}

int Resource::getRefCount()
{
	return _RefCount;
}

uint Resource::getNameID()
{
	return _NameID;
}

void Resource::setNameID(uint id)
{
	_NameID = id;
}

void Resource::increaseRefCount()
{
	_RefCount++;
}

void Resource::decreaseRefCount()
{
	_RefCount--;
}