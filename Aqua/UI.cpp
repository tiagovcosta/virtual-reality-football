#include "UI.h"

using namespace Aqua;

UI& getUI()
{
	static UI ui;

	return ui;
}

UI::UI()
{}

UI::~UI()
{}

void UI::init(ID3D11Device* pDevice, uint width, uint height)
{
	TwInit(TW_DIRECT3D11, pDevice);
	TwWindowSize(width, height);

	_MainBar = TwNewBar("Info");
	_DebugBar = TwNewBar("Debug");
	TwAddVarRO(_DebugBar, "FPS:", TW_TYPE_UINT32, &_FPS, "");
	TwAddVarRO(_DebugBar, "MSPF:", TW_TYPE_FLOAT, &_MSPF, "");

	TwDefine(" Info refresh=0.1 ");
}

void UI::addUInt(const char* name, uint* value, const char* config)
{
	TwAddVarRO(_MainBar, name, TW_TYPE_UINT32, value, config);
}

void UI::addString(const char* name, const char* value, const char* config)
{
	TwAddVarRO(_MainBar, name, TW_TYPE_CSSTRING(sizeof(value)), value, config);
}

void UI::addCheckBox(const char* name, bool* value, const char* config)
{
	TwAddVarRW(_DebugBar, name, TW_TYPE_BOOL8, value, config);
}

void UI::addSlider(const char* name, float* value, const char* config)
{
	TwAddVarRW(_DebugBar, name, TW_TYPE_FLOAT, value, config);
}

void UI::addDropList(const char* name, void* value, TwEnumVal options[], int numOptions, const char* config)
{
	TwType type = TwDefineEnum(name, options, numOptions);
	TwAddVarRW(_DebugBar, name, type, value, config);
}

void UI::draw(int fps)
{
	_FPS = fps;
	_MSPF = (float)(1000.0f/(float)fps);
	TwDraw();
}

void UI::shutdown()
{
	TwTerminate();
}