#include "GBufferPass.h"

using namespace Aqua;

GBufferPass::GBufferPass()
{
	_pCommandGroup        = NULL;
	_pDepthStencilTexture = NULL;
	_pDiffuseMapRT        = NULL;
	_pNormalMapRT         = NULL;
	_pDepthMapRT          = NULL;
}

GBufferPass::~GBufferPass()
{
	_pCommandGroup        = NULL;
	_pDepthStencilTexture = NULL;
	_pDiffuseMapRT        = NULL;
	_pNormalMapRT         = NULL;
	_pDepthMapRT          = NULL;
}

void GBufferPass::init(uint width, uint height)
{
	DepthStencilTextureConfig dstConfig;
	dstConfig.setDefaultDepthStencilTexture(width, height);

	_pDepthStencilTexture = getResourceManager().createDepthStencilTexture("mainDepthStencilTexture", dstConfig);

	RenderTextureConfig rtConfig;

	//Create DiffuseMap
	rtConfig.setDefaultRenderTexture(RT_RGBA16, width, height);
	_pDiffuseMapRT = getResourceManager().createRenderTexture("diffuseMap", rtConfig);

	//Create NormalMap
	rtConfig.setDefaultRenderTexture(RT_RGBA16, width, height);
	_pNormalMapRT = getResourceManager().createRenderTexture("normalMap", rtConfig);

	//Create DepthMap
	rtConfig.setDefaultRenderTexture(RT_R32, width, height);
	_pDepthMapRT = getResourceManager().createRenderTexture("depthMap", rtConfig);

	CommandGroupWriter cmdWriter;
	cmdWriter.begin(&getStackAllocator());

	cmdWriter.addClearDepthStencilTargetCommand(_pDepthStencilTexture->getDepthStencilTarget(), 1.0f);
	cmdWriter.addBindDepthStencilTargetCommand(_pDepthStencilTexture->getDepthStencilTarget());

	cmdWriter.addClearRenderTargetCommand(_pNormalMapRT->getRenderTarget(), 0.5f);
	cmdWriter.addClearRenderTargetCommand(_pDepthMapRT->getRenderTarget(), 1.0f);

	cmdWriter.addBindRenderTargetCommand(_pDiffuseMapRT->getRenderTarget(), 0);
	cmdWriter.addBindRenderTargetCommand(_pNormalMapRT->getRenderTarget(), 1);
	cmdWriter.addBindRenderTargetCommand(_pDepthMapRT->getRenderTarget(), 2);

	cmdWriter.addBindRasterizerStateCommand(NULL);
	cmdWriter.addBindDepthStencilStateCommand(NULL);
	cmdWriter.addBindBlendStateCommand(NULL);

	cmdWriter.addDrawSceneEntitiesCommand(NULL, ACTOR, FRONT_TO_BACK, getStringID("GBuffer"));

	cmdWriter.addBindRenderTargetCommand(NULL, 0);
	cmdWriter.addBindRenderTargetCommand(NULL, 1);
	cmdWriter.addBindRenderTargetCommand(NULL, 2);

	_pCommandGroup = cmdWriter.end();
}

CommandGroup* GBufferPass::draw(Scene* pScene, Camera* pCamera)
{
	return _pCommandGroup;
}