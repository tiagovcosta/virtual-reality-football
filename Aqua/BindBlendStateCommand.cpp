#include "BindRasterizerStateCommand.h"

using namespace Aqua;

BindRasterizerStateCommand::BindRasterizerStateCommand()
{
	_pRasterizerState = NULL;
}

BindRasterizerStateCommand::~BindRasterizerStateCommand()
{
	_pRasterizerState = NULL;
}

void BindRasterizerStateCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.getRSStage()->setState(_pRasterizerState->getState());
}

void BindRasterizerStateCommand::setRasterizerState(const RasterizerState* pRasterizerState)
{
	_pRasterizerState = pRasterizerState;
}