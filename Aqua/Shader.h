#ifndef SHADER_H
#define SHADER_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2011 - 2012              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "PCH.h"
#include "RendererEnums.h"

namespace Aqua
{
	class Shader
	{
	public:
		Shader();
		virtual ~Shader();

		uint         getNameID() const;
		u32          getPermutation() const;
		SHADER_TYPE  getType() const;

		virtual void destroy() = 0;

		//TODO
		void         printShaderDetails();

	protected:
		void         setShaderNameID(uint id);
		void         setPermutation(u32 permutation);

		void         setShaderBlob(ID3DBlob* pCompiledShader);

		uint         _NameID;
		u32          _Permutation;
		ID3DBlob*    _pCompiledShader;

		SHADER_TYPE  _Type;

		friend class ShaderManager;
	};
};

#endif