#ifndef CSMGENERATOR_H
#define CSMGENERATOR_H

#include "Scene.h"
#include "Camera.h"
#include "Viewport.h"

#include "PipelineStateManager.h"
#include "ShaderManager.h"

#include "ResourceManager.h"
#include "RenderTexture.h"
#include "DepthStencilTexture.h"

#define SHADOW_MAP_SIZE 2048
#define NUM_CASCADES 4

namespace Aqua
{
	struct CascadeGatherCBData
	{
		D3DXMATRIX cascadeMatrices[4];
		float      cascadesStart[4];
	};

	class CSMGenerator
	{
	public:
		CSMGenerator();
		~CSMGenerator();

		void init();

		CommandGroup* execute(const Scene* pScene, const Camera* pCamera, const D3DXVECTOR3& lightDir, const Texture** pOutTexture);
	private:
		Camera generateCascadeCamera(const D3DXVECTOR3& lightDir, const Camera* pCamera, float start, float end);
		void calculateCascadeCorners(const Camera* pCamera, float cascadeBegin, float cascadeEnd, D3DXVECTOR3* corners);

		D3DXVECTOR3 vectorMin(const D3DXVECTOR3& v1, const D3DXVECTOR3& v2);
		D3DXVECTOR3 vectorMax(const D3DXVECTOR3& v1, const D3DXVECTOR3& v2);

		Viewport                   _Viewports[NUM_CASCADES];
		const DepthStencilTexture* _pCascadesDST;

		const ConstantBuffer*      _pCascadeGatherCB;
		CascadeGatherCBData        _CascadeGatherCBData;

		Viewport                   _GatherViewport;
		const RenderTexture*       _pDeferredShadowMapRT;
		const Effect*              _pDeferredShadowMapEffect;

		const RenderTexture*       _pDepthMapRT;
		
		Camera                     _ShadowCameras[NUM_CASCADES];

		CommandGroup*              _pCommandGroup;
	};
};

#endif