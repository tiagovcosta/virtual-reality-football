#ifndef RESOURCE_H
#define RESOURCE_H

#define SAFE_RELEASE( x ) {if(x){(x)->Release();(x)=NULL;}}

#include "AquaTypedefs.h"

namespace Aqua
{
	enum RESOURCE_TYPE
	{
		UNKNOWN_RESOURCE,
		CBUFFER,
		TEXTURE,
		RENDER_TEXTURE,
		DEPTH_STENCIL_TEXTURE,
		EFFECT,
		MATERIAL,
		GEOMETRY,
		MODEL
	};

	class Resource
	{
	public:
		Resource();
		~Resource();

		RESOURCE_TYPE getType();

		int           getRefCount();

		uint          getNameID();

		void          setNameID(uint id);

		void          increaseRefCount();
		void          decreaseRefCount();

		virtual void destroy() = 0;

	protected:
		RESOURCE_TYPE _Type;

	private:
		uint          _NameID;
		int           _RefCount;
	};
};

#endif