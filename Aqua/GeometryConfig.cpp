#include "GeometryConfig.h"

using namespace Aqua;

GeometryConfig::GeometryConfig()
{}

GeometryConfig::~GeometryConfig()
{}

void GeometryConfig::setVertexBufferData(void* pData, uint size, uint vertexBufferStride)
{
	_VBData.pSysMem = pData;
	_VBConfig.setDefaultVertexBuffer(size, false);
	_VertexBufferStride = vertexBufferStride;
}

void GeometryConfig::setIndexBufferData(void* pData, uint size, INDEX_BUFFER_FORMAT indexFormat)
{
	_IBData.pSysMem = pData;
	_IBConfig.setDefaultIndexBuffer(size, false);
	_IndexFormat = indexFormat;
}

const BufferConfig& GeometryConfig::getVertexBufferConfig() const
{
	return _VBConfig;
}

const BufferConfig& GeometryConfig::getIndexBufferConfig() const
{
	return _IBConfig;
}

uint GeometryConfig::getVertexBufferStride() const
{
	return _VertexBufferStride;
}

INDEX_BUFFER_FORMAT GeometryConfig::getIndexFormat() const
{
	return _IndexFormat;
}

const D3D11_SUBRESOURCE_DATA& GeometryConfig::getVertexBufferData() const
{
	return _VBData;
}

const D3D11_SUBRESOURCE_DATA& GeometryConfig::getIndexBufferData() const
{
	return _IBData;
}