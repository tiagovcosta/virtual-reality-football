#ifndef OUTPUTMERGERSTAGE_H
#define OUTPUTMERGERSTAGE_H

#include "PCH.h"

namespace Aqua
{
	//LOODUS OUTPUT MERGER CURRENTLY DOESNT SUPPORT UAV BINDING
	class OutputMergerStage
	{
	public:
		OutputMergerStage();
		~OutputMergerStage();

		void init(ID3D11DeviceContext* pContext);
		void shutdown();

		//TODO: Fix update need check
		void bindRenderTarget(ID3D11RenderTargetView* pRenderTarget, uint index);
		void bindDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilView);
		//void SetUnorderedAccessView(Resource* pResource, uint index, unsigned int initial = -1 );

		void setBlendState(ID3D11BlendState* pState);
		void setDepthStencilState(ID3D11DepthStencilState* pState);

		void clearState();

		void applyState();

		void reApplyState();

	private:
		ID3D11DeviceContext*    _pContext;

		bool                    _UpdateRenderTargets;
		ID3D11RenderTargetView* _pRenderTargets[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT];
		ID3D11DepthStencilView* _pDepthStencilTarget;
		//ID3D11UnorderedAccessView*		UnorderedAccessViews[D3D11_PS_CS_UAV_REGISTER_COUNT];
		//unsigned int					UAVInitialCounts[D3D11_PS_CS_UAV_REGISTER_COUNT];

		bool                     _UpdateDSState;
		ID3D11DepthStencilState* _pDSState;

		bool                     _UpdateBState;
		ID3D11BlendState*        _pBState;

		friend class PipelineManager;
	};
};

#endif