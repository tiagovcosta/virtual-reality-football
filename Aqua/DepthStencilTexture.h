#ifndef DEPTHSTENCILTEXTURE_H
#define DEPTHSTENCILTEXTURE_H

#include "Resource.h"

#include "Texture.h"

#include <D3D11.h>

namespace Aqua
{
	class DepthStencilTexture : public Resource
	{
	public:
		DepthStencilTexture();
		~DepthStencilTexture();

		ID3D11DepthStencilView* getDepthStencilTarget() const;
		const Texture*            getTexture() const;

		void destroy();
	private:
		ID3D11DepthStencilView* _pDSTarget;
		Texture                 _Texture;
		
		friend class ResourceManager;
	};
};

#endif