#ifndef GEOMETRYSHADERSTAGE_H
#define GEOMETRYSHADERSTAGE_H

#include "ShaderStage.h"
#include "GeometryShader.h"

namespace Aqua
{
	class GeometryShaderStage : private ShaderStage
	{
	public:
		GeometryShaderStage();
		~GeometryShaderStage();

		void setShader(const GeometryShader* pShader);

		void clearState();

		void applyState();
	private:
		ID3D11GeometryShader* _pShader;

		friend class PipelineManager;
	};
};

#endif