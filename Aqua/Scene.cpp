#include "Scene.h"

using namespace Aqua;

Scene::Scene()
{
	_pActors.reserve(250);
	_pDirLights.reserve(10);
	_pPointLights.reserve(250);
}

Scene::~Scene()
{}

void Scene::init()
{

}

void Scene::shutdown()
{
	for(uint i = 0; i < _pActors.size(); i++)
		getStackAllocator().deleteObject(_pActors.at(i));

	_pActors.clear();

	for(uint i = 0; i < _pDirLights.size(); i++)
		getStackAllocator().deleteObject(_pDirLights.at(i));

	_pDirLights.clear();

	for(uint i = 0; i < _pPointLights.size(); i++)
		getStackAllocator().deleteObject(_pPointLights.at(i));

	_pPointLights.clear();
}

void Scene::update(float dt)
{

}

int Scene::loadScene(const std::string& filename)
{
	tinyxml2::XMLDocument doc;
	
	XMLElement* pElement     = NULL;

	const char* elementName;

	for(uint i = 0; i < 30; i++)
	{
		for(uint j = 0; j < 10; j++)
		{
			PointLight* pLight = createPointLight("");

			D3DXVECTOR3 vec;

			vec = D3DXVECTOR3(i*100 - 2700.0f, 50, j*100);
			pLight->positionRange = D3DXVECTOR4(vec, 150);

			vec = D3DXVECTOR3(i%3, (i+j)%3, j%3);
			pLight->color = vec;
		}
	}

	//Add the folder path to the filename
	std::stringstream filePath;
	filePath << "../scenes/";
	filePath << filename;
	doc.LoadFile(filePath.str().c_str());

	if(doc.Error())
	{
		return AqFAIL;
	}

	pElement = doc.FirstChildElement("Scene");

	if(pElement == NULL)
	{
		return AqFAIL;
	}

	pElement->QueryFloatAttribute("ambientLightColorR", &_AmbientLightColor.x);
	pElement->QueryFloatAttribute("ambientLightColorG", &_AmbientLightColor.y);
	pElement->QueryFloatAttribute("ambientLightColorB", &_AmbientLightColor.z);

	pElement = pElement->FirstChildElement();

	for(pElement; pElement; pElement = pElement->NextSiblingElement())
	{
		elementName = pElement->Value();

		if(strcmp(elementName, "Actor") == 0)
		{
			const char* name = pElement->Attribute("name");

			Actor* pActor = createActor(name);

			pActor->setModel(getResourceManager().getResourceID(MODEL, pElement->Attribute("model")));

			Vector3D vector;
			vector = Vector3D(pElement->FloatAttribute("posX"),
										 pElement->FloatAttribute("posY"),
										 pElement->FloatAttribute("posZ"));
			pActor->setPosition(vector);

			vector = Vector3D(pElement->FloatAttribute("scaleX"),
									  pElement->FloatAttribute("scaleY"),
									  pElement->FloatAttribute("scaleZ"));
			pActor->setScaling(vector.x, vector.y, vector.z);

			vector = Vector3D(pElement->FloatAttribute("rotateX"),
									  pElement->FloatAttribute("rotateY"),
									  pElement->FloatAttribute("rotateZ"));
			pActor->setRotation(vector.x, vector.y, vector.z);

		} else if(strcmp(elementName, "DirLight") == 0)
		{
			const char* name = pElement->Attribute("name");

			DirLight* pLight = createDirLight(name);

			D3DXVECTOR3 vec;

			vec = D3DXVECTOR3(pElement->FloatAttribute("dirX"), pElement->FloatAttribute("dirY"), pElement->FloatAttribute("dirZ"));
			pLight->dir = vec;

			vec = D3DXVECTOR3(pElement->FloatAttribute("colorR"), pElement->FloatAttribute("colorG"), pElement->FloatAttribute("colorB"));
			pLight->color = vec;

			if(strcmp(name, "sun") == 0)
				_pSun = pLight;

		} else if(strcmp(elementName, "PointLight") == 0)
		{
			const char* name = pElement->Attribute("name");

			PointLight* pLight = createPointLight(name);

			D3DXVECTOR3 vec;

			vec = D3DXVECTOR3(pElement->FloatAttribute("posX"), pElement->FloatAttribute("posY"), pElement->FloatAttribute("posZ"));
			pLight->positionRange = D3DXVECTOR4(vec, pElement->FloatAttribute("range"));

			vec = D3DXVECTOR3(pElement->FloatAttribute("colorR"), pElement->FloatAttribute("colorG"), pElement->FloatAttribute("colorB"));
			pLight->color = vec;
		}
	}

	return AqOK;
}

uint Scene::getNumActors() const
{
	return _pActors.size();
}

Actor* Scene::getActor(uint index) const
{
	return _pActors.at(index);
}

Actor* const * Scene::getActors() const
{
	if(_pActors.empty())
		return NULL;

	return &_pActors.at(0);
}

uint Scene::getNumDirLights() const
{
	return _pDirLights.size();
}

DirLight* Scene::getDirLight(uint index) const
{
	if(_pDirLights.size() <= index)
		return NULL;

	return _pDirLights.at(index);
}

DirLight* const * Scene::getDirLights() const
{
	if(_pDirLights.size() == 0)
		return NULL;

	return &_pDirLights.at(0);
}

uint Scene::getNumPointLights() const
{
	return _pPointLights.size();
}

PointLight* Scene::getPointLight(uint index) const
{
	if(_pPointLights.empty())
		return NULL;

	return _pPointLights.at(index);
}

PointLight* const * Scene::getPointLights() const
{
	if(_pPointLights.size() == 0)
		return NULL;

	return &_pPointLights.at(0);
}

const Vector4D& Scene::getAmbientLightColor() const
{
	return _AmbientLightColor;
}

Actor* Scene::createActor(const char* name)
{
	Actor* pActor = getStackAllocator().allocateNew<Actor>(Actor(getStringID(name)));

	_pActors.push_back(pActor);

	return pActor;
}

DirLight* Scene::createDirLight(const char* name)
{
	DirLight light;

	DirLight* pLight = getStackAllocator().allocateNew<DirLight>(light);

	_pDirLights.push_back(pLight);

	return pLight;
}

PointLight* Scene::createPointLight(const char* name)
{
	PointLight light;

	PointLight* pLight = getStackAllocator().allocateNew<PointLight>(light);

	_pPointLights.push_back(pLight);

	return pLight;
}

void Scene::setAmbientLightColor(float r, float g, float b, float a)
{
	_AmbientLightColor = Vector4D(r, g, b, a);
}

NodeHandle Scene::getNode(const char* name) const
{
	uint nameID = getStringID(name);

	for(uint i = 0; i < _NumNodes; i++)
		if(_NodesName[i] = nameID)
			return i;
}

void Scene::scaleNode(NodeHandle node, float x, float y, float z)
{
	_pNodesLocalTransformData[node].scaling.x += x;
	_pNodesLocalTransformData[node].scaling.y += y;
	_pNodesLocalTransformData[node].scaling.z += z;

	_pNodesDirty[node] = true;
}

void Scene::rotateNode(NodeHandle node, float x, float y, float z)
{
	D3DXMatrixRotationYawPitchRoll(&_pNodesLocalTransformData[node].rotation, static_cast<float>(D3DXToRadian(x)), static_cast<float>(D3DXToRadian(y)), static_cast<float>(D3DXToRadian(z)));

	_pNodesDirty[node] = true;
}

void Scene::translateNode(NodeHandle node, float x, float y, float z)
{
	_pNodesLocalTransformData[node].position.x += x;
	_pNodesLocalTransformData[node].position.y += y;
	_pNodesLocalTransformData[node].position.z += z;

	_pNodesDirty[node] = true;
}

void Scene::setNodeScale(NodeHandle node, float x, float y, float z)
{
	_pNodesLocalTransformData[node].scaling.x = x;
	_pNodesLocalTransformData[node].scaling.y = y;
	_pNodesLocalTransformData[node].scaling.z = z;

	_pNodesDirty[node] = true;
}

void Scene::setnodeRotation(NodeHandle node, float x, float y, float z)
{
	D3DXMatrixRotationYawPitchRoll(&_pNodesLocalTransformData[node].rotation, static_cast<float>(D3DXToRadian(x)), static_cast<float>(D3DXToRadian(y)), static_cast<float>(D3DXToRadian(z)));

	_pNodesDirty[node] = true;
}

void Scene::setNodeTranslation(NodeHandle node, float x, float y, float z)
{
	_pNodesLocalTransformData[node].position.x = x;
	_pNodesLocalTransformData[node].position.y = y;
	_pNodesLocalTransformData[node].position.z = z;

	_pNodesDirty[node] = true;
}

void Scene::updateSceneGraph()
{
	for(uint i = 0; i < _NumNodes; i++)
	{
		if(_pNodesDirty[i] == true)
		{
			D3DXMatrixIdentity(&_pNodesLocalTransform[i]);

			//Scale
			_pNodesLocalTransform[i]._11 *= _pNodesLocalTransformData[i].scaling.x;
			_pNodesLocalTransform[i]._12 *= _pNodesLocalTransformData[i].scaling.x;
			_pNodesLocalTransform[i]._13 *= _pNodesLocalTransformData[i].scaling.x;

			_pNodesLocalTransform[i]._21 *= _pNodesLocalTransformData[i].scaling.y;
			_pNodesLocalTransform[i]._22 *= _pNodesLocalTransformData[i].scaling.y;
			_pNodesLocalTransform[i]._23 *= _pNodesLocalTransformData[i].scaling.y;

			_pNodesLocalTransform[i]._31 *= _pNodesLocalTransformData[i].scaling.z;
			_pNodesLocalTransform[i]._32 *= _pNodesLocalTransformData[i].scaling.z;
			_pNodesLocalTransform[i]._33 *= _pNodesLocalTransformData[i].scaling.z;

			//Rotate
			D3DXMatrixMultiply(&_pNodesLocalTransform[i], &_pNodesLocalTransform[i], &_pNodesLocalTransformData[i].rotation);

			//Translate
			_pNodesLocalTransform[i]._41 = _pNodesLocalTransformData[i].position.x;
			_pNodesLocalTransform[i]._42 = _pNodesLocalTransformData[i].position.y;
			_pNodesLocalTransform[i]._43 = _pNodesLocalTransformData[i].position.z;
		}

		if(_pNodesParent[i] == 0  && _pNodesDirty[i] == true)
			_pNodesWorldTransform[i] = _pNodesLocalTransform[i];
		else if(_pNodesDirty[i] == true || _pNodesDirty[_pNodesParent[i]] == true)
		{
			D3DXMatrixMultiply(&_pNodesWorldTransform[i], &_pNodesLocalTransform[i], &_pNodesWorldTransform[_pNodesParent[i]]);
		}
	}

	for(uint i = 0; i < _NumNodes; i++)
		_pNodesDirty[i] = false;

}

DirLight* Scene::getSun()
{
	return _pSun;
}