#ifndef DEPTHSTENCILTEXTURECONFIG_H
#define DEPTHSTENCILTEXTURECONFIG_H

#include "PCH.h"

namespace Aqua
{
	class DepthStencilTextureConfig
	{
	public:
		DepthStencilTextureConfig();
		~DepthStencilTextureConfig();

		void setDefaultDepthStencilTexture(uint width, uint height);
		void setDefaultShadowMap(uint width, uint height);

		const D3D11_TEXTURE2D_DESC*            getTextureDesc() const;
		const D3D11_SHADER_RESOURCE_VIEW_DESC* getShaderResourceDesc() const;
		const D3D11_DEPTH_STENCIL_VIEW_DESC*   getDepthStencilDesc() const;
	private:
		bool                            _Default;
		D3D11_TEXTURE2D_DESC            _TextureDesc;
		D3D11_SHADER_RESOURCE_VIEW_DESC _ShaderResourceDesc;
		D3D11_DEPTH_STENCIL_VIEW_DESC   _DepthStencilDesc;
	};
};

#endif