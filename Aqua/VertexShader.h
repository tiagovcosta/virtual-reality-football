#ifndef VERTEXSHADER_H
#define VERTEXSHADER_H

#include "PCH.h"

#include "Shader.h"

namespace Aqua
{
	class VertexShader : public Shader
	{
	public:
		VertexShader(ID3D11VertexShader* pShader,ID3D11InputLayout*  pInputLayout);
		~VertexShader();

		void destroy();

		ID3D11VertexShader* getShader() const;
		ID3D11InputLayout*  getInputLayout() const;

	private:
		ID3D11VertexShader* _pShader;
		ID3D11InputLayout*  _pInputLayout;
	};
};

#endif