#include "Texture2DConfig.h"

using namespace Aqua;

Texture2DConfig::Texture2DConfig()
{
	setDefaults();
}

Texture2DConfig::~Texture2DConfig()
{}

Texture2DConfig::Texture2DConfig(const D3D11_TEXTURE2D_DESC& state)
{
	_State = state; 
}

void Texture2DConfig::setDefaults()
{
	// Set the state to the default configuration.  These are the D3D11 default
	// values as well.

    _State.Width = 1;
	_State.Height = 1;
    _State.MipLevels = 1;
    _State.ArraySize = 1;
    _State.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	_State.SampleDesc.Count = 1;
	_State.SampleDesc.Quality = 0;
    _State.Usage = D3D11_USAGE_DEFAULT;
    _State.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    _State.CPUAccessFlags = 0;
    _State.MiscFlags = 0;
}

void Texture2DConfig::setDefaultDepthBuffer(uint width, uint height)
{
	_State.Width = width;
	_State.Height = height;
	_State.MipLevels = 1;
	_State.ArraySize = 1;
	_State.Format = DXGI_FORMAT_D32_FLOAT;
	_State.SampleDesc.Count = 1;
	_State.SampleDesc.Quality = 0;
	_State.Usage = D3D11_USAGE_DEFAULT;
	_State.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	_State.CPUAccessFlags = 0;
	_State.MiscFlags = 0;
}

void Texture2DConfig::setDefaultRenderTarget(uint width, uint height)
{
	_State.Width = width;
	_State.Height = height;
	_State.MipLevels = 1;
	_State.ArraySize = 1;
	_State.Format = DXGI_FORMAT_R16G16B16A16_FLOAT;
	_State.SampleDesc.Count = 1;
	_State.SampleDesc.Quality = 0;
	_State.Usage = D3D11_USAGE_DEFAULT;
	_State.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	_State.CPUAccessFlags = 0;
	_State.MiscFlags = 0;
}

void Texture2DConfig::setWidth( uint state )
{
	_State.Width = state;
}

void Texture2DConfig::setHeight( uint state )
{
	_State.Height = state;
}

void Texture2DConfig::setMipLevels( uint state )
{
	_State.MipLevels = state;
}

void Texture2DConfig::setArraySize( uint state )
{
	_State.ArraySize = state;
}

void Texture2DConfig::setFormat( DXGI_FORMAT state )
{
	_State.Format = state;
}

void Texture2DConfig::setSampleDesc( DXGI_SAMPLE_DESC state )
{
	_State.SampleDesc = state;
}

void Texture2DConfig::setUsage( D3D11_USAGE state ) 
{
	_State.Usage = state;
}

void Texture2DConfig::setBindFlags( uint state )
{
	_State.BindFlags = state;
}

void Texture2DConfig::setCPUAccessFlags( uint state )
{
	_State.CPUAccessFlags = state;
}

void Texture2DConfig::setMiscFlags( uint state )
{
	_State.MiscFlags = state;
}

const D3D11_TEXTURE2D_DESC& Texture2DConfig::getTextureDesc() const
{
	return _State;
}