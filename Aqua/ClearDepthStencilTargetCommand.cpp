#include "ClearDepthStencilTargetCommand.h"

using namespace Aqua;

ClearDepthStencilTargetCommand::ClearDepthStencilTargetCommand()
{
	_pDepthStencilTarget = NULL;
	_Value = 0.0f;
}

ClearDepthStencilTargetCommand::~ClearDepthStencilTargetCommand()
{
	_pDepthStencilTarget = NULL;
	_Value = 0.0f;
}

void ClearDepthStencilTargetCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.clearDepthStencilTarget(_pDepthStencilTarget, _Value);
}

void ClearDepthStencilTargetCommand::setDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilTarget)
{
	_pDepthStencilTarget = pDepthStencilTarget;
}

void ClearDepthStencilTargetCommand::setValue(float value)
{
	_Value = value;
}