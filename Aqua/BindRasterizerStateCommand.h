#ifndef BINDRASTERIZERSTATECOMMAND_H
#define BINDRASTERIZERSTATECOMMAND_H

#include "Command.h"

#include "RasterizerState.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindRasterizerStateCommand : public Command
	{
	public:
		BindRasterizerStateCommand();
		~BindRasterizerStateCommand();

		void execute(PipelineManager& pipelineManager);

		void setRasterizerState(const RasterizerState* pRasterizerState);

	private:
		const RasterizerState* _pRasterizerState;
	};
};

#endif