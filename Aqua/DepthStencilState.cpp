#include "DepthStencilState.h"

using namespace Aqua;

DepthStencilState::DepthStencilState(ID3D11DepthStencilState* pState, uint nameID)
{
	_NameID = nameID;
	_pState = pState;
}

DepthStencilState::~DepthStencilState()
{}

ID3D11DepthStencilState* DepthStencilState::getState() const
{
	return _pState;
}

void DepthStencilState::destroy()
{
	SAFE_RELEASE(_pState);
}