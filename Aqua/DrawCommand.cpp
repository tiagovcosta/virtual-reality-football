#include "DrawCommand.h"

using namespace Aqua;

DrawCommand::DrawCommand()
{
	_VertexCount = 0;
	_StartVertex = 0;
}

DrawCommand::~DrawCommand()
{
	_VertexCount = 0;
	_StartVertex = 0;
}

void DrawCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.draw(_VertexCount, _StartVertex);
}

void DrawCommand::setVertexCount(uint vertexCount)
{
	_VertexCount = vertexCount;
}

void DrawCommand::setStartVertex(uint startVertex)
{
	_StartVertex = startVertex;
}