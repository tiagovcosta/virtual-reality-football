#ifndef CAMERA_H
#define CAMERA_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2010-2012              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "d3dx10.h"

#include "Entity.h"

#define DEGTORAD(degree) ((D3DX_PI / 180.0f) * (degree)) // converts from degrees to radians

/*enum
{
	INSIDE = 0,
	INTERSECT = 1,
	OUTSIDE = 2
};*/

enum CAMERA_TYPE
{
	FIRST_PERSON,
	THRID_PERSON,
	FREE
};

namespace Aqua
{
	class Camera : public Entity
	{
	public:
		Camera();
		Camera(uint name);
		~Camera();

		void               destroy();

		void               setCameraType(CAMERA_TYPE type);

		void               setPosition(float x, float y, float z);
		void               setLookDirection(float x, float y, float z);

		void               moveBy(float x, float y, float z);
		void               rotateBy(float xangle, float yangle);

		void               setLens(float fovY, float aspect, float zNear, float zFar);
		void               setLens(float left, float right, float top, float bottom, float zNear, float zFar);

		void               setViewMatrix(const D3DXMATRIX& matrix);

		void               update();

		const D3DXVECTOR3& getPosition() const;
		const D3DXVECTOR3& getLookDirection() const;
		const D3DXVECTOR3& getUp() const;
		const D3DXVECTOR3& getRight() const;
		const D3DXMATRIX&  getView() const;
		const D3DXMATRIX&  getProj() const;
		const D3DXMATRIX&  getViewProj() const;

		float              getFOV() const;
		float              getRatio() const;

		//Returns an array of D3DXVECTOR4 containing the position of the 8 frustum corners
		const D3DXVECTOR3* getCorners() const;

		//Returns D3DXVECTOR2(zNear, zFar)
		D3DXVECTOR2        getClipDistances() const;

		D3DXVECTOR2        getFarPlaneSize() const;

	public:
		void         updateCornersAndPlanes();

		bool         _Update;

		D3DXVECTOR3  _Position;
		D3DXVECTOR3  _Right;
		D3DXVECTOR3  _Up;
		D3DXVECTOR3  _Look;

		D3DXMATRIX   _View;
		D3DXMATRIX   _Proj;
		D3DXMATRIX   _ViewProj;

		float        _AngleX;
		float        _AngleY;

		float        _Fov;
		float        _Ratio;
		float        _ZNear;
		float        _ZFar;

		D3DXVECTOR3  _Corners[8];
		D3DXPLANE    _Planes[6];

		CAMERA_TYPE  _Type;
	};
};

#endif