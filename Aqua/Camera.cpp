#include "Camera.h"

using namespace Aqua;

Camera::Camera() : Entity(CAMERA, 0)
{
	_Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	_Right    = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	_Up       = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	_Look     = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	D3DXMatrixIdentity(&_View);
	D3DXMatrixIdentity(&_Proj);

	_AngleX = 0.0f;
	_AngleY = 0.0f;

	_Fov    = 0.0f;
	_Ratio  = 0.0f;
	_ZNear  = 0.0f;
	_ZFar   = 0.0f;

	_Update = false;
}

Camera::Camera(uint name) : Entity(CAMERA, name)
{
	_Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	_Right    = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	_Up       = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	_Look     = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	D3DXMatrixIdentity(&_View);
	D3DXMatrixIdentity(&_Proj);

	_AngleX = 0.0f;
	_AngleY = 0.0f;

	_Fov    = 0.0f;
	_Ratio  = 0.0f;
	_ZNear  = 0.0f;
	_ZFar   = 0.0f;

	_Update = false;
}

Camera::~Camera()
{}

const D3DXVECTOR3& Camera::getPosition() const
{
	return _Position;
}

const D3DXVECTOR3& Camera::getLookDirection() const
{
	return _Look;
}

const D3DXVECTOR3& Camera::getUp() const
{
	return _Up;
}

const D3DXVECTOR3& Camera::getRight() const
{
	return _Right;
}

const D3DXMATRIX& Camera::getView()const
{
	return _View;
}

const D3DXMATRIX& Camera::getProj()const
{
	return _Proj;
}

const D3DXMATRIX& Camera::getViewProj()const
{
	return _ViewProj;
}

float Camera::getFOV() const
{
	return _Fov;
}

float Camera::getRatio() const
{
	return _Ratio;
}

const D3DXVECTOR3* Camera::getCorners() const
{
	return _Corners;
}

D3DXVECTOR2 Camera::getClipDistances() const
{
	return D3DXVECTOR2(_ZNear, _ZFar);
}

void Camera::setLens(float _FovY, float aspect, float zn, float zf)
{
	_Fov = _FovY;
	_Ratio = aspect;
	_ZNear = zn;
	_ZFar = zf;
	D3DXMatrixPerspectiveFovLH(&_Proj, _FovY, aspect, zn, zf);
}

void Camera::setLens(float left, float right, float top, float bottom, float zNear, float zFar)
{
	D3DXMatrixOrthoOffCenterLH(&_Proj, left, right, bottom, top, zNear, zFar);
}

void Camera::setViewMatrix(const D3DXMATRIX& matrix)
{
	_View = matrix;

	D3DXMatrixMultiply(&_ViewProj, &_View, &_Proj);

	updateCornersAndPlanes();

	_Update = false;
}

void Camera::update()
{
	if(_Update)
	{
		// Keep camera's axes orthogonal to each other and of unit length.
		D3DXVec3Normalize(&_Look, &_Look);

		D3DXVec3Cross(&_Up, &_Look, &_Right);
		D3DXVec3Normalize(&_Up, &_Up);

		D3DXVec3Cross(&_Right, &_Up, &_Look);
		D3DXVec3Normalize(&_Right, &_Right);

		// Fill in the view matrix entries.
		float x = -D3DXVec3Dot(&_Position, &_Right);
		float y = -D3DXVec3Dot(&_Position, &_Up);
		float z = -D3DXVec3Dot(&_Position, &_Look);

		_View(0,0) = _Right.x; 
		_View(1,0) = _Right.y; 
		_View(2,0) = _Right.z; 
		_View(3,0) = x;   

		_View(0,1) = _Up.x;
		_View(1,1) = _Up.y;
		_View(2,1) = _Up.z;
		_View(3,1) = y;  

		_View(0,2) = _Look.x; 
		_View(1,2) = _Look.y; 
		_View(2,2) = _Look.z; 
		_View(3,2) = z;   

		_View(0,3) = 0.0f;
		_View(1,3) = 0.0f;
		_View(2,3) = 0.0f;
		_View(3,3) = 1.0f;

		D3DXMatrixMultiply(&_ViewProj, &_View, &_Proj);

		updateCornersAndPlanes();

		_Update = false;
	}
}

void Camera::setPosition(float x, float y, float z)
{
	_Position = D3DXVECTOR3(x, y, z);

	_Update = true;
}

void Camera::setLookDirection(float x, float y, float z)
{
	_Look = D3DXVECTOR3(x, y, z);

	D3DXVec3Normalize(&_Look, &_Look);
	D3DXVec3Normalize(&_Right, D3DXVec3Cross(&_Right, &D3DXVECTOR3(0.0f, 1.0f, 0.0f), &_Look));
	D3DXVec3Cross(&_Up, &_Look, &_Right);

	_Update = true;
}

void Camera::moveBy(float x, float y, float z)
{
	_Position += _Right*x;
	_Position += _Up*y;
	_Position += _Look*z;

	_Update = true;
}

void Camera::rotateBy(float xangle, float yangle)
{
	//Restrain pitch
	if(_AngleX + xangle > 1.57079633f)
	{
		xangle = 1.57079633f - _AngleX;
		_AngleX = 1.57079633f;
	} else if(_AngleX + xangle < -1.57079633f)
	{
		xangle = -1.57079633f - _AngleX;
		_AngleX = -1.57079633f;
	}else
	{
		_AngleX += xangle;
	}

	D3DXMATRIX R;

	D3DXMatrixRotationAxis(&R,&_Right,xangle);
	D3DXVec3TransformCoord(&_Look,&_Look,&R);
	D3DXVec3TransformCoord(&_Up, &_Up, &R);

	D3DXMatrixRotationY(&R,yangle);
	D3DXVec3TransformCoord(&_Look,&_Look,&R);
	
	D3DXVec3TransformCoord(&_Right, &_Right, &R);

	_Update = true;
}

D3DXVECTOR2 Camera::getFarPlaneSize() const
{
	float Hfar = 2 * tan(_Fov / 2) * _ZFar;
	float Wfar = Hfar * _Ratio;

	return D3DXVECTOR2(Hfar, Wfar);
}

void Camera::updateCornersAndPlanes()
{
	////// UPDATE CORNERS POSITION
	float Hnear = 2 * tan(_Fov / 2) * _ZNear;
	float Wnear = Hnear * _Ratio;

	D3DXVECTOR3 nearCenter = _Position + _Look * _ZNear;

	//Near Top Left
	_Corners[0] = nearCenter + (_Up * Hnear/2) - (_Right * Wnear/2);
	//Near Top Right
	_Corners[1] = nearCenter + (_Up * Hnear/2) + (_Right * Wnear/2);
	//Near Down Left
	_Corners[2] = nearCenter - (_Up * Hnear/2) - (_Right * Wnear/2);
	//Near Down Right
	_Corners[3] = nearCenter - (_Up * Hnear/2) + (_Right * Wnear/2);

	float Hfar = 2 * tan(_Fov / 2) * _ZFar;
	float Wfar = Hfar * _Ratio;

	D3DXVECTOR3 farCenter = _Position + _Look * _ZFar;

	//Far Top Left
	_Corners[4] = farCenter + (_Up * Hfar/2) - (_Right * Wfar/2);
	//Far Top Right
	_Corners[5] = farCenter + (_Up * Hfar/2) + (_Right * Wfar/2);
	//Far Down Left
	_Corners[6] = farCenter - (_Up * Hfar/2) - (_Right * Wfar/2);
	//Far Down Right
	_Corners[7] = farCenter - (_Up * Hfar/2) + (_Right * Wfar/2);

    // Planes face inward.

	// Top Plane
	_Planes[0].a = _ViewProj(0,3) - _ViewProj(0,1);
	_Planes[0].b = _ViewProj(1,3) - _ViewProj(1,1);
	_Planes[0].c = _ViewProj(2,3) - _ViewProj(2,1);
	_Planes[0].d = _ViewProj(3,3) - _ViewProj(3,1);

	// Bottom Plane
	_Planes[1].a = _ViewProj(0,3) + _ViewProj(0,1);
	_Planes[1].b = _ViewProj(1,3) + _ViewProj(1,1);
	_Planes[1].c = _ViewProj(2,3) + _ViewProj(2,1);
	_Planes[1].d = _ViewProj(3,3) + _ViewProj(3,1);

    // Left Plane
	_Planes[2].a = _ViewProj(0,3) + _ViewProj(0,0);
	_Planes[2].b = _ViewProj(1,3) + _ViewProj(1,0);
	_Planes[2].c = _ViewProj(2,3) + _ViewProj(2,0);
	_Planes[2].d = _ViewProj(3,3) + _ViewProj(3,0);

	// Right Plane
	_Planes[3].a = _ViewProj(0,3) - _ViewProj(0,0);
	_Planes[3].b = _ViewProj(1,3) - _ViewProj(1,0);
	_Planes[3].c = _ViewProj(2,3) - _ViewProj(2,0);
	_Planes[3].d = _ViewProj(3,3) - _ViewProj(3,0);

	// Near Plane
	_Planes[4].a = _ViewProj(0,2);
	_Planes[4].b = _ViewProj(1,2);
	_Planes[4].c = _ViewProj(2,2);
	_Planes[4].d = _ViewProj(3,2);
	
	// Far Plane
	_Planes[5].a = _ViewProj(0,3) - _ViewProj(0,2);
	_Planes[5].b = _ViewProj(1,3) - _ViewProj(1,2);
	_Planes[5].c = _ViewProj(2,3) - _ViewProj(2,2);
	_Planes[5].d = _ViewProj(3,3) - _ViewProj(3,2);

    for(int i = 0; i < 6; i++)
		D3DXPlaneNormalize(&_Planes[i],&_Planes[i]);

}

//Frustum culling code

/*int Camera::volumeInFrustum(BoundingBoxC* bBox)
{
	int result = INSIDE;
	float distance;
	int in, out;

	for(int x = 0; x < 6; x++)
	{
		in = 0;
		out = 0;
		/*distance = D3DXPlaneDotCoord(&_Planes[x], &bBox->min);
		if(distance < 0)
		{
			out++;
		} else
		{
			in++;
		}

		distance = D3DXPlaneDotCoord(&_Planes[x], &bBox->max);
		if(distance < 0)
		{
			out++;
		} else
		{
			in++;
		}*/

		/*for(int y = 0; y < 8; y++)
		{
			distance = D3DXPlaneDotCoord(&_Planes[x], &bBox->c[y]);
			if(distance < 0)
			{
				out++;
			} else
			{
				in++;
			}
		}

		if(in == 0)
		{
			result = OUTSIDE;
		} else if(out != 0)
		{
			result = INTERSECT;
		}
	}

	return result;
}

int Camera::volumeInFrustum(BoundingSphere* bSphere)
{
	int result = INSIDE;
	float distance;

	for(int x = 0; x < 6; x++)
	{
		distance = D3DXPlaneDotCoord(&_Planes[x], &bSphere->center);
		if(distance < -bSphere->radius)
		{
			return OUTSIDE;
		} else if(distance < bSphere->radius)
		{
			result =  INTERSECT;
		}
	}

	return result;
}

int Camera::point2DInFrustum(D3DXVECTOR2 point, float dist)
{
	int result = INSIDE;
	float distance;

	for(int x = 2; x < 6; x++)
	{
		distance = D3DXPlaneDotCoord(&_Planes[x], &D3DXVECTOR3(point.x, 0, point.y));
		if(distance < -dist)
		{
			return OUTSIDE;
		} else if(distance < dist)
		{
			result =  INTERSECT;
		}
	}

	return result;
} */