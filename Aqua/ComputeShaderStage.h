#ifndef COMPUTESHADERSTAGE_H
#define COMPUTESHADERSTAGE_H

#include "ShaderStage.h"
#include "ComputeShader.h"

namespace Aqua
{
	class ComputeShaderStage : private ShaderStage
	{
	public:
		ComputeShaderStage();
		~ComputeShaderStage();

		void setShader(const ComputeShader* pShader);

		void clearState();

		void applyState();
	private:
		ID3D11ComputeShader* _pShader;

		friend class PipelineManager;
	};
};

#endif