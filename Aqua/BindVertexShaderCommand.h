#ifndef BINDVERTEXSHADERCOMMAND_H
#define BINDVERTEXSHADERCOMMAND_H

#include "Command.h"

#include "VertexShader.h"

#include "PipelineManager.h"

typedef uint8_t u8;

namespace Aqua
{
	class BindVertexShaderCommand : public Command
	{
	public:
		BindVertexShaderCommand();
		~BindVertexShaderCommand();

		void execute(PipelineManager& pipelineManager);

		void setShader(VertexShader* pShader);

	private:
		VertexShader* _pShader;
	};
};

#endif