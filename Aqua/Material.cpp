#include "Material.h"

using namespace Aqua;

Material::Material()
{
	_Type = MATERIAL;
	_NumContexts = 0;
	_pContextsID = NULL;
	_pContexts   = NULL;
}

Material::Material(uint numContexts, MaterialContextDesc* pContextsDesc)
{
	_Type      = MATERIAL;

	_pContextsID = getStackAllocator().allocateArray<uint>(numContexts);
	_pContexts   = getStackAllocator().allocateArray<MaterialContext>(numContexts);
	_NumContexts = numContexts;

	for(uint i = 0; i < numContexts; i++)
	{
		MaterialContextDesc contextDesc = pContextsDesc[i];

		_pContextsID[i] = contextDesc.contextID;

		CommandGroupWriter cmdWriter;
		cmdWriter.begin(&getStackAllocator());

		for(uint j = 0; j < contextDesc.textures.size(); j++)
		{
			cmdWriter.addBindTextureCommand(contextDesc.textures[j].pTexture, contextDesc.textures[j].slot, contextDesc.textures[j].bindFlags);
		}

		_pContexts[i].pCmdGroup = cmdWriter.end();
		_pContexts[i].pEffect   = contextDesc.effect;
	}
}

Material::~Material()
{}

const MaterialContext* Material::getMaterialContext(uint contextID) const
{
	for(uint i = 0; i <  _NumContexts; i++)
		if(_pContextsID[i] == contextID)
			return &_pContexts[i];

	return NULL;
}

void Material::destroy()
{
	//Nothing to destroy
}