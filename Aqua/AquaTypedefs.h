#ifndef AQUATYPEDEFS_H
#define AQUATYPEDEFS_H

#include <cstdint>

namespace Aqua
{
	typedef unsigned int uint;

	typedef uint8_t u8;
	typedef uint16_t u16;
	typedef uint32_t u32;
	typedef uint64_t u64;

	typedef uintptr_t Marker;

	typedef const void* Blob;
}

#endif