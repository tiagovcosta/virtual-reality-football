#include "BindDepthStencilStateCommand.h"

using namespace Aqua;

BindDepthStencilStateCommand::BindDepthStencilStateCommand()
{
	_pDepthStencilState = NULL;
}

BindDepthStencilStateCommand::~BindDepthStencilStateCommand()
{
	_pDepthStencilState = NULL;
}

void BindDepthStencilStateCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.getOMStage()->setDepthStencilState(_pDepthStencilState->getState());
}

void BindDepthStencilStateCommand::setDepthStencilState(const DepthStencilState* pDepthStencilState)
{
	_pDepthStencilState = pDepthStencilState;
}