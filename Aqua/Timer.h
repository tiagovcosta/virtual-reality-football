#ifndef TIMER_H
#define TIMER_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2012              
/////////////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>

namespace Aqua
{
	class Timer
	{
	public:
		Timer();
		~Timer();

		void tick();
		void reset();
		void start();
		void stop();

		float getRuntime();
		float getElapsedTime();

		int getFramerate();
		int getFrameCount();
		int getMaxFramerate();

		float getMillisecondsPerFrame();

	private:
		float _Delta;
		int   _FramesPerSecond;
		int   _MaxFramesPerSecond;
		int   _FrameCount;

		float _FixedDelta;
		bool  _bUseFixedStep;

		unsigned __int64 _TicksPerSecond64; //How many ticks are incremented per second
		unsigned __int64 _StartTicks64; //When was start called
		unsigned __int64 _CurrentTicks64; //Current ammount of ticks
		unsigned __int64 _PrevTicks64; //Last frame ammount of ticks
		unsigned __int64 _OneSecTicks64; //Ammount of ticks in last second

		bool _bStopped;
	};
};

Aqua::Timer& getMainTimer();

#endif