#ifndef BINDDOMAINSHADERCOMMAND_H
#define BINDDOMAINSHADERCOMMAND_H

#include "Command.h"

#include "DomainShader.h"

#include "PipelineManager.h"

typedef uint8_t u8;

namespace Aqua
{
	class BindDomainShaderCommand : public Command
	{
	public:
		BindDomainShaderCommand();
		~BindDomainShaderCommand();

		void execute(PipelineManager& pipelineManager);

		void setShader(DomainShader* pShader);

	private:
		DomainShader* _pShader;
	};
};

#endif