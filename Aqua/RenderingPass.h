#ifndef RENDERING_PASS
#define RENDERING_PASS

#include "AquaTypedefs.h"

#include "StackAllocator.h"

#include "Commands.h"
#include "CommandGroupWriter.h"

#include "ResourceManager.h"
#include "PipelineStateManager.h"

#include "Camera.h"
#include "Scene.h"

namespace Aqua
{
	class RenderingPass
	{
	public:
		RenderingPass();
		~RenderingPass();

		//virtual void init(uint width, uint height) = 0;

		virtual CommandGroup* draw(Scene* pScene, Camera* pCamera) = 0;

	protected:
		CommandGroup* _pCommandGroup;
	};
};

#endif