#ifndef PCH_H
#define PCH_H

//DX 11 Library includes
#include <d3d11.h>
#include <dxgi.h>
#include <D3D11Shader.h>
#include <d3dCompiler.h>

//DX 10 Library inclucles
#include <d3d10.h>

//Windows Library includes
#include <Windows.h>

// Standard C++ Library includes
#include <fstream>
#include <string>
//#include <list>
#include <sstream>
//#include <algorithm>
//#include <map>
#include <vector>
#include <assert.h>

// Standard C Library includes
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <memory.h>

//Aqua Library includes
#include "Defines.h"
#include "AquaTypedefs.h"

#include "AqMath.h"

#include "tinyxml2.h"

using namespace tinyxml2;

/*#ifdef _DEBUG
   #ifndef DBG_NEW
      #define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
      #define new DBG_NEW
   #endif
#endif */ // _DEBUG

#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#endif

#endif