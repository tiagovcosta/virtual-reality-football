#include "BindConstantBufferCommand.h"

using namespace Aqua;

BindConstantBufferCommand::BindConstantBufferCommand()
{
	_pConstantBuffer = NULL;
	_Slot = 0;
	_BindFlags = 0;
}

BindConstantBufferCommand::~BindConstantBufferCommand()
{
	_pConstantBuffer = NULL;
	_Slot = 0;
	_BindFlags = 0;
}

void BindConstantBufferCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindConstantBuffer(_BindFlags, _pConstantBuffer, _Slot);
}

void BindConstantBufferCommand::setConstantBuffer(ID3D11Buffer* pConstantBuffer)
{
	_pConstantBuffer = pConstantBuffer;
}

void BindConstantBufferCommand::setSlot(uint slot)
{
	_Slot = slot;
}

void BindConstantBufferCommand::setBindFlags(u8 bindFlags)
{
	_BindFlags = bindFlags;
}