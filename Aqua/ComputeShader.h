#ifndef COMPUTESHADER_H
#define COMPUTESHADER_H

#include "PCH.h"

#include "Shader.h"

namespace Aqua
{
	class ComputeShader : public Shader
	{
	public:
		ComputeShader(ID3D11ComputeShader* pShader);
		~ComputeShader();

		void destroy();

		ID3D11ComputeShader* getShader() const;

	private:
		ID3D11ComputeShader* _pShader;
	};
};

#endif