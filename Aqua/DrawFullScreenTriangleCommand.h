#ifndef DRAWFULLSCREENTRIANGLECOMMAND_H
#define DRAWFULLSCREENTRIANGLECOMMAND_H

#include "Command.h"

#include "PixelShader.h"

namespace Aqua
{
	struct DrawFullScreenQuadCommand
	{
		PixelShader* pPixelShader;
	};
};

#endif