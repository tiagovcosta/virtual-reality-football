#ifndef HULLSHADER_H
#define HULLSHADER_H

#include "PCH.h"

#include "Shader.h"

namespace Aqua
{
	class HullShader : public Shader
	{
	public:
		HullShader(ID3D11HullShader* pShader);
		~HullShader();

		void destroy();

		ID3D11HullShader* getShader() const;

	private:
		ID3D11HullShader* _pShader;
	};
};

#endif