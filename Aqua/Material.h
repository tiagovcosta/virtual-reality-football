#ifndef MATERIAL_H
#define MATERIAL_H

#include <vector>

#include "PCH.h"
#include "StackAllocator.h"

#include "Commands.h"
#include "CommandGroup.h"
#include "CommandGroupWriter.h"

#include "Effect.h"
#include "Texture.h"

///////////////////////////////
// TODO: Improve context selection and make support to other textures more flexible
///////////////////////////////

namespace Aqua
{
	namespace MATERIAL_TEXTURE_BIND_FLAG
	{
		enum LIST
		{
			VERTEX_SHADER = 0x01,
			HULL_SHADER = 0x02,
			DOMAIN_SHADER = 0x04,
			GEOMETRY_SHADER = 0x08,
			PIXEL_SHADER = 0x10
		};
	};

	namespace MATERIAL_TEXTURE_SLOT
	{
		enum LIST
		{
			UNKNOWN = 0,
			DIFFUSE_MAP = 1,
			NORMAL_MAP = 2,
			SPECULAR_MAP = 3,
			HEIGHT_MAP = 4
		};
	};

	struct MaterialTexture
	{
		MaterialTexture()
		{
			pTexture = NULL;
			slot = MATERIAL_TEXTURE_SLOT::UNKNOWN;
			bindFlags = 0;
		}

		const Texture* pTexture;
		MATERIAL_TEXTURE_SLOT::LIST slot;
		u8 bindFlags;
	};

	struct MaterialContextDesc
	{
		uint                                contextID;
		const Effect*                       effect;
		std::vector<const MaterialTexture>  textures;
	};

	struct MaterialContext
	{
		CommandGroup* pCmdGroup;
		const Effect* pEffect;
	};

	class Material : public Resource
	{
	public:
		Material();
		Material(uint numContexts, MaterialContextDesc* pContextsDesc);
		~Material();

		const MaterialContext* getMaterialContext(uint contextID) const;

		void destroy();

	private:
		uint                      _NumContexts;
		uint*                     _pContextsID;
		MaterialContext*          _pContexts;
	};
};

#endif