#ifndef LIGHT_H
#define LIGHT_H

#include "PCH.h"

#include "Entity.h"

namespace Aqua
{
	struct DirLight
	{
		D3DXVECTOR3    color;
		float          pad1;
		D3DXVECTOR3    dir;
		float          pad2;
	};

	struct PointLight
	{
		D3DXVECTOR3    color;
		float          pad1;
		D3DXVECTOR4    positionRange;
	};
};

#endif