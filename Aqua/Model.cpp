#include "Model.h"

using namespace Aqua;

Model::Model()
{
	_Type = MODEL;

	_pGeometry = NULL;
	_NumSubsets = 0;
	_pSubsets = NULL;
}

Model::Model(const ModelDesc& modelDesc)
{
	ASSERT((modelDesc.numSubsets > 0));

	_Type       = MODEL;

	_pGeometry  = modelDesc.pGeometry;
	_NumSubsets = modelDesc.numSubsets;

	_pSubsets   = getStackAllocator().allocateArray<ModelSubset>(_NumSubsets);

	for(uint i = 0; i < _NumSubsets; i++)
	{
		ModelSubsetDesc subsetDesc = modelDesc.pSubsetsDesc[i];
		const GeometrySubset geometrySubset = _pGeometry->getSubset(subsetDesc.geometrySubset);

		CommandGroupWriter cmdWriter;
		cmdWriter.begin(&getStackAllocator());

		cmdWriter.addDrawIndexedCommand(geometrySubset.indexCount, geometrySubset.startIndex, 0);

		_pSubsets[i].pCmdGroup = cmdWriter.end();
		_pSubsets[i].pMaterial = subsetDesc.pMaterial;
	}
}

Model::~Model()
{}

const Geometry* Model::getGeometry() const
{
	return _pGeometry;
}

uint Model::getNumSubsets() const
{
	return _NumSubsets;
}

const ModelSubset* Model::getSubsets() const
{
	return _pSubsets;
}

void Model::destroy()
{

}