#include "MaterialPass.h"

using namespace Aqua;

MaterialPass::MaterialPass()
{
	_pCommandGroup        = NULL;
	_pDepthStencilTexture = NULL;
	_pSceneRT             = NULL;
	_pLightingMapRT       = NULL;
}

MaterialPass::~MaterialPass()
{
	_pCommandGroup        = NULL;
	_pDepthStencilTexture = NULL;
	_pSceneRT             = NULL;
	_pLightingMapRT       = NULL;
}

void MaterialPass::init(uint width, uint height)
{
	_pDepthStencilTexture = dynamic_cast<const DepthStencilTexture*>(getResourceManager().getResource(DEPTH_STENCIL_TEXTURE, "mainDepthStencilTexture"));

	RenderTextureConfig rtConfig;

	//Create SceneMap
	rtConfig.setDefaultRenderTexture(RT_RGBA16, width, height);
	_pSceneRT = getResourceManager().createRenderTexture("sceneMap", rtConfig);

	//Get Lighting map to use as shader resource
	_pLightingMapRT = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "lightingMap"));

	CommandGroupWriter cmdWriter;
	cmdWriter.begin(&getStackAllocator());

	//cmdWriter.addBindRasterizerStateCommand(NULL);
	//cmdWriter.addBindDepthStencilStateCommand(dynamic_cast<const DepthStencilState*>(getPipelineStateManager().getState("lessEqualDSS")));
	//cmdWriter.addBindBlendStateCommand(NULL);

	//cmdWriter.addDrawSceneEntitiesCommand(NULL, ACTOR, FRONT_TO_BACK, getStringID("Material"));

	_pCommandGroup = cmdWriter.end();
}

CommandGroup* MaterialPass::draw(Scene* pScene, Camera* pCamera)
{
	return _pCommandGroup;
}