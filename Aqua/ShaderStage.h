#ifndef SHADERSTATE_H
#define SHADERSTATE_H

#include "PCH.h"
#include "Shader.h"

namespace Aqua
{
	//TO DO - only bind new buffers/resources/samplers
	class ShaderStage
	{
	public:
		ShaderStage();
		virtual ~ShaderStage();

		void init(ID3D11DeviceContext* pContext);
		void shutdown();

		void setConstantBuffer(ID3D11Buffer* pResource, int slot);

		void setShaderResourceView(ID3D11ShaderResourceView* pResource, int slot);

		void setSamplerState(ID3D11SamplerState* pSamplerState, int slot);

		void clearStageResources();

	protected:
		ID3D11DeviceContext*      _pContext;

		int                       _StartCB;
		int                       _EndCB;
		ID3D11Buffer*             _pConstantBuffers[D3D11_COMMONSHADER_CONSTANT_BUFFER_API_SLOT_COUNT];

		int                       _StartSR;
		int                       _EndSR;
		ID3D11ShaderResourceView* _pShaderResources[D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT];

		int                       _StartST;
		int                       _EndST;
		ID3D11SamplerState*       _pSamplerStates[D3D11_COMMONSHADER_SAMPLER_SLOT_COUNT];

		bool                      _UpdateShader;

		friend class PipelineManager;
	};
};

#endif