#ifndef SCENE_H
#define SCENE_H

#include "PCH.h"
#include "Utilities.h"

#include "ResourceManager.h"

#include "Actor.h"
#include "Light.h"

namespace Aqua
{
	typedef uint NodeHandle;

	struct NodeLocalTransformData
	{
		Vector3D   position;
		D3DXMATRIX rotation;
		Vector3D   scaling;
	};

	class Scene
	{
	public:
		Scene();
		~Scene();

		void                init();
		void                shutdown();

		void                update(float dt);

		int                 loadScene(const std::string& filename);

		uint                getNumActors() const;
		Actor*              getActor(uint index) const;
		Actor* const *      getActors() const;

		uint                getNumDirLights() const;
		DirLight*           getDirLight(uint index) const;
		DirLight* const *   getDirLights() const;

		uint                getNumPointLights() const;
		PointLight*         getPointLight(uint index) const;
		PointLight* const * getPointLights() const;

		const Vector4D& getAmbientLightColor() const;

		void            setAmbientLightColor(float r, float g, float b, float a);

		NodeHandle      getNode(const char* name) const;

		void            scaleNode(NodeHandle node, float x, float y, float z);
		void            rotateNode(NodeHandle node, float x, float y, float z);
		void            translateNode(NodeHandle node, float x, float y, float z);

		void            setNodeScale(NodeHandle node, float x, float y, float z);
		void            setnodeRotation(NodeHandle node, float x, float y, float z);
		void            setNodeTranslation(NodeHandle node, float x, float y, float z);

		void            updateSceneGraph();

		DirLight*       getSun();

		Actor*          createActor(const char* name);

	private:
		
		DirLight*       createDirLight(const char* name);
		PointLight*     createPointLight(const char* name);

		uint                     _NumNodes;
		uint*                    _NodesName;
		NodeLocalTransformData*  _pNodesLocalTransformData;
		D3DXMATRIX*              _pNodesLocalTransform;
		NodeHandle*              _pNodesParent;
		D3DXMATRIX*              _pNodesWorldTransform;
		bool*                    _pNodesDirty;

		std::vector<Actor*>      _pActors;
		std::vector<DirLight*>   _pDirLights;
		std::vector<PointLight*> _pPointLights;

		DirLight*                _pSun;

		Vector4D                 _AmbientLightColor;
	};
};

#endif