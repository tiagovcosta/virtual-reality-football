#ifndef TEXTURE1DCONFIG_H
#define TEXTURE1DCONFIG_H

#include "PCH.h"

namespace Aqua
{
	class Texture1DConfig
	{
	public:
		Texture1DConfig();
		Texture1DConfig(const D3D11_TEXTURE1D_DESC& state);
		~Texture1DConfig();

		void setDefaults();

		void setWidth( uint state );
		void setMipLevels( uint state );
		void setArraySize( uint state );
		void setFormat( DXGI_FORMAT state );
		void setUsage( D3D11_USAGE state ); 
		void setBindFlags( uint state );
		void setCPUAccessFlags( uint state );
		void setMiscFlags( uint state );

		const D3D11_TEXTURE1D_DESC& getTextureDesc() const;

	protected:
		D3D11_TEXTURE1D_DESC 		_State;
	
	};
};

#endif