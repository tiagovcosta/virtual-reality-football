#include "CommandGroup.h"

using namespace Aqua;

CommandGroup::CommandGroup(uint size, uint numCommands, const void* pCommandsBlob)
{
	_Size = size;
	_NumCommands = numCommands;
	_pCommandsBlob = pCommandsBlob;
}

CommandGroup::~CommandGroup()
{}

uint CommandGroup::getSize() const
{
	return _Size;
}

uint CommandGroup::getNumCommands() const
{
	return _NumCommands;
}

Blob CommandGroup::getCommandsBlob() const
{
	return _pCommandsBlob;
}