#include "SkyDynamic.h"

using namespace Aqua;

static const float PI = 3.141592f;

SkyDynamic::SkyDynamic()
{
	_pSkyDome    = NULL;

	_Update      = true;

	_Theta       = PI/4;
	_Phi         = 0.0f;

	_Wavelengths = Vector3D(0.65f, 0.57f, 0.475f);

	changeSunDirection(0.0f, 0.0f);
}

SkyDynamic::~SkyDynamic()
{
	_pSkyDome = NULL;
}

void SkyDynamic::init(float radius)
{
	_radius = radius;

	_pSkyCBuffer = dynamic_cast<const ConstantBuffer*>(getResourceManager().createConstantBuffer(sizeof(D3DXMATRIX), "skyBoxCB"));

	RenderTextureConfig rtConfig;
	rtConfig.setDefaultRenderTexture(RT_RGBA16, 128, 64);
	
	_pRayleighRT = getResourceManager().createRenderTexture("rayleighRT", rtConfig);
	_pMieRT = getResourceManager().createRenderTexture("mieRT", rtConfig);

	_pUpdateRayleighPS = dynamic_cast<const PixelShader*>(getShaderManager().getShader("UpdateRayleighPS.hlsl"));
	_pUpdateMiePS      = dynamic_cast<const PixelShader*>(getShaderManager().getShader("UpdateMiePS.hlsl"));
	_pSkyDynamicEffect = dynamic_cast<const Effect*>(getResourceManager().getResource(EFFECT, "SkyDynamicFX.effect.xml"));

	_UpdateViewport.setSize(0.0f, 0.0f, 1.0f, 1.0f);

	buildSkyDome();

	_SkyDynamicData.InvWavelength.x = 1.0f / pow(_Wavelengths.x, 4.0f);
    _SkyDynamicData.InvWavelength.y = 1.0f / pow(_Wavelengths.y, 4.0f);
    _SkyDynamicData.InvWavelength.z = 1.0f / pow(_Wavelengths.z, 4.0f);

	_SkyDynamicData.WavelengthMie.x = pow(_Wavelengths.x, -0.84f);
    _SkyDynamicData.WavelengthMie.y = pow(_Wavelengths.y, -0.84f);
    _SkyDynamicData.WavelengthMie.z = pow(_Wavelengths.z, -0.84f);

	_pSkyDynamicCB = getResourceManager().createConstantBuffer(sizeof(SkyDynamicData), "UpdateSkyCB");

	_pSceneMapRT = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "lightingMap"));
	_pMainDepthStencilTexture = dynamic_cast<const DepthStencilTexture*>(getResourceManager().getResource(DEPTH_STENCIL_TEXTURE, "mainDepthStencilTexture"));

	CommandGroupWriter cmdWriter;

	//Draw SkyDome commands
	cmdWriter.begin(&getStackAllocator());
	
	cmdWriter.addBindViewportCommand(&_UpdateViewport, _pSceneMapRT->getWidth(), _pSceneMapRT->getHeight());
	cmdWriter.addBindRasterizerStateCommand(NULL);
	cmdWriter.addBindDepthStencilStateCommand(dynamic_cast<const DepthStencilState*>(getPipelineStateManager().getState("lessEqualDSS")));
	cmdWriter.addBindBlendStateCommand(NULL);

	cmdWriter.addBindRenderTargetCommand(_pSceneMapRT->getRenderTarget(), 0);
	cmdWriter.addBindDepthStencilTargetCommand(_pMainDepthStencilTexture->getDepthStencilTarget());
	cmdWriter.addUpdateCBufferCommand(_pSkyCBuffer->getBuffer(), &_WorldViewProj, sizeof(D3DXMATRIX));
	cmdWriter.addBindConstantBuffeCommand(_pSkyCBuffer->getBuffer(), 5, VERTEX_STAGE);
	cmdWriter.addBindTextureCommand(_pRayleighRT->getTexture(), 1, PIXEL_STAGE);
	cmdWriter.addBindTextureCommand(_pMieRT->getTexture(), 2, PIXEL_STAGE);
	cmdWriter.addDispatchCommandsCommand(_pSkyDome->getCommandGroup());
	cmdWriter.addDispatchCommandsCommand(_pSkyDynamicEffect->getCommandGroup());
	cmdWriter.addDrawIndexedCommand(_pSkyDome->getSubset(0).indexCount, _pSkyDome->getSubset(0).startIndex, 0);
	_pDrawCommandGroup = cmdWriter.end();

	//Update Rayleigh commands
	cmdWriter.begin(&getStackAllocator());

	cmdWriter.addBindRasterizerStateCommand(NULL);
	cmdWriter.addBindDepthStencilStateCommand(dynamic_cast<const DepthStencilState*>(getPipelineStateManager().getState("lessEqualDSS")));
	cmdWriter.addBindBlendStateCommand(NULL);

	cmdWriter.addUpdateCBufferCommand(_pSkyDynamicCB->getBuffer(), &_SkyDynamicData, sizeof(SkyDynamicData));
	cmdWriter.addBindConstantBuffeCommand(_pSkyDynamicCB->getBuffer(), 3, PIXEL_STAGE);
	cmdWriter.addBindDepthStencilTargetCommand(NULL);
	cmdWriter.addBindViewportCommand(&_UpdateViewport, _pRayleighRT->getWidth(), _pRayleighRT->getHeight());
	cmdWriter.addBindRenderTargetCommand(_pRayleighRT->getRenderTarget(), 0);
	cmdWriter.addDrawFullScreenCommand(_pUpdateRayleighPS);
	cmdWriter.addDispatchCommandsCommand(_pDrawCommandGroup);
	_pUpdateRayleighCommandGroup = cmdWriter.end();

	//Update Mie commands
	cmdWriter.begin(&getStackAllocator());

	cmdWriter.addBindRasterizerStateCommand(NULL);
	cmdWriter.addBindDepthStencilStateCommand(dynamic_cast<const DepthStencilState*>(getPipelineStateManager().getState("lessEqualDSS")));
	cmdWriter.addBindBlendStateCommand(NULL);

	cmdWriter.addUpdateCBufferCommand(_pSkyDynamicCB->getBuffer(), &_SkyDynamicData, sizeof(SkyDynamicData));
	cmdWriter.addBindConstantBuffeCommand(_pSkyDynamicCB->getBuffer(), 3, PIXEL_STAGE);
	cmdWriter.addBindDepthStencilTargetCommand(NULL);
	cmdWriter.addBindViewportCommand(&_UpdateViewport, _pMieRT->getWidth(), _pMieRT->getHeight());
	cmdWriter.addBindRenderTargetCommand(_pMieRT->getRenderTarget(), 0);
	cmdWriter.addDrawFullScreenCommand(_pUpdateMiePS);
	cmdWriter.addDispatchCommandsCommand(_pDrawCommandGroup);
	_pUpdateMieCommandGroup = cmdWriter.end();

};

void SkyDynamic::setSunDirection(const Vector3D& dir)
{
	_SkyDynamicData.v3SunDir = vector3DNormalize(Vector3D(-dir.x, -dir.y, -dir.z));

	_Update = true;
}

void SkyDynamic::changeSunDirection(float theta, float phi)
{
	_Theta += theta;
	_Phi += phi;

	if (_Theta > PI * 2.0f)
		_Theta = _Theta - PI * 2.0f;
	if (_Theta < 0.0f)
		_Theta = PI * 2.0f + _Theta;

	_SkyDynamicData.v3SunDir.x = sin(_Theta)*cos(_Phi);
	_SkyDynamicData.v3SunDir.y = cos(_Theta);
	_SkyDynamicData.v3SunDir.z = sin(_Theta)*sin(_Phi);

	_Update = true;
}

D3DXVECTOR3 SkyDynamic::getSunDirection() const
{
	return D3DXVECTOR3(-_SkyDynamicData.v3SunDir.x, -_SkyDynamicData.v3SunDir.y, -_SkyDynamicData.v3SunDir.z);
}

D3DXVECTOR3 SkyDynamic::getSunColor() const
{
	int nTurbidity = 2;

	float fBeta = 0.04608365822050f * nTurbidity - 0.04586025928522f;
	float fTauR, fTauA;
	float fTau[3];

	float coseno = cos(-_Theta + PI);
	double factor = -_Theta / PI * 180.0f;
	float jarl = pow(93.885f - (float)factor, -1.253f);
	float potencia = jarl;
	float m = -1.0f / (coseno + 0.15f * potencia);

	int i;
	float fLambda[3];
	fLambda[0] = _Wavelengths.x;
	fLambda[1] = _Wavelengths.y;
	fLambda[2] = _Wavelengths.z;


	for (i = 0; i < 3; i++)
	{
		potencia = pow(fLambda[i], 4.0f);
		fTauR = exp(-m * 0.008735f * potencia);

		const float fAlpha = 1.3f;
		potencia = pow(fLambda[i], -fAlpha);
		if (m < 0.0f)
			fTau[i] = 0.0f;
		else
		{
			fTauA = exp(-m * fBeta * potencia);
			fTau[i] = fTauR * fTauA;
		}

	}

	D3DXVECTOR3 color = D3DXVECTOR3(fTau[0], fTau[1], fTau[2]);
	
	return color;
}

const ConstantBuffer* SkyDynamic::getSkyDynamicCB() const
{
	return _pSkyDynamicCB;
}

CommandGroup* SkyDynamic::draw(Scene* pScene, Camera* pCamera)
{
	//Update WorldViewProj Matrix
	D3DXVECTOR3 eye = pCamera->getPosition();
	D3DXMatrixTranslation(&_WorldViewProj, eye.x, eye.y, eye.z);
	D3DXMatrixMultiply(&_WorldViewProj, &_WorldViewProj, &pCamera->getView());
	D3DXMatrixMultiply(&_WorldViewProj, &_WorldViewProj, &pCamera->getProj());

	if(_UpdateMie) //Update and draw
	{
		_UpdateMie = false;

		return _pUpdateMieCommandGroup;
	} else if(_Update) //Update Rayleigh Scatter and draw then Mie
	{
		_UpdateMie = true;
		_Update = false;
		return _pUpdateRayleighCommandGroup;
	} else // No need to update just draw
	{
		return _pDrawCommandGroup;
	}
}

void SkyDynamic::buildSkyDome()
{
	int DomeN = 32;
	int Latitude = DomeN / 2;
    int Longitude = DomeN;
    int DVSize = Longitude * Latitude;
    int DISize = (Longitude - 1) * (Latitude - 1) * 2;
    DVSize *= 2;
    DISize *= 2;

    PTCVertex* domeVerts = new PTCVertex[DVSize];

	// Fill Vertex Buffer
	int DomeIndex = 0;
	for (int i = 0; i < Longitude; i++)
	{
		double MoveXZ = 100.0f * (i / ((float)Longitude - 1.0f)) * D3DX_PI / 180.0;

		for (int j = 0; j < Latitude; j++)
		{
			double MoveY = D3DX_PI * j / (Latitude - 1);

			domeVerts[DomeIndex].pos.x = (float)(sin(MoveXZ) * cos(MoveY));
            domeVerts[DomeIndex].pos.y = (float)cos(MoveXZ);
            domeVerts[DomeIndex].pos.z = (float)(sin(MoveXZ) * sin(MoveY));

            domeVerts[DomeIndex].pos *= _radius;

			domeVerts[DomeIndex].texC.x = i / (float)Longitude;
            domeVerts[DomeIndex].texC.y = j / (float)Latitude;

            DomeIndex++;
		}
	}

	for (int i = 0; i < Longitude; i++)
	{
		double MoveXZ = 100.0f * (i / ((float)Longitude - 1.0f)) * D3DX_PI / 180.0;

		for (int j = 0; j < Latitude; j++)
		{
			double MoveY = (D3DX_PI * 2.0) - (D3DX_PI * j / (Latitude - 1));

			domeVerts[DomeIndex].pos.x = (float)(sin(MoveXZ) * cos(MoveY));
            domeVerts[DomeIndex].pos.y = (float)cos(MoveXZ);
            domeVerts[DomeIndex].pos.z = (float)(sin(MoveXZ) * sin(MoveY));

            domeVerts[DomeIndex].pos *= _radius;

			domeVerts[DomeIndex].texC.x = i / (float)Longitude;
            domeVerts[DomeIndex].texC.y = j / (float)Latitude;

            DomeIndex++;
		}
	}

	// Fill index buffer
	WORD* ib = new WORD[DISize * 3];
	int index = 0;
	for (WORD i = 0; i < Longitude - 1; i++)
	{
		for (WORD j = 0; j < Latitude - 1; j++)
		{
			ib[index++] = (WORD)(i * Latitude + j);
			ib[index++] = (WORD)((i + 1) * Latitude + j);
			ib[index++] = (WORD)((i + 1) * Latitude + j + 1);

			ib[index++] = (WORD)((i + 1) * Latitude + j + 1);
			ib[index++] = (WORD)(i * Latitude + j + 1);
			ib[index++] = (WORD)(i * Latitude + j);
		}
	}

	WORD Offset = (WORD)(Latitude * Longitude);
	for (WORD i = 0; i < Longitude - 1; i++)
	{
		for (WORD j = 0; j < Latitude - 1; j++)
		{
			ib[index++] = (WORD)(Offset + i * Latitude + j);
			ib[index++] = (WORD)(Offset + (i + 1) * Latitude + j + 1);
			ib[index++] = (WORD)(Offset + (i + 1) * Latitude + j);

			ib[index++] = (WORD)(Offset + i * Latitude + j + 1);
			ib[index++] = (WORD)(Offset + (i + 1) * Latitude + j + 1);
			ib[index++] = (WORD)(Offset + i * Latitude + j);
		}
	}

	GeometryConfig config;
	config.setVertexBufferData(domeVerts, DVSize*sizeof(PTCVertex), sizeof(PTCVertex));
	config.setIndexBufferData(ib, 3*DISize*sizeof(WORD));

	GeometrySubset subset;
	subset.indexCount = DISize*3;
	subset.startIndex = 0;

	_pSkyDome = getResourceManager().createGeometry("skyDome", config, 1, &subset);

	delete[] domeVerts;
	delete[] ib;
}