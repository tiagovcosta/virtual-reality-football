#include "Texture3DConfig.h"

using namespace Aqua;

Texture3DConfig::Texture3DConfig()
{
	setDefaults();
}

Texture3DConfig::Texture3DConfig(const D3D11_TEXTURE3D_DESC& state)
{
	_State = state; 
}

Texture3DConfig::~Texture3DConfig()
{
}

void Texture3DConfig::setDefaults()
{
	// Set the state to the default configuration.  These are the D3D11 default
	// values as well.

    _State.Width = 1;
	_State.Height = 1;
    _State.Depth = 1;
    _State.MipLevels = 1;
    _State.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    _State.Usage = D3D11_USAGE_DEFAULT;
    _State.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    _State.CPUAccessFlags = 0;
    _State.MiscFlags = 0;
}

void Texture3DConfig::setWidth( uint state )
{
	_State.Width = state;
}

void Texture3DConfig::setMipLevels( uint state )
{
	_State.MipLevels = state;
}

void Texture3DConfig::SetHeight( uint state )
{
	_State.Height = state;
}

void Texture3DConfig::SetDepth( uint state )
{
	_State.Depth = state;
}

void Texture3DConfig::setFormat( DXGI_FORMAT state )
{
	_State.Format = state;
}

void Texture3DConfig::setUsage( D3D11_USAGE state ) 
{
	_State.Usage = state;
}

void Texture3DConfig::setBindFlags( uint state )
{
	_State.BindFlags = state;
}

void Texture3DConfig::setCPUAccessFlags( uint state )
{
	_State.CPUAccessFlags = state;
}

void Texture3DConfig::setMiscFlags( uint state )
{
	_State.MiscFlags = state;
}

const D3D11_TEXTURE3D_DESC& Texture3DConfig::getTextureDesc() const
{
	return _State;
}