#include "BindVertexBufferCommand.h"

using namespace Aqua;

BindVertexBufferCommand::BindVertexBufferCommand()
{
	_pBuffer = NULL;
	_Stride = 0;
	_Offset = 0;
	_Slot = 0;
}

BindVertexBufferCommand::~BindVertexBufferCommand()
{
	_pBuffer = NULL;
	_Stride = 0;
	_Offset = 0;
	_Slot = 0;
}

void BindVertexBufferCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.getIAStage()->setVertexBuffer(_pBuffer, _Stride, _Offset, _Slot);
}

void BindVertexBufferCommand::setVertexBuffer(ID3D11Buffer* pBuffer)
{
	_pBuffer = pBuffer;
}

void BindVertexBufferCommand::setStride(uint stride)
{
	_Stride = stride;
}

void BindVertexBufferCommand::setOffset(uint offset)
{
	_Offset = offset;
}

void BindVertexBufferCommand::setSlot(uint slot)
{
	_Slot = slot;
}