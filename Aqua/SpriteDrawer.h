/*#ifndef SPRITEDRAWER_H
#define SPRITEDRAWER_H

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2011              
/////////////////////////////////////////////////////////////////////////////////////////////

#include "PCH.h"

#include "PipelineManager.h"
#include "VertexLayouts.h"

#include "ResourceManager.h"

#include "Effect.h"
#include "Texture.h"
#include "Mesh.h"

#define SPRITE_CBUFFER_SIZE 16

namespace Aqua
{
	struct CbPerSprite
	{
		D3DXVECTOR2 pos;
		D3DXVECTOR2 scale;
	};

	class SpriteDrawer
	{
	public:
		SpriteDrawer();
		~SpriteDrawer();

		void draw(Texture* pTexture, D3DXVECTOR2 pos, D3DXVECTOR2 scale);
		void draw(ID3D11ShaderResourceView* pTexture, D3DXVECTOR2 pos, D3DXVECTOR2 scale);

	private:
		void init(Mesh* pMesh, Effect effect, uint windowWidth, uint windowHeight);
		void shutdown();
		
		Effect          _Effect;

		ConstantBuffer* _pCBuffer;
		CbPerSprite     _CBData;

		Mesh*           _pMesh;

		uint            _WindowWidth;
		uint            _WindowHeight;
		
		friend class Renderer;
	};
};

Aqua::SpriteDrawer& getSpriteDrawer();

#endif*/