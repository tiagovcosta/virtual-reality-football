#include "BindComputeShaderCommand.h"

using namespace Aqua;

BindComputeShaderCommand::BindComputeShaderCommand()
{
	_pShader = NULL;
}

BindComputeShaderCommand::~BindComputeShaderCommand()
{
	_pShader = NULL;
}

void BindComputeShaderCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindShader(_pShader);
}

void BindComputeShaderCommand::setShader(ComputeShader* pShader)
{
	_pShader = pShader;
}