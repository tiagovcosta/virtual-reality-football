#ifndef COMMANDS_H
#define COMMANDS_H

#include "AquaTypedefs.h"

#include "RendererEnums.h"

#include <D3D11.h>

#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "GeometryShader.h"
#include "PixelShader.h"
#include "ComputeShader.h"

#include "BlendState.h"
#include "DepthStencilState.h"
#include "RasterizerState.h"

#include "Texture.h"

#include "Viewport.h"

#include "Entity.h"
#include "Camera.h"

#include "CommandGroup.h"

namespace Aqua
{
	enum COMMAND_TYPE : char
	{
		UNKNOWN,
		DRAW,
		DRAW_INDEXED,
		BIND_VERTEX_BUFFER,
		BIND_INDEX_BUFFER,
		BIND_CONSTANT_BUFFER,
		BIND_TEXTURE,
		BIND_VERTEX_SHADER,
		BIND_HULL_SHADER,
		BIND_DOMAIN_SHADER,
		BIND_GEOMETRY_SHADER,
		BIND_PIXEL_SHADER,
		BIND_COMPUTE_SHADER,
		BIND_SAMPLER_STATE,
		BIND_BLEND_STATE,
		BIND_RASTERIZER_STATE,
		BIND_DEPTH_STENCIL_STATE,
		BIND_VIEWPORT,
		BIND_RENDER_TARGET,
		BIND_DEPTH_STENCIL_TARGET,
		CLEAR_RENDER_TARGET,
		CLEAR_DEPTH_STENCIL_TARGET,
		UPDATE_CBUFFER,
		DRAW_FULLSCREEN,
		DRAW_SCENE_ENTITIES,
		BIND_MAIN_RENDER_TARGET,
		DISPATCH_COMMANDS,
	};

	struct DrawCommand
	{
		uint vertexCount;
		uint startVertex;
	};

	struct DrawIndexedCommand
	{
		uint indexCount;
		uint startIndex;
		uint startVertex;
	};

	struct BindVertexBufferCommand
	{
		ID3D11Buffer* pBuffer;
		uint          stride;
		uint          offset;
		u8            slot;
	};

	struct BindIndexBufferCommand
	{
		ID3D11Buffer*       pBuffer;
		INDEX_BUFFER_FORMAT format;
		uint                offset;
	};

	struct BindConstantBufferCommand
	{
		ID3D11Buffer* pConstantBuffer;
		u8            slot;
		u8            bindFlags;
	};

	struct BindTextureCommand
	{
		const Texture* pTexture;
		u8             slot;
		u8             bindFlags;
	};

	struct BindVertexShaderCommand
	{
		VertexShader* pShader;
	};

	struct BindHullShaderCommand
	{
		HullShader* pShader;
	};

	struct BindDomainShaderCommand
	{
		DomainShader* pShader;
	};

	struct BindGeometryShaderCommand
	{
		GeometryShader* pShader;
	};

	struct BindPixelShaderCommand
	{
		PixelShader* pShader;
	};

	struct BindComputeShaderCommand
	{
		ComputeShader* pShader;
	};

	struct BindSamplerStateCommand
	{
		ID3D11SamplerState* pSamplerState;
		u8                  bindFlags;
		u8                  slot;
	};

	struct BindBlendStateCommand
	{
		const BlendState* pBlendState;
	};

	struct BindDepthStencilStateCommand
	{
		const DepthStencilState* pDepthStencilState;
	};

	struct BindRasterizerStateCommand
	{
		const RasterizerState* pRasterizerState;
	};

	struct BindViewportCommand
	{
		const Viewport* pViewport;
		uint            targetWidth;
		uint            targetHeight;
	};

	struct BindRenderTargetCommand
	{
		ID3D11RenderTargetView* pRenderTarget;
		u8                      slot;
	};

	struct BindDepthStencilTargetCommand
	{
		ID3D11DepthStencilView* pDepthStencilTarget;
	};

	struct ClearRenderTargetCommand
	{
		ID3D11RenderTargetView* pRenderTarget;
		float                   value;
	};

	struct ClearDepthStencilTargetCommand
	{
		ID3D11DepthStencilView* pDepthStencilTarget;
		float                   value;
	};

	struct UpdateCBufferCommand
	{
		ID3D11Buffer* pCBuffer;
		const void*   pData;
		uint          size;
	};

	struct DrawFullScreenCommand
	{
		const PixelShader* pPixelShader;
	};

	struct DrawSceneEntitiesCommand
	{
		Camera*     pCamera;
		ENTITY_TYPE entitiesType;
		SORT_ORDER  sortOrder;
		uint        context;
	};

	struct DispatchCommandsCommand
	{
		const CommandGroup* pCmdGroup;
	};
};

#endif