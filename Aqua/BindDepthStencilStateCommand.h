#ifndef BINDDEPTHSTENCILSTATECOMMAND_H
#define BINDDEPTHSTENCILSTATECOMMAND_H

#include "Command.h"

#include "DepthStencilState.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindDepthStencilStateCommand : public Command
	{
	public:
		BindDepthStencilStateCommand();
		~BindDepthStencilStateCommand();

		void execute(PipelineManager& pipelineManager);

		void setDepthStencilState(const DepthStencilState* pDepthStencilState);

	private:
		const DepthStencilState* _pDepthStencilState;
	};
};

#endif