#include "BindHullShaderCommand.h"

using namespace Aqua;

BindHullShaderCommand::BindHullShaderCommand()
{
	_pShader = NULL;
}

BindHullShaderCommand::~BindHullShaderCommand()
{
	_pShader = NULL;
}

void BindHullShaderCommand::execute(PipelineManager& pipelineManager)
{
	pipelineManager.bindShader(_pShader);
}

void BindHullShaderCommand::setShader(HullShader* pShader)
{
	_pShader = pShader;
}