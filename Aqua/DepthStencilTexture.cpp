#include "DepthStencilTexture.h"

using namespace Aqua;

DepthStencilTexture::DepthStencilTexture()
{
	_Type = DEPTH_STENCIL_TEXTURE;
}

DepthStencilTexture::~DepthStencilTexture()
{}

ID3D11DepthStencilView* DepthStencilTexture::getDepthStencilTarget() const
{
	return _pDSTarget;
}

const Texture* DepthStencilTexture::getTexture() const
{
	return &_Texture;
}

void DepthStencilTexture::destroy()
{
	SAFE_RELEASE(_pDSTarget);
	_Texture.destroy();
}