#include "Entity.h"

using namespace Aqua;

Entity::Entity(ENTITY_TYPE type, uint name)
{
	_Type = type;
	_Name = name;
}

Entity::~Entity()
{}

ENTITY_TYPE Entity::getType() const
{
	return _Type;
}

uint Entity::getName() const
{
	return _Name;
}