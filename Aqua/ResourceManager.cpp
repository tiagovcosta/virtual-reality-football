#include "ResourceManager.h"

using namespace Aqua;

ResourceManager& getResourceManager()
{
	static ResourceManager manager;
	return manager;
}

ResourceManager::ResourceManager()
{
	_pDevice = NULL;
}

ResourceManager::~ResourceManager()
{} //The destruction of the ResourceManager is done using the shutdown method

void ResourceManager::init(ID3D11Device* pDevice)
{
	_pDevice  = pDevice;

	_pResources.push_back(NULL); //Resource at position 0 is NULL
}

void ResourceManager::shutdown()
{
	for(uint i = 1; i < _pResources.size(); i++)
		_pResources.at(i)->destroy();
}

uint ResourceManager::loadResourcePackage(const std::string& filename)
{
	return 0;
}

const Resource* ResourceManager::getResource(uint id)
{
	return _pResources.at(id);
};

uint ResourceManager::getResourceID(RESOURCE_TYPE type, const char* filename)
{
	uint stringID = getStringID(filename);
	for(uint i = 1; i < _pResources.size(); i++) //Start loop at i=1 because resource at index 0 is NULL
		if( (_pResources.at(i)->getNameID() == stringID) && (_pResources.at(i)->getType() == type) )
			return i;

	//If not try to load it now
	switch(type)
	{
	case CBUFFER:
		return NULL; //CBuffer are not loaded
		break;
	case TEXTURE:
		return loadTexture(filename);
		break;
	case RENDER_TEXTURE:
		return NULL; //Render texture are not loaded, they're created using RenderTextureConfig
		break;
	case DEPTH_STENCIL_TEXTURE:
		return NULL; //Render texture are not loaded, they're created using DepthStencilTextureConfig
		break;
	case EFFECT:
		return loadEffect(filename);
		break;
	case MATERIAL:
		return loadMaterial(filename);
		break;
	case GEOMETRY:
		return loadGeometry(filename);
		break;
	case MODEL:
		return loadModel(filename);
		break;
	default:
		ASSERT(false);
		break;
	}

	return 0;
}

const Resource* ResourceManager::getResource(RESOURCE_TYPE type, const char* filename)
{
	return getResource(getResourceID(type, filename));
}

const ConstantBuffer* ResourceManager::createConstantBuffer(uint size, const char* name)
{
	BufferConfig conf;
	conf.setDefaultConstantBuffer(size, true, true); // Use default constant buffer config

	ID3D11Buffer* pBuffer;

	if(FAILED(_pDevice->CreateBuffer(&conf.getBufferDesc(), NULL, &pBuffer)))
		return NULL;

	#if _DEBUG
		pBuffer->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
	#endif

	ConstantBuffer constantBuffer;
	constantBuffer.setNameID(getStringID(name));
	constantBuffer._pBuffer = pBuffer;

	ConstantBuffer* pCBuffer = getStackAllocator().allocateNew<ConstantBuffer>(constantBuffer);

	_pResources.push_back(pCBuffer);

	return pCBuffer;
}

const RenderTexture* ResourceManager::createRenderTexture(const char* name, const RenderTextureConfig& config)
{
	ID3D11Texture2D* pTexture = NULL;
	ID3D11RenderTargetView* pRTView = NULL;
	ID3D11ShaderResourceView* pSRView = NULL;

	//Create Texture2D/RenderTarget/ShaderResource using the given configs
	if(FAILED(_pDevice->CreateTexture2D(config.getTextureDesc(), 0, &pTexture)))
	{
		return NULL;
	}

	if(FAILED(_pDevice->CreateRenderTargetView(pTexture, config.getRenderTargetDesc(), &pRTView)))
	{
		return NULL;
	}

	if(FAILED(_pDevice->CreateShaderResourceView(pTexture, config.getShaderResourceDesc(), &pSRView)))
	{
		return NULL;
	}

	//Add  a name the Texture/RTV/SRV for debugging purposes
	#if _DEBUG
		pTexture->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
		pRTView->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
		pSRView->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
	#endif

	pTexture->Release();

	Texture texture;
	texture._pView = pSRView;

	RenderTexture renderTexture;

	renderTexture.setNameID(getStringID(name));
	renderTexture._pRenderTarget = pRTView;
	renderTexture._Texture = texture;
	renderTexture._Width = config.getTextureDesc()->Width;
	renderTexture._Height = config.getTextureDesc()->Height;
	renderTexture._Format = config.getFormat();

	//Allocate space for the RenderTexture
	RenderTexture* pRenderTexture = getStackAllocator().allocateNew<RenderTexture>(renderTexture);

	pRenderTexture->increaseRefCount();

	_pResources.push_back(pRenderTexture);

	return pRenderTexture;
}

const DepthStencilTexture* ResourceManager::createDepthStencilTexture(const char* name, const DepthStencilTextureConfig& config)
{
	ID3D11Texture2D* pTexture = NULL;
	ID3D11DepthStencilView* pDSView = NULL;
	ID3D11ShaderResourceView* pSRView = NULL;

	//Create Texture2D/DepthStencilView/ShaderResource using the given configs
	if(FAILED(_pDevice->CreateTexture2D(config.getTextureDesc(), 0, &pTexture)))
	{
		return NULL;
	}

	if(FAILED(_pDevice->CreateDepthStencilView(pTexture, config.getDepthStencilDesc(), &pDSView)))
	{
		return NULL;
	}

	if(config.getShaderResourceDesc())
	{
		if(FAILED(_pDevice->CreateShaderResourceView(pTexture, config.getShaderResourceDesc(), &pSRView)))
		{
			return NULL;
		}
	}

	//Add  a name the Texture/DSV/SRV for debugging purposes
	#if _DEBUG
		pTexture->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
		pDSView->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
		if(pSRView)
			pSRView->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
	#endif

	pTexture->Release();

	Texture texture;
	texture._pView = pSRView;

	DepthStencilTexture depthStencilTexture;
	depthStencilTexture.setNameID(getStringID(name));
	depthStencilTexture._pDSTarget = pDSView;
	depthStencilTexture._Texture = texture;

	//Allocate space for the DepthStencilTexture
	DepthStencilTexture* pDepthStencilTexture = getStackAllocator().allocateNew<DepthStencilTexture>(depthStencilTexture);

	pDepthStencilTexture->increaseRefCount();

	_pResources.push_back(pDepthStencilTexture);

	return pDepthStencilTexture;
}

const Effect* ResourceManager::createEffect(const char* name, const EffectDesc& desc)
{
	Effect effect(desc);
	effect.setNameID(getStringID(name));

	//Allocate space for the Effect
	Effect* pEffect = getStackAllocator().allocateNew<Effect>(effect);

	pEffect->increaseRefCount();

	_pResources.push_back(pEffect);

	return pEffect;
}

const Geometry* ResourceManager::createGeometry(const char* name, const GeometryConfig& config, uint numSubsets, const GeometrySubset* pSubsets)
{
	ID3D11Buffer* pVB;
	ID3D11Buffer* pIB;

	//Create the vertex/index buffers
	if(FAILED(_pDevice->CreateBuffer(&config.getVertexBufferConfig().getBufferDesc(), &config.getVertexBufferData(), &pVB)))
		return NULL;

	if(FAILED(_pDevice->CreateBuffer(&config.getIndexBufferConfig().getBufferDesc(), &config.getIndexBufferData(), &pIB)))
		return NULL;

	//Add  a name the Texture/RTV/SRV for debugging purposes
#if _DEBUG
	pVB->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
	pIB->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(name), name);
#endif

	GeometryDesc desc;
	desc.pVertexBuffer      = pVB;
	desc.vertexBufferStride = config.getVertexBufferStride();
	desc.pIndexBuffer       = pIB;
	desc.indexFormat        = config.getIndexFormat();
	desc.numSubsets         = numSubsets;
	desc.pSubsets           = pSubsets;

	Geometry geometry(desc);

	geometry.setNameID(getStringID(name));

	//Allocate space for the Mesh
	Geometry* pGeometry = getStackAllocator().allocateNew<Geometry>(geometry);

	pGeometry->increaseRefCount();

	_pResources.push_back(pGeometry);

	return pGeometry;
}

const Model* ResourceManager::createModel(const char* name, const ModelDesc& desc)
{
	Model model(desc);

	//Allocate space for the Model
	Model* pModel = getStackAllocator().allocateNew<Model>(model);

	pModel->increaseRefCount();

	_pResources.push_back(pModel);

	return pModel;
}

uint ResourceManager::loadTexture(const char* filename)
{
	//Add the folder path to the filename
	std::stringstream filePath;
	filePath << "../resources/textures/";
	filePath << filename;

	ID3D11ShaderResourceView* pShaderResource = NULL;

	//Create the Shader Resource View
	if(FAILED(D3DX11CreateShaderResourceViewFromFile(_pDevice, filePath.str().c_str(), NULL, NULL, &pShaderResource, NULL)))
	{
		getLogger().write("Unable to load " + std::string(filename) + " texture.", ERROR_MESSAGE);
		return 0;
	}

	//Add a name to the SRV for debugging purposes
	#if _DEBUG
		pShaderResource->SetPrivateData(WKPDID_D3DDebugObjectName, strlen(filename), filename);
	#endif

	Texture texture;
	texture._pView = pShaderResource;
	texture.setNameID(getStringID(filename));

	//Allocate space for the Texture in the Stack Allocator
	Texture* pTexture = getStackAllocator().allocateNew<Texture>(texture);

	pTexture->increaseRefCount();

	_pResources.push_back(pTexture);

	return _pResources.size()-1;
}

uint ResourceManager::loadEffect(const char* filename)
{
	//Add the folder path to the filename
	std::stringstream filePath;
	filePath << "../resources/effects/";
	filePath << filename;

	tinyxml2::XMLDocument doc;
	
	XMLElement* pElement = NULL;

	const char* effectName;
	const char* elementName;

	doc.LoadFile(filePath.str().c_str());

	if(!doc.Error())
	{
		EffectDesc desc;

		pElement = doc.FirstChildElement("Effect");

		effectName = pElement->Attribute("id");

		pElement = pElement->FirstChildElement();

		//Loop through each child element of the Effect node
		for(pElement; pElement; pElement = pElement->NextSiblingElement())
		{
			elementName = pElement->Value();
			
			if(strcmp(elementName, "Shader") == 0)
			{
				uint permutation = 0;
				pElement->QueryUnsignedAttribute("permutation", &permutation);

				if(pElement->Attribute("type", "vertex")) // Compare if attribute 'type' == "vertex"
					desc.pVertexShader = (VertexShader*) getShaderManager().getShader(pElement->Attribute("id"), permutation);
				else if(pElement->Attribute("type", "hull"))
					desc.pHullShader = (HullShader*) getShaderManager().getShader(pElement->Attribute("id"), permutation);
				else if(pElement->Attribute("type", "domain"))
					desc.pDomainShader = (DomainShader*) getShaderManager().getShader(pElement->Attribute("id"), permutation);
				else if(pElement->Attribute("type", "geometry"))
					desc.pGeometryShader = (GeometryShader*) getShaderManager().getShader(pElement->Attribute("id"), permutation);
				else if(pElement->Attribute("type", "pixel"))
					desc.pPixelShader = (PixelShader*) getShaderManager().getShader(pElement->Attribute("id"), permutation);
			}
		}

		createEffect(filename, desc);

		return _pResources.size() - 1;
	}

	getLogger().write("Unable to load " + std::string(filename) + " effect.", ERROR_MESSAGE);

	return 0;
}

uint ResourceManager::loadMaterial(const char* filename)
{
	//Add the folder path to the filename
	std::stringstream filePath;
	filePath << "../resources/materials/";
	filePath << filename;

	const char* materialName;

	tinyxml2::XMLDocument doc;

	doc.LoadFile(filePath.str().c_str());

	if(!doc.Error())
	{
		XMLElement* pElement = doc.FirstChildElement("Material");

		materialName = pElement->Attribute("id");

		std::vector<MaterialContextDesc> contextsDesc;

		uint counter = 0;

		pElement = pElement->FirstChildElement("Context");

		//Loop through each Pass node
		for(pElement; pElement; pElement = pElement->NextSiblingElement("Context"))
		{
			MaterialContextDesc desc;
			desc.effect = dynamic_cast<const Effect*>(getResource(EFFECT, pElement->Attribute("effect")));
			desc.contextID = getStringID(pElement->Attribute("id"));

			XMLElement* pPassChild = pElement->FirstChildElement("Texture");
			
			//Loop through each Texture node
			for(pPassChild; pPassChild; pPassChild = pPassChild->NextSiblingElement("Texture"))
			{
				MaterialTexture texture;
				texture.pTexture = dynamic_cast<const Texture*>(getResource(TEXTURE, pPassChild->Attribute("id")));
			
				if(pPassChild->Attribute("type", "diffuseMap"))
				{
					texture.slot = MATERIAL_TEXTURE_SLOT::DIFFUSE_MAP;
					texture.bindFlags = MATERIAL_TEXTURE_BIND_FLAG::PIXEL_SHADER;
				} else if(pPassChild->Attribute("type", "normalMap"))
				{
					texture.slot = MATERIAL_TEXTURE_SLOT::NORMAL_MAP;
					texture.bindFlags = MATERIAL_TEXTURE_BIND_FLAG::PIXEL_SHADER;
				} else if(pPassChild->Attribute("type", "specularMap"))
				{
					texture.slot = MATERIAL_TEXTURE_SLOT::SPECULAR_MAP;
					texture.bindFlags = MATERIAL_TEXTURE_BIND_FLAG::PIXEL_SHADER;
				} else if(pPassChild->Attribute("type", "heightMap"))
				{
					texture.slot = MATERIAL_TEXTURE_SLOT::HEIGHT_MAP;
					texture.bindFlags = MATERIAL_TEXTURE_BIND_FLAG::PIXEL_SHADER;
				}

				desc.textures.push_back(texture);
			}

			contextsDesc.push_back(desc);
		}

		Material material(contextsDesc.size(), &contextsDesc.at(0));
		material.setNameID(getStringID(materialName));

		Material* pMaterial = getStackAllocator().allocateNew<Material>(material);

		_pResources.push_back(pMaterial);

		return _pResources.size()-1;
	}

	getLogger().write("Unable to load " + std::string(filename) + " material.", ERROR_MESSAGE);
	
	return 0;
}

uint ResourceManager::loadGeometry(const char* filename)
{
	//Add the folder path to the filename
	std::stringstream filePath;
	filePath << "../resources/geometry/";
	filePath << filename;

	PTNTCVertex*     vertices;
	WORD*            indices;
	GeometrySubset*  pSubsets;

	File file;
	if(file.open(filePath.str().c_str()))
	{
		while(file.nextWord().compare("batch") != 0); //Ignore file format headers

		int numVertices = file.nextInt();

		vertices = new PTNTCVertex[numVertices];

		PTNTCVertex vertex;

		for(int i = 0; i < numVertices; i++)
		{
			file.nextWord(); // vertex
			vertex.pos.x = file.nextFloat();
			vertex.pos.y = file.nextFloat();
			vertex.pos.z = file.nextFloat();
			file.nextWord(); //tangent
			vertex.tangent.x = file.nextFloat();
			vertex.tangent.y = file.nextFloat();
			vertex.tangent.z = file.nextFloat();
			file.nextWord(); //normal
			vertex.normal.x = file.nextFloat();
			vertex.normal.y = file.nextFloat();
			vertex.normal.z = file.nextFloat();
			file.nextWord(); //uv
			vertex.texC.x = file.nextFloat();
			vertex.texC.y = file.nextFloat();

			vertices[i] = vertex;
		}

		//std::string str = file.nextWord(); // triangleList
		while(file.nextWord().compare("triangleList") != 0); //Ignore file format headers
		int numIndices = file.nextInt()*3;

		indices = new WORD[numIndices];

		for(int i = 0; i < numIndices; i++)
			indices[i] = file.nextInt();

		file.nextWord(); //subsets
		uint numSubsets = file.nextUnsigned();

		pSubsets = new GeometrySubset[numSubsets];

		for(uint i = 0; i < numSubsets; i++)
		{
			file.nextWord(); //startIndex
			pSubsets[i].startIndex = file.nextUnsigned();
			file.nextWord(); //numIndices
			pSubsets[i].indexCount = file.nextUnsigned();
		}

		GeometryConfig config;
		config.setVertexBufferData(vertices, sizeof(PTNTCVertex)*numVertices, sizeof(PTNTCVertex));
		config.setIndexBufferData(indices, sizeof(WORD)*numIndices);

		createGeometry(filename, config, numSubsets, pSubsets);

		delete[] vertices;
		delete[] indices;
		delete[] pSubsets;

		file.close();

		return _pResources.size()-1;
	}

	getLogger().write("Unable to load " + std::string(filename) + " geometry.", ERROR_MESSAGE);
	return 0;
}

uint ResourceManager::loadModel(const char* filename)
{
	//Add the folder path to the filename
	std::stringstream filePath;
	filePath << "../resources/models/";
	filePath << filename;

	tinyxml2::XMLDocument doc;
	
	XMLElement* pElement = NULL;

	const char* elementName;

	doc.LoadFile(filePath.str().c_str());

	if(!doc.Error())
	{
		ModelDesc desc;

		pElement = doc.FirstChildElement("Model");

		pElement->QueryUnsignedAttribute("numSubsets", &desc.numSubsets);
		desc.pSubsetsDesc = new ModelSubsetDesc[desc.numSubsets];

		pElement = pElement->FirstChildElement();

		//Loop through each child of the Model node
		for(pElement; pElement; pElement = pElement->NextSiblingElement())
		{
			elementName = pElement->Value();
			
			if(strcmp(elementName, "Geometry") == 0)
			{
				const char* geometryName = pElement->Attribute("id");
				desc.pGeometry = dynamic_cast<const Geometry*>(getResource(GEOMETRY, geometryName));
			} else if(strcmp(elementName, "Subset") == 0)
			{
				uint      subsetNum;

				pElement->QueryUnsignedAttribute("num", &subsetNum);

				ASSERT((subsetNum < desc.numSubsets));

				pElement->QueryUnsignedAttribute("geometrySubsetIndex", &(desc.pSubsetsDesc[subsetNum].geometrySubset));

				const char* materialName = pElement->Attribute("material");

				desc.pSubsetsDesc[subsetNum].pMaterial = dynamic_cast<const Material*>(getResource(MATERIAL, materialName));
			}
		}

		createModel(filename, desc);

		delete[] desc.pSubsetsDesc;

		return _pResources.size()-1;
	}

	getLogger().write("Unable to load " + std::string(filename) + " model.", ERROR_MESSAGE);

	return 0;
}