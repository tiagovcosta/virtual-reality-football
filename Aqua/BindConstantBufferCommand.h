#ifndef BINDCONSTANTBUFFERCOMMAND_H
#define BINDCONSTANTBUFFERCOMMAND_H

#include <D3DX11.h>

#include "AquaTypedefs.h"

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindConstantBufferCommand : public Command
	{
	public:
		BindConstantBufferCommand();
		~BindConstantBufferCommand();

		void execute(PipelineManager& pipelineManager);

		void setConstantBuffer(ID3D11Buffer* pConstantBuffer);
		void setSlot(uint slot);
		void setBindFlags(u8 bindFlags);

	private:
		ID3D11Buffer* _pConstantBuffer;
		uint    _Slot;
		u8      _BindFlags;
	};
};

#endif