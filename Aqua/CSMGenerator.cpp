#include "CSMGenerator.h"

#include <math.h>

using namespace Aqua;

CSMGenerator::CSMGenerator()
{

}

CSMGenerator::~CSMGenerator()
{

}

void CSMGenerator::init()
{
	for(uint i = 0; i < NUM_CASCADES; i++)
		_Viewports[i].setSize(i * (1.0f/NUM_CASCADES), 0.0f, 1.0f/NUM_CASCADES, 1.0f);

	DepthStencilTextureConfig dstConfig;
	dstConfig.setDefaultShadowMap(SHADOW_MAP_SIZE * NUM_CASCADES, SHADOW_MAP_SIZE);

	_pCascadesDST = getResourceManager().createDepthStencilTexture("cascadedShadowMapDST", dstConfig);

	//Gather cascades pass
	_pCascadeGatherCB = getResourceManager().createConstantBuffer(sizeof(CascadeGatherCBData), "CascadeGatherCB");

	_GatherViewport.setSize(0.0f, 0.0f, 1.0f, 1.0f);

	RenderTextureConfig rtConfig;
	rtConfig.setDefaultRenderTexture(RT_R8, 1600, 900);

	_pDeferredShadowMapRT = getResourceManager().createRenderTexture("deferredShadowMapRT", rtConfig);

	//Load shaders
	_pDeferredShadowMapEffect = dynamic_cast<const Effect*>(getResourceManager().getResource(EFFECT, "DeferredShadowMapFX.effect.xml"));

	//Get Depth Map RT;
	_pDepthMapRT = dynamic_cast<const RenderTexture*>(getResourceManager().getResource(RENDER_TEXTURE, "depthMap"));

	//Create command group
	CommandGroupWriter cmdWriter;
	cmdWriter.begin(&getStackAllocator());
	cmdWriter.addBindDepthStencilStateCommand(NULL);
	cmdWriter.addBindBlendStateCommand(NULL);

	cmdWriter.addBindRenderTargetCommand(NULL, 0);
	cmdWriter.addClearDepthStencilTargetCommand(_pCascadesDST->getDepthStencilTarget(), 1.0f);
	cmdWriter.addBindDepthStencilTargetCommand(_pCascadesDST->getDepthStencilTarget());

	for(uint i = 0; i < NUM_CASCADES; i++)
	{
		cmdWriter.addBindViewportCommand(&_Viewports[i], SHADOW_MAP_SIZE*NUM_CASCADES, SHADOW_MAP_SIZE);
		cmdWriter.addDrawSceneEntitiesCommand(&_ShadowCameras[i], ACTOR, BACK_TO_FRONT, getStringID("ShadowMap"));
	}

	cmdWriter.addBindRenderTargetCommand(_pDeferredShadowMapRT->getRenderTarget(), 0);
	cmdWriter.addBindDepthStencilTargetCommand(NULL);
	cmdWriter.addBindViewportCommand(&_GatherViewport, _pDeferredShadowMapRT->getWidth(), _pDeferredShadowMapRT->getHeight());
	
	cmdWriter.addBindTextureCommand(_pDepthMapRT->getTexture(), 1, PIXEL_STAGE);
	cmdWriter.addBindTextureCommand(_pCascadesDST->getTexture(), 2, PIXEL_STAGE);

	cmdWriter.addUpdateCBufferCommand(_pCascadeGatherCB->getBuffer(), &_CascadeGatherCBData, sizeof(CascadeGatherCBData));
	cmdWriter.addBindConstantBuffeCommand(_pCascadeGatherCB->getBuffer(), 2, PIXEL_STAGE);

	cmdWriter.addDispatchCommandsCommand(_pDeferredShadowMapEffect->getCommandGroup());
	cmdWriter.addDrawCommand(3, 0);

	cmdWriter.addBindTextureCommand(NULL, 1, PIXEL_STAGE);

	_pCommandGroup = cmdWriter.end();
}

CommandGroup* CSMGenerator::execute(const Scene* pScene, const Camera* pCamera, const D3DXVECTOR3& lightDir, const Texture** pOutTexture)
{
	//Update shadow cameras
	for(uint i = 0; i < NUM_CASCADES; i++)
	{
		if(i == 0)
		{
			_ShadowCameras[i] = generateCascadeCamera(lightDir, pCamera, 0.0f, 0.025f);
			_CascadeGatherCBData.cascadesStart[i] = 0.0f;
		} else if(i == 1)
		{
			_ShadowCameras[i] = generateCascadeCamera(lightDir, pCamera, 0.025f, 0.075f);
			_CascadeGatherCBData.cascadesStart[i] = 0.025f;
		} else if(i == 2)
		{
			_ShadowCameras[i] = generateCascadeCamera(lightDir, pCamera, 0.075f, 0.2f);
			_CascadeGatherCBData.cascadesStart[i] = 0.075f;
		} else
		{
			_ShadowCameras[i] = generateCascadeCamera(lightDir, pCamera, 0.20f, 1.0f);
			_CascadeGatherCBData.cascadesStart[i] = 0.20f;
		}

		_CascadeGatherCBData.cascadeMatrices[i] = _ShadowCameras[i].getViewProj();
	}

	*pOutTexture = _pDeferredShadowMapRT->getTexture();

	return _pCommandGroup;
}

float round2(float number)
{
	return number < 0.0f ? ceilf(number - 0.5f) : floorf(number + 0.5f);
}

Camera CSMGenerator::generateCascadeCamera(const D3DXVECTOR3& lightDir, const Camera* pCamera, float start, float end)
{
	D3DXVECTOR2 clipDistances = pCamera->getClipDistances();

	D3DXVECTOR3 corners[8];
	calculateCascadeCorners(pCamera, start*clipDistances.y, end*clipDistances.y, corners);

	D3DXVECTOR3 center;
	float radius;

	D3DXComputeBoundingSphere(corners, 8, sizeof(D3DXVECTOR3), &center, &radius);

	D3DXVECTOR3 shadowCamPos = center + lightDir * (-10.0f);

	Camera camera;
	camera.setPosition(shadowCamPos.x, shadowCamPos.y, shadowCamPos.z);

	D3DXVECTOR3 lookDir = center - shadowCamPos;

	camera.setLookDirection(lookDir.x, lookDir.y, lookDir.z);
	camera.setLens(-radius, radius, radius, -radius, 0.1f, 10.0f + radius);
	camera.update();

	D3DXVECTOR4 shadowOrigin = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);
	D3DXMATRIX shadowMatrix = camera.getViewProj();

	D3DXVec4Transform(&shadowOrigin, &shadowOrigin, &shadowMatrix);

	shadowOrigin *= (SHADOW_MAP_SIZE / 2.0f);
	D3DXVECTOR2 roundedOrigin = D3DXVECTOR2(round2(shadowOrigin.x), round2(shadowOrigin.y));
	D3DXVECTOR2 rounding = roundedOrigin - D3DXVECTOR2(shadowOrigin.x, shadowOrigin.y);
	rounding /= (SHADOW_MAP_SIZE / 2.0f);

	D3DXMATRIX roundMatrix;
	D3DXMatrixTranslation(&roundMatrix, rounding.x, rounding.y, 0.0f);

	camera._ViewProj *= roundMatrix;

	/*shadowCamPos += D3DXVECTOR3(rounding.x, rounding.y, 0.0f);

	camera.setPosition(shadowCamPos.x, shadowCamPos.y, shadowCamPos.z);

	lookDir = center - shadowCamPos;

	camera.setLookDirection(lookDir.x, lookDir.y, lookDir.z);
	camera.update();*/

	//shadowOrigin = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f);
	//shadowMatrix = camera.getViewProj();

	//D3DXVec4Transform(&shadowOrigin, &shadowOrigin, &shadowMatrix);
	//shadowOrigin *= (SHADOW_MAP_SIZE / 2.0f);

	return camera;
}

void CSMGenerator::calculateCascadeCorners(const Camera* pCamera, float cascadeBegin, float cascadeEnd, D3DXVECTOR3* corners)
{
	float Hnear = 2 * tan(pCamera->getFOV() / 2) * cascadeBegin;
	float Wnear = Hnear * pCamera->getRatio();

	D3DXVECTOR3 nearCenter = pCamera->getPosition() + pCamera->getLookDirection() * cascadeBegin;

	corners[0] = nearCenter + (pCamera->getUp() * Hnear/2) - (pCamera->getRight() * Wnear/2);
	corners[1] = nearCenter + (pCamera->getUp() * Hnear/2) + (pCamera->getRight() * Wnear/2);
	corners[2] = nearCenter - (pCamera->getUp() * Hnear/2) - (pCamera->getRight() * Wnear/2);
	corners[3] = nearCenter - (pCamera->getUp() * Hnear/2) + (pCamera->getRight() * Wnear/2);

	float Hfar = 2 * tan(pCamera->getFOV() / 2) * cascadeEnd;
	float Wfar = Hfar * pCamera->getRatio();

	D3DXVECTOR3 farCenter = pCamera->getPosition() + pCamera->getLookDirection() * cascadeEnd;

	corners[4] = farCenter + (pCamera->getUp() * Hfar/2) - (pCamera->getRight() * Wfar/2);
	corners[5] = farCenter + (pCamera->getUp() * Hfar/2) + (pCamera->getRight() * Wfar/2);
	corners[6] = farCenter - (pCamera->getUp() * Hfar/2) - (pCamera->getRight() * Wfar/2);
	corners[7] = farCenter - (pCamera->getUp() * Hfar/2) + (pCamera->getRight() * Wfar/2);
}

D3DXVECTOR3 CSMGenerator::vectorMin(const D3DXVECTOR3& v1, const D3DXVECTOR3& v2)
{
	D3DXVECTOR3 result;

	result.x = (v1.x < v2.x) ? v1.x : v2.x;
	result.y = (v1.y < v2.y) ? v1.y : v2.y;
	result.z = (v1.z < v2.z) ? v1.z : v2.z;

	return result;
}

D3DXVECTOR3 CSMGenerator::vectorMax(const D3DXVECTOR3& v1, const D3DXVECTOR3& v2)
{
	D3DXVECTOR3 result;

	result.x = (v1.x > v2.x) ? v1.x : v2.x;
	result.y = (v1.y > v2.y) ? v1.y : v2.y;
	result.z = (v1.z > v2.z) ? v1.z : v2.z;

	return result;
}