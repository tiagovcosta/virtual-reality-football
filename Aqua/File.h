#ifndef FILE_H
#define FILE_H

#include <fstream>
#include <string>
#include <vector>

#include "AquaTypedefs.h"

namespace Aqua
{
	class File
	{
	public:
		int open(const char* filename);
		void close();

		uint getLength();

		const std::string& nextWord();
		int nextInt();
		float nextFloat();
		uint nextUnsigned();

	private:
		std::ifstream _InputStream;

		std::vector<std::string> _Words;
	};
};

#endif