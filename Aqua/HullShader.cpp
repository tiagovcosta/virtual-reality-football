#include "HullShader.h"

using namespace Aqua;

HullShader::HullShader(ID3D11HullShader* pShader)
{
	_pShader = pShader;
	_Type = HULL_SHADER;
}

HullShader::~HullShader()
{}

void HullShader::destroy()
{
	//getLogger().Write(L"Shader: \"" + _FileName + L"\" destroyed.");
	SAFE_RELEASE(_pShader);
}

ID3D11HullShader* HullShader::getShader() const
{
	return _pShader;
}