#ifndef GEOMETRYSHADER_H
#define GEOMETRYSHADER_H

#include "PCH.h"

#include "Shader.h"

namespace Aqua
{
	class GeometryShader : public Shader
	{
	public:
		GeometryShader(ID3D11GeometryShader* pShader);
		~GeometryShader();

		void destroy();

		ID3D11GeometryShader* getShader() const;

	private:
		ID3D11GeometryShader* _pShader;
	};
};

#endif