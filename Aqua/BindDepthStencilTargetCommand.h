#ifndef BINDDEPTHSTENCILTARGET_H
#define BINDDEPTHSTENCILTARGET_H

#include "Command.h"

#include "PipelineManager.h"

namespace Aqua
{
	class BindDepthStencilTargetCommand : public Command
	{
	public:
		BindDepthStencilTargetCommand();
		~BindDepthStencilTargetCommand();

		void execute(PipelineManager& pipelineManager);

		void setDepthStencilTarget(ID3D11DepthStencilView* pDepthStencilTarget);

	private:
		ID3D11DepthStencilView* _pDepthStencilTarget;
	};
};

#endif