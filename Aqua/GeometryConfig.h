#ifndef MESHCONFIG_H
#define MESHCONFIG_H

#include "PCH.h"

#include "BufferConfig.h"

#include "InputAssemblerStage.h"

namespace Aqua
{
	class GeometryConfig
	{
	public:
		GeometryConfig();
		~GeometryConfig();

		void                          setVertexBufferData(void* pData, uint size, uint vertexBufferStride);
		void                          setIndexBufferData(void* pData, uint size, INDEX_BUFFER_FORMAT indexFormat = UINT16_INDEX);

		const BufferConfig&           getVertexBufferConfig() const;
		const BufferConfig&           getIndexBufferConfig() const;

		uint                          getVertexBufferStride() const;
		INDEX_BUFFER_FORMAT           getIndexFormat() const;

		const D3D11_SUBRESOURCE_DATA& getVertexBufferData() const;
		const D3D11_SUBRESOURCE_DATA& getIndexBufferData() const;
	private:
		BufferConfig           _VBConfig;
		D3D11_SUBRESOURCE_DATA _VBData;
		uint                   _VertexBufferStride;

		BufferConfig           _IBConfig;
		D3D11_SUBRESOURCE_DATA _IBData;
		INDEX_BUFFER_FORMAT    _IndexFormat;
	};
};

#endif