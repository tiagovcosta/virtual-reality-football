#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <Windows.h>

#include "AquaTypedefs.h"

namespace Aqua
{
	struct MousePos
	{
		MousePos()
		{
			x = 0;
			y = 0;
		}
		MousePos(int _x, int _y)
		{
			x = _x;
			y = _y;
		}
		int x;
		int y;
	};

	class InputManager
	{
	public:
		InputManager();
		~InputManager();

		void init(HWND hWnd, uint wndWidth, uint wndHeight);

		//returns whatever the key has been pressed or not since the last call
		int keyPressed(char c);

		//returns the mouse position
		POINT getMousePos();
		
		//returns the variation of the mousepos since last call
		POINT getMousePosVariation();

		void moveMouseTo(int x, int y);

		void setMousePos(int x, int y);
	private:
		HWND _hWnd;
		uint _WndWidth;
		uint _WndHeight;
		POINT _LastMousePos;
		POINT _CurrMousePos;
	};
};

Aqua::InputManager& getInputManager();

#endif