#include "DomainShader.h"

using namespace Aqua;

DomainShader::DomainShader(ID3D11DomainShader* pShader)
{
	_pShader = pShader;
	_Type = DOMAIN_SHADER;
}

DomainShader::~DomainShader()
{}

void DomainShader::destroy()
{
	//getLogger().Write(L"Shader: \"" + _FileName + L"\" destroyed.");
	SAFE_RELEASE(_pShader);
}

ID3D11DomainShader* DomainShader::getShader() const
{
	return _pShader;
}