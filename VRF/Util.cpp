
#include "Util.h"

#include <math.h>

bool NearlyEqual(float f1, float f2, double epsilon /*= 0.00001*/)
{
	return (fabs(f1 - f2) < epsilon);
}


OMBoneMatrix D3DtoOMMatrix(const D3DXMATRIX& mat)
{
	OMBoneMatrix out;

	for(int i = 0; i<4; ++i)
		for(int j = 0; j<4; ++j)
			out.m[i][j] = mat.m[i][j];

	return out;
}

D3DXMATRIX OMtoD3DMatrix(const OMBoneMatrix & mat)
{
	D3DXMATRIX out;

	for(int i = 0; i<4; ++i)
		for(int j = 0; j<4; ++j)
			out.m[i][j] = mat.m[i][j];

	return out;
}