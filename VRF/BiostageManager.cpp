#include "BiostageManager.h"

#include "Util.h"

#include <string>
#include <cassert>

BiostageManager::BiostageManager()
{
	_client      = nullptr;
	_frame       = nullptr;
	_time_handle = nullptr;
	_bone_handle = nullptr;

	_bone_count                 = 0;
	_current_bone_matrices_raw  = nullptr;
	_current_bone_matrices_conv = nullptr;
	_tstance_matrices           = nullptr;
	_bone_dims                  = nullptr;
	_parent_indices             = nullptr;

	_at_least_one_bone_set      = false;
	_is_tracking                = false;
}

BiostageManager::~BiostageManager()
{

}

bool BiostageManager::init(const char* ip)
{
	_client = COMSDKFactory::CreateOMClient();

	if(_client->Connect(ip) != OMOK)
	{
		return false;
	}

	_config = COMSDKFactory::CreateOMStreamConfig();
	_bone_handle = _config->AddBoneData();
	_time_handle = _config->AddTimeData();

	int handle_count = _config->GetHandleCount();

	if(handle_count == 0)
	{
		COMSDKFactory::DestroyOMStreamConfig(_config);
		_config = nullptr;

		return false;
	}

	IOMHandle* handles[10];
	_config->GetHandlesAll(handles);

	//Create Stream

	OMRESULT r = _client->CreateStream(_config, _stream_id);
	if(r != OMOK)
	{
		return false;
	}

	IOMStream* stream = _client->GetStream(_stream_id);
	_frame = stream->GetFrame();

	stream->StartStream();

	if(!setupSkeleton())
	{
		return false;
	}

	return true;
}

void BiostageManager::update()
{
	//=========================================================================
	//	OMSDK Calls
	//		Typically these are better done on a separate network thread.
	//=========================================================================
	int num_bones = 0;

	//::QueryPerformanceCounter(&m_liDataGatherStart);

	OMRESULT res;
	//see if there's a new frame available
	if((_frame != NULL) && ((res = _frame->Update())>0))
	{
		//bNewData = TRUE;

		//time data
		/*IOMTimeDataReaderList * pTimeDataReaderList = NULL;
		pTimeDataReaderList = _frame->TimeData();
		if(pTimeDataReaderList != NULL)
		{

			unsigned int iFrameId = 0;
			__int64 timestamp;
			__int64 timestampFreq;
			(*pTimeDataReaderList)->GetAllTimeInfo(iFrameId, timestamp, timestampFreq);


			m_uiLastFrameId = m_uiFrameId;
			m_uiFrameId = iFrameId;

			//if we can calculate it, figure out the time from the last obtained frame,
			//to the current obtained frame.
			//don't calculate if:
			bool bFrameToFrameTimeInValid = ((m_liLastDataTimeStamp == -1) ||
				(iFrameId == 0) ||
				(m_liLastDataTimeStamp>timestamp));

			if(bFrameToFrameTimeInValid){
				dFrameToFrameTimeLastToCurrentMs = 0.0;
			}
			else
			{
				double dTimeDiffS = double(timestamp - m_liLastDataTimeStamp) / double(timestampFreq);
				//this is the value used later
				dFrameToFrameTimeLastToCurrentMs = dTimeDiffS * 1000.0;

				int iNumFrames = m_uiFrameId - m_uiLastFrameId;
				double dDiffPerFrameMs = dFrameToFrameTimeLastToCurrentMs / double(iNumFrames);

			}
			//save as lastdatatimestamp _after_ we've calculated with it
			m_liLastDataTimeStamp = timestamp;
		}*/


		//get bone data
		IOMBoneDataReaderList * bone_data_reader_list = _frame->BoneMatrixData();
		if(bone_data_reader_list != NULL)
		{
			_is_tracking = (*bone_data_reader_list)->IsTracking() == OMTRUE ? true : false;

			int num_bones_read = (*bone_data_reader_list)->GetBoneCount();
			setBoneCount(num_bones_read);

			if(_current_bone_matrices_raw != NULL)
			{
				(*bone_data_reader_list)->GetAllBoneMatrices(_current_bone_matrices_raw, _bone_count);
				_at_least_one_bone_set = true;

				//fill in absolute matrices buffer
				if(_current_bone_matrices_conv != NULL)
				{
					//==============================================================
					//	Relative to Absolute matrix conversion
					//==============================================================
					//Multiply the hierarchy out here, so we have both.
					//NOTE that we do the multiply even when the matrices from the server are absolute
					//In this case the m_pCurrentBoneMatricesConv will contain useless data
					for(int i = 0; i < _bone_count; ++i)
					{
						//as the documentation says, at each iteration we can assume that 
						//the absolute matrix of our parent has been calculated, since the matrix buffer
						//is ordered based on the hierarchy.
						int parent_index = _parent_indices[i];

						if(parent_index < 0)
						{
							//this is the root, and the root abs matrix is the same as root rel matrix
							_current_bone_matrices_conv[i] = _current_bone_matrices_raw[i];
						}
						else
						{
							D3DXMATRIX curr_abs;
							D3DXMATRIX parent_abs = OMtoD3DMatrix(_current_bone_matrices_conv[parent_index]);
							D3DXMATRIX curr_rel = OMtoD3DMatrix(_current_bone_matrices_raw[i]);

							curr_abs = curr_rel * parent_abs;

							_current_bone_matrices_conv[i] = D3DtoOMMatrix(curr_abs);
						}

					}
				}
			}
		}

		_frame->Release();

		//::QueryPerformanceCounter(&m_liDataGatherEnd);
	}
	else
	{
		Sleep(1);
	}
}

bool BiostageManager::hasBoneData()
{
	return _at_least_one_bone_set;
}

unsigned int BiostageManager::boneCount()
{
	return (unsigned int)_bone_count;
}

OMBoneMatrix* BiostageManager::getCurrentBoneMatrices()
{
	return _current_bone_matrices_conv;
}

OMBoneDimension* BiostageManager::getBoneDimensions()
{
	return _bone_dims;
}

bool BiostageManager::setupSkeleton()
{
	IOMSkeletonControl * _skeleton_control = NULL;

	OMRESULT res = _client->GetSkeletonControl(&_skeleton_control);

	if(res != OMOK){
		return false;
	}

	int bone_count = 0;
	res = _skeleton_control->GetBoneCount(&bone_count);
	if(res != OMOK){
		return false;
	}

	setBoneCount(bone_count);

	int bone_count_out = bone_count;
	res = _skeleton_control->GetBoneSizeList(_bone_dims, bone_count, &bone_count_out);
	if(res != OMOK){
		return false;
	}

	//this shouldn't happen, but we'd like to know if it does.
	assert(bone_count == bone_count_out);

	//numBones is modified by the function; reset it before the next call.
	res = _skeleton_control->GetBoneParentList(_parent_indices, bone_count, &bone_count_out);
	if(res != OMOK){
		return false;
	}

	res = _skeleton_control->GetTStanceMatrices(_tstance_matrices, bone_count, &bone_count_out);
	if(res != OMOK){
		return false;
	}
	//GetTtanceMatrices() outputs relative matrices so multiply them out
	//start at 0 since the root is relative and absolute
	for(int i = 1; i < bone_count; ++i)
	{
		//as the documentation says, at each iteration we can assume that 
		//the absolute matrix of our parent has been calculated, since the matrix buffer
		//is ordered based on the hierarchy. This also means we can do it in-place, as we do here.
		int parent_index = _parent_indices[i];
		//we start after the root, so this should never happen
		assert(parent_index >= 0);

		D3DXMATRIX currAbs;
		D3DXMATRIX parentAbs = OMtoD3DMatrix(_tstance_matrices[parent_index]);
		D3DXMATRIX currRel = OMtoD3DMatrix(_tstance_matrices[i]);
		currAbs = currRel * parentAbs;
		_tstance_matrices[i] = D3DtoOMMatrix(currAbs);
	}

	//this shouldn't happen, but we'd like to know if it does.
	assert(bone_count == bone_count_out);

	return true;
}

void BiostageManager::setBoneCount(int num_bones)
{
	if(_bone_count == num_bones)
		return;

	_bone_count = num_bones;

	if(_current_bone_matrices_raw != nullptr)
		delete[] _current_bone_matrices_raw;

	if(_current_bone_matrices_conv != nullptr)
		delete[] _current_bone_matrices_conv;

	if(_tstance_matrices != nullptr)
		delete[] _tstance_matrices;

	if(_parent_indices != nullptr)
		delete[] _parent_indices;

	if(_bone_dims != nullptr)
		delete[] _bone_dims;

	if(_bone_count > 0)
	{
		_current_bone_matrices_raw = new OMBoneMatrix[num_bones];
		memset(_current_bone_matrices_raw, 0, num_bones*sizeof(OMBoneMatrix));

		_current_bone_matrices_conv = new OMBoneMatrix[num_bones];
		memset(_current_bone_matrices_conv, 0, num_bones*sizeof(OMBoneMatrix));

		_parent_indices = new int[num_bones];
		memset(_parent_indices, 0, num_bones*sizeof(int));

		_bone_dims = new OMBoneDimension[num_bones];
		memset(_bone_dims, 0, num_bones*sizeof(OMBoneDimension));

		_tstance_matrices = new OMBoneMatrix[num_bones];
		memset(_tstance_matrices, 0, num_bones*sizeof(OMBoneMatrix));
	}

	_at_least_one_bone_set = false;
}