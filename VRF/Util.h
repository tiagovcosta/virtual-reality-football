#pragma once

#include <PCH.h>

#include <d3d11.h>

#include <OMSDKTypes.h>

bool NearlyEqual(float f1, float f2, double epsilon = 0.00001);

OMBoneMatrix D3DtoOMMatrix(const D3DXMATRIX& mat);

D3DXMATRIX OMtoD3DMatrix(const OMBoneMatrix & mat);