#include <Aqua.h>
#include <InputManager.h>

#include <GBufferPass.h>
#include <LightingPass.h>
#include <SkyDynamic.h>
#include <MaterialPass.h>

#include <CSMGenerator.h>

#include "BiostageManager.h"
#include <PhysicsManager.h>

#include <UI.h>

#include <lua.hpp>

static float GOAL_WIDTH = 7.0f;
static float GOAL_HEIGHT = 2.5f;
static float GOAL_DEPTH = 0.2f;

//#define HMD

/////////////////////////////////////////////////////////////////////////////////////////////
///////////////// Tiago Costa, 2014              
/////////////////////////////////////////////////////////////////////////////////////////////

using namespace Aqua;

D3DXVECTOR3    bones_translations[21];
D3DXQUATERNION bones_rotations[21];
D3DXVECTOR3    bones_scales[21];

char message[100];

int getBoneTranslation(lua_State* state)
{
	int index = luaL_checkint(state, 1);

	luaL_argcheck(state, index >= 0 && index < 21, 1, "Invalid index");

	lua_pushlightuserdata(state, &bones_translations[index]);

	return 1;
}

int getBoneRotation(lua_State* state)
{
	int index = luaL_checkint(state, 1);

	luaL_argcheck(state, index >= 0 && index < 21, 1, "Invalid index");

	lua_pushlightuserdata(state, &bones_rotations[index]);

	return 1;
}

int getBoneScale(lua_State* state)
{
	int index = luaL_checkint(state, 1);

	luaL_argcheck(state, index >= 0 && index < 21, 1, "Invalid index");

	lua_pushlightuserdata(state, &bones_scales[index]);

	return 1;
}

int write(lua_State* state)
{
	const char* str = luaL_checkstring(state, 1);

	strcpy(message, str);

	return 0;
}

int getX(lua_State* state)
{
	int type = lua_type(state, -1);
	if(!lua_islightuserdata(state, -1))
	{
		luaL_error(state, "Invalid vector address");
	}

	D3DXVECTOR3* vec = (D3DXVECTOR3*) lua_touserdata(state, -1);

	lua_pushnumber(state, vec->x);

	return 1;
}

int getY(lua_State* state)
{
	if(!lua_islightuserdata(state, -1))
	{
		luaL_error(state, "Invalid vector address");
	}

	D3DXVECTOR3* vec = (D3DXVECTOR3*)lua_touserdata(state, -1);

	lua_pushnumber(state, vec->y);

	return 1;
}

int getZ(lua_State* state)
{
	if(!lua_islightuserdata(state, -1))
	{
		luaL_error(state, "Invalid vector address");
	}

	D3DXVECTOR3* vec = (D3DXVECTOR3*)lua_touserdata(state, -1);

	lua_pushnumber(state, vec->z);

	return 1;
}

int getW(lua_State* state)
{
	if(lua_islightuserdata(state, -1))
	{
		luaL_error(state, "Invalid vector address");
	}

	D3DXVECTOR4* vec = (D3DXVECTOR4*)lua_touserdata(state, -1);

	lua_pushnumber(state, vec->w);

	return 1;
}

class BasicExample : public AquaGame
{
public:
	BasicExample(LPCSTR gameTitle, HINSTANCE hInstance, int showCmd, int width, int hiehgt, int nSamples, int samplesQuality);
	~BasicExample();

	void init();
	void update(float dt);
	void shutdown();

private:
	GBufferPass  _GBufferPass;
	LightingPass _LightingPass;
	SkyDynamic   _SkyDynamicPass;
	MaterialPass _MaterialPass;

	CSMGenerator _CSMGenerator;

	Scene        _Scene;
	Camera       _Camera1;
	Camera       _Camera2;

	DirLight*    _pSun;

	BiostageManager _biostage;
	PhysicsManager _physics;

	Actor*                 _bones[21];
	OMBoneDimension        _bone_dimensions[21];
	physx::PxRigidDynamic* _bones_physics_actors[21];

	Actor*                 _ball;
	physx::PxRigidDynamic* _ball_physics;

	bool _reseting_ball;
	bool _left_camera;

	double _dt_since_shift;

	bool k;

	Actor* _trigger;

	lua_State* _lua_state;
};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	BasicExample game("Aqua -- DX11 -- Tiago Costa", hInstance, nShowCmd, 1600, 900, 1, 0);

	aqua->init();

	return aqua->run();
}

uint num_points = 0;

void increasePoints(const void* data)
{
	if(data != nullptr && strcmp((const char*)data, "ball") == 0)
		num_points++;
}

BasicExample::BasicExample(LPCSTR newGameTitle, HINSTANCE hInstance, int nShowCmd, int width, int hiehgt, int nSamples, int samplesQuality)
	: AquaGame(newGameTitle, hInstance, nShowCmd, width, hiehgt, nSamples, samplesQuality), _Camera1(getStringID("mainCamera1")), _Camera2(getStringID("mainCamera2"))
{
	_reseting_ball = false;
	_left_camera = true;

	_dt_since_shift = 5;

	k = false;
}

BasicExample::~BasicExample()
{}

void BasicExample::init()
{
	//ShowCursor(false);
	AquaGame::init();

	const Texture* pSkyTexture = dynamic_cast<const Texture*>(getResourceManager().getResource(TEXTURE, "tenerife_CM1.dds"));

	_GBufferPass.init(1600, 900);
	_LightingPass.init(1600, 900);
	_SkyDynamicPass.init(150.0f);
	//_MaterialPass.init(1600, 900);

	getRenderer().addRenderingPass(&_GBufferPass);
	getRenderer().addRenderingPass(&_LightingPass);
	//getRenderer().addRenderingPass(&_MaterialPass);
	getRenderer().addRenderingPass(&_SkyDynamicPass);

#ifndef HMD
	_Camera1.setLens( (float)D3DX_PI * 0.25f, 1600.0f / 900.0f, 0.1f, 150.0f );
	_Camera1.setPosition(3.0f, 2.0f, 0.0f);
	_Camera1.setLookDirection(-1.0f, -0.25f, 0.0f);

	_Camera2.setLens( (float)D3DX_PI * 0.25f, 1600.0f / 900.0f, 0.1f, 150.0f );
	_Camera2.setPosition(3.0f, 2.0f, 0.05f);
	_Camera2.setLookDirection(-1.0f, -0.25f, 0.0f);
#else
	_Camera1.setLens( (float)D3DX_PI * 0.25f, 800.0f / 900.0f, 0.1f, 150.0f );
	_Camera1.setPosition(3.0f, 2.0f, 0.0f);
	_Camera1.setLookDirection(-1.0f, -0.25f, 0.0f);

	_Camera2.setLens( (float)D3DX_PI * 0.25f, 800.0f / 900.0f, 0.1f, 150.0f );
	_Camera2.setPosition(3.05f, 2.0f, 0.0f);
	_Camera2.setLookDirection(-1.0f, -0.25f, 0.0f);
#endif
	_Scene.loadScene("testScene.aqscene");

	_pSun = _Scene.getSun();

	_CSMGenerator.init();
	getRenderer().setCSMGenerator(&_CSMGenerator);

	//_biostage.init("192.168.85.14");
	_biostage.init("127.0.0.1");

	_physics.init();


	//CREATE GROUND
	int num_tiles = 10;
	float tile_size = 10.0f;

	for(int i = 0; i < num_tiles*num_tiles; i++)
	{
		Actor* x = _Scene.createActor("ground");
		x->setModel(getResourceManager().getResourceID(MODEL, "groundPlane.model.xml"));
		x->setScaling(tile_size,1,tile_size);
		x->setPosition(Vector3D(tile_size*(i/num_tiles) - num_tiles * tile_size/2, 0, tile_size*(i%num_tiles)  - num_tiles * tile_size/2));
	}

	//CREATE SKELETON
	for(int i = 0; i < 21; i++)
	{
		_bones[i] = _Scene.createActor("bone");
		_bones[i]->setModel(getResourceManager().getResourceID(MODEL, "blue.model.xml"));

		_bones_physics_actors[i] = nullptr;
	}

	//CREATE BALL
	_ball         = _Scene.createActor("ball");
	_ball->setModel(getResourceManager().getResourceID(MODEL, "sphere.model.xml"));
	_ball->setScaling(.4f,.4f,.4f);
	_ball_physics = _physics.createBall(physx::PxTransform(physx::PxVec3(-1.0f, 1.0f, 0.2f)), 0.2f);

	//CREATE BALIZA

	//Poste 1
	Actor* x = _Scene.createActor("poste1");
	x->setModel(getResourceManager().getResourceID(MODEL, "red.model.xml"));
	x->setScaling(GOAL_DEPTH, GOAL_HEIGHT, GOAL_DEPTH);
	x->setPosition(Vector3D(-10.0f, 0.0f, -GOAL_WIDTH / 2 - GOAL_DEPTH/2));

	_physics.createStaticBox(physx::PxTransform(-10.0f, 1.25f, -GOAL_WIDTH/2 - GOAL_DEPTH/2),
		                     GOAL_DEPTH / 2, 1.25f, GOAL_DEPTH / 2);

	//Poste 2
	x = _Scene.createActor("poste2");
	x->setModel(getResourceManager().getResourceID(MODEL, "red.model.xml"));
	x->setScaling(GOAL_DEPTH, GOAL_HEIGHT, GOAL_DEPTH);
	x->setPosition(Vector3D(-10.0f, 0.0f, GOAL_WIDTH / 2 + GOAL_DEPTH/2));

	_physics.createStaticBox(physx::PxTransform(-10.0f, 1.25f, GOAL_WIDTH / 2 + GOAL_DEPTH / 2),
		                     GOAL_DEPTH / 2, 1.25f, GOAL_DEPTH / 2);

	//Trave
	x = _Scene.createActor("trave");
	x->setModel(getResourceManager().getResourceID(MODEL, "red.model.xml"));
	x->setScaling(GOAL_DEPTH, GOAL_DEPTH, GOAL_WIDTH + 2*GOAL_DEPTH);
	x->setPosition(Vector3D(-10.0f, GOAL_HEIGHT, 0.0f));

	_physics.createStaticBox(physx::PxTransform(-10.0f, GOAL_HEIGHT + GOAL_DEPTH/2, 0.0f), 
		                     GOAL_DEPTH / 2, GOAL_DEPTH / 2, GOAL_WIDTH / 2 + GOAL_DEPTH);

	//Trigger
	/*_trigger = _Scene.createActor("trigger");
	_trigger->setModel(getResourceManager().getResourceID(MODEL, "red.model.xml"));
	_trigger->setScaling(GOAL_DEPTH, GOAL_HEIGHT, 7.0f);
	_trigger->setPosition(Vector3D(-10.0f, 0.0f, 0.0f));*/

	_physics.createTrigger(physx::PxTransform(-10.0f, GOAL_HEIGHT/2, 0.0f), 
		                   GOAL_DEPTH / 2, GOAL_HEIGHT/2, GOAL_WIDTH / 2, &increasePoints);

	getUI().addUInt("Points", &num_points, nullptr);

	////////////////////////////////////////////
	//INIT LUA
	////////////////////////////////////////////

	_lua_state = luaL_newstate();

	luaL_openlibs(_lua_state);

	if(luaL_dofile(_lua_state, "../config.lua"))
	{
		getLogger().write("Error loading config file", MESSAGE_LEVEL::ERROR_MESSAGE);
	}

	/////////////////////////
	//Create 'game' table
	/////////////////////////

	lua_newtable(_lua_state);

	//register functions
	lua_pushcfunction(_lua_state, write);
	lua_setfield(_lua_state, -2, "write");

	lua_setglobal(_lua_state, "game");

	/////////////////////////
	//Create 'biostage' table
	/////////////////////////

	lua_newtable(_lua_state);
	
	//register functions
	lua_pushcfunction(_lua_state, getBoneTranslation);
	lua_setfield(_lua_state, -2, "getBoneTranslation");

	lua_pushcfunction(_lua_state, getBoneRotation);
	lua_setfield(_lua_state, -2, "getBoneRotation");

	lua_pushcfunction(_lua_state, getBoneScale);
	lua_setfield(_lua_state, -2, "getBoneScale");

	lua_setglobal(_lua_state, "biostage");

	/////////////////////////
	//Create 'vec3' table
	/////////////////////////

	lua_newtable(_lua_state);

	//register functions
	lua_pushcfunction(_lua_state, getX);
	lua_setfield(_lua_state, -2, "getX");

	lua_pushcfunction(_lua_state, getY);
	lua_setfield(_lua_state, -2, "getY");

	lua_pushcfunction(_lua_state, getZ);
	lua_setfield(_lua_state, -2, "getZ");

	lua_setglobal(_lua_state, "vec3");

	strcpy(message, "none");

	getUI().addString("Info", message, nullptr);
}

void BasicExample::update(float dt)
{
	_Scene.update(dt);

	if(GetAsyncKeyState(VK_UP) & 0x8000) 
		_SkyDynamicPass.changeSunDirection(-0.4f*dt, 0.0f);

	if(GetAsyncKeyState(VK_DOWN) & 0x8000) 
		_SkyDynamicPass.changeSunDirection(0.4f*dt, 0.0f);

	if(_dt_since_shift < 0.3)
		_dt_since_shift += dt;
	else if(GetAsyncKeyState('P') & 0x8000)
	{
		_left_camera = !_left_camera;
		_dt_since_shift = 0.0;
	} else if(GetAsyncKeyState('O') & 0x8000)
	{
		k = !k;
		_dt_since_shift = 0.0;
	}

	//Camera Update
	float walkSpeed = 2.0f;

	if(GetAsyncKeyState(VK_SHIFT) & 0x8000)
		walkSpeed = 5.0f;
		
	if(GetAsyncKeyState('W') & 0x8000)
	{
		_Camera1.moveBy(0.0f,0.0f,walkSpeed*dt);
		_Camera2.moveBy(0.0f,0.0f,walkSpeed*dt);
	}

	if(GetAsyncKeyState('S') & 0x8000)
	{
		_Camera1.moveBy(0.0f,0.0f,-walkSpeed*dt);
		_Camera2.moveBy(0.0f,0.0f,-walkSpeed*dt);
	}

	if(GetAsyncKeyState('A') & 0x8000)
	{
		_Camera1.moveBy(-walkSpeed*dt,0.0f,0.0f);
		_Camera2.moveBy(-walkSpeed*dt,0.0f,0.0f);
	}

	if(GetAsyncKeyState('D') & 0x8000)
	{
		_Camera1.moveBy(walkSpeed*dt,0.0f,0.0f);
		_Camera2.moveBy(walkSpeed*dt,0.0f,0.0f);
	}

	if(GetAsyncKeyState('K') & 0x8000)
		_ball_physics->setLinearVelocity(physx::PxVec3(-10.0f, 5.0f, 3.5f)); //simulate kick

	if(GetAsyncKeyState('R') & 0x8000) // reset ball position
	{
		_ball_physics->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
		_ball_physics->setKinematicTarget(physx::PxTransform(physx::PxVec3(-1.0f, 1.0f, 0.2f)));

		_reseting_ball = true;
	} else if(_reseting_ball)
	{
		_ball_physics->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, false);
		_reseting_ball = false;
	}

	/*if(GetAsyncKeyState('N') & 0x8000) // add new ball
	{
		_ball         = _Scene.createActor("ball");
		_ball->setModel(getResourceManager().getResourceID(MODEL, "sphere.model.xml"));
		_ball->setScaling(.223f,.223f,.223f);
		_ball_physics = _physics.createBall(physx::PxTransform(physx::PxVec3(-1.5f, 1.0f, 0.2f)), 0.1115f);
	}*/

	if(!(GetAsyncKeyState(VK_CONTROL) & 0x8000))
	{
		//ShowCursor(true);
		POINT pos = getInputManager().getMousePos();
		if(pos.y != 0.0)
		{
			_Camera1.rotateBy( (float)(-pos.y * DEGTORAD(0.5f)), 0.0f);
			_Camera2.rotateBy( (float)(-pos.y * DEGTORAD(0.5f)), 0.0f);
		}
		if(pos.x != 0.0)
		{
			_Camera1.rotateBy(0.0f, (float)(pos.x * DEGTORAD(0.5f)));
			_Camera2.rotateBy(0.0f, (float)(pos.x * DEGTORAD(0.5f)));
		}

		getInputManager().moveMouseTo(0,0);
	}

	_pSun->dir   = _SkyDynamicPass.getSunDirection();
	_pSun->color = _SkyDynamicPass.getSunColor();

	_biostage.update();

	_physics.update(dt);

	_ball->setWorldMatrix((D3DXMATRIX&)physx::PxMat44(_ball_physics->getGlobalPose()));

	//update the bones
	if(_biostage.hasBoneData())
	{
		OMBoneMatrix* current_bone_matrices = _biostage.getCurrentBoneMatrices();
		OMBoneDimension* bone_dimensions = _biostage.getBoneDimensions();

		D3DXMATRIX rotate;
		D3DXMatrixRotationX(&rotate, DEGTORAD(-90.0f));

		D3DXMATRIX y_offset;
		D3DXMatrixTranslation(&y_offset, 0.0f, 0.1f, 0.0f);

		for(unsigned int b = 0; b < _biostage.boneCount(); b++)
		{
			OMBoneMatrix bone_mat = current_bone_matrices[b];

			//generate the scale matrix with the bone dimensions
			OMBoneDimension dim = bone_dimensions[b];

			D3DXMATRIX bone_scale;
			D3DXMatrixScaling(&bone_scale, dim.boxsize.x, dim.boxsize.y, dim.boxsize.z);

			D3DXMATRIX bone_world = bone_scale * (D3DXMATRIX&)bone_mat * rotate * y_offset;

			//apply it to the world matrix
			_bones[b]->setWorldMatrix(bone_world);

			D3DXVECTOR3 p, s;
			D3DXQUATERNION r;
			D3DXMatrixDecompose(&s, &r, &p, &bone_world);

			physx::PxTransform transform(p.x, p.y, p.z, physx::PxQuat(r.x, r.y, r.z, r.w));
			transform.q.normalize();

			if( dim.boxsize.x != _bone_dimensions[b].boxsize.x || 
				dim.boxsize.y != _bone_dimensions[b].boxsize.y ||
				dim.boxsize.z != _bone_dimensions[b].boxsize.z)
			{
				getLogger().write("Different bone", ERROR_MESSAGE);
				_bone_dimensions[b] = dim;

				if(_bones_physics_actors[b] != nullptr)
					_physics.deleteBone(_bones_physics_actors[b]);

				_bones_physics_actors[b] = _physics.createBone(transform, dim.boxsize.x / 2, dim.boxsize.y / 2, dim.boxsize.z / 2);
			} else
			{
				_bones_physics_actors[b]->setKinematicTarget(transform);
			}

			//draw the corresponding mesh
			/*if(dim.type == OMST_Sphere)
			{
				g_SphereMarker->DrawSubset(0);
			}
			else if(dim.type == OMST_Box)
			{
				g_pMeshBox->DrawSubset(0);
			}*/
		}

		D3DXVECTOR3 p(0.0f, 0.0f, 0.0f);
		D3DXVec3TransformCoord(&p, &p, &_bones[10]->getWorldMatrix());

		D3DXMATRIX rotate1;
		D3DXMATRIX rotate2;
		D3DXMatrixRotationX(&rotate1, DEGTORAD(90));
		D3DXMatrixRotationY(&rotate2, DEGTORAD(90));

		D3DXVECTOR3 d1(0.0f, 0.0f, 0.0f);
		D3DXVECTOR3 d2(0.0f, 0.0f, 1.0f);

		D3DXVec3TransformCoord(&d1, &d1, &(_bones[10]->getWorldMatrix() * rotate1 * rotate2));
		D3DXVec3TransformCoord(&d2, &d2, &(_bones[10]->getWorldMatrix() * rotate1 * rotate2));

		if(k)
		{
			_Camera1.setPosition(p.x, p.y, p.z);
			_Camera2.setPosition(p.x, p.y, p.z+0.1);
		}

		/*float k[3];
		getRotation(_bones[10]->getWorldMatrix(), k);

		D3DXQUATERNION q;
		D3DXQuaternionRotationMatrix(&q, &_bones[10]->getWorldMatrix());*/

		//_Camera.setLookDirection(d2.x - d1.x, d2.y - d1.y, d2.z - d1.z);
	}

#ifndef HMD
	D3D11_VIEWPORT viewport;
	viewport.Width = (FLOAT)1600;
	viewport.Height = (FLOAT)900;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;

	if(_left_camera)
		getRenderer().drawScene(dt, &_Scene, &_Camera1, viewport);
	else
		getRenderer().drawScene(dt, &_Scene, &_Camera2, viewport);
#else
	D3D11_VIEWPORT viewport;
	viewport.Width = (FLOAT)800;
	viewport.Height = (FLOAT)900;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	getRenderer().drawScene(dt, &_Scene, &_Camera1, viewport);

	/*viewport.TopLeftX = 800.0f;
	getRenderer().drawScene(dt, &_Scene, &_Camera2, viewport);*/

#endif

	for(uint i = 0; i < 21; i++)
		D3DXMatrixDecompose(&bones_scales[i], &bones_rotations[i], &bones_translations[i], &_bones[i]->getWorldMatrix());

	lua_getglobal(_lua_state, "update");
	lua_pcall(_lua_state, 0, 0, 0);
	
	getRenderer().present();
}

void BasicExample::shutdown()
{
	lua_close(_lua_state);

	AquaGame::shutdown();
}