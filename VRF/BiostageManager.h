#pragma once

#include <d3d11.h>

#include <OMSDKTypes.h>
#include <OMSDKFactory.h>
#include <OMSDKClientInterfaces.h>

class BiostageManager
{
public:
	BiostageManager();
	~BiostageManager();

	bool init(const char* ip);

	void update();

	bool hasBoneData();
	unsigned int boneCount();

	OMBoneMatrix* getCurrentBoneMatrices();
	OMBoneDimension* getBoneDimensions();

	bool setupSkeleton();
	void setBoneCount(int num_bones);

private:
	BiostageManager(const BiostageManager&);
	BiostageManager& operator=(BiostageManager);

	IOMClient*					_client;
	IOMDataFrame*				_frame;
	IOMTimeDataHandle*			_time_handle;
	IOMBoneMatrixDataHandle*	_bone_handle;
	OMStreamId					_stream_id;
	IOMStreamConfig*            _config;

	int                 _bone_count;
	OMBoneMatrix*		_current_bone_matrices_raw;	//what we get from server
	OMBoneMatrix*		_current_bone_matrices_conv;	//multiplied out
	OMBoneMatrix*		_tstance_matrices;
	OMBoneDimension*	_bone_dims;
	int*				_parent_indices;

	bool _at_least_one_bone_set;
	bool _is_tracking;
};