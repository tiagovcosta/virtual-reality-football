
static const float PI = 3.14159265358979323846;

float4 ParallelLight(float3 diffuse, float3 spec, float roughness,
                     float3 pos, float3 normal,
					 float3 lightDir, float3 lightColor)
{
	float3 litColor   =  float3(0.0f, 0.0f, 0.0f);

	float NdL         = saturate(dot(normal, lightDir));

	if(NdL > 0.0f)
	{
		float3 V          = normalize(float3(0.0f, 0.0f, 0.0f) - pos);
		float3 H          = normalize(lightDir + V);
		
		float3 fDiff  = 1.0f - spec; //Diffuse fresnel approximation
		float3 fSpec = spec + fDiff * pow((1 - dot(lightDir, H)), 5); //Schlick specular fresnel
		
		float r_2 = roughness * roughness;
		
		float Pix4 = PI * 4.0f;
		
		float NdH_2 = pow(dot(normal, H), 2);
		
		float Dbottom = Pix4 * pow(NdH_2 * (r_2 - 1) + 1, 2); 
		
		float D = r_2/Dbottom;
		
		litColor = ( (diffuse / PI) * fDiff) + fSpec * D;
		litColor = litColor * NdL * lightColor;
	}
	
	return float4(litColor, 1.0f);
}

float Attenuation(float distance, float range)
{
	//return max( 0, 1.0f - ( distance / range ) );	//Linear

	float atten = saturate(1-distance/range);
	return atten*atten;
}

float4 PointLight(float3 diffuse, float3 spec, float roughness,
                  float3 pos, float3 normal,
				  float3 lightPos, float3 lightColor, float range)
{
	float Atten = Attenuation(distance(pos, lightPos), range);
	float3 litColor    =  float3(0.0f, 0.0f, 0.0f);

	if(Atten > 0.0f)
	{
		float3 lightDir   = normalize(lightPos - pos);

		float NdL         = max( 0.0f, dot(normal, lightDir));
		
		if(NdL > 0.0f)
		{
			float3 V          = normalize(float3(0.0f, 0.0f, 0.0f) - pos);
			float3 H          = normalize(lightDir + V);
			
			float3 fDiff  = 1.0f - spec; //Diffuse fresnel approximation
			float3 fSpec = spec + fDiff * pow((1 - dot(lightDir, H)), 5); //Schlick specular fresnel
			
			float r_2 = roughness * roughness;
			
			float Pix4 = PI * 4.0f;
			
			float NdH_2 = pow(dot(normal, H), 2);
			
			float Dbottom = Pix4 * pow(NdH_2 * (r_2 - 1) + 1, 2); 
			
			float D = r_2/Dbottom;
			
			litColor = (diffuse / PI * fDiff) + fSpec * D;
			litColor = litColor * NdL * lightColor * Atten;
		}
	}

	return float4(litColor, 1.0f);
}

 
 