// Function - SGeometryVS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 15
//

#include "AquaShadersHeader.hlsl"

struct VS_IN
{
	float3 posL     : POSITION;
	float3 tangentL : TANGENT;
	float3 normalL  : NORMAL;
	float2 texC     : TEXCOORD;
};

struct VS_OUT
{
	float4 posH     : SV_POSITION;
        float2 texC     : TEXCOORD0;

};

VS_OUT SGeometryVS(VS_IN vIn)
{
	VS_OUT vOut;

	vOut.posH = mul(float4(vIn.posL, 1.0f), gWorldViewProj);
	
	vOut.texC  = vIn.texC;
	
	return vOut;
}