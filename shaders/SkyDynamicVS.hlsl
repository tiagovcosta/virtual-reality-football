// Function - SkyDynamicVS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 9
//

#include "AquaShadersHeader.hlsl"

cbuffer cbPerSky : register(b5)
{
	float4x4 gSkyWorldViewProj;
};

struct VS_IN
{
	float3 posL : POSITION;
	float2 texC : TEXCOORD;
};

struct VS_OUT
{
	float4 posH : SV_POSITION;
    	float2 tex0 : TEXCOORD0;
	float3 tex1 : TEXCOORD1;
};
 
VS_OUT SkyDynamicVS(VS_IN vIn)
{
	VS_OUT vOut;

	// set z = w so that z/w = 1 (i.e., skydome always on far plane).
	vOut.posH = mul(float4(vIn.posL, 1.0f), gSkyWorldViewProj).xyzw;

	vOut.tex0 = vIn.texC;
	vOut.tex1 = -vIn.posL;
	
	return vOut;
}
