// Function - SGeometryPS
// Profile - ps_4_0
// Type - Pixel
//

#include "AquaShadersHeader.hlsl"

Texture2D gLightBuffer : register(t0);
Texture2D gDiffuseMap : register(t1);

struct PS_IN
{
	float4 posH     : SV_POSITION;
        float2 texC     : TEXCOORD0;
};

float4 SGeometryPS(PS_IN pIn) : SV_Target
{
	float4 diffuse = pow(abs(gDiffuseMap.Sample(gAnisoSampler, pIn.texC)), 2.2);
	clip(diffuse.a - 0.10f);

	float3 color = float3(0.0f,0.0f,0.0f);

	float4 light = gLightBuffer.Load(int3(pIn.posH.xy, 0));

	//color += light.rgb * diffuse.rgb + light.aaa * diffuse.rgb * light.rgb + (gAmbientLight.rgb * diffuse.rgb);
	color = (diffuse.rgb + light.aaa) * light.rgb + (gAmbientLight.rgb * diffuse.rgb);

	//return float4(pow(abs(color),1/2.2f) , 1.0f);
	return float4(color, 1.0f);
}