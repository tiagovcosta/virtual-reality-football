// Function - SpriteVS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 9
//

cbuffer cbPerSprite : register(b2)
{
	float2 pos;
	float2 scale; 
}

struct VS_INPUT
{
	float3 pos  : POSITION;
	float2 texC : TEXCOORD;
};

struct VS_OUTPUT
{
	float4 screenPos : SV_POSITION;
	float2 texC      : TEXCOORD0;
};

VS_OUTPUT SpriteVS(VS_INPUT vIn)
{
	VS_OUTPUT vOut;

	vOut.screenPos = float4(pos.x + vIn.pos.x * scale.x, pos.y + vIn.pos.y * scale.y, 0.0f, 1.0f);
	vOut.texC = vIn.texC;

	return vOut;
};