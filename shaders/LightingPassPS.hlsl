// Function - LightingPassPS
// Profile - ps_4_0
// Type - Pixel
// Defines - DIRECTIONAL_LIGHT
//

#include "AquaShadersHeader.hlsl"
#include "Lighting.hlsl"

Texture2D gDiffuseMap : register(t0);
Texture2D gNormalMap : register(t1);
Texture2D gDepthMap : register(t2);
Texture2D gShadowMap : register(t3);
TextureCube gEnvMap : register(t4);

struct PS_IN
{
	float4 posH     	: SV_Position;
	float3 viewRay		: VIEWRAY;

	nointerpolation float3 lightColor 	   : COLOR;
	
	#ifndef DIRECTIONAL_LIGHT
	nointerpolation float4 lightPosVSRange : POSITIONRANGE;
	#else
	nointerpolation float3 lightDirVS      : DIRECTION;
	#endif
};

float4 LightingPassPS( PS_IN pIn ) : SV_Target
{
	float4 light       = float4(0.0f,0.0f,0.0f,0.0f);

	int3 sampleIndices = int3(pIn.posH.xy, 0);
	float depth        = gDepthMap.Load(sampleIndices).r;
	
	float4 normalData  = gNormalMap.Load(sampleIndices);
	float3 normal      = 2.0f * normalData.xyz - 1.0f;
	float  roughness   = normalData.a + 0.001f;
	
	float4 diffuseData = gDiffuseMap.Load(sampleIndices);
	float3 diffuse     = diffuseData.rgb;
	float  f0          = diffuseData.a;
	
	float metal = saturate(f0*2-1);
    float nonMetalSpec = float(saturate(f0*2))*0.2;
	
	float3 Ks = lerp( nonMetalSpec, diffuse, metal );
    float3 Kd = lerp( diffuse, 0.0f, metal );
	
	//roughness = 0.05f;
	//f0        = 0.05f;
	
	float shadowFactor = gShadowMap.Load(sampleIndices).r;

	float3 positionVS  = pIn.viewRay * depth;

	#ifdef DIRECTIONAL_LIGHT
    light = PI * ParallelLight(Kd, Ks,roughness,
						  positionVS, normal,
						  -pIn.lightDirVS, pIn.lightColor) * shadowFactor;
	
	light += gAmbientLight * float4(Kd, 1.0f);
	
	/*float3 positionWS = mul(float4(positionVS, 1.0f), gInvView).xyz;
	float3 normalWS   = mul(float4(normal, 0.0f), gInvView).xyz;
	float3 incident = positionWS - gEyePosition;
	float3 reflectDir = reflect(incident, normalWS);
	float4 reflectedColor = pow(abs(gEnvMap.Sample(gTriLinearSampler, reflectDir)), 2.2f);
	
	float3 fSpec = Ks + (1.0f - Ks) * pow((1 - dot(incident, normalWS)), 5);
	
	light += (reflectedColor * float4(fSpec,1.0f));*/
	
	#else
	light = PointLight(Kd, Ks,roughness,
					   positionVS, normal,
					   pIn.lightPosVSRange.xyz, pIn.lightColor, pIn.lightPosVSRange.w);
	#endif

	return light;
}
