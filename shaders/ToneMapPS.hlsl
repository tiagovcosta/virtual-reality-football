// Function - ToneMapPS
// Profile - ps_4_0
// Type - Pixel
//

#include "AquaShadersHeader.hlsl"
#include "ToneMap.hlsl"

Texture2D<float4> SceneTexture : register(t0);
Texture2D<float4> AverageLuminance : register(t1);
Texture2D<float4> BloomTexture : register(t2);

struct PS_INPUT
{
	float4 screenPos : SV_POSITION;
	float2 texC      : TEXCOORD0;
};

/*static const float A = 0.15f;
static const float B = 0.50f;
static const float C = 0.10f;
static const float D = 0.20f;
static const float E = 0.02f;
static const float F = 0.30f;
static const float W = 11.2f;*/

float4 ToneMapPS(PS_INPUT pIn) : SV_TARGET0
{
	float3 color = SceneTexture.Sample(gTriPointSampler, pIn.texC).rgb;
	float avgLum = AverageLuminance.Sample(gTriPointSampler, pIn.texC).r;
	float3 bloom = BloomTexture.Sample(gTriLinearSampler, pIn.texC).rgb;

	if(gToneMapOperator == 0)
		color = FilmicToneMap(color, avgLum, gMiddleGrey, A, B, C, D, E, F, W);
	else if(gToneMapOperator == 1)
		color = ReinhardToneMap(color, avgLum, gMiddleGrey, gWhite);

	//color += bloom * gBloomIntensity;

	color = pow(abs(color), 1/2.2f);

	color += bloom * gBloomIntensity;

	return float4(color, 1.0f);
};