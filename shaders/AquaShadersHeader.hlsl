//Samplers
SamplerState gTriLinearSampler : register(s0);
SamplerState gTriPointSampler  : register(s1);
SamplerState gAnisoSampler     : register(s2);

cbuffer cbPerFrame : register(b1)
{
	float4x4 gView;
	float4x4 gProj;
	float4x4 gViewProj;
	float4x4 gInvViewProj;
	float4x4 gInvView;
	float4x4 gInvProj;
	float4   gAmbientLight;
	float3   gEyePosition;
	float    gDeltaTime;
	float2   gClipDistances;
}

cbuffer cbPerActor : register(b3)
{
	float4x4 gWorldView;
	float4x4 gWorldViewProj;
};

cbuffer cbPerPostProcess : register(b4)
{
	float gMiddleGrey;
	float gWhite;
	float gBrightPassThreshold;
	float gBloomIntensity;
	int   gToneMapOperator;
	float A;
	float B;
	float C;
	float D;
	float E;
	float F;
	float W;
};