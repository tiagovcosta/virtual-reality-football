// Function - SpritePS
// Profile - ps_4_0
// Type - Pixel
//

Texture2D<float4> DiffuseMap : register(t0);

#include "AquaShadersHeader.hlsl"

struct PS_INPUT
{
	float4 screenPos : SV_POSITION;
	float2 texC      : TEXCOORD0;
};

float4 SpritePS(PS_INPUT pIn) : SV_TARGET0
{
	return DiffuseMap.Sample(gTriLinearSampler, pIn.texC);
};