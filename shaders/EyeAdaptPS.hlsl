// Function - EyeAdaptPS
// Profile - ps_4_0
// Type - Pixel
//

static const float LUM_MIP_LEVEL = 8;

Texture2D<float4> SceneLuminance : register(t2);
Texture2D<float4> PrevLuminance : register(t1);

#include "AquaShadersHeader.hlsl"

struct PS_INPUT
{
	float4 screenPos : SV_POSITION;
	float2 texC      : TEXCOORD0;
};

float EyeAdaptPS(PS_INPUT pIn) : SV_TARGET0
{
	float lum = exp(SceneLuminance.SampleLevel(gTriPointSampler, pIn.texC, 8)).r;
	float prevLum = PrevLuminance.Sample(gTriPointSampler, pIn.texC).r;

	lum = prevLum + (lum - prevLum) * ( 1 - pow( 0.98f, 60 * gDeltaTime ) );

    	return lum;
};