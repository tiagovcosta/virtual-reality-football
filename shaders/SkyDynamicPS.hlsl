// Function - SkyDynamicPS
// Profile - ps_4_0
// Type - Pixel
//

#include "AquaShadersHeader.hlsl"

Texture2D RayleighScatter : register(t1);
Texture2D MieScatter : register(t2);

static const float g = -0.990;
static const float g2 = (-0.990) * (-0.990);

cbuffer cbDynamicSky : register(b3)
{
	float3 InvWavelength;
	float3 WavelengthMie;
	float3 v3SunDir;
}

struct PS_IN
{
	float4 posH : SV_POSITION;
    	float2 tex0 : TEXCOORD0;
	float3 tex1 : TEXCOORD1; 
};

float getRayleighPhase(float fCos2)
{
	return 0.75 * (1.0 + fCos2);
}

float getMiePhase(float fCos, float fCos2)
{
	float3 v3HG;
	v3HG.x = 1.5f * ( (1.0f - g2) / (2.0f + g2) );
	v3HG.y = 1.0f + g2;
	v3HG.z = 2.0f * g;
	return v3HG.x * (1.0 + fCos2) / pow(abs(v3HG.y - v3HG.z * fCos), 1.5);
}

float4 SkyDynamicPS(PS_IN pIn) : SV_Target
{
	float fCos = dot( v3SunDir, pIn.tex1 ) / length( pIn.tex1 );
	float fCos2 = fCos * fCos;
	
	float3 v3RayleighSamples = RayleighScatter.Sample(gTriLinearSampler, pIn.tex0).xyz;
	float3 v3MieSamples = MieScatter.Sample(gTriLinearSampler, pIn.tex0).xyz;

	float3 color;
	color.rgb = getRayleighPhase(fCos2) * v3RayleighSamples.rgb + getMiePhase(fCos, fCos2) * v3MieSamples.rgb;

	color.rgb += max(0, 1-color.rgb)*float3(0.00f, 0.00f, 0.005f);

	return float4(color.rgb, 1);
}
