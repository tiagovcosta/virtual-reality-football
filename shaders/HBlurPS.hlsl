// Function - HBlur
// Profile - ps_4_0
// Type - Pixel
//

#include "AquaShadersHeader.hlsl"
#include "ToneMap.hlsl"

Texture2D<float4> BrightPassTexture : register(t2);

static const float GAUSSIAN_WEIGHTS[15]     = {0.00874f, 0.017997f, 0.03315886f, 0.05466797f, 0.08065644f, 0.106482f, 0.125794f, 0.13298f, 0.125794f, 0.106482f, 0.080654644f, 0.0546697f, 0.03315886f, 0.01997f, 0.00817997f};
//static const float GAUSSIAN_WEIGHTS[9]     = {0.05f, 0.09f, 0.12f, 0.015f, 0.16f, 0.15f, 0.12f, 0.09f, 0.05f};

static const float BLOOM_X_SIZE = 1/320.f;
static const float BLOOM_Y_SIZE = 1/200.f;

struct PS_INPUT
{
	float4 screenPos : SV_POSITION;
	float2 texC      : TEXCOORD0;
};

float4 HBlur(PS_INPUT pIn) : SV_TARGET
{
	float4 color = 0.0f;

	for(int x = -7; x <= 7; x++)
	{
 		color += BrightPassTexture.Sample(gTriLinearSampler, pIn.texC + float2(x * BLOOM_X_SIZE, 0.0f)) * (GAUSSIAN_WEIGHTS[x+7] * 1.25);
	}
	
	return color;
}