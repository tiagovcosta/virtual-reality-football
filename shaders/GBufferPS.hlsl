// Function - GBufferPS
// Profile - ps_4_0
// Type - Pixel
//

#include "AquaShadersHeader.hlsl"

Texture2D gDiffuseMap : register(t1);
Texture2D gNormalMap : register(t2);
Texture2D gSpecMap : register(t3);

struct PS_IN
{
	float4 posH      : SV_POSITION;
	float3 tangentVS : TEXCOORD0;
    float3 normalVS  : TEXCOORD1;
    float2 texC      : TEXCOORD2;
	float3 posVS     : TEXCOORD3;
};

struct PS_OUT
{
	float4 diffuse  : SV_TARGET0;
	float4 normal   : SV_TARGET1;
	float4 depth    : SV_TARGET2;	
};

PS_OUT GBufferPS(PS_IN pIn)
{
	PS_OUT pOut;

	pOut.diffuse   = pow(abs(gDiffuseMap.Sample(gAnisoSampler, pIn.texC)), 2.2);
	
	clip(pOut.diffuse.a - 0.10f);
	
	float2 spec = gSpecMap.Sample( gTriLinearSampler, pIn.texC ).rg;
	pOut.diffuse.a = spec.r;
	pOut.normal.a  = spec.g;
	
	float3 normalT = gNormalMap.Sample( gTriLinearSampler, pIn.texC ).xyz;

	normalT = 2.0f*normalT - 1.0f;

	float3 N = normalize(pIn.normalVS);
	float3 T = normalize(pIn.tangentVS - dot(pIn.tangentVS, N)*N);
	float3 B = cross(N,T);
	
	float3x3 TBN = float3x3(T, B, N);
	float3 bumpedNormalVS = normalize(mul(normalT, TBN));
    
	pOut.normal.rgb = 0.5f * (normalize(bumpedNormalVS) + 1.0f);
	
	pOut.depth = pIn.posVS.z/gClipDistances.y;

	return pOut;
}