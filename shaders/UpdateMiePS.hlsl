// Function - UpdateMiePS
// Profile - ps_4_0
// Type - Pixel
//

#include "AquaShadersHeader.hlsl"

static const float InnerRadius = 6356.7523142f;
static const float OuterRadius = 6356.7523142f * 1.0157313f;
static const float PI = 3.1415159;
static const float NumSamples = 10;
static const float fScale = 1.0 / (6356.7523142f * 1.0157313f - 6356.7523142f);
static const float2 v2dRayleighMieScaleHeight = {0.25f, 0.1f};

static const float KrESun = 0.0025f * 20.0f;
static const float KmESun = 0.0010f * 20.0f;
static const float Kr4PI = 0.0025f * 4.0f * PI;
static const float Km4PI = 0.0010f * 4.0f * PI;

cbuffer cbUpdateRayleigh : register(b3)
{
	float3 InvWavelength;
	float3 WavelengthMie;
	float3 v3SunDir;
}

struct PS_INPUT
{
	float4 screenPos : SV_POSITION;
	float2 texC      : TEXCOORD0;
};

float HitOuterSphere( float3 O, float3 Dir ) 
{
	float3 L = -O;

	float B = dot( L, Dir );
	float C = dot( L, L );
	float D = C - B * B; 
	float q = sqrt( OuterRadius * OuterRadius - D );
	float t = B;
	t += q;

	return t;
}

float2 GetDensityRatio( float fHeight )
{
	const float fAltitude = (fHeight - InnerRadius) * fScale;
	return exp( -fAltitude / v2dRayleighMieScaleHeight.xy );
}

float2 t( float3 P, float3 Px )
{
	float2 OpticalDepth = 0;

	float3 v3Vector =  Px - P;
	float fFar = length( v3Vector );
	float3 v3Dir = v3Vector / fFar;
			
	float fSampleLength = fFar / NumSamples;
	float fScaledLength = fSampleLength * fScale;
	float3 v3SampleRay = v3Dir * fSampleLength;
	P += v3SampleRay * 0.5f;
			
	for(int i = 0; i < NumSamples; i++)
	{
		float fHeight = length( P );
		OpticalDepth += GetDensityRatio( fHeight );
		P += v3SampleRay;
	}		

	OpticalDepth *= fScaledLength;
	return OpticalDepth;
}

float4 UpdateMiePS(PS_INPUT pIn) : SV_TARGET0
{
	float2 Tex0 = pIn.texC;
	 
	const float3 v3PointPv = float3( 0, InnerRadius + 0.001f, 0 );
	const float AngleY = 100.0 * Tex0.x * PI / 180.0;
	const float AngleXZ = PI * Tex0.y;
	
	float3 v3Dir;
	v3Dir.x = sin( AngleY ) * cos( AngleXZ  );
	v3Dir.y = cos( AngleY );
	v3Dir.z = sin( AngleY ) * sin( AngleXZ  );
	v3Dir = normalize( v3Dir );

	float fFarPvPa = HitOuterSphere( v3PointPv , v3Dir );
	float3 v3Ray = v3Dir;

	float3 v3PointP = v3PointPv;
	float fSampleLength = fFarPvPa / NumSamples;
	float fScaledLength = fSampleLength * fScale;
	float3 v3SampleRay = v3Ray * fSampleLength;
	v3PointP += v3SampleRay * 0.5f;
				
	float3 v3MieSum = 0;

	for( int k = 0; k < NumSamples; k++ )
	{
		float PointPHeight = length( v3PointP );

		float2 DensityRatio = GetDensityRatio( PointPHeight );
		DensityRatio *= fScaledLength;

		float2 ViewerOpticalDepth = t( v3PointP, v3PointPv );
						
		float dFarPPc = HitOuterSphere( v3PointP, v3SunDir );
		float2 SunOpticalDepth = t( v3PointP, v3PointP + v3SunDir * dFarPPc );

		float2 OpticalDepthP = SunOpticalDepth.xy + ViewerOpticalDepth.xy;
		float3 v3Attenuation = exp( - Kr4PI * InvWavelength * OpticalDepthP.x - Km4PI * OpticalDepthP.y );

		v3MieSum += DensityRatio.y * v3Attenuation;

		v3PointP += v3SampleRay;
	}

	float3 Mie = v3MieSum * KmESun;
	Mie *= WavelengthMie;
	
	return float4( Mie, 1 );
};