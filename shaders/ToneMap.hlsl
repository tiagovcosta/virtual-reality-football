static const float3 LUMINANCE = float3(0.2125f, 0.7154f, 0.0721f);

float3 Uncharted2Tonemap(float3 x, float A, float B, float C, float D, float E, float F)
{
	return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

float3 FilmicToneMap(float3 color, float avgLum, float middleGrey,
                     float a, float b, float c, float d, float e, float f, float w)
{
	float middleGrey2 = 1.03 - 2/(2+log10(avgLum + 1));

	color *= middleGrey2/(avgLum + 0.001f);
	float3 curr = Uncharted2Tonemap(color, a, b, c, d, e, f);
	float3 whiteScale = 1.0f/Uncharted2Tonemap(w, a, b, c, d, e, f);
	color.rgb = curr*whiteScale;

	return color;
}

float3 ReinhardToneMap(float3 color, float avgLum, float middleGrey, float white)
{
	float lum = dot(color, LUMINANCE);
	float lumScaled = (lum * middleGrey) / avgLum;	
	float lumCompressed = (lumScaled * (1 + (lumScaled / (white * white)))) / (1 + lumScaled);
	color *= (lumCompressed/lum);

	return color;
};