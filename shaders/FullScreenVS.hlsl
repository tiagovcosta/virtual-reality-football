// Function - FullScreenVS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 0
//

struct VS_OUT
{
	float4 posH : SV_Position;
	float2 texC : TEXCOORD0;
};

VS_OUT FullScreenVS(uint vertexID : SV_VertexID)
{
	VS_OUT vOut;

	vOut.texC =  float2((vertexID << 1) & 2, vertexID & 2);
    vOut.posH = float4(vOut.texC * float2(2,-2) + float2(-1,1), 0, 1);

	return vOut;
}