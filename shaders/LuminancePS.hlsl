// Function - LuminancePS
// Profile - ps_4_0
// Type - Pixel
//

static const float4 LUMINANCE = float4(0.2125f, 0.7154f, 0.0721f, 0.0f);
static const float DELTA = 0.0001f;
static const float3 BLUE_SHIFT = float3(1.05f, 0.97f, 1.27f);

static const float LUMINANCE_TEXEL_SIZE[5] = {1/900.0f, 1/128.0f, 1/64.0f, 1/16.0f, 1/4.0f};

Texture2D<float4> SceneTexture : register(t0);

#include "AquaShadersHeader.hlsl"

struct PS_INPUT
{
	float4 screenPos : SV_POSITION;
	float2 texC      : TEXCOORD0;
};

float LuminancePS(PS_INPUT pIn) : SV_TARGET0
{
	float logLumSum = 0.0f;

	for(int y = -1; y <= 1; y++)
	{
		for(int x = -1; x <= 1; x++)
		{
			logLumSum += log(dot(SceneTexture.Sample(gTriPointSampler, pIn.texC + float2(x * LUMINANCE_TEXEL_SIZE[0], y * LUMINANCE_TEXEL_SIZE[0])), LUMINANCE) + DELTA);
		}
	}

	logLumSum /= 9;

	return logLumSum;
};