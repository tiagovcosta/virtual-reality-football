// Function - GBufferVS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 15
//

#include "AquaShadersHeader.hlsl"

struct VS_IN
{
	float3 posL     : POSITION;
	float3 tangentL : TANGENT;
	float3 normalL  : NORMAL;
	float2 texC     : TEXCOORD;
};

struct VS_OUT
{
	float4 posH      : SV_POSITION;
	float3 tangentVS : TEXCOORD0;
        float3 normalVS  : TEXCOORD1;
        float2 texC      : TEXCOORD2;
	float3 posVS     : TEXCOORD3;
};

VS_OUT GBufferVS(VS_IN vIn)
{
	VS_OUT vOut;

	vOut.tangentVS = mul(float4(vIn.tangentL, 0.0f), gWorldView).xyz;
	vOut.normalVS = mul(float4(vIn.normalL, 0.0f), gWorldView).xyz;

	vOut.posH = mul(float4(vIn.posL,1.0f), gWorldViewProj);
	
	vOut.texC  = vIn.texC;

	float4 posVS = mul(float4(vIn.posL,1.0f), gWorldView);
	vOut.posVS = posVS.xyz;
	
	return vOut;
}