// Function - LightingPassGS
// Profile - gs_4_0
// Type - Geometry
// Defines - DIRECTIONAL_LIGHT
//

#include "AquaShadersHeader.hlsl"
#include "Lighting.hlsl"

cbuffer cbPerLight : register(b2)
{
	float3 color;
	#ifndef DIRECTIONAL_LIGHT
	float4 positionRange;
	#else
	float3 direction;
	#endif
}

struct GSInput
{
	uint id : ID;
};

struct GSOutput
{
	float4 posH     	: SV_Position;
	float3 viewRay		: VIEWRAY;

	nointerpolation float3 lightColor 	   : COLOR;
	
	#ifndef DIRECTIONAL_LIGHT
	nointerpolation float4 lightPosVSRange : POSITIONRANGE;
	#else
	nointerpolation float3 lightDirVS      : DIRECTION;
	#endif
};

//-------------------------------------------------------------------------------------------------
// Determines the extents of a minimum-area quad fit to a spherical light volume
//-------------------------------------------------------------------------------------------------
void CalcLightQuad( in float3 lightPosVS, in float lightRange, out float2 topLeft, out float2 bottomRight )
{
	// Fit a bounding sphere to the light, where the center is the light position and the radius
	// is the light's range.
	float4 lightCenterVS = float4( lightPosVS, 1.0f );
	float radius = lightRange;

	// Figure out the four points at the top, bottom, left, and right of the sphere
	float4 topVS = lightCenterVS + float4( 0.0f, radius, 0.0f, 0.0f );
	float4 bottomVS = lightCenterVS - float4( 0.0f, radius, 0.0f, 0.0f );
	float4 leftVS = lightCenterVS - float4( radius, 0.0f, 0.0f, 0.0f );
	float4 rightVS = lightCenterVS + float4( radius, 0.0f, 0.0f, 0.0f );

	// Figure out whether we want to use the top and right from quad
	// tangent to the front of the sphere, or the back of the sphere
	leftVS.z = leftVS.x < 0.0f ? leftVS.z - radius : leftVS.z + radius;
	rightVS.z = rightVS.x < 0.0f ? rightVS.z + radius : rightVS.z - radius;
	topVS.z = topVS.y < 0.0f ? topVS.z + radius : topVS.z - radius;
	bottomVS.z = bottomVS.y < 0.0f ? bottomVS.z - radius : bottomVS.z + radius;

	// Clamp the z coordinate to the clip planes
	leftVS.z = clamp( leftVS.z, gClipDistances.x, gClipDistances.y );
	rightVS.z = clamp( rightVS.z, gClipDistances.x, gClipDistances.y );
	topVS.z = clamp( topVS.z, gClipDistances.x, gClipDistances.y );
	bottomVS.z = clamp( bottomVS.z, gClipDistances.x, gClipDistances.y );

	// Figure out the rectangle in clip-space by applying the perspective transform.
	// We assume that the perspective transform is symmetrical with respect to X and Y.
	float rectLeftCS = leftVS.x * gProj[0][0] / leftVS.z;
	float rectRightCS = rightVS.x * gProj[0][0] / rightVS.z;
	float rectTopCS = topVS.y * gProj[1][1] / topVS.z;
	float rectBottomCS = bottomVS.y * gProj[1][1] / bottomVS.z;

	// clamp the rectangle to the screen extents
	rectTopCS = clamp( rectTopCS, -1.0f, 1.0f );
	rectBottomCS = clamp( rectBottomCS, -1.0f, 1.0f );
	rectLeftCS = clamp( rectLeftCS, -1.0f, 1.0f );
	rectRightCS = clamp( rectRightCS, -1.0f, 1.0f );

	topLeft = float2( rectLeftCS, rectTopCS );
	bottomRight = float2( rectRightCS, rectBottomCS );
}

[maxvertexcount(4)]
void LightingPassGS(point GSInput input[1], inout TriangleStream<GSOutput> QuadStream)
{
	GSOutput output;

	// Pass along light properties
	output.lightColor = color;

	#ifndef DIRECTIONAL_LIGHT
	output.lightPosVSRange.xyz = mul(float4(positionRange.xyz, 1.0f), gView).xyz;
	output.lightPosVSRange.w = positionRange.w;
	#else
	output.lightDirVS = normalize(mul(float4(direction, 0.0f), gView).xyz);
	#endif

	#ifndef DIRECTIONAL_LIGHT			
		// Figure out a depth for the quad, by using the point on the sphere 
		// furthest from the camera. We combine this with "greater-than" depth testing,
		// so that any pixels where the sphere is "floating in air" will be culled and only
		// pixels where the volume intersects with geometry will be lit. This is based on 
		// the fact that our scene is mostly an empty volume, with walls on the far side.
		// However we'll clamp to the far plane if the light intersects with it, so that
		// we don't inadvertantly clip pixels that still need to be lit.

		float lightDepth = output.lightPosVSRange.z + output.lightPosVSRange.w;
		if ( output.lightPosVSRange.z - output.lightPosVSRange.w < gClipDistances.y )
			lightDepth = min( lightDepth, gClipDistances.y );

		// Project the light depth
		lightDepth = mul( float4( 0, 0, lightDepth, 1.0f ), gProj ).z / lightDepth;

		// Figure out the quad extents
		float2 topLeft, bottomRight;
		CalcLightQuad( output.lightPosVSRange.xyz, output.lightPosVSRange.w, topLeft, bottomRight );

		float4 quadVerts[4] =
		{
			float4( topLeft.x, topLeft.y, lightDepth, 1.0f ),
			float4( bottomRight.x, topLeft.y, lightDepth, 1.0f ),
			float4( topLeft.x, bottomRight.y, lightDepth, 1.0f ),
			float4( bottomRight.x, bottomRight.y, lightDepth, 1.0f )
		};

		// Emit 4 new verts, and 2 new triangles
		for (int i = 0; i < 4; i++)
		{
			output.posH = quadVerts[i];

			// Calculate the view ray by unprojecting the vertex position into view space.
			// For a quad we can clamp in the vertex shader, since we only interpolate in the XY direction.
			float4 positionVS = mul( quadVerts[i], gInvProj );
			positionVS.xyz /= positionVS.w;
			output.viewRay = float3(positionVS.xy * (gClipDistances.y / positionVS.z), gClipDistances.y);

			QuadStream.Append(output);
		}

		QuadStream.RestartStrip();
	#else
		float4 quadVerts[3] =
		{
			float4( -1, 1, 1, 1 ),
			float4( 3, 1, 1, 1 ),
			float4( -1, -3, 1, 1 )
		};

		// Emit 3 new verts, and 1 new triangle
		for (int i = 0; i < 3; i++)
		{
			output.posH = quadVerts[i];

			// Calculate the view ray by unprojecting the vertex position into view space.
			// For a quad we can clamp in the vertex shader, since we only interpolate in the XY direction.
			float4 positionVS = mul( quadVerts[i], gInvProj );
			positionVS.xyz /= positionVS.w;
			output.viewRay = float3(positionVS.xy, gClipDistances.y);

			QuadStream.Append(output);
		}

		QuadStream.RestartStrip();
	#endif
}