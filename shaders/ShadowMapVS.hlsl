// Function - ShadowMapVS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 15
//

#include "AquaShadersHeader.hlsl"

/*cbuffer cbPerObject
{
	float4x4 gWorld;
}*/

struct VS_IN
{
	float3 posL     : POSITION;
	float3 tangentL : TANGENT;
	float3 normalL  : NORMAL;
	float2 texC     : TEXCOORD;
};

struct VS_OUT
{
	float4 posH     : SV_POSITION;
};

VS_OUT ShadowMapVS(VS_IN vIn)
{
	VS_OUT vOut;

	vOut.posH = mul(float4(vIn.posL,1.0f), gWorldViewProj);
	
	return vOut;
}