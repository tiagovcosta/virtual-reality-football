// Function - FullScreenVS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 0
//

#include "AquaShadersHeader.hlsl"

struct VS_OUT
{
	float4 posH    : SV_Position;
	float3 viewRay : TEXCOORD0;
};

VS_OUT FullScreenVS(uint vertexID : SV_VertexID)
{
	VS_OUT vOut;

	float2 texC =  float2((vertexID << 1) & 2, vertexID & 2);
    	vOut.posH = float4(texC * float2(2,-2) + float2(-1,1), 1.0f, 1.0f);

	//float4 viewRay = mul(vOut.posH, gInvViewProj);
	//vOut.viewRay.xyz = viewRay.xyz / viewRay.w;
	//vOut.viewRay -= gEyePosition;
	
	float4 positionVS = mul( vOut.posH, gInvProj );
	positionVS.xyz /= positionVS.w;
	vOut.viewRay = float3(positionVS.xy , gClipDistances.y);

	return vOut;
}