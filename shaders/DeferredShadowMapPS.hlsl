// Function - DeferredShadowMapPS
// Profile - ps_4_0
// Type - Pixel
//

static const int NUMBER_CASCADES = 4;
static const float SHADOW_EPSILON = 0.003;
static const float SMAP_SIZE = 2048.0f;
static const float SMAP_DX = (1.0f / SMAP_SIZE)/NUMBER_CASCADES;
static const float SMAP_DY = 1.0f / SMAP_SIZE;

#include "AquaShadersHeader.hlsl"

cbuffer cbPerLight : register(b2)
{
	float4x4 gCascadeMatrix[4];
	float4   gCascadesStart;
};

Texture2D gShadowMap : register(t2);
Texture2D gDepthMap : register(t1);

struct PS_IN
{
	float4 posH          : SV_POSITION;
	float3 viewRay       : TEXCOORD0;
};

float4 DeferredShadowMapPS(PS_IN pIn) : SV_TARGET
{
	float4 posLightHS  = float4(0.0f,0.0f,0.0f,0.0f);
	float shadowFactor = 0.0f;
	
	int3 sampleIndices = int3(pIn.posH.xy, 0);
	float depth        = gDepthMap.Load(sampleIndices).r;

	float4 posWS       = mul(float4(pIn.viewRay * depth, 1.0f), gInvView);

	float cascadeIndex = 0;
	[unroll]
	for(int i = 0; i < NUMBER_CASCADES; i++)
	{
		[flatten]
		if(depth > gCascadesStart[i])
		{
			cascadeIndex = i;
		}
	}
	
	posLightHS = mul(posWS, gCascadeMatrix[cascadeIndex]);
	posLightHS.xyz /= posLightHS.w;
	posLightHS.xy = posLightHS.xy * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
	posLightHS.x /= NUMBER_CASCADES;
	float2 cascadeOffset = float2( ( cascadeIndex / (float)NUMBER_CASCADES ), 0.0f);
	posLightHS.xy  += cascadeOffset;

	float depthS = posLightHS.z;

	for(int x = -1; x <= 1; x++)
	{
		for(int y = -1; y <= 1; y++)
		{
			float d = gShadowMap.Sample(gTriPointSampler, posLightHS.xy + float2((float) x * SMAP_DX, (float) y * SMAP_DY)).r;
			shadowFactor += depthS <= d + SHADOW_EPSILON;
		}
	}

	return shadowFactor/9;
	
	/*
	// Determine the lerp amounts           
	float2 vLerps = frac(posLightHS.xy);

	// Read in the 4 samples, doing a depth check for each
	float fSamples[4];	
	fSamples[0] = (gShadowMap.Sample(gTriLinearSampler, posLightHS.xy).x + SHADOW_EPSILON < depthS) ? 0.0f: 1.0f;  
	fSamples[1] = (gShadowMap.Sample(gTriLinearSampler, posLightHS.xy + float2(SMAP_DX, 0)).x + SHADOW_EPSILON < depthS) ? 0.0f: 1.0f;  
	fSamples[2] = (gShadowMap.Sample(gTriLinearSampler, posLightHS.xy + float2(0, SMAP_DY)).x + SHADOW_EPSILON < depthS) ? 0.0f: 1.0f;  
	fSamples[3] = (gShadowMap.Sample(gTriLinearSampler, posLightHS.xy + float2(SMAP_DX, SMAP_DY)).x + SHADOW_EPSILON < depthS) ? 0.0f: 1.0f;  
    
	// lerp between the shadow values to calculate our light amount
	return lerp(lerp(fSamples[0], fSamples[1], vLerps.x), lerp( fSamples[2], fSamples[3], vLerps.x), vLerps.y);
	*/
	
	/*float d = gShadowMap.Sample(gTriLinearSampler, posLightHS.xy).r;
	shadowFactor += depthS <= d + SHADOW_EPSILON;

	return shadowFactor;*/
}