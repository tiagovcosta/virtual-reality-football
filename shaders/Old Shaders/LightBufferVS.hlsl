// Function - VS
// Profile - vs_4_0
// Type - Vertex
// InputLayout - 9
// Defines - DIRECTIONAL_LIGHT
//

#include "AquaShadersHeader.hlsl"

#ifndef DIRECTIONAL_LIGHT
cbuffer cbPerLight : register(b2)
{
	float4x4 gWorld;
}
#endif

struct VS_IN
{
	float3 posL : POSITION;
	float3 texC : TEXCOORD;
};

struct VS_OUT
{
	float4 posH          : SV_POSITION;
	float3 posVS         : TEXCOORD0;
};

VS_OUT VS(VS_IN vIn)
{
	VS_OUT vOut;

	#ifdef DIRECTIONAL_LIGHT
	float4 pos = mul(float4(vIn.posL, 1.0f), gInvProj);
	vOut.posVS = pos.xyz / pos.w;

	vOut.posH = float4(vIn.posL, 1.0f);
	#else
	vOut.posVS = mul(float4(vIn.posL, 1.0f), gWorld).xyz;
	vOut.posH = mul(float4(vOut.posVS, 1.0f), gProj);
	#endif

	return vOut;
}