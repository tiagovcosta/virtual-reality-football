struct Light
{
	float3 pos;
	float3 dir;
	float4 diffuse;
	float4 spec;
	float3 att;
	float3 spotPowRangeType;

};

struct Surface
{
   	float4 pos;
    	float3 normal;
    	float specPower;
};

float4 ParallelLight(Surface v, Light L, float3 eyePos)
{
	float3 lightDir   = -normalize(L.dir);	

	float NdL         = max( 0.0f, dot(v.normal, lightDir));

	float3 R          = normalize(reflect(-lightDir, v.normal));

	float3 V          = normalize(eyePos - v.pos.xyz);

	float RdV         = saturate(dot(R, V));
	
	float specular    = pow(RdV, v.specPower); 
	
	return float4(L.diffuse.x * NdL, L.diffuse.y * NdL, L.diffuse.z * NdL, specular * NdL);
}

float Attenuation(float distance, float range, float3 att)
{
    float atten = 1.0f / ( att.x * distance * distance + att.y * distance + att.z );

    return step(distance, range) * saturate( atten );
}

float4 PointLight(Surface v, Light L, float3 eyePos)
{
	float Atten        = Attenuation(distance(v.pos.xyz, L.pos), L.spotPowRangeType.y, L.att);
	float4 litColor    =  float4(0.0f, 0.0f, 0.0f, 0.0f);

	if(Atten > 0.0f)
	{
		float3 lightDir    = normalize(L.pos - v.pos.xyz);

		float NdL          = max( 0.0f, dot(v.normal, lightDir));
	
		float3 R          = normalize(reflect(-lightDir, v.normal));

		float3 V          = normalize(eyePos - v.pos.xyz);

		float RdV         = saturate(dot(R, V));
	
		float specular    = pow(RdV, v.specPower);

		litColor = float4(L.diffuse.x * NdL* Atten, L.diffuse.y * NdL* Atten, L.diffuse.z * NdL* Atten, specular * NdL* Atten);
	}

	return litColor;
}

float4 SpotLight(Surface v, Light L, float3 eyePos)
{
	float4 litColor = PointLight(v, L, eyePos);

	float3 lightDir    = normalize(L.pos - v.pos.xyz);

	float spotAtten = pow(max(dot(-lightDir, L.dir), 0.0f), L.spotPowRangeType.x);

	return litColor*spotAtten;
}

 
 