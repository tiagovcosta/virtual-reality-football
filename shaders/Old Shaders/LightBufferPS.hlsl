// Function - PS
// Profile - ps_4_0
// Type - Pixel
// Defines - DIRECTIONAL_LIGHT
//

#include "AquaShadersHeader.hlsl"
#include "Lighting.fx"

static const int NUMBER_CASCADES = 4;
static const float SHADOW_EPSILON = 0.0003f;
static const float SMAP_SIZE = 2048.0f;
static const float SMAP_DX = (1.0f / SMAP_SIZE)/NUMBER_CASCADES;
static const float SMAP_DY = 1.0f / SMAP_SIZE;

cbuffer cbPerLight : register(b2)
{
	//float4x4 gLightView;
	//float    gCascadesEnd[4];
	//float4x4 gLightProj[4];
	Light    gLight;
}

Texture2D gNormalMap : register(t0); //specular power in alpha channel
Texture2D gDepthMap : register(t1);
//Texture2D gShadowMap : register(t3);

struct PS_IN
{
	float4 posH          : SV_POSITION;
	float3 posVS         : TEXCOORD0;
};

float4 PS(PS_IN pIn) : SV_TARGET
{
	float4 light = float4(0.0f,0.0f,0.0f,0.0f);

	int3 sampleIndices = int3(pIn.posH.xy, 0);
	float depth        = gDepthMap.Load(sampleIndices).r;
	float4 normalData  = gNormalMap.Load(sampleIndices);
	float3 normal      = 2.0f * normalData.xyz - 1.0f;
	float specPower    = normalData.a * 255.0f;

	//float4 positionLightHS = float4(0.0f,0.0f,0.0f,0.0f);
	//float csmDepth = 0.0f;
	//float shadowFactor = 1.0f;

	#ifdef DIRECTIONAL_LIGHT
	float3 positionVS = pIn.posVS * depth;
	#else
	float3 viewRay = float3(pIn.posVS.xy * (gClipDistances.y / pIn.posVS.z), gClipDistances.y);
	float3 positionVS = viewRay * depth;
	#endif

	Light l = gLight;
	l.pos = mul(float4(gLight.pos, 1.0f), gView).xyz;
	l.dir = mul(float4(gLight.dir, 0.0f), gView).xyz;

	/*positionLightHS = mul(positionWS, gLightView);

	for(int i = 0; i < NUMBER_CASCADES; i++)
	{
		if(depth * 10000 <= gCascadesEnd[i])
		{
			positionLightHS = mul(positionLightHS, gLightProj[i]);
			positionLightHS.xyz /= positionLightHS.w;
			positionLightHS.xy = positionLightHS.xy * float2(0.5f, -0.5f) + float2(0.5f, 0.5f);
			positionLightHS.x /= NUMBER_CASCADES;
			float2 cascadeOffset = float2( ( i / (float)NUMBER_CASCADES ), 0.0f);
			positionLightHS.xy  += cascadeOffset;
			
			break;
		}
	}

	float depthS = positionLightHS.z;

	for(int x = -1; x <= 1; x++)
	{
		for(int y = -1; y <= 1; y++)
		{
			float d = gShadowMap.Sample(gTriPointSam, positionLightHS.xy + float2((float) x * SMAP_DX, (float) y * SMAP_DY)).r;
			shadowFactor += depthS <= d + SHADOW_EPSILON;
		}
	}

	shadowFactor /= 9;*/

	Surface v = {positionVS, normal, specPower};

	if(gLight.type == 0)
	{
        	light = ParallelLight(v, l, float3(0.0f, 0.0f, 0.0f)) /** shadowFactor*/;
	}
	else if(gLight.type == 1)
	{
		light = PointLight(v, l, float3(0.0f, 0.0f, 0.0f));
	}
	else
	{
		light = SpotLight(v, gLight, gEyePosition);
	}

	return light;
}